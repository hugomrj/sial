

function Programa(){
    
   this.tipo = "programa";   
   this.recurso = "programas";   
   this.value = 0;
   this.form_descrip = "programa_descripcion";
   this.json_descrip = "nombre";
   
   this.dom="";
   this.carpeta=  "/aplicacion";   
   
   
   this.titulosin = "Programa";
   this.tituloplu = "Programas";   
      
   
   this.campoid=  'programa';
   this.tablacampos =  ['codigo', 'descripcion' ];
   
   this.etiquetas =  ['Codigo', 'Descripcion' ];
    
   this.tablaformat = ['N', 'C' ];                                  
   
   this.tbody_id = "programa-tb";
      
   this.botones_lista = [ this.lista_new] ;
   this.botones_form = "programa-acciones";   
         
   this.parent = null;
   
   
}





Programa.prototype.new = function( obj  ) {                

    reflex.form_new( obj ); 
    reflex.acciones.button_add_promise(obj);        
    
};





Programa.prototype.form_ini = function() {    
  
    var programa_codigo = document.getElementById('programa_codigo');            
     programa_codigo.onblur  = function() {                  
         programa_codigo.value = fmtNum(programa_codigo.value);      
    };      
    programa_codigo.onblur();          
    
    
};






Programa.prototype.form_validar = function() {    
    
   
    var programa_descripcion = document.getElementById('programa_descripcion');    
    if (programa_descripcion.value == "")         
    {
        msg.error.mostrar("Campo de descripcion esta vacio");           
        programa_descripcion.focus();
        programa_descripcion.select();        
        return false;
    }              

    
    return true;
};










Programa.prototype.main_list = function(obj, page) {    


    if (page === undefined) {    
        page = 1;
    }

    let promesa = arasa.vista.lista_paginacion(obj, page);
    
    promesa        
        .then(( xhr ) => {              
            arasa.html.url.redirect(xhr.status);                                                          
            // botones de accion - nuevo para este caso
            
            // botones de accion - nuevo para este caso       
            boton.objeto = ""+obj.tipo;
            document.getElementById( obj.tipo +'_acciones_lista' ).innerHTML 
                =  boton.basicform.get_botton_new();                
            
            var btn_objeto_nuevo = document.getElementById('btn_' + obj.tipo + '_nuevo');
            btn_objeto_nuevo.addEventListener('click',
                function(event) {  
                    
                    obj.new( obj );

                },
                false
            );              
            
           

        })
        .catch(( xhr ) => { 
            console.log(xhr.message);
        }); 

};







Programa.prototype.combobox = function( dom ) {     
            
    loader.inicio();
                        
    var url = html.url.absolute() + '/api/'+this.recurso+'/all' ;    


    //var data = {username: 'example'};
    var headers = new Headers();    
    headers.append('token', localStorage.getItem('token'));



    fetch( url ,
        {
            method: 'GET', 
            headers: headers
        })
        .then(response => {
            return response.text();
        })
        .then(data => {

            var domobj = document.getElementById(dom);
            var idedovalue = domobj.value;            


            var oJson = JSON.parse( data ) ;

            for( x=0; x < oJson.length; x++ ) {

                var jsonvalue = (oJson[x]['programa'] );            

                if (idedovalue != jsonvalue )
                {  
                    var opt = document.createElement('option');            
                    opt.value = jsonvalue;
                    opt.innerHTML = oJson[x]['descripcion'];                        
                    domobj.appendChild(opt);                     
                }

            }            
            
                //resolve( data );
            loader.fin();
        })
        /*
        .catch(function(error) {
            console.log(error);            
        });
        */

}





