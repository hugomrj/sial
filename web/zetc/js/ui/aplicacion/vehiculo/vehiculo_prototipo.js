

function Vehiculo(){
    
   this.tipo = "vehiculo";   
   this.recurso = "vehiculos";   
   this.value = 0;
   this.form_descrip = "vehiculo_descripcion";
   this.json_descrip = "nombre";
   
   this.dom="";
   this.carpeta=  "/aplicacion";   
   
   
   this.titulosin = "Vehiculo"
   this.tituloplu = "Vehiculos"   
      
   
   this.campoid=  'vehiculo';
   this.tablacampos =  ['codigo', 'marca' , 'modelo', 'chapa' ];
   
   this.etiquetas =  ['Codigo', 'Marca' , 'Modelo', 'Chapa' ];
    
   this.tablaformat = ['N', 'C' , 'C', 'C' ];                                  
   
   this.tbody_id = "vehiculo-tb";
      
   this.botones_lista = [ this.lista_new] ;
   this.botones_form = "vehiculo-acciones";   
         
   this.parent = null;
   
   
}





Vehiculo.prototype.new = function( obj  ) {                

    reflex.form_new( obj ); 
    reflex.acciones.button_add_promise(obj);        
    
};





Vehiculo.prototype.form_ini = function() {    
  
    var vehiculo_codigo = document.getElementById('vehiculo_codigo');            
     vehiculo_codigo.onblur  = function() {                  
         vehiculo_codigo.value = fmtNum(vehiculo_codigo.value);      
    };      
    vehiculo_codigo.onblur();          
    
    
};






Vehiculo.prototype.form_validar = function() {    
    
   
    var marca = document.getElementById('vehiculo_marca');    
    if (marca.value == "")         
    {
        msg.error.mostrar("Campo de marca esta vacio");           
        marca.focus();
        marca.select();        
        return false;
    }              

    
   
    var modelo = document.getElementById('vehiculo_modelo');    
    if (modelo.value == "")         
    {
        msg.error.mostrar("Campo de modelo esta vacio");           
        modelo.focus();
        modelo.select();        
        return false;
    }              
        
    
    
    return true;
};










Vehiculo.prototype.main_list = function(obj, page) {    


    if (page === undefined) {    
        page = 1;
    }

    let promesa = arasa.vista.lista_paginacion(obj, page);
    
    promesa        
        .then(( xhr ) => {              
            arasa.html.url.redirect(xhr.status);                                                          
            // botones de accion - nuevo para este caso
            
            // botones de accion - nuevo para este caso       
            boton.objeto = ""+obj.tipo;
            document.getElementById( obj.tipo +'_acciones_lista' ).innerHTML 
                =  boton.basicform.get_botton_new();                
            
            var btn_objeto_nuevo = document.getElementById('btn_' + obj.tipo + '_nuevo');
            btn_objeto_nuevo.addEventListener('click',
                function(event) {  
                    
                    obj.new( obj );

                },
                false
            );              
            
           

        })
        .catch(( xhr ) => { 
            console.log(xhr.message);
        }); 

};







Vehiculo.prototype.combobox = function( dom ) {     
            
    loader.inicio();
                        
    var url = html.url.absolute() + '/api/'+this.recurso+'/all' ;    

    var headers = new Headers();    
    headers.append('token', localStorage.getItem('token'));



    fetch( url ,
        {
            method: 'GET', 
            headers: headers
        })
        .then(response => {

            return response.text();
        })
        .then(data => {


            var domobj = document.getElementById(dom);
            var idedovalue = domobj.value;            
      
            var oJson = JSON.parse( data ) ;


            for( x=0; x < oJson.length; x++ ) {

                var jsonvalue = (oJson[x]['vehiculo'] );            

                if (idedovalue != jsonvalue )
                {  
                    
                    var opt = document.createElement('option');     
                    
                    opt.value = jsonvalue;
      
                
                    opt.innerHTML = oJson[x]['marca'] 
                            +" " +oJson[x]['modelo']                         
                            +" " +oJson[x]['chapa']  ;                        
                    
                
                    domobj.appendChild(opt);                     
                }

            }            
            
                //resolve( data );
            loader.fin();
        })
        /*
        .catch(function(error) {
            console.log(error);            
        });
        */

}





