
function GestionSocial(){
    
   this.tipo = "gestion_social";   
   this.recurso = "gestionessociales";   
   this.value = 0;
   this.form_descrip = "";
   this.json_descrip = "";
   
   this.dom="";
   this.carpeta=  "/aplicacion";   
   
   
   this.titulosin = "Actividades del Plan Gestion Social";
   this.tituloplu = "Actividades del Plan Gestion Social";   
      
   
   this.campoid=  'id';
   this.tablacampos =  ['id', 'fecha', 'programa.descripcion',
       'actividad_nombre', 'participantes_cantidad'];
   
   this.etiquetas =  ['Código', 'Fecha', 'Programa' ,
       'Actividad nombre', 'participantes cantidad'];
    
   this.tablaformat = ['N', 'D',  'C',
       'C', 'N' ];                                  
   
   this.tbody_id = "gestion_social-tb";
      
   this.botones_lista = [ this.lista_new] ;
   this.botones_form = "gestion_social-acciones";   
         
   this.parent = null;   
      
    this.combobox = 
        {
            "programa":{
                "value":"programa",
                "inner":"descripcion"
            }   
        };      

   this.filtro = "";
  
}





GestionSocial.prototype.new = function( obj  ) {                

    reflex.form_new( obj ); 
    reflex.acciones.button_add_promise(obj);        
  
    // no mostrar form de archivo
    var form_archivo = document.getElementById('form_archivo');      
    form_archivo.style.display = "none";        
      
  
    var fecha = document.getElementById('gestion_social_fecha');
    fecha.focus(); 
    fecha.select();   
  
  
};





GestionSocial.prototype.form_ini = function() {    



    var gestion_social_participantes_cantidad = document.getElementById('gestion_social_participantes_cantidad');          
    gestion_social_participantes_cantidad.onblur  = function() {     
        gestion_social_participantes_cantidad.value  = fmtNum(gestion_social_participantes_cantidad.value);
    };     
    gestion_social_participantes_cantidad.onblur();       
    
    var gestion_social_sexo_masculino = document.getElementById('gestion_social_sexo_masculino');          
    gestion_social_sexo_masculino.onblur  = function() {     
        gestion_social_sexo_masculino.value  = fmtNum(gestion_social_sexo_masculino.value);
    };     
    gestion_social_sexo_masculino.onblur();       
    
    var gestion_social_sexo_femenino = document.getElementById('gestion_social_sexo_femenino');          
    gestion_social_sexo_femenino.onblur  = function() {     
        gestion_social_sexo_femenino.value  = fmtNum(gestion_social_sexo_femenino.value);
    };     
    gestion_social_sexo_femenino.onblur();       
    
    var gestion_social_sexo_otros = document.getElementById('gestion_social_sexo_otros');          
    gestion_social_sexo_otros.onblur  = function() {     
        gestion_social_sexo_otros.value  = fmtNum(gestion_social_sexo_otros.value);
    };     
    gestion_social_sexo_otros.onblur();       
    
    var gestion_social_zona1 = document.getElementById('gestion_social_zona1');          
    gestion_social_zona1.onblur  = function() {     
        gestion_social_zona1.value  = fmtNum(gestion_social_zona1.value);
    };     
    gestion_social_zona1.onblur();       
    
    var gestion_social_zona2 = document.getElementById('gestion_social_zona2');          
    gestion_social_zona2.onblur  = function() {     
        gestion_social_zona2.value  = fmtNum(gestion_social_zona2.value);
    };     
    gestion_social_zona2.onblur();       
    
    var gestion_social_zona3 = document.getElementById('gestion_social_zona3');          
    gestion_social_zona3.onblur  = function() {     
        gestion_social_zona3.value  = fmtNum(gestion_social_zona3.value);
    };     
    gestion_social_zona3.onblur();       
    
    var gestion_social_zona4 = document.getElementById('gestion_social_zona4');          
    gestion_social_zona4.onblur  = function() {     
        gestion_social_zona4.value  = fmtNum(gestion_social_zona4.value);
    };     
    gestion_social_zona4.onblur();       
    
    var gestion_social_zona5 = document.getElementById('gestion_social_zona5');          
    gestion_social_zona5.onblur  = function() {     
        gestion_social_zona5.value  = fmtNum(gestion_social_zona5.value);
    };     
    gestion_social_zona5.onblur();       
    
    var gestion_social_zona6 = document.getElementById('gestion_social_zona6');          
    gestion_social_zona6.onblur  = function() {     
        gestion_social_zona6.value  = fmtNum(gestion_social_zona6.value);
    };     
    gestion_social_zona6.onblur();       
    
    var gestion_social_zona7 = document.getElementById('gestion_social_zona7');          
    gestion_social_zona7.onblur  = function() {     
        gestion_social_zona7.value  = fmtNum(gestion_social_zona7.value);
    };     
    gestion_social_zona7.onblur();       
    
    var gestion_social_zona8 = document.getElementById('gestion_social_zona8');          
    gestion_social_zona8.onblur  = function() {     
        gestion_social_zona8.value  = fmtNum(gestion_social_zona8.value);
    };     
    gestion_social_zona8.onblur();       
    
    var gestion_social_zona9 = document.getElementById('gestion_social_zona9');          
    gestion_social_zona9.onblur  = function() {     
        gestion_social_zona9.value  = fmtNum(gestion_social_zona9.value);
    };     
    gestion_social_zona9.onblur();      
    
    var gestion_social_zona10 = document.getElementById('gestion_social_zona10');          
    gestion_social_zona10.onblur  = function() {     
        gestion_social_zona10.value  = fmtNum(gestion_social_zona10.value);
    };     
    gestion_social_zona10.onblur();      

    
    var gestion_social_zona_banco_hovy = document.getElementById('gestion_social_zona_banco_hovy');          
    gestion_social_zona_banco_hovy.onblur  = function() {     
        gestion_social_zona_banco_hovy.value  = fmtNum(gestion_social_zona_banco_hovy.value);
    };     
    gestion_social_zona_banco_hovy.onblur();       
    
    var gestion_social_zona_refugio_sen = document.getElementById('gestion_social_zona_refugio_sen');          
    gestion_social_zona_refugio_sen.onblur  = function() {     
        gestion_social_zona_refugio_sen.value  = fmtNum(gestion_social_zona_refugio_sen.value);
    };     
    gestion_social_zona_refugio_sen.onblur();       
    
    var gestion_social_zona_otro = document.getElementById('gestion_social_zona_otro');          
    gestion_social_zona_otro.onblur  = function() {     
        gestion_social_zona_otro.value  = fmtNum(gestion_social_zona_otro.value);
    };     
    gestion_social_zona_otro.onblur();       
    
    var gestion_social_edad_5_12 = document.getElementById('gestion_social_edad_5_12');          
    gestion_social_edad_5_12.onblur  = function() {     
        gestion_social_edad_5_12.value  = fmtNum(gestion_social_edad_5_12.value);
    };     
    gestion_social_edad_5_12.onblur();       
    
    var gestion_social_edad_13_17 = document.getElementById('gestion_social_edad_13_17');          
    gestion_social_edad_13_17.onblur  = function() {     
        gestion_social_edad_13_17.value  = fmtNum(gestion_social_edad_13_17.value);
    };     
    gestion_social_edad_13_17.onblur();       
    
    var gestion_social_edad_18_29 = document.getElementById('gestion_social_edad_18_29');          
    gestion_social_edad_18_29.onblur  = function() {     
        gestion_social_edad_18_29.value  = fmtNum(gestion_social_edad_18_29.value);
    };     
    gestion_social_edad_18_29.onblur();       
    
    var gestion_social_edad_30_49 = document.getElementById('gestion_social_edad_30_49');          
    gestion_social_edad_30_49.onblur  = function() {     
        gestion_social_edad_30_49.value  = fmtNum(gestion_social_edad_30_49.value);
    };     
    gestion_social_edad_30_49.onblur();       
    
    var gestion_social_edad_50_64 = document.getElementById('gestion_social_edad_50_64');          
    gestion_social_edad_50_64.onblur  = function() {     
        gestion_social_edad_50_64.value  = fmtNum(gestion_social_edad_50_64.value);
    };     
    gestion_social_edad_50_64.onblur();       
    
    var gestion_social_edad_65_mas = document.getElementById('gestion_social_edad_65_mas');          
    gestion_social_edad_65_mas.onblur  = function() {     
        gestion_social_edad_65_mas.value  = fmtNum(gestion_social_edad_65_mas.value);
    };     
    gestion_social_edad_65_mas.onblur();       
    
                    
    
    
    // boton enviar
    var btn_enviar = document.getElementById('btn_enviar');
    btn_enviar.onclick = function(event) {     
       
        var codid  = document.getElementById('gestion_social_id').value;                
        var filetxt = document.getElementById("migracion_file").files[0];
        var nombre_archivo = filetxt.name;

        var form = document.getElementById('form_archivo');
        var formdata = new FormData(form);

        formdata.append("filetxt", filetxt);
        
        file_upload_promesa( formdata, nombre_archivo, codid )
            .then(( xhr ) => {
                
                if (xhr.status == 200){
                    
                    var obj = new GestionSocial();    
                    obj.dom = 'arti_form';                    
                    reflex.form_id_promise( obj, codid );                    
                    msg.ok.mostrar("archivo guardado");  
                }
            })
            .catch(( xhr ) => {                                         
                console.log(xhr.message);                    
            });             

    }
    
    
    
    
    
};






GestionSocial.prototype.form_validar = function() {    
    
        
    var gestion_social_fecha = document.getElementById('gestion_social_fecha');
    if (gestion_social_fecha.value == ""){
        msg.error.mostrar("Falta fecha ");                    
        gestion_social_fecha.focus();
        gestion_social_fecha.select();                                       
        return false;        
    }
            
            
    var gestion_social_actividad_nombre = document.getElementById('gestion_social_actividad_nombre');
    if (gestion_social_actividad_nombre.value == ""){
        msg.error.mostrar("Falta nombre de actividad ");                    
        gestion_social_actividad_nombre.focus();
        gestion_social_actividad_nombre.select();                                       
        return false;        
    }
            
    
    var participantes_cantidad = 0
    var gestion_social_participantes_cantidad = document.getElementById('gestion_social_participantes_cantidad');    
    if (fmtNum(gestion_social_participantes_cantidad.value) <= 0)         
    {
        msg.error.mostrar("Falta agregar la cantidad de participantes");           
        gestion_social_participantes_cantidad.focus();
        gestion_social_participantes_cantidad.select();        
        return false;
    }  
    else {        
        participantes_cantidad = Number(gestion_social_participantes_cantidad.value);
    }
    
    
    
   var sexo_masculino = document.getElementById('gestion_social_sexo_masculino').value;     
   var sexo_femenino = document.getElementById('gestion_social_sexo_femenino').value;     
   var sexo_otros = document.getElementById('gestion_social_sexo_otros').value;     
   var sexo_total = Number(sexo_masculino) + Number(sexo_femenino) + Number(sexo_otros);
   
   if (sexo_total != participantes_cantidad ) {
       msg.error.mostrar("La suma los campos de sexo no es igual a la cantidad de participantes");  
       return false;
   }
   
   
   
    var zona1 = document.getElementById('gestion_social_zona1').value;     
    var zona2 = document.getElementById('gestion_social_zona2').value;     
    var zona3 = document.getElementById('gestion_social_zona3').value;     
    var zona4 = document.getElementById('gestion_social_zona4').value;     
    var zona5 = document.getElementById('gestion_social_zona5').value;     
    var zona6 = document.getElementById('gestion_social_zona6').value;     
    var zona7 = document.getElementById('gestion_social_zona7').value;     
    var zona8 = document.getElementById('gestion_social_zona8').value;     
    var zona9 = document.getElementById('gestion_social_zona9').value;     
    var zona10 = document.getElementById('gestion_social_zona10').value;     
    
    var banco_hovy = document.getElementById('gestion_social_zona_banco_hovy').value;     
    var refugio_sen = document.getElementById('gestion_social_zona_refugio_sen').value;     
    var zona_otro = document.getElementById('gestion_social_zona_otro').value;     
    
    var zona_total = Number(zona1) + Number(zona2) + Number(zona3) + Number(zona4)
            + Number(zona5) + Number(zona6) + Number(zona7) +  Number(zona8) 
            + Number(zona9) + Number(zona10) 
            +  Number(banco_hovy) + Number(refugio_sen) 
            + Number(zona_otro) ;
    
    
    console.log(zona_total)
    
    if (zona_total != participantes_cantidad ) {
        msg.error.mostrar("La suma los campos de zona no es igual a la cantidad de participantes");  
        return false;
    }
       
       
    var edad_5_12 = document.getElementById('gestion_social_edad_5_12').value;     
    var edad_13_17 = document.getElementById('gestion_social_edad_13_17').value;     
    var edad_18_29 = document.getElementById('gestion_social_edad_18_29').value;     
    var edad_30_49 = document.getElementById('gestion_social_edad_30_49').value;     
    var edad_50_64 = document.getElementById('gestion_social_edad_50_64').value;         
    var edad_65_mas = document.getElementById('gestion_social_edad_65_mas').value;   
    
    var edad_total = Number(edad_5_12) + Number(edad_13_17) + Number(edad_18_29)
        + Number(edad_30_49) + Number(edad_50_64) +  Number(edad_65_mas) ;

    if (edad_total != participantes_cantidad ) {
        msg.error.mostrar("La suma los campos de edad no es igual a la cantidad de participantes");  
        return false;
    }

                      
    return true;
};










GestionSocial.prototype.main_list = function(obj, page) {    


    if (page === undefined) {    
        page = 1;
    }

    let promesa = arasa.vista.lista_paginacion(obj, page);
    
    promesa        
        .then(( xhr ) => {              
            arasa.html.url.redirect(xhr.status);                                                          
            // botones de accion - nuevo para este caso
            
            // botones de accion - nuevo para este caso       
            boton.objeto = ""+obj.tipo;
            document.getElementById( obj.tipo +'_acciones_lista' ).innerHTML 
                =  boton.basicform.get_botton_new();                
            
            var btn_objeto_nuevo = document.getElementById('btn_' + obj.tipo + '_nuevo');
            btn_objeto_nuevo.addEventListener('click',
                function(event) {  
                    
                    obj.new( obj );

                },
                false
            );              
            
           

        })
        .catch(( xhr ) => { 
            console.log(xhr.message);
        }); 

};







GestionSocial.prototype.post_form_id = function( obj  ) {                
    

    var ojson = JSON.parse( form.json ) ;   
    var json = JSON.stringify(ojson['documento']) ;       
    
    if (typeof json === 'undefined'){ 
        document.getElementById( 'fileD' ).style.display = "none";         
    } 
    else {   
        
        document.getElementById( 'fileU' ).style.display = "none";   

        var ojson = JSON.parse( json ) ;                                            
        document.getElementById( 'fileD_nombre' ).innerHTML  
                = ojson['file_name']



        var afileD = document.getElementById( 'afileD');
        afileD.onclick = function()
        {  
            var url = html.url.absolute() + "/gestionsocial/download";
            
            // Use XMLHttpRequest instead of Jquery $ajax
            xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                var a;
                if (xhttp.readyState === 4 && xhttp.status === 200) {
                    // Trick for making downloadable link
                    a = document.createElement('a');
                    a.href = window.URL.createObjectURL(xhttp.response);
                    // Give filename you wish to download

                    var file_name =  xhttp.getResponseHeader("file_name") ; 
                    
                    a.download = file_name;
                    a.style.display = 'none';
                    document.body.appendChild(a);
                    a.click();
                }
            };
            // Post data to URL which handles post request
            xhttp.open("GET", url);
            xhttp.setRequestHeader("Content-Type", "application/json");
            xhttp.setRequestHeader("codid", document.getElementById( 'gestion_social_id' ).value );

            // You should set responseType as blob for binary responses
            xhttp.responseType = 'blob';
            xhttp.send();
        }
            

        var btn_cambiar = document.getElementById( 'btn_cambiar');
        btn_cambiar.onclick = function()
        {  
            document.getElementById( 'fileD' ).style.display = "none";   
            document.getElementById( 'fileU' ).style.display = "initial";  
        };   
    } 
        
          

};



GestionSocial.prototype.carga_combos = function( obj  ) {
    
    var vehi = new Programa(); 
    vehi.combobox("gestion_social_programa");        
    
};





GestionSocial.prototype.getUrlFiltro = function( obj  ) {                
    
    var ret = "";    
    ret = obj.filtro;

    return ret;
    
};
