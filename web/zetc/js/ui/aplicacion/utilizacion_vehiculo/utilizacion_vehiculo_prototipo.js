
function UtilizacionVehiculo(){
    
   this.tipo = "utilizacion_vehiculo";   
   this.recurso = "utilizacionvehiculo";   
   this.value = 0;
   this.form_descrip = "";
   this.json_descrip = "";
   
   this.dom="";
   this.carpeta=  "/aplicacion";   
   
   
   this.titulosin = "Utilizacion de Vehiculos";
   this.tituloplu = "Utilizacion de Vehiculos";   
      
   
   this.campoid=  'id';
   this.tablacampos =  ['id', 'fecha', 'vehiculo.descripcion', 'nombre_destino' ,
                        'kms'];
   
   this.etiquetas =  ['Código', 'Fecha', 'Vehiculo', 'Destino', 'kms' ];
    
   this.tablaformat = ['N', 'D','C', 'C', 'N' ];                                  
   
   this.tbody_id = "utilizacion_vehiculo-tb";
      
   this.botones_lista = [ this.lista_new] ;
   this.botones_form = "utilizacion_vehiculo-acciones";   
         
   this.parent = null;
   
      
    this.combobox = 
        {
            "vehiculo":{
                "value":"vehiculo",
                "inner":"descripcion"
            }                

        };      
        
   this.filtro = "";
   

   
  
}





UtilizacionVehiculo.prototype.new = function( obj  ) {                

    reflex.form_new( obj ); 
    reflex.acciones.button_add_promise(obj);      
    
    
    // no mostrar form de archivo
    var form_archivo = document.getElementById('form_archivo');      
    form_archivo.style.display = "none";        
      
  
    var fecha = document.getElementById('utilizacion_vehiculo_fecha');
    fecha.focus(); 
    fecha.select();   
  
      
    
  
};





UtilizacionVehiculo.prototype.form_ini = function() {    

   
   // cuando se hace click en otros habilitar el campo de texto
   
   var utilizacion_vehiculo_otro_texto = document.getElementById('utilizacion_vehiculo_otro_texto');    
   utilizacion_vehiculo_otro_texto.value = "";
   
   
   var utilizacion_vehiculo_otro = document.getElementById('utilizacion_vehiculo_otro');    
       utilizacion_vehiculo_otro.onclick = function() {        
        if (utilizacion_vehiculo_otro.checked){                    
            
            utilizacion_vehiculo_otro_texto.disabled = false;
            utilizacion_vehiculo_otro_texto.focus();
            utilizacion_vehiculo_otro_texto.select();            
        }
        else{            
            utilizacion_vehiculo_otro_texto.disabled = true;
            //utilizacion_vehiculo_otros_texto.value = "";
        }    
    }
   
   
   
   
   
    
    var utilizacion_vehiculo_kms = document.getElementById('utilizacion_vehiculo_kms');          
    utilizacion_vehiculo_kms.onblur  = function() {     
        utilizacion_vehiculo_kms.value  = fmtNum(utilizacion_vehiculo_kms.value);
    };     
    utilizacion_vehiculo_kms.onblur();       
    
    
    
       
   
    // select vehiculo 
    var linea_otro_vehiculo = document.getElementById('linea_otro_vehiculo');                  
    linea_otro_vehiculo.style.display = "none"; 
        
    var utilizacion_vehiculo_vehiculo = document.getElementById('utilizacion_vehiculo_vehiculo');          
    utilizacion_vehiculo_vehiculo.addEventListener('change', function() {
        
        
        if (utilizacion_vehiculo_vehiculo.value == 13){         
            linea_otro_vehiculo.style.display = "block"; 
        }
        else{
            linea_otro_vehiculo.style.display = "none"; 
            document.getElementById('utilizacion_vehiculo_otro_vehiculo').value = "";
        }
        
    });
    //utilizacion_vehiculo_vehiculo.onchange();       
    
    
    
   
    // boton enviar
    var btn_enviar = document.getElementById('btn_enviar');
    btn_enviar.onclick = function(event) {     
       
        var codid  = document.getElementById('utilizacion_vehiculo_id').value;                
        var filetxt = document.getElementById("migracion_file").files[0];
        var nombre_archivo = filetxt.name;

        var form = document.getElementById('form_archivo');
        var formdata = new FormData(form);

        formdata.append("filetxt", filetxt);
        
        file_upload_promesa( formdata, nombre_archivo, codid )
            .then(( xhr ) => {
                
                if (xhr.status == 200){
                    
                    var obj = new UtilizacionVehiculo();    
                    obj.dom = 'arti_form';                    
                    reflex.form_id_promise( obj, codid );                    
                    msg.ok.mostrar("archivo guardado");  
                }
            })
            .catch(( xhr ) => {                                         
                console.log(xhr.message);                    
            });             

    }
    
       
    
    
    
    
    
};






UtilizacionVehiculo.prototype.form_validar = function() {    
    
        
    var utilizacion_vehiculo_fecha = document.getElementById('utilizacion_vehiculo_fecha');
    if (utilizacion_vehiculo_fecha.value == ""){
        msg.error.mostrar("Falta fecha ");                    
        utilizacion_vehiculo_fecha.focus();
        utilizacion_vehiculo_fecha.select();                                       
        return false;        
    }
    
    var nombre_destino = document.getElementById('utilizacion_vehiculo_nombre_destino');        
    nombre_destino.value = "";   
    
    // validar seleccion 
    var destino = false;
    var ele = document.getElementById('utilizacion_vehiculo_zafira');
    if (ele.checked){ 
        destino = true;        
        nombre_destino.value = "Edificio Zafira";     
    }
    else{
        var ele = document.getElementById('utilizacion_vehiculo_temporal');
        if (ele.checked){ 
            destino = true;        
            nombre_destino.value = "Barrio Temporal";
        }
        else{
            var ele = document.getElementById('utilizacion_vehiculo_tacumbu');
            if (ele.checked){ 
                destino = true;   
                nombre_destino.value = "Barrio Tacumbu";
            }        
            else{
                var ele = document.getElementById('utilizacion_vehiculo_camsap');
                if (ele.checked){ 
                    destino = true;        
                    nombre_destino.value = "APUF CAMSAP";
                }                                
                else{
                    var ele = document.getElementById('utilizacion_vehiculo_otro');
                    if (ele.checked){ 
                        destino = true;        
                        nombre_destino.value = document.getElementById('utilizacion_vehiculo_otro_texto').value;
                    }                       
                }
            }
        }
    }
            
          
 
            
    
    if (destino == false){
        msg.error.mostrar("falta marcar destino");                    
        return false;        
    }
    
    
    var ele = document.getElementById('utilizacion_vehiculo_otro');
    if (ele.checked){ 
        
        var utilizacion_vehiculo_otro_texto = document.getElementById('utilizacion_vehiculo_otro_texto');
        if (utilizacion_vehiculo_otro_texto.value == ""){
            msg.error.mostrar("Falta agregar otro destino ");                    
            utilizacion_vehiculo_otro_texto.focus();
            utilizacion_vehiculo_otro_texto.select();                                       
            return false;        
        }                
    }
    else{        
        document.getElementById('utilizacion_vehiculo_otro_texto').value = "";            
    }
    
        
        
   
    var utilizacion_vehiculo_kms = document.getElementById('utilizacion_vehiculo_kms');    
    if (fmtNum(utilizacion_vehiculo_kms.value) <= 0)         
    {
        msg.error.mostrar("Falta agregar la cantidad de kilometros");           
        utilizacion_vehiculo_kms.focus();
        utilizacion_vehiculo_kms.select();        
        return false;
    }  
   
           
    return true;
};










UtilizacionVehiculo.prototype.main_list = function(obj, page) {    


    if (page === undefined) {    
        page = 1;
    }

    let promesa = arasa.vista.lista_paginacion(obj, page);
    
    promesa        
        .then(( xhr ) => {              
            arasa.html.url.redirect(xhr.status);                                                          
            // botones de accion - nuevo para este caso
            
            // botones de accion - nuevo para este caso       
            boton.objeto = ""+obj.tipo;
            document.getElementById( obj.tipo +'_acciones_lista' ).innerHTML 
                =  boton.basicform.get_botton_new();                
            
            var btn_objeto_nuevo = document.getElementById('btn_' + obj.tipo + '_nuevo');
            btn_objeto_nuevo.addEventListener('click',
                function(event) {  
                    
                    obj.new( obj );

                },
                false
            );              
            
           

        })
        .catch(( xhr ) => { 
            console.log(xhr.message);
        }); 

};







UtilizacionVehiculo.prototype.post_form_id = function( obj  ) {                
    
    var ojson = JSON.parse(form.json) ; 
    
    var valor  = ojson["zafira"];
    var ele = document.getElementById('utilizacion_vehiculo_zafira');   
    if (valor == true){                    
        ele.checked = true;        
    }

    var valor  = ojson["temporal"];
    var ele = document.getElementById('utilizacion_vehiculo_temporal');   
    if (valor == true){                    
        ele.checked = true;        
    }

    var valor  = ojson["tacumbu"];
    var ele = document.getElementById('utilizacion_vehiculo_tacumbu');   
    if (valor == true){                    
        ele.checked = true;        
    }

    var valor  = ojson["camsap"];
    var ele = document.getElementById('utilizacion_vehiculo_camsap');   
    if (valor == true){                    
        ele.checked = true;        
    }

    var valor  = ojson["otro"];
    var ele = document.getElementById('utilizacion_vehiculo_otro');   
    if (valor == true){                    
        ele.checked = true;        
    }


    var valor  = ojson["otro_texto"];
    var ele = document.getElementById('utilizacion_vehiculo_otro_texto'); 
    ele.value = valor;


    // vehiculo    
    var utilizacion_vehiculo_vehiculo = document.getElementById('utilizacion_vehiculo_vehiculo'); 
    if (utilizacion_vehiculo_vehiculo.value == 13){
        var linea_otro_vehiculo = document.getElementById('linea_otro_vehiculo');                  
        linea_otro_vehiculo.style.display = "block";         
    }
    
        



    var ojson = JSON.parse( form.json ) ;   
    var json = JSON.stringify(ojson['documento']) ;       
    
    if (typeof json === 'undefined'){ 
        document.getElementById( 'fileD' ).style.display = "none";         
    } 
    else {   
        
        document.getElementById( 'fileU' ).style.display = "none";   

        var ojson = JSON.parse( json ) ;                                            
        document.getElementById( 'fileD_nombre' ).innerHTML  
                = ojson['file_name']



        var afileD = document.getElementById( 'afileD');
        afileD.onclick = function()
        {  
            var url = html.url.absolute() + "/utilizacionvehiculo/download";
            
            // Use XMLHttpRequest instead of Jquery $ajax
            xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                var a;
                if (xhttp.readyState === 4 && xhttp.status === 200) {
                    // Trick for making downloadable link
                    a = document.createElement('a');
                    a.href = window.URL.createObjectURL(xhttp.response);
                    // Give filename you wish to download

                    var file_name =  xhttp.getResponseHeader("file_name") ; 
                    
                    a.download = file_name;
                    a.style.display = 'none';
                    document.body.appendChild(a);
                    a.click();
                }
            };
            // Post data to URL which handles post request
            xhttp.open("GET", url);
            xhttp.setRequestHeader("Content-Type", "application/json");
            xhttp.setRequestHeader("codid", document.getElementById( 'utilizacion_vehiculo_id' ).value );

            // You should set responseType as blob for binary responses
            xhttp.responseType = 'blob';
            xhttp.send();
        }
            

        var btn_cambiar = document.getElementById( 'btn_cambiar');
        btn_cambiar.onclick = function()
        {  
            document.getElementById( 'fileD' ).style.display = "none";   
            document.getElementById( 'fileU' ).style.display = "initial";  
        };   
    } 
        


};



UtilizacionVehiculo.prototype.carga_combos = function( obj  ) {
    
    var vehi = new Vehiculo(); 
    vehi.combobox("utilizacion_vehiculo_vehiculo");        
    
    
};





UtilizacionVehiculo.prototype.form_getjson = function( obj  ) {                
    
    var str = "";
    
    var ele = document.getElementById('utilizacion_vehiculo_id');
    str =   str + form.datos.elemetiq(ele) ;  
    
    
    str =  str  + ","  ;
    ele = document.getElementById('utilizacion_vehiculo_fecha');
    str =   str + form.datos.elemetiq(ele) ;  
    
    
    str =  str  + ","  ;
    ele = document.getElementById('utilizacion_vehiculo_vehiculo');
    str =  str + form.datos.elemetcombo(ele) ;    
    
    
    str =  str  + ","  ;
    ele = document.getElementById('utilizacion_vehiculo_zafira');    
    str =  str + form.radios.getelejson(ele, "zafira");

    str =  str  + ","  ;
    ele = document.getElementById('utilizacion_vehiculo_temporal');    
    str =  str + form.radios.getelejson(ele, "temporal");

    str =  str  + ","  ;
    ele = document.getElementById('utilizacion_vehiculo_tacumbu');    
    str =  str + form.radios.getelejson(ele, "tacumbu");

    str =  str  + ","  ;
    ele = document.getElementById('utilizacion_vehiculo_camsap');    
    str =  str + form.radios.getelejson(ele, "camsap");

    str =  str  + ","  ;
    ele = document.getElementById('utilizacion_vehiculo_otro');    
    str =  str + form.radios.getelejson(ele, "otro");


    str =  str  + ","  ;
    ele = document.getElementById('utilizacion_vehiculo_otro_texto');
    str =   str + form.datos.elemetiq(ele) ;  
    
    str =  str  + ","  ;
    ele = document.getElementById('utilizacion_vehiculo_kms');
    str =   str + form.datos.elemetiq(ele) ;  
    
    str =  str  + ","  ;
    ele = document.getElementById('utilizacion_vehiculo_nombre_destino');
    str =   str + form.datos.elemetiq(ele) ;  
    
    str =  str  + ","  ;
    ele = document.getElementById('utilizacion_vehiculo_otro_vehiculo');
    str =   str + form.datos.elemetiq(ele) ;  
    

    return "{" +str+ "}"  ;    

};




UtilizacionVehiculo.prototype.getUrlFiltro = function( obj  ) {                
    
    var ret = "";    
    ret = obj.filtro;

    return ret;
    
};

