

SELECT fecha, 
  case WHEN sillas = true THEN 'X' ELSE '' END AS sillas, 
  case WHEN mesas = true THEN 'X' ELSE '' END AS mesas, 
  case WHEN refrigerios = true THEN 'X' ELSE '' END AS refrigerios, 
  case WHEN microfono_parlante = true THEN 'X' ELSE '' END AS microfono_parlante, 
  case WHEN vehiculos = true THEN 'X' ELSE '' END AS vehiculos, 
  case WHEN proyector = true THEN 'X' ELSE '' END AS proyector, 
  case WHEN computadora = true THEN 'X' ELSE '' END AS computadora, 
  case WHEN materiales_impresos = true THEN 'X' ELSE '' END AS materiales_impresos, 
  case WHEN boligrafos = true THEN 'X' ELSE '' END AS boligrafos, 
  case WHEN hojas_blancas = true THEN 'X' ELSE '' END AS hojas_blancas, 
  case WHEN otros = true THEN 'X' ELSE '' END AS otros, 
  observaciones 
  FROM aplicacion.ordenes_servicios  
where fecha between 'v0' and 'v1'   
order by fecha   

  