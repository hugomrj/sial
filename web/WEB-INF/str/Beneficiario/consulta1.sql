
SELECT fecha, 
  case WHEN sexo_masc = true THEN 'X' ELSE '' END AS sexo_masc, 
  case WHEN sexo_feme = true THEN 'X' ELSE '' END AS sexo_feme, 
  case WHEN sexo_otro = true THEN 'X' ELSE '' END AS sexo_otro, 
  case WHEN sexo_nocon = true THEN 'X' ELSE '' END AS sexo_nocon, 
  case WHEN edad_5_12 = true THEN 'X' ELSE '' END AS edad_5_12, 
  case WHEN edad_13_17 = true THEN 'X' ELSE '' END AS edad_13_17, 
  case WHEN edad_18_29 = true THEN 'X' ELSE '' END AS edad_18_29, 
  case WHEN edad_30_49 = true THEN 'X' ELSE '' END AS edad_30_49, 
  case WHEN edad_50_64 = true THEN 'X' ELSE '' END AS edad_50_64, 
  case WHEN edad_65_mas = true THEN 'X' ELSE '' END AS edad_65_mas, 
  case WHEN edad_nocon = true THEN 'X' ELSE '' END AS edad_nocon, 
  case WHEN zona_1 = true THEN 'X' ELSE '' END AS zona_1, 
  case WHEN zona_2 = true THEN 'X' ELSE '' END AS zona_2, 
  case WHEN zona_3 = true THEN 'X' ELSE '' END AS zona_3, 
  case WHEN zona_4 = true THEN 'X' ELSE '' END AS zona_4, 
  case WHEN zona_5 = true THEN 'X' ELSE '' END AS zona_5, 
  case WHEN zona_6 = true THEN 'X' ELSE '' END AS zona_6, 
  case WHEN zona_7 = true THEN 'X' ELSE '' END AS zona_7, 
  case WHEN zona_8 = true THEN 'X' ELSE '' END AS zona_8, 
  case WHEN zona_9 = true THEN 'X' ELSE '' END AS zona_9, 
  case WHEN zona_10 = true THEN 'X' ELSE '' END AS zona_10,  
  case WHEN zona_hovy = true THEN 'X' ELSE '' END AS zona_hovy, 
  case WHEN zona_sen = true THEN 'X' ELSE '' END AS zona_sen, 
  case WHEN zona_otro = true THEN 'X' ELSE '' END AS zona_otro,     
  case WHEN acti_quejas = true THEN 'X' ELSE '' END AS acti_quejas, 
  case WHEN acti_acompa = true THEN 'X' ELSE '' END AS acti_acompa, 
  case WHEN acti_info = true THEN 'X' ELSE '' END AS acti_info, 
  case WHEN acti_interme = true THEN 'X' ELSE '' END AS acti_interme, 
  case WHEN acti_otro = true THEN 'X' ELSE '' END AS acti_otro   
FROM aplicacion.beneficiarios 
where fecha between 'v0' and 'v1'  
order by fecha  

  