
select 1 as ord, 
id, CAST ( fecha AS character varying ) fecha, 
(vehiculos.marca || ' ' || vehiculos.modelo  || ' ' || vehiculos.chapa ) AS vehiculo_nombre,  
case WHEN zafira = true THEN 'X' ELSE '' END AS zafira,   
case WHEN temporal = true THEN 'X' ELSE '' END AS temporal,  
case WHEN tacumbu = true THEN 'X' ELSE '' END AS tacumbu,  
case WHEN camsap = true THEN 'X' ELSE '' END AS camsap,  
case WHEN otro = true THEN 'X' ELSE '' END AS otro,  
 kms  
FROM aplicacion.utilizacion_vehiculos  
inner join aplicacion.vehiculos   
on (utilizacion_vehiculos.vehiculo = vehiculos.vehiculo)  
where fecha between 'v0' and 'v1'    

union 

select 2 as ord, 0 as id, 'TOTALES' as fecha, 
	CAST (count(*) AS character varying ) as cantidad, 
	CAST (sum(zafira) AS character varying ) as zafira, 
	CAST (sum(temporal) AS character varying ) as temporal, 
	CAST (sum(tacumbu) AS character varying ) as tacumbu, 
	CAST (sum(camsap) AS character varying ) as camsap, 
	CAST (sum(otro) AS character varying ) as otro, 
	sum(kms) as kms 
from ( 
    SELECT 
	case WHEN zafira = true THEN 1 ELSE 0 END AS zafira,   
	case WHEN temporal = true THEN 1 ELSE 0 END AS temporal, 
	case WHEN tacumbu = true THEN 1 ELSE 0 END AS tacumbu, 
	case WHEN camsap = true THEN 1 ELSE 0 END AS camsap, 
	case WHEN otro = true THEN 1 ELSE 0 END AS otro, 
 	kms  
	FROM aplicacion.utilizacion_vehiculos  
	inner join aplicacion.vehiculos   
	on (utilizacion_vehiculos.vehiculo = vehiculos.vehiculo) 
        where fecha between 'v0' and 'v1'    
) as t 
order by ord, fecha 



