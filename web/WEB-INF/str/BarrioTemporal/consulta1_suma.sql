
select  
        count(*) as cantidad,
	sum(jardineria) as jardineria, 
	sum(transformador) as transformador, 
	sum(bomba_agua) as bomba_agua,	
	sum(luminica) as luminica,	
	sum(limpieza) as limpieza,	
	sum(incendio) as incendio,	
	sum(otros) as otros	
from ( 
SELECT fecha,  
  case WHEN jardineria = true THEN 1 ELSE 0 END AS jardineria,  
  case WHEN transformador = true THEN 1 ELSE 0 END AS transformador, 
  case WHEN bomba_agua = true THEN 1 ELSE 0 END AS bomba_agua, 
  case WHEN luminica = true THEN 1 ELSE 0 END AS luminica, 
  case WHEN limpieza = true THEN 1 ELSE 0 END AS limpieza, 
  case WHEN incendio = true THEN 1 ELSE 0 END AS incendio, 
  case WHEN otros = true THEN 1 ELSE 0 END AS otros,  
  observaciones 
FROM aplicacion.barrio_temporal 
where fecha between 'v0' and 'v1'  
) as t  

