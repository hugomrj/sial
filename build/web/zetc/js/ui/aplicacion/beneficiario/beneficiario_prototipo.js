
function Beneficiario(){
    
   this.tipo = "beneficiario";   
   this.recurso = "beneficiarios";   
   this.value = 0;
   this.form_descrip = "";
   this.json_descrip = "";
   
   this.dom="";
   this.carpeta=  "/aplicacion";   
   
   
   this.titulosin = "Beneficiario";
   this.tituloplu = "Beneficiarios";   
      
   
   this.campoid=  'id';
   this.tablacampos =  ['id', 'fecha', 'sexo_seleccion', 
       'edad_seleccion', 'zona_seleccion', 'acti_seleccion' ];
   
   this.etiquetas =  ['Código', 'Fecha', 'Sexo',
        'Edad', 'Zona', 'Actividad'];
    
   this.tablaformat = ['N', 'D', 'C',
        'C', 'C', 'C'];
   
   this.tbody_id = "beneficiario-tb";
      
   this.botones_lista = [ this.lista_new] ;
   this.botones_form = "beneficiario-acciones";   
         
   this.parent = null;
   
   this.filtro = "";
  
  
}





Beneficiario.prototype.new = function( obj  ) {                

    reflex.form_new( obj ); 
    reflex.acciones.button_add_promise(obj);     
    
    
        var beneficiario_fecha = document.getElementById('beneficiario_fecha');
        beneficiario_fecha.focus(); 
        beneficiario_fecha.select();      
        
        
    // no mostrar form de archivo
    var form_archivo = document.getElementById('form_archivo');      
    form_archivo.style.display = "none";        
        
  
};





Beneficiario.prototype.form_ini = function() {    
    
    // boton enviar
    var btn_enviar = document.getElementById('btn_enviar');
    btn_enviar.onclick = function(event) {     
       
        var codid  = document.getElementById('beneficiario_id').value;                
        var filetxt = document.getElementById("migracion_file").files[0];
        var nombre_archivo = filetxt.name;

        var form = document.getElementById('form_archivo');
        var formdata = new FormData(form);

        formdata.append("filetxt", filetxt);
        
        file_upload_promesa( formdata, nombre_archivo, codid )
            .then(( xhr ) => {
                
                if (xhr.status == 200){
                    
                    var obj = new Beneficiario();    
                    obj.dom = 'arti_form';                    
                    reflex.form_id_promise( obj, codid );                    
                    msg.ok.mostrar("archivo guardado");  
                }
            })
            .catch(( xhr ) => {                                         
                console.log(xhr.message);                    
            });             

    }
    
};








Beneficiario.prototype.form_validar = function() {    
    
        
    var beneficiario_fecha = document.getElementById('beneficiario_fecha');
    if (beneficiario_fecha.value == ""){
        msg.error.mostrar("Falta fecha ");                    
        beneficiario_fecha.focus();
        beneficiario_fecha.select();                                       
        return false;        
    }
    
    
    
    
    // validar seleccion     
    var beneficiario_sexo_seleccion = document.getElementById('beneficiario_sexo_seleccion');        
    beneficiario_sexo_seleccion.value = "";       
    
    var sexo = false;
    var ele = document.getElementById('beneficiario_sexo_masc');
    if (ele.checked){ 
        sexo = true;        
        beneficiario_sexo_seleccion.value = "Masculino";     
    }
    else{
        var ele = document.getElementById('beneficiario_sexo_feme');
        if (ele.checked){ 
            sexo = true;        
            beneficiario_sexo_seleccion.value = "Femenino";
        }
        else{
            var ele = document.getElementById('beneficiario_sexo_otro');
            if (ele.checked){ 
                sexo = true;   
                beneficiario_sexo_seleccion.value = "Otros";
            }        
            else{
                var ele = document.getElementById('beneficiario_sexo_nocon');
                if (ele.checked){ 
                    sexo = true;        
                    beneficiario_sexo_seleccion.value = "No constesta";
                }                                
            }
        }
    }
    
    if (sexo == false){
        msg.error.mostrar("falta marcar el sexo");                    
        return false;        
    }    
    
    
    
    // validar seleccion     
    var beneficiario_edad_seleccion = document.getElementById('beneficiario_edad_seleccion');        
    beneficiario_edad_seleccion.value = "";       
    
    var edad = false;
    var ele = document.getElementById('beneficiario_edad_5_12');
    if (ele.checked){ 
        edad = true;        
        beneficiario_edad_seleccion.value = "5 - 12";     
    }
    else{
        var ele = document.getElementById('beneficiario_edad_13_17');
        if (ele.checked){ 
            edad = true;        
            beneficiario_edad_seleccion.value = "13 - 17";
        }
        else{
            var ele = document.getElementById('beneficiario_edad_18_29');
            if (ele.checked){ 
                edad = true;        
                beneficiario_edad_seleccion.value = "18 - 29";
            }           
            else{
                var ele = document.getElementById('beneficiario_edad_30_49');
                if (ele.checked){ 
                    edad = true;        
                    beneficiario_edad_seleccion.value = "30 - 49";
                }     
                else{
                    var ele = document.getElementById('beneficiario_edad_50_64');
                    if (ele.checked){ 
                        edad = true;        
                        beneficiario_edad_seleccion.value = "50 - 64";
                    }  
                    else{
                        var ele = document.getElementById('beneficiario_edad_65_mas');
                        if (ele.checked){ 
                            edad = true;        
                            beneficiario_edad_seleccion.value = "65 - mas";
                        }  
                        else{
                            var ele = document.getElementById('beneficiario_edad_nocon');
                            if (ele.checked){ 
                                edad = true;        
                                beneficiario_edad_seleccion.value = "No contesta";
                            }  
                        }
                    }                    
                }
            }
        }
    }    
            
    if (edad == false){
        msg.error.mostrar("falta marcar el edad");                    
        return false;        
    }    
    
    
    
    // validar seleccion     
    var beneficiario_zona_seleccion = document.getElementById('beneficiario_zona_seleccion');        
    beneficiario_zona_seleccion.value = "";         
    
    var zona = false;
    var ele = document.getElementById('beneficiario_zona_1');
    if (ele.checked){ 
        zona = true;        
        beneficiario_zona_seleccion.value = "1";     
    }
    else{
        var ele = document.getElementById('beneficiario_zona_2');
        if (ele.checked){ 
            zona = true;        
            beneficiario_zona_seleccion.value = "2";
        }
        else{
            var ele = document.getElementById('beneficiario_zona_3');
            if (ele.checked){ 
                zona = true;        
                beneficiario_zona_seleccion.value = "3";
            }
            else{
                var ele = document.getElementById('beneficiario_zona_4');
                if (ele.checked){ 
                    zona = true;        
                    beneficiario_zona_seleccion.value = "4";
                }             
                else{
                    var ele = document.getElementById('beneficiario_zona_5');
                    if (ele.checked){ 
                        zona = true;        
                        beneficiario_zona_seleccion.value = "5";
                    }                                 
                    else{
                        var ele = document.getElementById('beneficiario_zona_6');
                        if (ele.checked){ 
                            zona = true;        
                            beneficiario_zona_seleccion.value = "6";
                        }                                         
                        else{
                            var ele = document.getElementById('beneficiario_zona_7');
                            if (ele.checked){ 
                                zona = true;        
                                beneficiario_zona_seleccion.value = "7";
                            }                                                                     
                            else{
                                var ele = document.getElementById('beneficiario_zona_8');
                                if (ele.checked){ 
                                    zona = true;        
                                    beneficiario_zona_seleccion.value = "8";
                                }                                                                                                     
                                else{
                                    var ele = document.getElementById('beneficiario_zona_9');
                                    if (ele.checked){ 
                                        zona = true;        
                                        beneficiario_zona_seleccion.value = "9";
                                    }     
                                    else{
                                        var ele = document.getElementById('beneficiario_zona_hovy');
                                        if (ele.checked){ 
                                            zona = true;        
                                            beneficiario_zona_seleccion.value = "Zona Hovy";
                                        }     
                                        else{
                                            var ele = document.getElementById('beneficiario_zona_sen');
                                            if (ele.checked){ 
                                                zona = true;        
                                                beneficiario_zona_seleccion.value = "Zona Sen";
                                            }                                        
                                            else{
                                                var ele = document.getElementById('beneficiario_zona_otro');
                                                if (ele.checked){ 
                                                    zona = true;        
                                                    beneficiario_zona_seleccion.value 
                                                            = document.getElementById('beneficiario_zona_otro_texto').value;
                                                }                                                         
                                                else{
                                                    var ele = document.getElementById('beneficiario_zona_10');
                                                    if (ele.checked){ 
                                                        zona = true;        
                                                        beneficiario_zona_seleccion.value = "10 (BT)";
                                                    }                                                       
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }        
    }
    
            
    if (zona == false){
        msg.error.mostrar("falta marcar la zona");                    
        return false;        
    }    

    var beneficiario_zona_otro_texto = document.getElementById('beneficiario_zona_otro_texto');
    var ele = document.getElementById('beneficiario_zona_otro');
    if (ele.checked){ 
        if (beneficiario_zona_otro_texto.value == ""){
            msg.error.mostrar("Falta agregar otra zona");                    
            beneficiario_zona_otro_texto.focus();
            beneficiario_zona_otro_texto.select();                                       
            return false;        
        }                
    }
    else{        
        beneficiario_zona_otro_texto.value = "";            
    }
    
         
    // actividad    
    var beneficiario_acti_seleccion = document.getElementById('beneficiario_acti_seleccion');        
    beneficiario_acti_seleccion.value = "";         
    
    var acti = false;
    var ele = document.getElementById('beneficiario_acti_quejas');
    if (ele.checked){ 
        acti = true;        
        beneficiario_acti_seleccion.value = "Quejas y Reclamos";     
    }    
    else{
        var ele = document.getElementById('beneficiario_acti_acompa');
        if (ele.checked){ 
            acti = true;        
            beneficiario_acti_seleccion.value = "Acompañamiento Social";     
        }           
        else{
            var ele = document.getElementById('beneficiario_acti_info');
            if (ele.checked){ 
                acti = true;        
                beneficiario_acti_seleccion.value = "Información";     
            }            
            else{
                var ele = document.getElementById('beneficiario_acti_interme');
                if (ele.checked){ 
                    acti = true;        
                    beneficiario_acti_seleccion.value = "Intermediación Laboral";     
                } 
                else{
                    var ele = document.getElementById('beneficiario_acti_otro');
                    if (ele.checked){ 
                        acti = true;        
                        beneficiario_acti_seleccion.value 
                            = document.getElementById('beneficiario_acti_otro_texto').value;
                    } 
                }
            }            
        }
    }
    
    if (acti == false){
        msg.error.mostrar("falta marcar la actividad");                    
        return false;        
    }       
    
    var beneficiario_acti_otro_texto = document.getElementById('beneficiario_acti_otro_texto');
    var ele = document.getElementById('beneficiario_acti_otro');
    if (ele.checked){ 
        if (beneficiario_acti_otro_texto.value == ""){
            msg.error.mostrar("Falta agregar otra actividad");                    
            beneficiario_acti_otro_texto.focus();
            beneficiario_acti_otro_texto.select();                                       
            return false;        
        }                
    }
    else{        
        beneficiario_acti_otro_texto.value = "";            
    }
        
    
    
    
    
           
    return true;
};










Beneficiario.prototype.main_list = function(obj, page) {    


    if (page === undefined) {    
        page = 1;
    }
    
    

    let promesa = arasa.vista.lista_paginacion(obj, page);    
    promesa        
        .then(( xhr ) => {              
            arasa.html.url.redirect(xhr.status);                                                          

    
            boton.objeto = ""+obj.tipo;
            document.getElementById( obj.tipo +'_acciones_lista' ).innerHTML 
                =  boton.basicform.get_botton_new();                
            
            
            var btn_objeto_nuevo = document.getElementById('btn_' + obj.tipo + '_nuevo');
            btn_objeto_nuevo.addEventListener('click',
                function(event) {                      
                    obj.new( obj );
                },
                false
            );                                    

        })
        .catch(( xhr ) => { 
            console.log(xhr.message);
        }); 

};







Beneficiario.prototype.post_form_id = function( obj  ) {                
    
    var ojson = JSON.parse(form.json) ; 
    
    var valor  = ojson["sexo_masc"];
    var ele = document.getElementById('beneficiario_sexo_masc');   
    if (valor == true){                    
        ele.checked = true;        
    }    
    
    var valor  = ojson["sexo_feme"];
    var ele = document.getElementById('beneficiario_sexo_feme');   
    if (valor == true){                    
        ele.checked = true;        
    }    

    var valor  = ojson["sexo_otro"];
    var ele = document.getElementById('beneficiario_sexo_otro');   
    if (valor == true){                    
        ele.checked = true;        
    }    
    
    var valor  = ojson["sexo_nocon"];
    var ele = document.getElementById('beneficiario_sexo_nocon');   
    if (valor == true){                    
        ele.checked = true;        
    }    
    
    
    var valor  = ojson["edad_5_12"];
    var ele = document.getElementById('beneficiario_edad_5_12');   
    if (valor == true){                    
        ele.checked = true;        
    }    
    
    var valor  = ojson["edad_13_17"];
    var ele = document.getElementById('beneficiario_edad_13_17');   
    if (valor == true){                    
        ele.checked = true;        
    }    
    
    var valor  = ojson["edad_18_29"];
    var ele = document.getElementById('beneficiario_edad_18_29');   
    if (valor == true){                    
        ele.checked = true;        
    }    
    
    var valor  = ojson["edad_30_49"];
    var ele = document.getElementById('beneficiario_edad_30_49');   
    if (valor == true){                    
        ele.checked = true;        
    }    
    
    var valor  = ojson["edad_50_64"];
    var ele = document.getElementById('beneficiario_edad_50_64');   
    if (valor == true){                    
        ele.checked = true;        
    }    
    
    var valor  = ojson["edad_65_mas"];
    var ele = document.getElementById('beneficiario_edad_65_mas');   
    if (valor == true){                    
        ele.checked = true;        
    }    
    
    var valor  = ojson["zona_1"];
    var ele = document.getElementById('beneficiario_zona_1');   
    if (valor == true){                    
        ele.checked = true;        
    }    
    var valor  = ojson["zona_2"];
    var ele = document.getElementById('beneficiario_zona_2');   
    if (valor == true){                    
        ele.checked = true;        
    }    
    var valor  = ojson["zona_3"];
    var ele = document.getElementById('beneficiario_zona_3');   
    if (valor == true){                    
        ele.checked = true;        
    }    
    var valor  = ojson["zona_4"];
    var ele = document.getElementById('beneficiario_zona_4');   
    if (valor == true){                    
        ele.checked = true;        
    }    
    var valor  = ojson["zona_5"];
    var ele = document.getElementById('beneficiario_zona_5');   
    if (valor == true){                    
        ele.checked = true;        
    }    
    var valor  = ojson["zona_6"];
    var ele = document.getElementById('beneficiario_zona_6');   
    if (valor == true){                    
        ele.checked = true;        
    }    
    var valor  = ojson["zona_7"];
    var ele = document.getElementById('beneficiario_zona_7');   
    if (valor == true){                    
        ele.checked = true;        
    }    
    var valor  = ojson["zona_8"];
    var ele = document.getElementById('beneficiario_zona_8');   
    if (valor == true){                    
        ele.checked = true;        
    }    
    var valor  = ojson["zona_9"];
    var ele = document.getElementById('beneficiario_zona_9');   
    if (valor == true){                    
        ele.checked = true;        
    }    
    var valor  = ojson["zona_10"];
    var ele = document.getElementById('beneficiario_zona_10');   
    if (valor == true){                    
        ele.checked = true;        
    }        
    var valor  = ojson["zona_hovy"];
    var ele = document.getElementById('beneficiario_zona_hovy');   
    if (valor == true){                    
        ele.checked = true;        
    }    
    var valor  = ojson["zona_sen"];
    var ele = document.getElementById('beneficiario_zona_sen');   
    if (valor == true){                    
        ele.checked = true;        
    }    
    var valor  = ojson["zona_otro"];
    var ele = document.getElementById('beneficiario_zona_otro');   
    if (valor == true){                    
        ele.checked = true;        
    }    
    
    
    var valor  = ojson["acti_quejas"];
    var ele = document.getElementById('beneficiario_acti_quejas');   
    if (valor == true){                    
        ele.checked = true;        
    }    
    var valor  = ojson["acti_acompa"];
    var ele = document.getElementById('beneficiario_acti_acompa');   
    if (valor == true){                    
        ele.checked = true;        
    }    
    var valor  = ojson["acti_info"];
    var ele = document.getElementById('beneficiario_acti_info');   
    if (valor == true){                    
        ele.checked = true;        
    }    
    var valor  = ojson["acti_interme"];
    var ele = document.getElementById('beneficiario_acti_interme');   
    if (valor == true){                    
        ele.checked = true;        
    }    
    var valor  = ojson["acti_otro"];
    var ele = document.getElementById('beneficiario_acti_otro');   
    if (valor == true){                    
        ele.checked = true;        
    }    
    
    
    
    

    var ojson = JSON.parse( form.json ) ;   
    var json = JSON.stringify(ojson['documento']) ;       
    
    if (typeof json === 'undefined'){ 
        document.getElementById( 'fileD' ).style.display = "none";         
    } 
    else {   
        
        document.getElementById( 'fileU' ).style.display = "none";   

        var ojson = JSON.parse( json ) ;                                            
        document.getElementById( 'fileD_nombre' ).innerHTML  
                = ojson['file_name']



        var afileD = document.getElementById( 'afileD');
        afileD.onclick = function()
        {  
            var url = html.url.absolute() + "/beneficiario/download";
            
            // Use XMLHttpRequest instead of Jquery $ajax
            xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                var a;
                if (xhttp.readyState === 4 && xhttp.status === 200) {
                    // Trick for making downloadable link
                    a = document.createElement('a');
                    a.href = window.URL.createObjectURL(xhttp.response);
                    // Give filename you wish to download

                    var file_name =  xhttp.getResponseHeader("file_name") ; 
                    
                    a.download = file_name;
                    a.style.display = 'none';
                    document.body.appendChild(a);
                    a.click();
                }
            };
            // Post data to URL which handles post request
            xhttp.open("GET", url);
            xhttp.setRequestHeader("Content-Type", "application/json");
            xhttp.setRequestHeader("codid", document.getElementById( 'beneficiario_id' ).value );

            // You should set responseType as blob for binary responses
            xhttp.responseType = 'blob';
            xhttp.send();
        }
            

        var btn_cambiar = document.getElementById( 'btn_cambiar');
        btn_cambiar.onclick = function()
        {  
            document.getElementById( 'fileD' ).style.display = "none";   
            document.getElementById( 'fileU' ).style.display = "initial";  
        };   
    } 
        
        
    
};





Beneficiario.prototype.form_getjson = function( obj  ) {                

    var str = "";
    
    var ele = document.getElementById('beneficiario_id');
    str =   str + form.datos.elemetiq(ele) ;  
    
    str =  str  + ","  ;
    ele = document.getElementById('beneficiario_fecha');
    str =   str + form.datos.elemetiq(ele) ;     

    str =  str  + ","  ;
    ele = document.getElementById('beneficiario_sexo_masc');    
    str =  str + form.radios.getelejson(ele, "sexo_masc");
    
    str =  str  + ","  ;
    ele = document.getElementById('beneficiario_sexo_feme');    
    str =  str + form.radios.getelejson(ele, "sexo_feme");
    
    str =  str  + ","  ;
    ele = document.getElementById('beneficiario_sexo_otro');    
    str =  str + form.radios.getelejson(ele, "sexo_otro");
    
    str =  str  + ","  ;
    ele = document.getElementById('beneficiario_sexo_nocon');    
    str =  str + form.radios.getelejson(ele, "sexo_nocon");
    
    str =  str  + ","  ;
    ele = document.getElementById('beneficiario_edad_5_12');    
    str =  str + form.radios.getelejson(ele, "edad_5_12");
    
    str =  str  + ","  ;
    ele = document.getElementById('beneficiario_edad_13_17');    
    str =  str + form.radios.getelejson(ele, "edad_13_17");
    
    str =  str  + ","  ;
    ele = document.getElementById('beneficiario_edad_18_29');    
    str =  str + form.radios.getelejson(ele, "edad_18_29");
    
    str =  str  + ","  ;
    ele = document.getElementById('beneficiario_edad_30_49');    
    str =  str + form.radios.getelejson(ele, "edad_30_49");
    
    str =  str  + ","  ;
    ele = document.getElementById('beneficiario_edad_50_64');    
    str =  str + form.radios.getelejson(ele, "edad_50_64");
    
    str =  str  + ","  ;
    ele = document.getElementById('beneficiario_edad_65_mas');    
    str =  str + form.radios.getelejson(ele, "edad_65_mas");
    
    str =  str  + ","  ;
    ele = document.getElementById('beneficiario_edad_nocon');    
    str =  str + form.radios.getelejson(ele, "edad_nocon");
    
    str =  str  + ","  ;
    ele = document.getElementById('beneficiario_zona_1');    
    str =  str + form.radios.getelejson(ele, "zona_1");
    
    str =  str  + ","  ;
    ele = document.getElementById('beneficiario_zona_2');    
    str =  str + form.radios.getelejson(ele, "zona_2");
    
    str =  str  + ","  ;
    ele = document.getElementById('beneficiario_zona_3');    
    str =  str + form.radios.getelejson(ele, "zona_3");
    
    str =  str  + ","  ;
    ele = document.getElementById('beneficiario_zona_4');    
    str =  str + form.radios.getelejson(ele, "zona_4");
    
    str =  str  + ","  ;
    ele = document.getElementById('beneficiario_zona_5');    
    str =  str + form.radios.getelejson(ele, "zona_5");
    
    str =  str  + ","  ;
    ele = document.getElementById('beneficiario_zona_6');    
    str =  str + form.radios.getelejson(ele, "zona_6");
    
    str =  str  + ","  ;
    ele = document.getElementById('beneficiario_zona_7');    
    str =  str + form.radios.getelejson(ele, "zona_7");
    
    str =  str  + ","  ;
    ele = document.getElementById('beneficiario_zona_8');    
    str =  str + form.radios.getelejson(ele, "zona_8");
    
    str =  str  + ","  ;
    ele = document.getElementById('beneficiario_zona_9');    
    str =  str + form.radios.getelejson(ele, "zona_9");
    
    str =  str  + ","  ;
    ele = document.getElementById('beneficiario_zona_10');    
    str =  str + form.radios.getelejson(ele, "zona_10");    
    
    str =  str  + ","  ;
    ele = document.getElementById('beneficiario_zona_sen');    
    str =  str + form.radios.getelejson(ele, "zona_sen");
    
    str =  str  + ","  ;
    ele = document.getElementById('beneficiario_zona_hovy');    
    str =  str + form.radios.getelejson(ele, "zona_hovy");
    
    str =  str  + ","  ;
    ele = document.getElementById('beneficiario_zona_otro');    
    str =  str + form.radios.getelejson(ele, "zona_otro");
    
    
    str =  str  + ","  ;
    ele = document.getElementById('beneficiario_zona_otro_texto');
    str =   str + form.datos.elemetiq(ele) ;      
    
    str =  str  + ","  ;
    ele = document.getElementById('beneficiario_acti_quejas');    
    str =  str + form.radios.getelejson(ele, "acti_quejas");  
    
    str =  str  + ","  ;
    ele = document.getElementById('beneficiario_acti_acompa');    
    str =  str + form.radios.getelejson(ele, "acti_acompa");  
    
    str =  str  + ","  ;
    ele = document.getElementById('beneficiario_acti_info');    
    str =  str + form.radios.getelejson(ele, "acti_info");  
    
    str =  str  + ","  ;
    ele = document.getElementById('beneficiario_acti_interme');    
    str =  str + form.radios.getelejson(ele, "acti_interme");  
    
    str =  str  + ","  ;
    ele = document.getElementById('beneficiario_acti_otro');    
    str =  str + form.radios.getelejson(ele, "acti_otro");  
    
    str =  str  + ","  ;
    ele = document.getElementById('beneficiario_acti_otro_texto');
    str =   str + form.datos.elemetiq(ele) ;      

    str =  str  + ","  ;
    ele = document.getElementById('beneficiario_sexo_seleccion');
    str =   str + form.datos.elemetiq(ele) ;      

    str =  str  + ","  ;
    ele = document.getElementById('beneficiario_edad_seleccion');
    str =   str + form.datos.elemetiq(ele) ;      

    str =  str  + ","  ;
    ele = document.getElementById('beneficiario_zona_seleccion');
    str =   str + form.datos.elemetiq(ele) ;      

    str =  str  + ","  ;
    ele = document.getElementById('beneficiario_acti_seleccion');
    str =   str + form.datos.elemetiq(ele) ;      

    return "{" +str+ "}"  ;    

};





Beneficiario.prototype.getUrlFiltro = function( obj  ) {                
    
    var ret = "";    
    ret = obj.filtro;

    return ret;
    
};
