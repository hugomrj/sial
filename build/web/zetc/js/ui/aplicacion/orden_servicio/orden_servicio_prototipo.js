

function OrdenServicio(){
    
   this.tipo = "orden_servicio";   
   this.recurso = "ordenesservicios";   
   this.value = 0;
   this.form_descrip = "";
   this.json_descrip = "";
   
   this.dom="";
   this.carpeta=  "/aplicacion";   
   
   
   this.titulosin = "Órdenes Servicios";
   this.tituloplu = "Órdenes Servicios";   
      
   
   this.campoid=  'orden_servicio';
   this.tablacampos =  ['orden_servicio', 'fecha', 'sillas', 'mesas', 'refrigerios',
        'microfono_parlante', 'vehiculos', 'proyector', 'computadora',
        'materiales_impresos', 'boligrafos', 'hojas_blancas', 'otros', 
        'observaciones'];
   
   this.etiquetas =  ['Orden servicio', 'Fecha', 'sillas', 'mesas', 'refrigerios',
        'microfono parlante', 'vehiculos', 'proyector', 'computadora' ,
        'materiales impresos', 'boligrafos', 'hojas blancas', 'otros',
        'observaciones'];
    
   this.tablaformat = ['N', 'D','B', 'B' ,'B',
        'B', 'B','B','B', 
        'B', 'B','B','B',
        'C'];                                  
   
   this.tbody_id = "orden_servicio-tb";
      
   this.botones_lista = [ this.lista_new] ;
   this.botones_form = "orden_servicio-acciones";   
         
   this.parent = null;
   this.filtro = "";
   
}





OrdenServicio.prototype.new = function( obj  ) {                

    reflex.form_new( obj ); 
    reflex.acciones.button_add_promise(obj);        
  
    document.getElementById('orden_servicio_otros_texto').disabled = true;
    
    
    // no mostrar form de archivo
    var form_archivo = document.getElementById('form_archivo');      
    form_archivo.style.display = "none";        
      
  
    var fecha = document.getElementById('orden_servicio_fecha');
    fecha.focus(); 
    fecha.select();   
  
};





OrdenServicio.prototype.form_ini = function() {    
  
   var orden_servicio_otros_texto =  document.getElementById('orden_servicio_otros_texto');
   
    var orden_servicio_otros =  document.getElementById('orden_servicio_otros');
   
    orden_servicio_otros.onclick = function() {
        
        if (orden_servicio_otros.checked){                    
            orden_servicio_otros_texto.disabled = false;
            orden_servicio_otros_texto.focus();
            orden_servicio_otros_texto.select();            
        }
        else{            
            orden_servicio_otros_texto.disabled = true;
            orden_servicio_otros_texto.value = "";
        }    
    }
   
   
   
   
    // boton enviar
    var btn_enviar = document.getElementById('btn_enviar');
    btn_enviar.onclick = function(event) {     
       
        var codid  = document.getElementById('orden_servicio_orden_servicio').value;                
        var filetxt = document.getElementById("migracion_file").files[0];
        var nombre_archivo = filetxt.name;

        var form = document.getElementById('form_archivo');
        var formdata = new FormData(form);

        formdata.append("filetxt", filetxt);
        
        file_upload_promesa( formdata, nombre_archivo, codid )
            .then(( xhr ) => {
                
                if (xhr.status == 200){
                    
                    var obj = new OrdenServicio();    
                    obj.dom = 'arti_form';                    
                    reflex.form_id_promise( obj, codid );                    
                    msg.ok.mostrar("archivo guardado");  
                }
            })
            .catch(( xhr ) => {                                         
                console.log(xhr.message);                    
            });             

    }
    
       
   
   
   
    
};






OrdenServicio.prototype.form_validar = function() {    
    
    
    var orden_servicio_fecha = document.getElementById('orden_servicio_fecha');
    if (orden_servicio_fecha.value == ""){
        msg.error.mostrar("Falta fecha ");                    
        orden_servicio_fecha.focus();
        orden_servicio_fecha.select();                                       
        return false;        
    }
    
    
    
    return true;
};










OrdenServicio.prototype.main_list = function(obj, page) {    


    if (page === undefined) {    
        page = 1;
    }

    let promesa = arasa.vista.lista_paginacion(obj, page);
    
    promesa        
        .then(( xhr ) => {              
            arasa.html.url.redirect(xhr.status);                                                          
            // botones de accion - nuevo para este caso
            
            // botones de accion - nuevo para este caso       
            boton.objeto = ""+obj.tipo;
            document.getElementById( obj.tipo +'_acciones_lista' ).innerHTML 
                =  boton.basicform.get_botton_new();                
            
            var btn_objeto_nuevo = document.getElementById('btn_' + obj.tipo + '_nuevo');
            btn_objeto_nuevo.addEventListener('click',
                function(event) {  
                    
                    obj.new( obj );

                },
                false
            );              
            
           

        })
        .catch(( xhr ) => { 
            console.log(xhr.message);
        }); 

};







OrdenServicio.prototype.post_form_id = function( obj  ) {                
    


    var ojson = JSON.parse( form.json ) ;   
    var json = JSON.stringify(ojson['documento']) ;       
    
    if (typeof json === 'undefined'){ 
        document.getElementById( 'fileD' ).style.display = "none";         
    } 
    else {   
        
        document.getElementById( 'fileU' ).style.display = "none";   

        var ojson = JSON.parse( json ) ;                                            
        document.getElementById( 'fileD_nombre' ).innerHTML  
                = ojson['file_name']



        var afileD = document.getElementById( 'afileD');
        afileD.onclick = function()
        {  
            var url = html.url.absolute() + "/ordenservicio/download";
            
            // Use XMLHttpRequest instead of Jquery $ajax
            xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                var a;
                if (xhttp.readyState === 4 && xhttp.status === 200) {
                    // Trick for making downloadable link
                    a = document.createElement('a');
                    a.href = window.URL.createObjectURL(xhttp.response);
                    // Give filename you wish to download

                    var file_name =  xhttp.getResponseHeader("file_name") ; 
                    
                    a.download = file_name;
                    a.style.display = 'none';
                    document.body.appendChild(a);
                    a.click();
                }
            };
            // Post data to URL which handles post request
            xhttp.open("GET", url);
            xhttp.setRequestHeader("Content-Type", "application/json");
            xhttp.setRequestHeader("codid", document.getElementById( 'orden_servicio_orden_servicio' ).value );

            // You should set responseType as blob for binary responses
            xhttp.responseType = 'blob';
            xhttp.send();
        }
            

        var btn_cambiar = document.getElementById( 'btn_cambiar');
        btn_cambiar.onclick = function()
        {  
            document.getElementById( 'fileD' ).style.display = "none";   
            document.getElementById( 'fileU' ).style.display = "initial";  
        };   
    } 
        

};




OrdenServicio.prototype.getUrlFiltro = function( obj  ) {                
    
    var ret = "";    
    ret = obj.filtro;

    return ret;
    
};

