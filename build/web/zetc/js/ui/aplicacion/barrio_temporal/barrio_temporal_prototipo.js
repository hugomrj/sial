

function BarrioTemporal(){
    
   this.tipo = "barrio_temporal";   
   this.recurso = "barriotemporal";   
   this.value = 0;
   this.form_descrip = "";
   this.json_descrip = "";
   
   this.dom="";
   this.carpeta=  "/aplicacion";   
   
   
   this.titulosin = "Mantenimiento de Barrio Temporal";
   this.tituloplu = "Mantenimiento de Barrio Temporal";   
      
   
   this.campoid=  'id';
   this.tablacampos =  ['id', 'fecha', 'jardineria' , 'transformador',
            'bomba_agua', 'luminica', 'limpieza', 'incendio',
            'otros'];
   
   this.etiquetas =  ['Código', 'Fecha', 'Jardineria', 'Tranformador Eléctrico',
            'Bomba de agua', 'Luminica', 'Limpieza', 'Prevención Incendio',
            'otros'];
    
   this.tablaformat = ['N', 'D','B', 'B',
                'B', 'B', 'B', 'B',
                'B'];                                  
   
   this.tbody_id = "barrio_temporal-tb";
      
   this.botones_lista = [ this.lista_new] ;
   this.botones_form = "barrio_temporal-acciones";   
         
   this.parent = null;
   this.filtro = "";
   
   
}





BarrioTemporal.prototype.new = function( obj  ) {                

    reflex.form_new( obj ); 
    reflex.acciones.button_add_promise(obj);        
  
    document.getElementById('barrio_temporal_otros_texto').disabled = true;
    
    
};





BarrioTemporal.prototype.form_ini = function() {    

   
    var otros_texto =  document.getElementById('barrio_temporal_otros_texto');   
    var barrio_temporal_otros =  document.getElementById('barrio_temporal_otros');
   
    barrio_temporal_otros.onclick = function() {
        
        if (barrio_temporal_otros.checked){                    
            otros_texto.disabled = false;
            otros_texto.focus();
            otros_texto.select();            
        }
        else{
            
            otros_texto.disabled = true;
            otros_texto.value = "";

        }
    }
   
    
    
    
    
    
    // boton enviar
    var btn_enviar = document.getElementById('btn_enviar');
    btn_enviar.onclick = function(event) {     
       
        var codid  = document.getElementById('barrio_temporal_id').value;                
        var filetxt = document.getElementById("migracion_file").files[0];
        var nombre_archivo = filetxt.name;

        var form = document.getElementById('form_archivo');
        var formdata = new FormData(form);

        formdata.append("filetxt", filetxt);
        
        file_upload_promesa( formdata, nombre_archivo, codid )
            .then(( xhr ) => {
                
                if (xhr.status == 200){
                    
                    var obj = new BarrioTemporal();    
                    obj.dom = 'arti_form';                    
                    reflex.form_id_promise( obj, codid );                    
                    msg.ok.mostrar("archivo guardado");  
                }
            })
            .catch(( xhr ) => {                                         
                console.log(xhr.message);                    
            });             

    }
    
    
    
    
    
};






BarrioTemporal.prototype.form_validar = function() {    
    
    
    var barrio_temporal_fecha = document.getElementById('barrio_temporal_fecha');
    if (barrio_temporal_fecha.value == ""){
        msg.error.mostrar("Falta fecha ");                    
        barrio_temporal_fecha.focus();
        barrio_temporal_fecha.select();                                       
        return false;        
    }
    
        
    return true;
};










BarrioTemporal.prototype.main_list = function(obj, page) {    




    if (page === undefined) {    
        page = 1;
    }

    let promesa = arasa.vista.lista_paginacion(obj, page);
    
    promesa        
        .then(( xhr ) => {              
            arasa.html.url.redirect(xhr.status);                                                          
            // botones de accion - nuevo para este caso
            
            // botones de accion - nuevo para este caso       
            boton.objeto = ""+obj.tipo;
            document.getElementById( obj.tipo +'_acciones_lista' ).innerHTML 
                =  boton.basicform.get_botton_new();                
            
            var btn_objeto_nuevo = document.getElementById('btn_' + obj.tipo + '_nuevo');
            btn_objeto_nuevo.addEventListener('click',
                function(event) {  
                    
                    obj.new( obj );

                },
                false
            );              
            
           

        })
        .catch(( xhr ) => { 
            console.log(xhr.message);
        }); 

};







BarrioTemporal.prototype.post_form_id = function( obj  ) {                
    

    var ojson = JSON.parse( form.json ) ;   
    var json = JSON.stringify(ojson['documento']) ;       
    
    if (typeof json === 'undefined'){ 
        document.getElementById( 'fileD' ).style.display = "none";         
    } 
    else {   
        
        document.getElementById( 'fileU' ).style.display = "none";   

        var ojson = JSON.parse( json ) ;                                            
        document.getElementById( 'fileD_nombre' ).innerHTML  
                = ojson['file_name']



        var afileD = document.getElementById( 'afileD');
        afileD.onclick = function()
        {  
            var url = html.url.absolute() + "/barriotemporal/download";
            
            // Use XMLHttpRequest instead of Jquery $ajax
            xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                var a;
                if (xhttp.readyState === 4 && xhttp.status === 200) {
                    // Trick for making downloadable link
                    a = document.createElement('a');
                    a.href = window.URL.createObjectURL(xhttp.response);
                    // Give filename you wish to download

                    var file_name =  xhttp.getResponseHeader("file_name") ; 
                    
                    a.download = file_name;
                    a.style.display = 'none';
                    document.body.appendChild(a);
                    a.click();
                }
            };
            // Post data to URL which handles post request
            xhttp.open("GET", url);
            xhttp.setRequestHeader("Content-Type", "application/json");
            xhttp.setRequestHeader("codid", document.getElementById( 'barrio_temporal_id' ).value );

            // You should set responseType as blob for binary responses
            xhttp.responseType = 'blob';
            xhttp.send();
        }
            

        var btn_cambiar = document.getElementById( 'btn_cambiar');
        btn_cambiar.onclick = function()
        {  
            document.getElementById( 'fileD' ).style.display = "none";   
            document.getElementById( 'fileU' ).style.display = "initial";  
        };   
    } 
      
};




BarrioTemporal.prototype.getUrlFiltro = function( obj  ) {                
    
    var ret = "";    
    ret = obj.filtro;

    return ret;
    
};

