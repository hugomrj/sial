
select   
        count(*) as cantidad, 
	CAST (sum(jardineria) AS character varying ) as jardineria,   
	CAST (sum(transformador) AS character varying ) as transformador, 
	CAST (sum(bomba_agua) AS character varying ) as bomba_agua, 
	CAST (sum(luminica) AS character varying ) as luminica, 
	CAST (sum(limpieza) AS character varying ) as limpieza, 
	CAST (sum(incendio) AS character varying ) as incendio, 
	CAST (sum(otros) AS character varying ) as otros	 
from (  
SELECT fecha,   
  case WHEN jardineria = true THEN 1 ELSE 0 END AS jardineria,   
  case WHEN transformador = true THEN 1 ELSE 0 END AS transformador,  
  case WHEN bomba_agua = true THEN 1 ELSE 0 END AS bomba_agua,  
  case WHEN luminica = true THEN 1 ELSE 0 END AS luminica,  
  case WHEN limpieza = true THEN 1 ELSE 0 END AS limpieza,  
  case WHEN incendio = true THEN 1 ELSE 0 END AS incendio,  
  case WHEN otros = true THEN 1 ELSE 0 END AS otros,   
  observaciones  
FROM aplicacion.barrio_temporal 
where fecha between 'v0' and 'v1'  
) as t   
 


