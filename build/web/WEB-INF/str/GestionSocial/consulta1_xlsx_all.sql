

select 1 as ord, 
CAST (fecha AS character varying ) fecha, 
CAST (gestion_social.programa AS character varying ) programa, 
actividad_nombre, participantes_cantidad,  
sexo_masculino, sexo_femenino, sexo_otros, zona1, zona2, zona3, zona4,  
zona5, zona6, zona7, zona8, zona9, zona10, 
zona_banco_hovy, zona_refugio_sen,  
zona_otro, edad_5_12, edad_13_17, edad_18_29, edad_30_49, edad_50_64,  
edad_65_mas,  programas.descripcion  
FROM aplicacion.gestion_social inner join aplicacion.programas  
on (gestion_social.programa = programas.programa)  
where fecha between 'v0' and 'v1'     

union 

select 2 as ord,  
'' as f , '' as p, 'TOTALES' as actividad_nombre, 
sum(participantes_cantidad) participantes_cantidad,   
sum(sexo_masculino) sexo_masculino, sum(sexo_femenino) sexo_femenino,   
sum(sexo_otros) sexo_otros, sum(zona1) zona1, sum(zona2) zona2, 
sum(zona3) zona3, sum(zona4) zona4, sum(zona5) zona5, sum(zona6) zona6,  
sum(zona7) zona7, sum(zona8) zona8, sum(zona9) zona9, sum(zona10) zona10, 
sum(zona_banco_hovy) zona_banco_hovy,  
sum(zona_refugio_sen) zona_refugio_sen, sum(zona_otro) zona_otro,  
sum(edad_5_12) edad_5_12, sum(edad_13_17) edad_13_17, sum(edad_18_29) edad_18_29,  
sum(edad_30_49) edad_30_49, sum(edad_50_64) edad_50_64,  
sum(edad_65_mas) edad_65_mas, '' as d 
FROM aplicacion.gestion_social  
where fecha between 'v0' and 'v1'   

order by ord, fecha  






