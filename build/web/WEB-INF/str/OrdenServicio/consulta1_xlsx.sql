
select  1 as cod, orden_servicio, 
  CAST ( fecha AS character varying ) fecha,  
  case WHEN sillas = true THEN 'X' ELSE '' END AS sillas,  
  case WHEN mesas = true THEN 'X' ELSE '' END AS mesas,  
  case WHEN refrigerios = true THEN 'X' ELSE '' END AS refrigerios,  
  case WHEN microfono_parlante = true THEN 'X' ELSE '' END AS microfono_parlante,  
  case WHEN vehiculos = true THEN 'X' ELSE '' END AS vehiculos,  
  case WHEN proyector = true THEN 'X' ELSE '' END AS proyector,  
  case WHEN computadora = true THEN 'X' ELSE '' END AS computadora,  
  case WHEN materiales_impresos = true THEN 'X' ELSE '' END AS materiales_impresos,  
  case WHEN boligrafos = true THEN 'X' ELSE '' END AS boligrafos,  
  case WHEN hojas_blancas = true THEN 'X' ELSE '' END AS hojas_blancas,  
  case WHEN otros = true THEN 'X' ELSE '' END AS otros,  
  observaciones    
FROM aplicacion.ordenes_servicios 
where fecha between 'v0' and 'v1'  
    
  union 

select 2 as cod, 0 as os,  'TOTALES' as f,	 
	CAST (sum(sillas) AS character varying ) as sillas,  
	CAST (sum(mesas) AS character varying ) as mesas, 
	CAST (sum(refrigerios) AS character varying ) as refrigerios, 
	CAST (sum(microfono_parlante) AS character varying ) as microfono_parlante, 
	CAST (sum(vehiculos) AS character varying ) as vehiculos, 
	CAST (sum(proyector) AS character varying ) as proyector, 
	CAST (sum(computadora) AS character varying ) as computadora, 
	CAST (sum(materiales_impresos) AS character varying ) as materiales_impresos, 
	CAST (sum(boligrafos) AS character varying ) as boligrafos, 
	CAST (sum(hojas_blancas) AS character varying ) as hojas_blancas, 
	CAST (sum(otros) AS character varying ) as otros, 
	'' as o		
from (  
SELECT fecha,  
  case WHEN sillas = true THEN 1 ELSE 0 END AS sillas,  
  case WHEN mesas = true THEN  1 ELSE 0 END AS mesas,  
  case WHEN refrigerios = true THEN  1 ELSE 0 END AS refrigerios,  
  case WHEN microfono_parlante = true THEN  1 ELSE 0 END AS microfono_parlante,  
  case WHEN vehiculos = true THEN  1 ELSE 0 END AS vehiculos,  
  case WHEN proyector = true THEN  1 ELSE 0 END AS proyector,  
  case WHEN computadora = true THEN  1 ELSE 0 END AS computadora,  
  case WHEN materiales_impresos = true THEN  1 ELSE 0 END AS materiales_impresos,  
  case WHEN boligrafos = true THEN  1 ELSE 0 END AS boligrafos,  
  case WHEN hojas_blancas = true THEN  1 ELSE 0 END AS hojas_blancas,  
  case WHEN otros = true THEN  1 ELSE 0 END AS otros,  
  observaciones  
  FROM aplicacion.ordenes_servicios   
  where fecha between 'v0' and 'v1'  
  ) as t1  
  
  order by cod, fecha   
  