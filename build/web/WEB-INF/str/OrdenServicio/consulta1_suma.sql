
select 
	sum(sillas) sillas, 
	sum(mesas) mesas, 
	sum(refrigerios) refrigerios, 
	sum(microfono_parlante) microfono_parlante, 
	sum(vehiculos) vehiculos, 
	sum(proyector) proyector, 
	sum(computadora) computadora, 
	sum(materiales_impresos) materiales_impresos, 
	sum(boligrafos) boligrafos, 
	sum(hojas_blancas) hojas_blancas, 
	sum(otros) otros 
from (  
SELECT fecha,  
  case WHEN sillas = true THEN 1 ELSE 0 END AS sillas, 
  case WHEN mesas = true THEN  1 ELSE 0 END AS mesas, 
  case WHEN refrigerios = true THEN  1 ELSE 0 END AS refrigerios, 
  case WHEN microfono_parlante = true THEN  1 ELSE 0 END AS microfono_parlante, 
  case WHEN vehiculos = true THEN  1 ELSE 0 END AS vehiculos, 
  case WHEN proyector = true THEN  1 ELSE 0 END AS proyector, 
  case WHEN computadora = true THEN  1 ELSE 0 END AS computadora, 
  case WHEN materiales_impresos = true THEN  1 ELSE 0 END AS materiales_impresos, 
  case WHEN boligrafos = true THEN  1 ELSE 0 END AS boligrafos, 
  case WHEN hojas_blancas = true THEN  1 ELSE 0 END AS hojas_blancas, 
  case WHEN otros = true THEN  1 ELSE 0 END AS otros, 
  observaciones 
  FROM aplicacion.ordenes_servicios 
  where fecha between 'v0' and 'v1'  
  ) as t1 

