
select 
	count(*) cantidad, 
	sum(zafira) zafira, 
	sum(temporal) temporal, 
	sum(tacumbu) tacumbu, 
	sum(camsap) camsap, 
	sum(otro) otro, 
	sum(kms) kms 
from ( 
	select  
	case WHEN zafira = true THEN 1 ELSE 0 END AS zafira,  
	case WHEN temporal = true THEN 1 ELSE 0 END AS temporal, 
	case WHEN tacumbu = true THEN 1 ELSE 0 END AS tacumbu, 
	case WHEN camsap = true THEN 1 ELSE 0 END AS camsap, 
	case WHEN otro = true THEN 1 ELSE 0 END AS otro, 
	 kms 
	FROM aplicacion.utilizacion_vehiculos 
	inner join aplicacion.vehiculos  
	on (utilizacion_vehiculos.vehiculo = vehiculos.vehiculo) 
        where fecha between 'v0' and 'v1'    
        and vehiculos.vehiculo = v2    
	order by fecha, vehiculos.vehiculo  
) as t 
