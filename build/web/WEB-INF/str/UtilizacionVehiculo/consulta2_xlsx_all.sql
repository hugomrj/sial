

select 1 as ord, * 
from (	 
	select  
                count(*) cant, 
		vehiculo_nombre,   
		CAST (sum(zafira) AS character varying ) as zafira,  
		CAST (sum(temporal) AS character varying ) as temporal,  
		CAST (sum(tacumbu) AS character varying ) as tacumbu,  
		CAST (sum(camsap) AS character varying ) as camsap, 
		CAST (sum(otro) AS character varying ) as otro, 
		sum(kms) as kms , vehiculo  
	from (		
		SELECT  vehiculos.vehiculo, 
		(vehiculos.marca || ' ' || vehiculos.modelo  || ' ' || vehiculos.chapa ) AS vehiculo_nombre, 
		case WHEN zafira = true THEN 1 ELSE 0 END AS zafira,   
		case WHEN temporal = true THEN 1 ELSE 0 END AS temporal,  
		case WHEN tacumbu = true THEN 1 ELSE 0 END AS tacumbu,  
		case WHEN camsap = true THEN 1 ELSE 0 END AS camsap, 
		case WHEN otro = true THEN 1 ELSE 0 END AS otro,  
		 kms 
		FROM aplicacion.utilizacion_vehiculos  
		inner join aplicacion.vehiculos   
		on (utilizacion_vehiculos.vehiculo = vehiculos.vehiculo)  
                where fecha between 'v0' and 'v1'    
	) as t		 
	group by vehiculo, vehiculo_nombre 
) as t1 
union  
select 2 as ord, * 
from ( 
	select  
                count(*) cant, 
		CAST ('' AS character varying ) as nombre,  
		CAST (sum(zafira) AS character varying ) as zafira, 
		CAST (sum(temporal) AS character varying ) as temporal, 
		CAST (sum(tacumbu) AS character varying ) as tacumbu, 
		CAST (sum(camsap) AS character varying ) as camsap, 
		CAST (sum(otro) AS character varying ) as otro, 
                sum(kms) as kms , 0 as vehiculo   
	from ( 
		select  
		case WHEN zafira = true THEN 1 ELSE 0 END AS zafira,  
		case WHEN temporal = true THEN 1 ELSE 0 END AS temporal, 
		case WHEN tacumbu = true THEN 1 ELSE 0 END AS tacumbu, 
		case WHEN camsap = true THEN 1 ELSE 0 END AS camsap, 
		case WHEN otro = true THEN 1 ELSE 0 END AS otro, 
		 kms 
		FROM aplicacion.utilizacion_vehiculos 
		inner join aplicacion.vehiculos  
		on (utilizacion_vehiculos.vehiculo = vehiculos.vehiculo) 
		where fecha between 'v0' and 'v1'    
	) as t  
) as t2 
order by ord, vehiculo  
