--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.25
-- Dumped by pg_dump version 9.5.25

-- Started on 2023-01-13 14:44:57 -03

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 214 (class 1259 OID 43435)
-- Name: medidas_compensaciones; Type: TABLE; Schema: aplicacion; Owner: postgres
--

CREATE TABLE aplicacion.medidas_compensaciones (
    codigo integer NOT NULL,
    descripcion character varying
);


ALTER TABLE aplicacion.medidas_compensaciones OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 43422)
-- Name: padron_familias; Type: TABLE; Schema: aplicacion; Owner: hugo
--

CREATE TABLE aplicacion.padron_familias (
    cedula integer NOT NULL,
    nombre character varying,
    apellido character varying,
    sexo character varying,
    fecha_nacimiento date,
    nacionalidad character varying,
    telefono character varying,
    estado_civil character varying,
    indigena character varying,
    afrodescendiente character varying,
    otra_poblacion character varying,
    censo_2017 character varying,
    censo_2020 character varying,
    codigo_censo integer,
    barrio_origen character varying,
    georeferencia character varying,
    doc_titular_vivienda character varying,
    doc_postulacion_vivienda character varying,
    medida_compensacion integer,
    observaciones character varying
);


ALTER TABLE aplicacion.padron_familias OWNER TO hugo;

--
-- TOC entry 2270 (class 0 OID 43435)
-- Dependencies: 214
-- Data for Name: medidas_compensaciones; Type: TABLE DATA; Schema: aplicacion; Owner: postgres
--

COPY aplicacion.medidas_compensaciones (codigo, descripcion) FROM stdin;
1	RESTITUCION DE VIVIENDAS
2	BANCO HOVY
3	BARRIO TEMPORAL
4	PESCADORES, ESPINELEROS Y LATONERAS
5	REFUGIO SEN
\.


--
-- TOC entry 2269 (class 0 OID 43422)
-- Dependencies: 213
-- Data for Name: padron_familias; Type: TABLE DATA; Schema: aplicacion; Owner: hugo
--

COPY aplicacion.padron_familias (cedula, nombre, apellido, sexo, fecha_nacimiento, nacionalidad, telefono, estado_civil, indigena, afrodescendiente, otra_poblacion, censo_2017, censo_2020, codigo_censo, barrio_origen, georeferencia, doc_titular_vivienda, doc_postulacion_vivienda, medida_compensacion, observaciones) FROM stdin;
1238568	Guillermo	Abad Clivio	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	169	Mto. Ursicino Velasco	\N	\N	\N	4	Misma Observación del lote 7 B.
7074039	Aureliana Antonia	Acevedo López	MASCULINO	\N	PARAGUAYO/A	0981514022	UNION DE HECHO	\N	\N	\N	SI	NO	2712	Ursicino Velasco	\N	\N	\N	0	La señora Aureliana actualmente es encargada de AMUCOES. La misma tiene una casa precaria dentro del predio de la organización hace un año
798319	María Romualda	Achucarro de Benítez	FEMENINO	\N	PARAGUAYO/A	.	CASADO/A	\N	\N	\N	SI	NO	2596	8 de Diciembre	\N	\N	\N	0	.
484460	Marciana	Achucarro	FEMENINO	\N	PARAGUAYO/A	0984 350 415	SOLTERO/A	\N	\N	\N	SI	NO	2601	8 de Diciembre	\N	\N	\N	0	.
4790261	Derlis Agustín	Acosta Báez	MASCULINO	\N	PARAGUAYO/A	0986437128	SOLTERO/A	\N	\N	\N	SI	NO	226	YVOTY	\N	\N	\N	0	.
381637	León Ramón	Acosta Bogado	MASCULINO	\N	PARAGUAYO/A	0981951675	.	\N	\N	\N	SI	NO	936	Nuevo Amanecer	\N	\N	\N	0	.
4481642	Nathalia Elizabeth	Acosta Cáceres	FEMENINO	\N	PARAGUAYO/A	0984486586	UNION DE HECHO	\N	\N	\N	SI	NO	1250	Pajagua Naranja	\N	\N	\N	0	.
5004982	Elvira	Acosta Cardozo	FEMENINO	\N	PARAGUAYO/A	0985389280	UNION DE HECHO	\N	\N	\N	SI	NO	824	24 de Junio c/ San Cayetano	\N	\N	\N	0	Viene en el mismo terreno de la señora María Silvia Almada
4227326	Francisco	Acosta Cardozo	MASCULINO	1985-04-10	PARAGUAYO/A	0994631437	CASADO/A	\N	\N	\N	SI	NO	2604	Rancho 8	\N	\N	\N	0	Placa Electrica 1 La pareja lleva 1 ano de casado y dos anos de concubinato,no tienen lote propio ,pero si un techo dentro del lote de la madre ,se manejan independientemente.
5591520	Sandra Mabel	Acosta Cardozo	FEMENINO	\N	PARAGUAYO/A	0971918296	SOLTERO/A	\N	\N	\N	SI	NO	2603	Rancho 8	\N	\N	\N	0	Placa Electrica 1 La censada esta separada del padre de sus hijos hace 3 meses,la misma vende minutas informalmente hace 3 meses no tiene lote propio.
2854702	Nancy Elizabeth	Acosta de Ocampo	FEMENINO	\N	PARAGUAYO/A	0985986061	CASADO/A	\N	\N	\N	SI	NO	1733	Ursicino Velasco	\N	\N	\N	0	Es propietaria del lote y la vivienda con ella vive una de la hija con su pareja no tiene hijos cuentan con una casita de material 4x4mts.independiente.
2197520	Agustín	Acosta Delgado	MASCULINO	\N	PARAGUAYO/A	0982807033	SOLTERO/A	\N	\N	\N	SI	NO	1654	Tape Pyahu	\N	\N	\N	0	.
1547488	Lidia Maríana	Acosta Delgado	FEMENINO	\N	PARAGUAYO/A	0982154317	SEPARADO/A	\N	\N	\N	SI	NO	1658	Virgen de Guadalupe	\N	\N	\N	0	Tiene dos fotos de frente porque es grande y no entra en una sola
1667687	Rosa Juliána	Acosta Delgado	FEMENINO	2068-09-02	PARAGUAYO/A	0992822297	SOLTERO/A	\N	\N	\N	SI	NO	1458	Virgen de guadalupe	\N	\N	\N	0	.
4792675	Julia Cristina	Acosta Denis	FEMENINO	1995-05-05	PARAGUAYO/A	0995684306	UNION DE HECHO	\N	\N	\N	SI	NO	1581	Ramon Talavera	\N	\N	\N	0	Es propietaria del lote con ella vive la mam en otra vivienda precaria, esta construyendo un hormigon de 8 x4.
4514134	Paola Ines	Acosta Denis	FEMENINO	\N	PARAGUAYO/A	0992621665	UNION DE HECHO	\N	\N	\N	SI	NO	1584	Ramon Talavera	\N	\N	\N	6	El Sr. Hugo vive en una casa precaria, vive con Paola porque el agua alcanzo su casa.
2966892	Juan Carlos	Acosta Franco	MASCULINO	\N	PARAGUAYO/A	0985963516	SOLTERO/A	\N	\N	\N	SI	NO	657	Virgen de Lourdes	\N	\N	\N	0	La vivienda esta en construccion hace 5 anos.
2327890	Gloria Beatriz	Acosta Gaona	FEMENINO	\N	PARAGUAYO/A	0971775197	CASADO/A	\N	\N	\N	SI	NO	860	Ursicino Velasco	\N	\N	\N	0	.
5359400	Mario	Acosta Lovera	MASCULINO	1994-06-07	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1439	Ursicino Velasco	\N	\N	\N	0	Hace 7 meses se concubiunaron Mario de 4 anos es el hijo del titular.
6212794	Ramona Auxilidora	Acosta Maidana	MASCULINO	\N	PARAGUAYO/A	0984857150	UNION DE HECHO	\N	\N	\N	SI	NO	560	8 de Diciembre	\N	\N	\N	0	La casa de material esta pegada a otro bloque .Provida en total la dimension de la vivienda es de 12 cm x 6.
4685155	Tamara Jzmin	Acosta Moray	FEMENINO	1997-12-07	PARAGUAYO/A	0984977464	UNION DE HECHO	\N	\N	\N	SI	NO	391	.	\N	\N	\N	0	.
1748463	Mercedes	Acosta Oviedo	FEMENINO	\N	PARAGUAYO/A	0982179513	CASADO/A	\N	\N	\N	SI	NO	551	Ursicino Velasco	\N	\N	\N	0	.
1244580	Catalina	Acosta Paez	FEMENINO	\N	PARAGUAYO/A	0992543270	UNION DE HECHO	\N	\N	\N	SI	NO	284	Ursicino Velasco	\N	\N	\N	0	La titular se dedica a la venta de cosmestibles en su vivienda. En este lote hay 2 techos
2085620	Eliodora	Acosta Paez	FEMENINO	2062-04-04	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	168	Nuevo Amanecer	\N	\N	\N	0	Vive en el mismo lote pero no es fliar. Dicen que es un lote aparte , pero no se observa división fisica.
5356177	Debora Jazmín de María	Acosta Portillo	FEMENINO	1994-12-11	PARAGUAYO/A	0992570243	UNION DE HECHO	\N	\N	\N	SI	NO	285	Ursicino Velasco	\N	\N	\N	0	La misma vive dentro del lote de la tia quien es la duena del terreno.
5405419	Juana Verónica	Acosta Rodríguez	MASCULINO	1992-12-03	PARAGUAYO/A	0985578028	UNION DE HECHO	\N	\N	\N	SI	NO	143	Urcisino Velasco y Oleros	\N	\N	\N	0	.
3838508	María del Rosario	Acosta Rodríguez	FEMENINO	1983-10-10	PARAGUAYO/A	0991237105	UNION DE HECHO	\N	\N	\N	SI	NO	113	YVOTY	\N	\N	\N	0	.
5663487	Mario Adrian	Acosta Rodríguez	MASCULINO	1993-12-09	PARAGUAYO/A	0971498340	UNION DE HECHO	\N	\N	\N	SI	NO	119	YVOTY	\N	\N	\N	0	.
4825126	Mauricio Rogelio	Acosta Rodríguez	MASCULINO	1988-10-06	PARAGUAYO/A	0994948648	UNION DE HECHO	\N	\N	\N	SI	NO	2469	Aromita	\N	\N	\N	0	.
3986823	Cinthia Carolina	Acosta	FEMENINO	\N	PARAGUAYO/A	0972715344	UNION DE HECHO	\N	\N	\N	SI	NO	1181	Urcicino Velazco	\N	\N	\N	0	vive dentro del lote de la mama con su familia en una pieza
606902	Elias	Acosta	MASCULINO	\N	PARAGUAYO/A	0982868228	UNION DE HECHO	\N	\N	\N	SI	NO	1659	Virgen de Guadalupe	\N	\N	\N	0	Tiene Corredor.
4628966	Julia Griselda	Acosta	FEMENINO	\N	PARAGUAYO/A	0971116904	UNION DE HECHO	\N	\N	\N	SI	NO	624	San Cayetano	\N	\N	\N	0	.
1423897	Julián	Acosta	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	2018	Ursisino Velazco c/ Mbo'ehára	\N	\N	\N	0	.
2228726	Lucía	Acosta	FEMENINO	2064-06-07	PARAGUAYO/A	0982291932	UNION DE HECHO	\N	\N	\N	SI	NO	1891	Julio Benitez	\N	\N	\N	0	.
3876351	María Antonia	Acosta	FEMENINO	2051-04-05	PARAGUAYO/A	0983479616	UNION DE HECHO	\N	\N	\N	SI	NO	1598	Nuestra Senora de la Asunción	\N	\N	\N	0	.
3211792	Máxima	Acosta	FEMENINO	\N	PARAGUAYO/A	0984 292505	UNION DE HECHO	\N	\N	\N	SI	NO	1744	Virgen de Guadalupe	\N	\N	\N	0	.
6328569	Natalia Noemí	Acosta	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	2711	Ursino Velasco	\N	\N	\N	0	.
2054378	Ninfa	Acosta	FEMENINO	\N	PARAGUAYO/A	0982224973	UNION DE HECHO	\N	\N	\N	SI	NO	307	Virgen de Lourdes c/ Oleros	\N	\N	\N	0	El ítem 10 es ahijada del titular del titular de la vivienda
1374315	Rogelio	Acosta	MASCULINO	\N	PARAGUAYO/A	0991691742	UNION DE HECHO	\N	\N	\N	SI	NO	109	Virgen de Lourdes	\N	\N	\N	4	.
6378117	Osmar	Adorno Benítez	MASCULINO	1992-09-02	PARAGUAYO/A	0991915580	UNION DE HECHO	\N	\N	\N	SI	NO	409	Julio Jara e/ Nuevo Amanecer	\N	\N	\N	0	Vive dentro del terreno de la Sra.Maria Castorina Insfran y cuenta con una casa propia de madera.
948828	Catalina	Adorno de Sánchez	FEMENINO	\N	PARAGUAYO/A	0982191309	SOLTERO/A	\N	\N	\N	SI	NO	452	Ursicino Velasco	\N	\N	\N	0	.
1315520	Victoriana	Adorno de Segovia	MASCULINO	\N	PARAGUAYO/A	.	CASADO/A	\N	\N	\N	SI	NO	791	Virgen de Lourdes	\N	\N	\N	0	La titular Tiene una casilla de minutas sobre Ursicino Velasco y San Felipe y Santiago(2x1,5).
3389415	Sonia Elizabeth	Adorno Maidana	FEMENINO	1992-11-09	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	704	Mar del Plata	\N	\N	\N	5	.
7241125	Francisco Javier	Adorno Quintana	MASCULINO	2017-07-07	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	218	Virgen de Lujan y Virgen de Lourdes	\N	\N	\N	0	.
4352224	Gloria Beatriz	Adorno Ramírez	FEMENINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	441	San Cayetano	\N	\N	\N	0	.
1511103	María Cristina	Adorno Ramírez	FEMENINO	\N	PARAGUAYO/A	0981998760	.	\N	\N	\N	SI	NO	428	Ursicino Velazco c/ Martin Fierro	\N	\N	\N	0	La ocupante manifiesta que tiene otro lote en la misma manzana que es la M30 LOTE 44 dimensión de su lote es de 12 Mts. X 18 Mts.y de construccion 6 Mts. De ancho x 5 mts. De largo de amterial de construccion y techo de chapa zinc
3220698	Cosme Damian	Adorno	MASCULINO	\N	PARAGUAYO/A	0981489876	UNION DE HECHO	\N	\N	\N	SI	NO	793	Virgen de Lujan	\N	\N	\N	0	Tecnico Deposito Comercial.
4848265	Daniel Rotela	Adorno	MASCULINO	1989-03-01	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	794	Virgen de Lourdes	\N	\N	\N	0	.
3308652	de Benitez, María Evangelista	Adorno	FEMENINO	\N	PARAGUAYO/A	0971793075	CASADO/A	\N	\N	\N	SI	NO	2635	Santa Librada	\N	\N	\N	0	.
2142665	Isabel	Adorno	FEMENINO	2065-08-05	PARAGUAYO/A	0981395982	SOLTERO/A	\N	\N	\N	SI	NO	1577	Ramon Talavera	\N	\N	\N	0	.
5009505	Karen Vanessa	Adorno	FEMENINO	1993-01-06	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	220	Virgen de lourdes	\N	\N	\N	0	.
1468375	María Venancia	Adorno	FEMENINO	2061-01-04	PARAGUAYO/A	098654623	SOLTERO/A	\N	\N	\N	SI	NO	562	San Cayetano	\N	\N	\N	0	Dentro del techo de la longuitud vive una pareja in hijos.
5313633	Patricia Carolina	Adorno	FEMENINO	\N	PARAGUAYO/A	0982309656	UNION DE HECHO	\N	\N	\N	SI	NO	564	San Cayetano	\N	\N	\N	0	Esta pareja lleva 6 años de concubinato, tienen dos hijos, viven en el mismo lote de la titular pero con techo aparte.
8002	Agrupación de Boys Scout	.	.	\N	.	0971 347 203	.	\N	\N	\N	SI	NO	1839	Ursicino Velazco	\N	\N	\N	0	Las actividades que se realizan son: Adiestramiento e preservación de la naturaleza y el medio ambiente.
1214410	Gladys Irene	Agüero Benítez	FEMENINO	\N	PARAGUAYO/A	0983155192	UNION DE HECHO	\N	\N	\N	SI	NO	2047	Ursicino Velasco c/ Sotero Colman	\N	\N	\N	0	.
5174200	Ingrid Dalila	Agüero Benítez	FEMENINO	1988-12-12	PARAGUAYO/A	0982367240	CASADO/A	\N	\N	\N	SI	NO	2048	Ursicino Velasco c/ Sotero Colman	\N	\N	\N	0	Secarropas 1.
5000872	Eliane	Agüero Cardosa	FEMENINO	1990-06-02	PARAGUAYO/A	0985474179	SOLTERO/A	\N	\N	\N	SI	NO	1476	Payagua naranja	\N	\N	\N	0	Vive con la suegra su marido se encuentra privado de su libertad.
4207204	Roque Ramón Rufino	Agüero Chávez	MASCULINO	\N	PARAGUAYO/A	0992679813	UNION DE HECHO	\N	\N	\N	SI	NO	2497	Virgen de lourdes	\N	\N	\N	0	.
1246811	Melania	Agüero de Bogado	FEMENINO	2064-03-12	PARAGUAYO/A	0982719781	CASADO/A	\N	\N	\N	SI	NO	1566	Ursicino Velasco	\N	\N	\N	0	Es propietaria del lote.Al fondo vive uno de sus hijos con sus nietos, en una vivienda de material bien cosntituida.
1214376	Luisa	Agüero de Pérez	FEMENINO	\N	PARAGUAYO/A	0984184954	CASADO/A	\N	\N	\N	SI	NO	2190	Satero Colman	\N	\N	\N	0	corredor de hormigón
6595695	Venancio	Agüero Fernández	MASCULINO	1991-08-04	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	2106	.	\N	\N	\N	0	.
4848267	Cynthia Evangelina	Agüero Guerrero	FEMENINO	\N	PARAGUAYO/A	0994111694	UNION DE HECHO	\N	\N	\N	SI	NO	1487	Rancho 8	\N	\N	\N	0	1 Carrito.
4481673	Hugo Sebastian	Agüero Leguizamon	MASCULINO	\N	PARAGUAYO/A	0971 636022	CASADO/A	\N	\N	\N	SI	NO	1673	Yacare Pito	\N	\N	\N	0	Pareja, casados hace 14 años
4401952	Maida Yesenia Claribel	Agüero Quintana	FEMENINO	1990-12-06	PARAGUAYO/A	0994744049	CASADO/A	\N	\N	\N	SI	NO	957	Angel c/ Virgen de Lourdes	\N	\N	\N	0	La sra Nicolasa Quintana es la ropietaria del terreno M40 Lote 015
6100677	Celia	Agüero	FEMENINO	1983-06-03	PARAGUAYO/A	0994264082	UNION DE HECHO	\N	\N	\N	SI	NO	2100	Chuiquero	\N	\N	\N	0	.
5569623	Cintia	Agüero	FEMENINO	\N	PARAGUAYO/A	0972835853	SOLTERO/A	\N	\N	\N	SI	NO	2521	Ursicino Velasco	\N	\N	\N	0	Vive hace 3 meses en este terreno pero lleva mas anos en el barrio.
5031215	Juan Carlos	Agüero	MASCULINO	1991-03-09	PARAGUAYO/A	0985676251	UNION DE HECHO	\N	\N	\N	SI	NO	2050	Ursicino Velasco c/ Sotero Colman	\N	\N	\N	0	Estan de pareja hace 2 anos y vive en el terreno de la propietaria.
1707294	Patricia Leguizamón	Agüero	FEMENINO	\N	PARAGUAYO/A	0972608188	UNION DE HECHO	\N	\N	\N	SI	NO	209	Aromita	\N	\N	\N	0	.
3255716	Elena Beatriz	Aguiar Romero	FEMENINO	\N	PARAGUAYO/A	0994261427	UNION DE HECHO	\N	\N	\N	SI	NO	501	San Felipe y Santiago c/ 23 de Junio	\N	\N	\N	0	El terreno cuenbta con una extencion de vivienda de 10x4 de construccion donde se encuentra viviendo la senora Noemi de la Cruz y Sr. Francisco Riquelme
1113446	Elodia	Aguilar Agüero	FEMENINO	\N	PARAGUAYO/A	0991867926	UNION DE HECHO	\N	\N	\N	SI	NO	2626	Ursicino Velascoc/ Mita Saraki	\N	\N	\N	6	su terreno actualmente esta inundado,vive dentro del barrio en la Zona 6 en la casa de Petrona Farfan.
668868	Victoriana Ramona	Aguilar de Bernal	MASCULINO	\N	PARAGUAYO/A	0984202674	.	\N	\N	\N	SI	NO	854	Ursicino Velasco Psillo S/ Nombre	\N	\N	\N	0	Tiene un Carrito para su Hamburgueseria.
1429026	Cinthia Liza	Aguilar de Duarte	FEMENINO	\N	PARAGUAYO/A	0983355050	DIVORCIADO/A	\N	\N	\N	SI	NO	1066	Ursicino Velasco	\N	\N	\N	0	Cuenta con dos construcciones muy amplias desconoce la dimension facilita un No. De expediente de la municipalidad donde dice se encuentra todos los datos
3815615	Sandra Elizabeth	Aguilar de González	FEMENINO	1977-09-01	PARAGUAYO/A	0986639255	CASADO/A	\N	\N	\N	SI	NO	853	Pasilo Sin Nombre	\N	\N	\N	0	.
4332871	María Beatriz	Aguilar Mendoza	FEMENINO	1987-11-06	PARAGUAYO/A	0983811456	SOLTERO/A	\N	\N	\N	SI	NO	931	Pasillo Angel Luis	\N	\N	\N	0	.
3211594	Osvaldo Gaspar	Aguilar Mendoza	MASCULINO	\N	PARAGUAYO/A	0971853016	UNION DE HECHO	\N	\N	\N	SI	NO	1211	Virgen de Lourdes	\N	\N	\N	0	.
4647258	Antonio Ramón	Aguilar	MASCULINO	\N	PARAGUAYO/A	0971537099	UNION DE HECHO	\N	\N	\N	SI	NO	242	Virgen de Lourdes	\N	\N	\N	0	.
1346409	Juan Máximo	Aguilar	MASCULINO	2063-09-06	PARAGUAYO/A	0984874911	SOLTERO/A	\N	\N	\N	SI	NO	241	Virgen de Lourds e/Angel Luis	\N	\N	\N	0	.
1571328	Myrian Mercedes	Aguilar	FEMENINO	\N	PARAGUAYO/A	0985746574	SOLTERO/A	\N	\N	\N	SI	NO	1113	Virgen de Lourdes c/ Angel Luis	\N	\N	\N	0	.
5751941	Victor Daniel	Aguilera González	MASCULINO	\N	PARAGUAYO/A	0983146161	UNION DE HECHO	\N	\N	\N	SI	NO	130	AROMITA	\N	\N	\N	0	.
3486489	Rafaela Luz	Aguilera	MASCULINO	\N	PARAGUAYO/A	0981 591 323	UNION DE HECHO	\N	\N	\N	SI	NO	1954	Pasillo Martin Fierro	\N	\N	\N	0	La pareja se conoció hace 18 años.
3444125	Hermelinda	Aguirre	FEMENINO	1978-09-04	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	319	P/ 4 de octubre	\N	\N	\N	0	.
4603206	Griselda	Alarcón Sosa	FEMENINO	\N	PARAGUAYO/A	0982214875	.	\N	\N	\N	SI	NO	1040	Mar de plata	\N	\N	\N	0	.
314959	Mercedes	Albarenga Araña	FEMENINO	\N	PARAGUAYO/A	0961962874	SOLTERO/A	\N	\N	\N	SI	NO	734	Vecinos Unidos c/ 26 de Julio 5B	\N	\N	\N	0	En este mon¡mento nadie vive en la casa. La tía le llevo al hijo de la Sr Margarita dueña del terreno, despúes de que esta falleciera, el es especial.
1326625	Alejandro	Albarenga Mesa	MASCULINO	\N	PARAGUAYO/A	09672295260	.	\N	\N	\N	SI	NO	1526	Urcicino Velasco	\N	\N	\N	0	Es el propietario del lote
5776777	Sonia Elizabeth	Alderete Merlo	FEMENINO	\N	PARAGUAYO/A	0984913546	CASADO/A	\N	\N	\N	SI	NO	2572	Yacare Pito	\N	\N	\N	0	Placa Electrica 1.
1750714	Carlos Amancio	Alegre Caballero	MASCULINO	2069-04-11	PARAGUAYO/A	0992548937	SOLTERO/A	\N	\N	\N	SI	NO	150	Ursicino Velasco y Malvinas	\N	\N	\N	0	En este terreno hay un techo .Dentro del mismo hay dos parejas viviendo bajo el mismo techodel titular.
2121439	Estanislaa Florinda	Alegre Caballero	FEMENINO	1972-07-05	PARAGUAYO/A	0983733243	CASADO/A	\N	\N	\N	SI	NO	40	Angel Luis y la Esperanza	\N	\N	\N	0	.
1111943	Juana Bautista	Alegre Caballero	MASCULINO	\N	PARAGUAYO/A	0986882682	.	\N	\N	\N	SI	NO	531	16 de Agosto y Malvinas	\N	\N	\N	0	Tiene dos Construcciones uno de ellos es de la Cooperativa.
4058205	Julio Cesar	Alegre Caballero	MASCULINO	1985-09-01	PARAGUAYO/A	0994153350	.	\N	\N	\N	SI	NO	393	.	\N	\N	\N	0	Tiene un hijo menor que no es de su pareja actual pero hace 6 anos que vive con ellos.
4462684	Lorena Beatriz	Alegre Caballero	FEMENINO	1983-03-11	PARAGUAYO/A	0991476622	UNION DE HECHO	\N	\N	\N	SI	NO	394	Ursicino Velasco	\N	\N	\N	0	.
4657026	Lourdes Raquel	Alegre Caballero	FEMENINO	1987-02-04	PARAGUAYO/A	0994747662	SOLTERO/A	\N	\N	\N	SI	NO	587	8 de Diciembre	\N	\N	\N	0	La vivienda consta de una loza en construcción.
2041522	Mirian Graciela	Alegre Caballero	FEMENINO	\N	PARAGUAYO/A	0984783722	SOLTERO/A	\N	\N	\N	SI	NO	780	Jakare Yrupe	\N	\N	\N	0	.
3537526	Rosa María	Alegre Caballero	FEMENINO	1980-10-04	PARAGUAYO/A	0981856576	UNION DE HECHO	\N	\N	\N	SI	NO	1436	Ursicino Velasco	\N	\N	\N	0	Placa Electrica y Centrifugadora una unidad c/u.
4225572	Pablino	Alegre Encina	MASCULINO	\N	PARAGUAYO/A	0983141142	CASADO/A	\N	\N	\N	SI	NO	2077	Pasillo Sin Nombre	\N	\N	\N	0	Viv dentro del lote de la mama cuenta con vivenda propia.
5767505	Marían Natalia	Alegre	FEMENINO	1994-07-02	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	532	16 de Agosto y Malvinas	\N	\N	\N	0	Vive dentro del lote de la mama en una vivienda precaria.
4096420	Sandra Carolina	Alfonso de Ramirez	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	2695	PAYAGUA NARANJA	\N	\N	\N	0	corredor 1, el terreno posee un pasillo de 3x10
1105143	Zulmira	Alfonso de Villalba	FEMENINO	\N	PARAGUAYO/A	0982738972	SEPARADO/A	\N	\N	\N	SI	NO	1720	Ursicino Velasco	\N	\N	\N	0	Es propietaria del lote y la casa.Es una de las piezas vive Elida con sus hijos.
2920464	Gloria Isabel	Alfonso Garcia	FEMENINO	\N	PARAGUAYO/A	0984423259	UNION DE HECHO	\N	\N	\N	SI	NO	1033	Yvoty Rory c/ Mita Saraki	\N	\N	\N	0	.
1899368	Laura	Alfonso Garcia	FEMENINO	\N	PARAGUAYO/A	0985163926	UNION DE HECHO	\N	\N	\N	SI	NO	1749	.	\N	\N	\N	0	En la parte trasera de la casa viven Oliver Antonio y Pedro Amaro quienes tienen una salida independiente a la casa principal.
4271847	Andrea Fabiola	Alfonso Jara	FEMENINO	\N	PARAGUAYO/A	0981143199	SOLTERO/A	\N	\N	\N	SI	NO	1642	Pajagua Naranja	\N	\N	\N	0	Cocina a Gas 1.
4271827	Gisella Analia	Alfonso Jara	FEMENINO	1992-06-09	PARAGUAYO/A	0972541575	CASADO/A	\N	\N	\N	SI	NO	1640	Pajagua Naranja	\N	\N	\N	0	Cocina a Gas 1. Gisella Alfonso se censa como pobladora del Banado, no esta con el proyecto porque ella sigue pagando mensualmente por su casa hace un ano.La casa pertenecia a su amdre.
1847	Néstor Damián	Alfonso Recalde	MASCULINO	1994-02-01	PARAGUAYO/A	0971642654	SOLTERO/A	\N	\N	\N	SI	NO	2532	Pajagua Naranja	\N	\N	\N	0	Según manifestación de vecinos el propietario viajo al Chaco por motivos laborales desconocen mas datos de el.Vive solo.
2319490	Celina	Alfonso Vda. De García	FEMENINO	2068-12-08	PARAGUAYO/A	0982739637	SOLTERO/A	\N	\N	\N	SI	NO	2146	Agosto Poty	\N	\N	\N	0	Es propietaria del lote,dentro del lote una de sus hijos, tiene una vivienda totalmente independiente de dos piezas y un bano.
5896171	Rita Alejandra	Allende González	FEMENINO	\N	PARAGUAYO/A	0981649246	UNION DE HECHO	\N	\N	\N	SI	NO	1316	Remancito	\N	\N	\N	0	.
2641265	María Silvia	Almada Ferreira	FEMENINO	2063-03-11	PARAGUAYO/A	0986625032	UNION DE HECHO	\N	\N	\N	SI	NO	488	23 de Junio	\N	\N	\N	0	La ocupante manifiesta que cuenta con una casa ampliada precaria de 4 x4,5.
4126008	Liliana Raquel	Almada Ibanez	FEMENINO	\N	PARAGUAYO/A	0982853520	.	\N	\N	\N	SI	NO	406	Algarrobo	\N	\N	\N	0	.
943295	Alberto Daniel	Almada Melgarejo	MASCULINO	\N	PARAGUAYO/A	0983982942	UNION DE HECHO	\N	\N	\N	SI	NO	2054	Ursicino Velasco	\N	\N	\N	0	Vive dentro del lote y la casa de la mama con su pareja e hijos.
2864146	Claudia Marlene	Almada Sanabria	FEMENINO	\N	PARAGUAYO/A	.	CASADO/A	\N	\N	\N	SI	NO	1284	.	\N	\N	\N	0	La Sra. Claudia vive dentro del terreno y en la casa de la Hna. Raquel , tiene un terreno al lado de la casa de su hna. El cual no fue enumerado , actualmente tiene en su terreno materiales de construccion.
938864	Raquel Beatriz	Almada Sanabria	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1283	.	\N	\N	\N	0	.
38325579	Matias	Alonso Ariola	MASCULINO	1994-03-06	PARAGUAYO/A	0982432131	UNION DE HECHO	\N	\N	\N	SI	NO	2122	Ursicino Velasco c/ San Martin de Porres	\N	\N	\N	0	Hace 3 anos que estan en pareja y vive con la propietaria del terreno.
2001696	Miriam Gabriela	Alonso de Capdevila	FEMENINO	2069-03-12	PARAGUAYO/A	0992958537	CASADO/A	\N	\N	\N	SI	NO	421	Julio Jara	\N	\N	\N	0	La duena del terreno vive en un palafito de madera sobre una construccion de ladrillo.
6041579	Sixta Raquel	Alonso Escobar	FEMENINO	\N	PARAGUAYO/A	0985193454	CASADO/A	\N	\N	\N	SI	NO	2114	Sin Nombre	\N	\N	\N	0	.
580234	Emilio	Alonzo Cardozo	MASCULINO	\N	PARAGUAYO/A	0982558363	CASADO/A	\N	\N	\N	SI	NO	2039	Mbo'ehára	\N	\N	\N	0	La casa cuenta una de las piezas c/ hormigon y un dormitorio 4x4 donde vive la hija llamada Estela.
4777988	María Belén	Alonzo Penayo	FEMENINO	\N	PARAGUAYO/A	0972614092	SEPARADO/A	\N	\N	\N	SI	NO	2042	Mbo'ehára	\N	\N	\N	0	.
4985738	Lincy Beatriz	Alvarenga Olmedo	FEMENINO	1991-02-05	PARAGUAYO/A	0972295266	UNION DE HECHO	\N	\N	\N	SI	NO	1734	Ursicino Velasco	\N	\N	\N	0	Vive dentro del lote del papa es propietaria de la casa.
4451073	Juan Antonio	Alvarez Maciel	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	10	Recicla	\N	\N	\N	0	.
2242536	Pedro	Alvarez Rojas	MASCULINO	\N	PARAGUAYO/A	0984857650	CASADO/A	\N	\N	\N	SI	NO	2630	Ursicino Velasco y Santa Librada	\N	\N	\N	0	Marisa Barrios es conyuge de Aurelio Gonzalez.
4670016	Marlene Elizabeth	Alvarez	FEMENINO	1986-11-03	PARAGUAYO/A	4670016	UNION DE HECHO	\N	\N	\N	SI	NO	1587	Nuestra Senora de la Asunción	\N	\N	\N	0	Vive en el refugio por motivos de inudacion.
3829899	Julio Cesar	Amarilla Acosta	MASCULINO	\N	PARAGUAYO/A	0982973615	.	\N	\N	\N	SI	NO	1888	Julio Benitez	\N	\N	\N	0	El lote hace 50 anos era de la abuela materna y quedo el como titular e sus sobrinos.
2151479	María Cristina	Amarilla de Bernal	FEMENINO	\N	PARAGUAYO/A	0994769486	CASADO/A	\N	\N	\N	SI	NO	155	Ursicino Velasco c/ 8 de Diciembre	\N	\N	\N	0	La titular vende cosmeticos irregularmente.
423752	Panfilo	Amarilla de Escobar	MASCULINO	2045-01-06	PARAGUAYO/A	0981100128	CASADO/A	\N	\N	\N	SI	NO	1649	Jakare Pito	\N	\N	\N	3	.
1250687	Cecilia	Amarilla de Gamarra	FEMENINO	2067-01-02	PARAGUAYO/A	0991455575	SOLTERO/A	\N	\N	\N	SI	NO	1313	Maestro Ursicino Velasco	\N	\N	\N	0	Es propietaria del lote en el fondo del lote vive su nuera e hijo en una vivienda precaria.
1327841	Raimunda Teresa	Amarilla de López	FEMENINO	2061-06-01	PARAGUAYO/A	0985341248	.	\N	\N	\N	SI	NO	174	Julio Jara y Nuevo Amanecer	\N	\N	\N	0	.
5852807	Verónica Monserrath	Amarilla Díaz	FEMENINO	\N	PARAGUAYO/A	0992688306	UNION DE HECHO	\N	\N	\N	SI	NO	396	.	\N	\N	\N	0	Anteriormente vivia con la mama los vecinos dicen que hace 2 MECES Se mudaron.
4184059	Emilia Celeste	Amarilla Encina	FEMENINO	\N	PARAGUAYO/A	0982186498	UNION DE HECHO	\N	\N	\N	SI	NO	2345	Nuestra Sra de la Asunción	\N	\N	\N	6	.
4980820	Felicia Ramona	Amarilla Encina	FEMENINO	\N	PARAGUAYO/A	0986941280	UNION DE HECHO	\N	\N	\N	SI	NO	830	Vecinos Unidos	\N	\N	\N	0	.
6099982	Karina	Amarilla Encina	FEMENINO	1992-05-09	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	2454	Nuestra Sra. De la Asunción	\N	\N	\N	6	La titular de dedica a la venta de minutas hace 7 anos.Actualmente vive en una casa precaria en la Villa Esperanza por causa de la inundación.
1709349	Hector Bernabe	Amarilla Flores	MASCULINO	\N	PARAGUAYO/A	424730	UNION DE HECHO	\N	\N	\N	SI	NO	1953	Ursicino Valasco C/ Puente Pesoa	\N	\N	\N	0	.
2109400	Enriqueta	Amarilla López	FEMENINO	\N	PARAGUAYO/A	0971953405	UNION DE HECHO	\N	\N	\N	SI	NO	2515	Yacare Yrupe	\N	\N	\N	5	Cocina a Gas 1.
3904931	Gloria	Amarilla Sánchez	FEMENINO	1976-02-01	PARAGUAYO/A	091262074	SOLTERO/A	\N	\N	\N	SI	NO	154	Ursicino Velasco c/ 8 de Diciembre	\N	\N	\N	0	Madre soltera con3 hijos.
1240628	Sixto Ramón	Amarilla Saucedo	MASCULINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	779	.	\N	\N	\N	0	.
5273640	Alba Andrea	Amarilla Silvano	FEMENINO	1992-01-09	PARAGUAYO/A	.	SEPARADO/A	\N	\N	\N	SI	NO	2221	Sotero Colman	\N	\N	\N	0	La familia vive en extrema pobreza. Viven con la dueña de casa y usa todo lo de la casa.
368402	Alicia	Amarilla Vda. De Lenguaza	FEMENINO	\N	PARAGUAYO/A	0986695883	SOLTERO/A	\N	\N	\N	SI	NO	1082	Martin Fierro	\N	\N	\N	0	.
1646095	Cynthia Elizabeth	Amarilla	FEMENINO	\N	PARAGUAYO/A	0991677932	UNION DE HECHO	\N	\N	\N	SI	NO	1311	Remancito	\N	\N	\N	0	.
2217669	Eva Marlene	Amarilla	FEMENINO	\N	PARAGUAYO/A	0984327617	SOLTERO/A	\N	\N	\N	SI	NO	1264	Vecinos Unidos C/ Santa Ana	\N	\N	\N	0	La flia. Actualmente se encuentra viviendo fuera del barrio en un alquiler ya que su vivienda se habia derrumbado durante la crecida.Tienen un contrato de alquiler hasta Junio, piensan volver, refirio la duena del lote.En el terreno hay una vivienda prec
4260421	Gloria Beatriz	Amarilla	FEMENINO	\N	PARAGUAYO/A	0986545213	UNION DE HECHO	\N	\N	\N	SI	NO	156	Ursicino Velasco c/8 de Diciembre	\N	\N	\N	0	.
3438801	Juana Evangelina	Amarilla	MASCULINO	1977-06-05	PARAGUAYO/A	0982254526	UNION DE HECHO	\N	\N	\N	SI	NO	1601	Nuestra Senora de la Asunción	\N	\N	\N	6	.
4732484	Claudia Leticia	Amaro Alfonso	FEMENINO	1989-10-02	PARAGUAYO/A	0986449528	UNION DE HECHO	\N	\N	\N	SI	NO	2544	Mandy Jura	\N	\N	\N	0	Dentro del lote tiene un Tinglado de 6x6 donde realizan los trabajos de Herreria y Ojalateria,tambien una construcción que utiliza como deposito.
4732452	Inocencio	Amaro Alfonso	MASCULINO	1991-05-11	PARAGUAYO/A	0994703205	UNION DE HECHO	\N	\N	\N	SI	NO	2131	Agosto Poty	\N	\N	\N	0	Hace 9 anos que estan de pareja.
6719726	Joanna Lujan	Amaro Alfonso	FEMENINO	1999-08-01	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1115	Virgen de Lourdes	\N	\N	\N	0	Alquila predio del Club Atletico Boqueron.
4732453	Marciana Trinidad	Amaro Alfonso	FEMENINO	1990-09-07	PARAGUAYO/A	0985495112	UNION DE HECHO	\N	\N	\N	SI	NO	1962	.	\N	\N	\N	0	.
1214464	Erodita	Amaro de Guzmán	FEMENINO	2066-06-10	PARAGUAYO/A	0986823296	CASADO/A	\N	\N	\N	SI	NO	351	P/ 4 de Octubre	\N	\N	\N	0	.
3462300	Margarita	Amaro de Ocampo	FEMENINO	\N	PARAGUAYO/A	0983178190	CASADO/A	\N	\N	\N	SI	NO	1745	Ursicino Velasco	\N	\N	\N	0	.
3418485	Gladys Elizabeth	Amaro Rojas	FEMENINO	1979-06-06	PARAGUAYO/A	0991303262	UNION DE HECHO	\N	\N	\N	SI	NO	1409	Bañado Koeti	\N	\N	\N	5	.
1214414	Salvadora Ramona	Amaro Rojas	FEMENINO	\N	PARAGUAYO/A	0972492109	UNION DE HECHO	\N	\N	\N	SI	NO	1759	Koety	\N	\N	\N	0	.
3816077	Silvia Carolina	Amaro Rojas	FEMENINO	\N	PARAGUAYO/A	0981921224	UNION DE HECHO	\N	\N	\N	SI	NO	1696	Bañado Poty	\N	\N	\N	5	.
3723848	Genara	Amoa	FEMENINO	\N	PARAGUAYO/A	0983448347	.	\N	\N	\N	SI	NO	87	Virgen de Lujan	\N	\N	\N	0	.
4999680	Roberto Daniel	Aponte Candia	MASCULINO	1989-10-09	PARAGUAYO/A	0982215459	CASADO/A	\N	\N	\N	SI	NO	896	Martin Fierro	\N	\N	\N	0	La Sra posee una Peluqueria en la Zona 5A en la casa de la Sra. Balbina Jara. Alquila.
4427341	Bidalia	Aquino Acosta	FEMENINO	\N	PARAGUAYO/A	0984966535	UNION DE HECHO	\N	\N	\N	SI	NO	2323	Nuestra Sra. De la Asunción	\N	\N	\N	6	Cocina a Gas 1.
5883640	María Elvira	Aquino Acosta	FEMENINO	\N	PARAGUAYO/A	0984349178	UNION DE HECHO	\N	\N	\N	SI	NO	1599	Nuestra Senora de la Asunción	\N	\N	\N	0	Vive en el Refugio por motivo de la inundación.
5852096	Nilda Nelida	Aquino Acosta	FEMENINO	1992-10-01	PARAGUAYO/A	0984434677	UNION DE HECHO	\N	\N	\N	SI	NO	1597	Nuestra Senora de la Asunción	\N	\N	\N	0	.
4530625	José Asunción	Aquino Bogado	MASCULINO	1984-07-12	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	2528	26 de Julio	\N	\N	\N	0	La pareja se juntaron hace 16 anos viven con los padres.
2175136	María Teresa	Aquino de Castillo	FEMENINO	\N	PARAGUAYO/A	0992687897	.	\N	\N	\N	SI	NO	329	Mar del Plata	\N	\N	\N	0	Tiene un corredor.
1373643	Julia Ramona	Aquino de Cubilla	FEMENINO	2038-12-04	PARAGUAYO/A	0982223666	CASADO/A	\N	\N	\N	SI	NO	2087	Ursicino Velazco	\N	\N	\N	0	.
5291432	Claudia Soledad	Aquino González	FEMENINO	1991-02-12	PARAGUAYO/A	0985575321	UNION DE HECHO	\N	\N	\N	SI	NO	52	.	\N	\N	\N	3	.
4197182	Fernando	Aquino González	MASCULINO	\N	PARAGUAYO/A	0986215214	SOLTERO/A	\N	\N	\N	SI	NO	75	Angel Luis	\N	\N	\N	3	.
5378227	Nilsa Adriana	Aquino González	FEMENINO	1993-05-02	PARAGUAYO/A	0986117768	UNION DE HECHO	\N	\N	\N	SI	NO	68	RECICLA	\N	\N	\N	3	.
4799113	Regina Isabel	Aquino González	FEMENINO	1984-07-09	PARAGUAYO/A	0984423847	SOLTERO/A	\N	\N	\N	SI	NO	867	Ursicino Velasco	\N	\N	\N	0	.
3942738	Eugenia Epifania	Aquino Mascareno	FEMENINO	\N	PARAGUAYO/A	0991764912	SOLTERO/A	\N	\N	\N	SI	NO	1348	Primavera	\N	\N	\N	0	Es propietaria del lote y la vivienda ,Jorge y Santiago cuentan con casitas precarias dentro del lote de la mama, una de las casitas tiene 234x280 la otra es de 3,20x4,10.
2379103	Derlis Raúl	Aquino Melgarejo	MASCULINO	\N	PARAGUAYO/A	0983127380	CASADO/A	\N	\N	\N	SI	NO	2073	Chiquero	\N	\N	\N	0	.
4110873	Diana Leticia	Aquino Méndez	FEMENINO	\N	PARAGUAYO/A	0991240227	SOLTERO/A	\N	\N	\N	SI	NO	1015	Mto Ursicino Velasco	\N	\N	\N	0	Cuenta con un salon en el frente p/ merceria de hormigon (5x3mts2).
620155	Felipa	Aquino Paiva	FEMENINO	\N	PARAGUAYO/A	0986744069	.	\N	\N	\N	SI	NO	78	Angel Luis	\N	\N	\N	0	.
4926034	Maria Basilia	Aquino Velazquez	FEMENINO	\N	PARAGUAYO/A	0971664080	SOLTERO/A	\N	\N	\N	SI	NO	2722	Mandy jura	\N	\N	\N	0	.
1972714	Nicolás	Aquino Zorrila	MASCULINO	2057-06-12	PARAGUAYO/A	0985427298	UNION DE HECHO	\N	\N	\N	SI	NO	74	Angel Luis	\N	\N	\N	3	.
6611959	Noelia Mabel	Aquino	FEMENINO	\N	PARAGUAYO/A	0991764912	UNION DE HECHO	\N	\N	\N	SI	NO	1350	Primavera	\N	\N	\N	0	Vive dentro del lote de la mama en una pieza salio hace una semana del hospital en este momento se encuentra discapacitado quedo con varias secuelas del accidente.
2339888	Alberta Ramona	Aranda Cuevas	FEMENINO	\N	PARAGUAYO/A	0981286313	SOLTERO/A	\N	\N	\N	SI	NO	788	Virgen de Lujan	\N	\N	\N	0	.
2507171	Librada	Arguello	FEMENINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	2130	Agosto Poty	\N	\N	\N	0	Secarropa 1.
2447520	Teodora	Aranda de Moray	FEMENINO	\N	PARAGUAYO/A	0991905823	.	\N	\N	\N	SI	NO	467	Martin Fierro	\N	\N	\N	0	Teodora es la propietaria del lote vive con ella 1 hijo soltero en una extension de su vivienda.
1308184	Francisca	Aranda Domínguez	FEMENINO	\N	PARAGUAYO/A	.	SEPARADO/A	\N	\N	\N	SI	NO	2241	Urcisino Velazco c/ Sotero Colman	\N	\N	\N	0	.
3664805	Juan Marcelino	Aranda Franco	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	834	.	\N	\N	\N	0	.
5024845	Andrea Victoria	Aranda Insaurralde	FEMENINO	1990-05-06	PARAGUAYO/A	0984169688	UNION DE HECHO	\N	\N	\N	SI	NO	2640	Martin Fierro	\N	\N	\N	0	Según la Cunada en este momento la chica vive en alquiler luego volvera a su casa.
4627292	Javier	Aranda Insaurralde	MASCULINO	\N	PARAGUAYO/A	0986134515	.	\N	\N	\N	SI	NO	2155	Virgen de Guadalupe	\N	\N	\N	0	Vive dentro del lote de la hermana uuna pieza.
1108929	Ángela	Aranda	FEMENINO	\N	PARAGUAYO/A	0971593346	VIUDO/A	\N	\N	\N	SI	NO	2118	Ursicino Velazco	\N	\N	\N	0	.
1168893	Candida	Aranda	FEMENINO	2051-07-06	PARAGUAYO/A	0982967100	SOLTERO/A	\N	\N	\N	SI	NO	1491	Mainumby	\N	\N	\N	0	.
9999901	Joséfa	Aranda	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	1464	Pasillo sin nombre	\N	\N	\N	0	vivia con la madre, altora falleció.Tiene problemas de aprendizaje,(analfabeta)
899593	Jorgelina	Araujo Sosa	MASCULINO	\N	PARAGUAYO/A	09838779520	UNION DE HECHO	\N	\N	\N	SI	NO	1418	Ursicino Velasco	\N	\N	\N	0	La titular cuenta con dos viviendas dentro del lote .Una casa de material(6x4) y un palafito(6x5) .
2104425	Yolanda	Araujo Vda. De Méndez	FEMENINO	\N	PARAGUAYO/A	0981878734	VIUDO/A	\N	\N	\N	SI	NO	1986	Nasaindy	\N	\N	\N	5	Es propietario del lote y de la casa con ella vive uno de sus hijos con sus nietos.
3307444	Alejandro David	Arce Chamorro	MASCULINO	1987-10-03	PARAGUAYO/A	0971872780	.	\N	\N	\N	SI	NO	248	San Felipe y Santiago	\N	\N	\N	0	Deposito de Discoteca.
1890187	Bacilia	Arce de Sánchez	FEMENINO	\N	PARAGUAYO/A	0984925660	SEPARADO/A	\N	\N	\N	SI	NO	605	Ursicino Velazco y San Cayetano	\N	\N	\N	5	L a titular cuenta con una casilla (venta de comestibles). Es abuela y tía de los mencionados que viven dentro de su techo
3661725	Sonia Elizabeth	Arce Gill	FEMENINO	1980-12-06	PARAGUAYO/A	0982331339	SEPARADO/A	\N	\N	\N	SI	NO	2097	.	\N	\N	\N	0	En este momento no se encuentra en el lugar ,vive en la casa de la madre.
1083600	Mario Tomás	Arce Ojeda	MASCULINO	2061-03-09	PARAGUAYO/A	0985924072	SOLTERO/A	\N	\N	\N	SI	NO	963	Nuevo Amanecer	\N	\N	\N	0	Los DatosComparte con el Lote 14-A1.
4840062	Ada Faviola	Arce	FEMENINO	\N	PARAGUAYO/A	0992306110	UNION DE HECHO	\N	\N	\N	SI	NO	447	San Cayetano	\N	\N	\N	0	.
575155	Candido	Arce	MASCULINO	2048-11-03	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	2028	Chiquero	\N	\N	\N	5	La pareja posee otros lotes que estan ennumerados 031 24,50x7,8 y otro de 12,20 uno de ellos utiliza para su trabajo y el otro es establo de sus vacas.
4839983	Fabio Javier	Arce	MASCULINO	\N	PARAGUAYO/A	0971775197	UNION DE HECHO	\N	\N	\N	SI	NO	861	Ursicino Velasco	\N	\N	\N	0	La pareja lleva tres añops de concubinato , tienen dos hijos menores. Tienen un techo precario indepndiente al techo de la titular.
1220447	María de Jesús	Arce	FEMENINO	2058-04-01	PARAGUAYO/A	0984167033	UNION DE HECHO	\N	\N	\N	SI	NO	1981	Nasaindy	\N	\N	\N	5	.
4539202	Victor Hugo	Arce	MASCULINO	\N	PARAGUAYO/A	0983972571	SOLTERO/A	\N	\N	\N	SI	NO	449	San Cayetano	\N	\N	\N	0	Anteriormente estuvo aconcubinado con la mamá de u hijo por 16 años, hace 8 años que vive separado, con su hijo en su lote y techo propio
1071478	Arenera Bañado Sur -Juan Rotela	.	.	\N	PARAGUAYO/A	0981153833	.	\N	\N	\N	SI	NO	2506	23 de Junio	\N	\N	\N	0	El mismo cuenta con licencia ammbiental, licencia hidrografica, y gestion de permiso de ocupaciòn.
24016	Arenera Bañado Sur -Juan Rotela	.	.	\N	PARAGUAYO/A	0981153853	.	\N	\N	\N	SI	NO	2512	23 de Junio	\N	\N	\N	0	La vivienda cuentaa con tres piletas para tilapia con sistema de desague de 10m por 30 m de dimenciòn, cuenta con licencia ambiental y gestion de permiso de ocupacion.
2918	Arenera Tres Hermanos	.	.	\N	.	0981153853	.	\N	\N	\N	SI	NO	472	23 de Junio	\N	\N	\N	0	La Arenera Tres hermanos funcionando hace 3 anos cuenta con 6 personales dentro del lote hay 6 personales y 6 construcciones es 3 construida se utiliza para los encargados y nales 2 para taller y un deposito los funcionarios que se realizan son: Amarrade
4868638	Lilian	Arevalo Varela	FEMENINO	\N	PARAGUAYO/A	0981989638	UNION DE HECHO	\N	\N	\N	SI	NO	1129	Virgen de Lujan c/ Virgen de Lourdes	\N	\N	\N	0	.
4626143	José Alberto	Arevalos Ayala	MASCULINO	\N	PARAGUAYO/A	0982328638	CASADO/A	\N	\N	\N	SI	NO	1522	Urcicino Velasco	\N	\N	\N	0	vive en el terreno de la propietaria en una construccion de 12x10, bien construida
4160150	Lurdes Concepción	Arévalos Ayala	FEMENINO	\N	PARAGUAYO/A	0972756229	UNION DE HECHO	\N	\N	\N	SI	NO	1519	Urcicino Velasco	\N	\N	\N	0	.
3562550	Gloria Estela	Arévalos de Bazán	FEMENINO	\N	PARAGUAYO/A	0985419862	CASADO/A	\N	\N	\N	SI	NO	2053	Mbo'ehara	\N	\N	\N	0	No se puede sacar foto del cotado de la casa, linda con otro vecino. La casa tiene una construcción incinclusa con un techo de cgapa zinc
1632570	Pabla Emiliana	Arévalos de Coronel	FEMENINO	\N	PARAGUAYO/A	0992 387 519	CASADO/A	\N	\N	\N	SI	NO	2526	Martin Fierro	\N	\N	\N	5	La casa es antigua (aproximadamente 30 años de construcción).
2167467	Olinda	Arevalos Larrea	FEMENINO	2033-02-10	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	1385	.	\N	\N	\N	0	.
3226551	Cecilia Rocío	Arevalos	FEMENINO	1978-01-01	PARAGUAYO/A	0991900273	UNION DE HECHO	\N	\N	\N	SI	NO	869	Martin Fierro	\N	\N	\N	0	La hija vive en una pieza 4x4 de material en el patio de la mama.
5215975	Leticia Mabel	Arguello Arguello	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1672	Pasillo Sin Nombre	\N	\N	\N	0	La pareja vive en una extrema pobreza.
4640781	Yaninna Romina	Arguello de Ayala	FEMENINO	\N	PARAGUAYO/A	0991 829 134	CASADO/A	\N	\N	\N	SI	NO	1836	Ursicino Velazco con Virgen de Guadalupe	\N	\N	\N	0	La propietaria del terreno manifiesta que esta vivienda en el terreno de su madre porque su construcción se derrumbó a causa de la inundación.
1004403	Joséfa	Arguello de Osorio	MASCULINO	\N	PARAGUAYO/A	0984 848 866	CASADO/A	\N	\N	\N	SI	NO	2684	Ursicino Velazco	\N	\N	\N	0	La propietaria cuenta con un comedor para niños/as
6100693	María Angelica	Arguello Insfran	FEMENINO	1987-09-07	PARAGUAYO/A	0985167484	UNION DE HECHO	\N	\N	\N	SI	NO	1017	Mar de Plata	\N	\N	\N	4	.
5774324	María Luisa	Arguello Insfrán	FEMENINO	\N	PARAGUAYO/A	0992653544	UNION DE HECHO	\N	\N	\N	SI	NO	1023	Mar de plata	\N	\N	\N	0	Es una casa muy precaria.
4511734	Cinthia	Arguello López	FEMENINO	\N	PARAGUAYO/A	0994637935	UNION DE HECHO	\N	\N	\N	SI	NO	1462	tape pyahu	\N	\N	\N	0	.
5026474	Rodrigo Pastor	Arguello López	MASCULINO	\N	PARAGUAYO/A	0983 805 900	CASADO/A	\N	\N	\N	SI	NO	1833	Ursicino Velazco con Virgen de Guadalupe	\N	\N	\N	0	vive en el mismo terreno de la propietaria.
4511733	Shirley Nair	Arguello López	FEMENINO	\N	PARAGUAYO/A	0971 728 271	UNION DE HECHO	\N	\N	\N	SI	NO	1830	Ursicino Velazco con Virgen de Guadalupe	\N	\N	\N	0	Viven en el terreno de la propietaria
1195254	Eustaquia	Arguello	FEMENINO	\N	PARAGUAYO/A	0994637935	.	\N	\N	\N	SI	NO	1460	Virgen de guadalupe	\N	\N	\N	0	La señora cobra el sueldo de 3º Edad,
6049580	Ilce	Arguello	FEMENINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	2643	Ramon Talavera	\N	\N	\N	0	.
1432770	María Dolores	Arguello	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1580	Ramon Talavera	\N	\N	\N	6	.
6908400	Sara Yanina	Arguello	FEMENINO	\N	PARAGUAYO/A	0992304182	SOLTERO/A	\N	\N	\N	SI	NO	1579	Ramon Talavera	\N	\N	\N	6	.
423227	José Simeón	Arias Bogado	MASCULINO	\N	PARAGUAYO/A	0986641401	SOLTERO/A	\N	\N	\N	SI	NO	2096	Nasaindy	\N	\N	\N	0	.
2137091	María Basilia	Armoa de Cáceres	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	84	Virgen de Lujan y V. de Lourdes	\N	\N	\N	0	.
2444015	Diseria	Armoa de Dure	FEMENINO	1978-12-11	PARAGUAYO/A	0985535221	CASADO/A	\N	\N	\N	SI	NO	1141	Virgen de Lujan c/ Los sauces	\N	\N	\N	0	.
3173595	Alicia Mabel	Armoa Núñez	FEMENINO	1976-04-02	PARAGUAYO/A	0991214222	CASADO/A	\N	\N	\N	SI	NO	873	Pasillo Sin NOMBRE	\N	\N	\N	0	En al casa no viven en este momento.
3822426	Carina Doralice	Armoa Núñez	FEMENINO	\N	PARAGUAYO/A	.	CASADO/A	\N	\N	\N	SI	NO	128	AROMITA	\N	\N	\N	0	.
3341070	Liz Carolina	Armoa Núñez	FEMENINO	\N	PARAGUAYO/A	0981547378	UNION DE HECHO	\N	\N	\N	SI	NO	39	Virgen de Lourdes	\N	\N	\N	0	.
4739032	Mirta Elizabeth	Armoa Núñez	FEMENINO	\N	PARAGUAYO/A	0984354902	UNION DE HECHO	\N	\N	\N	SI	NO	663	Yvoty	\N	\N	\N	0	.
3173693	Petrona Elizabet	Armoa Núñez	FEMENINO	\N	PARAGUAYO/A	0991477432	CASADO/A	\N	\N	\N	SI	NO	1133	Virgen de Lujan c/ Los Sauces	\N	\N	\N	0	.
3173517	Rafael	Armoa Núñez	MASCULINO	\N	PARAGUAYO/A	0985694839	UNION DE HECHO	\N	\N	\N	SI	NO	607	Ursicino Velzaco y San Cayetano	\N	\N	\N	0	La pareja tiene 7 años de concubinato
2686443	Victor Daniel	Armoa Núñez	MASCULINO	\N	PARAGUAYO/A	0981902469	UNION DE HECHO	\N	\N	\N	SI	NO	626	Angel Luis	\N	\N	\N	0	Tiene una Sala Comedor adentro.
4739019	Wencelaa	Armoa Núñez	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	38	San Felipe y Santiago	\N	\N	\N	0	.
6065875	Blanca Graciela	Armoa Ramires	FEMENINO	\N	PARAGUAYO/A	0991222987	SOLTERO/A	\N	\N	\N	SI	NO	1791	Bañado Koeti	\N	\N	\N	5	Es propietaria del lote actualmente vive en la casa de un cunado porque su casa fue derrumbada.
4581433	Liz Paola	Armoa Zayas	FEMENINO	\N	PARAGUAYO/A	0991183427	UNION DE HECHO	\N	\N	\N	SI	NO	357	Virgen de Lujan	\N	\N	\N	0	Vive con la mama.
4029904	Alberto	Armoa	MASCULINO	1977-11-04	PARAGUAYO/A	0982182888	UNION DE HECHO	\N	\N	\N	SI	NO	642	Angel Luis	\N	\N	\N	0	Tiene pareja e hijos.
1577875	Eladio Ramón	Armoa	MASCULINO	\N	.	.	SOLTERO/A	\N	\N	\N	SI	NO	920	Angel Luis	\N	\N	\N	0	Eladio Armoa es dueño del terreno , actualmente esta alquilando la casa a ala Flia. Ramirez de Alvarez hace 5 años.
4780435	Liz Natalia	Armoa	FEMENINO	\N	PARAGUAYO/A	0991838368	UNION DE HECHO	\N	\N	\N	SI	NO	875	.	\N	\N	\N	0	En la casa no viven en este momento.
3257532	Alexis Ramón	Arriola Martínez	MASCULINO	1990-11-12	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	17	San Felipe y Santiago	\N	\N	\N	0	Dispone un patio en el fondo su terreno
3257531	María Cistina	Arriola Martínez	FEMENINO	\N	PARAGUAYO/A	096158338485	CASADO/A	\N	\N	\N	SI	NO	349	Virgen de Lujan c/ Los Sauces	\N	\N	\N	0	.
2104439	María Tomasa	Arrua Cazal	FEMENINO	\N	PARAGUAYO/A	00972214198	SOLTERO/A	\N	\N	\N	SI	NO	172	Julio Jara	\N	\N	\N	0	.
2669892	Miriam esther	Arrua de Maciel	FEMENINO	1997-05-01	PARAGUAYO/A	0981153041	CASADO/A	\N	\N	\N	SI	NO	700	Julio Jara	\N	\N	\N	0	.
2493028	Mirna Raquel	Arrúa Resquín	FEMENINO	1977-10-04	PARAGUAYO/A	0984530624	VIUDO/A	\N	\N	\N	SI	NO	2003	Ñasaindy	\N	\N	\N	5	Actualmente vive en la casa de la hija con sus 4 hijos.
2190301	Digna Beatriz	Arzamendia de Quiñorez	FEMENINO	\N	PARAGUAYO/A	0981909321	.	\N	\N	\N	SI	NO	311	Virgen de Lourdes Y Mar del Plata	\N	\N	\N	0	La censada tiene una Construccion de 3x6 en el lote de su madre.
2010	Asociacion de  Mujeres Coescucha	.	.	\N	.	0985114835	.	\N	\N	\N	SI	NO	2389	Maestro Ursicino Velasco	\N	\N	\N	0	.
1240625	Asociacion de Carreros	.	.	2053-07-01	PARAGUAYO/A	0972546871	.	\N	\N	\N	SI	NO	2400	Ursicino Velasco casi Martin Fierro	\N	\N	\N	0	fue fundada el 6 de julio de 1995 cuenta con ocho asociados y la casa se utiliza para reuniones de los soscios, anteriormente el terreno era mas amplio pero se fracciono una parte del  terreno en pequeños, los cuales fueron distribuidos  a algunos socios
20	Asociación de Espineleros	.	.	\N	.	0981543041	.	\N	\N	\N	SI	NO	126	Nuevo Amanecer	\N	\N	\N	0	La asociación de Espineleros Mar de Plata, no requiere RUC. Fundada el 12 de Mayo de 2002, se dedican a la captura de pescados y venta de los mismos.
3030	Asociacion de Espineleros Marzal Cue	.	.	\N	.	.	.	\N	\N	\N	SI	NO	460	.	\N	\N	\N	0	Vivienda se utiliza para reuniones de Espineleros no cuenta con encargado.
468	Asociación de Oleros Unidos	.	.	\N	.	0983452376	.	\N	\N	\N	SI	NO	2471	Yvoty e/ Mar del Plata	\N	\N	\N	0	Asociación de Oleros Unidos Banado Tacúmbu
4954	Asociación de Pescadores Unidos B. T.	.	.	\N	.	0994152808	.	\N	\N	\N	SI	NO	188	Mto. Ursicino Velasco	\N	\N	\N	0	tiene 1 carrito p/ transporte de pescado, 1 Sierra Electrica , 1 Balanza Electrica , Utensilios del Comedor, 2 Estantes Metalicos , 1 Cocina Industrial, 1 tanque con motor electrictricos. la Asociacion funciona desdeel 26-Julio-83 , cuenta con 66 asociad
1663920	Astilleria El Norteno	.	.	\N	.	0972502028	.	\N	\N	\N	SI	NO	996	8 de Diciembre	\N	\N	\N	0	El Sr Raul es propietario independiente de la Astilleria trabajan en la misma los hijos mayores, no tiene asociados.
620	Astillero AGUAPE - Lineas Panchita G S.A.	.	.	\N	.	021421343	.	\N	\N	\N	SI	NO	2736	Ursicino Velasco	\N	\N	\N	0	R.U.C.:80001768-4.
80001768	Astillero AGUAPE - Lineas Panchita G S.A.	.	.	\N	.	021421343	.	\N	\N	\N	SI	NO	2418	Ursicino Velasco	\N	\N	\N	0	.
80065699	Astillero AVISMAR S.A.	.	.	\N	.	0971238212	.	\N	\N	\N	SI	NO	1104	23 de Junio	\N	\N	\N	0	Empresario de los haceres humanos de la categoria baja cuenta con 3 construcciones , 1 para deposito otro para pieza mas deposito , una oficina mas vivienda , puesto de operaciones AVISMAR en el lote hay una limpiadora , 1 encargado de los operarios de l
1234123	Astillero Benítez	.	.	\N	.	0994390192	.	\N	\N	\N	SI	NO	2531	Ursicino Velasco c/ Yvoty Rory	\N	\N	\N	0	La Astilleria no tiene Tinglado esta en proceso de colocación ,trabaja al aire libre.Cuenta con los documentos legalesde la Municipalidad (permiso de ocupación),la SEAM Y solicitud Naval como Astillero, cuenta con un propio Transformador de la ANDE.
1230151	Astillero El Tuku	.	.	\N	.	0981945825	.	\N	\N	\N	SI	NO	494	23 de Junio	\N	\N	\N	0	R.U.C.:1230151-5.
80005361	Astillero Litoral S.R.L	.	.	\N	.	421 729	.	\N	\N	\N	SI	NO	2296	Urcisino Velazco	\N	\N	\N	0	Dentro del lote hay dos tinglados y dos construcciones. Una de las construcciones es utilizada para vivienda y la otra se encuentra en reconstrucción. Los tinglados son utilizados para guardar todos los vehiculos, tractores, oxigenos e insumos en general
1602230	Astillero Mar del Plata	.	.	\N	.	0981280191	.	\N	\N	\N	SI	NO	2717	Mar del Plata c/ Nuevo Amanecer	\N	\N	\N	0	El Astillero Mar del Plata funciona desde el año 2010, realizan reparación de todo tipo de embarcaciones, pero actulamente no se encuentra en funcionamiento , cuenta con una edificacion de 200 mts2. que funciona como Administracion, deposito de herramien
523	Astillero Postiguet	.	.	\N	.	.	.	\N	\N	\N	SI	NO	2718	El dueño del Astiilero es Juan que falleció hace u	\N	\N	\N	0	.
80083426	Astillero RB S.R.L	.	.	\N	.	.	.	\N	\N	\N	SI	NO	2686	Ursicino Velazco	\N	\N	\N	0	Ambos son propietarios del astillero y representantes legales. Cuenta con un personal de diez personas.
80067561	Astillero Riveiro S.A	.	.	\N	.	0971198890	.	\N	\N	\N	SI	NO	2384	Maestro Uricino Velasco	\N	\N	\N	0	.
664697	Astillero YPORA	.	.	\N	.	021420048	.	\N	\N	\N	SI	NO	1105	San Felipe y Santiago	\N	\N	\N	0	El Astillero YPORA cuenta con 2 construcciones , 1 tnglado, 1 para oficina. Tiene 40 personales ,el represante legal es el senor Hector Asereto ,las acatividades que se realizan son:
4345093	Cinthia Noemi	Avalos Arguello	FEMENINO	\N	PARAGUAYO/A	0982680404	CASADO/A	\N	\N	\N	SI	NO	1820	Rancho 8	\N	\N	\N	0	la pareja lleva 13 años de casados, tienen dos hijas menores, tienen lote y techho propio
1292645	Dionisia	Avalos Arguello	FEMENINO	\N	PARAGUAYO/A	0971664080	UNION DE HECHO	\N	\N	\N	SI	NO	1395	Payagua Naranja	\N	\N	\N	0	Tiene Comedor.
2023	Eugenia	Avalos Arguello	FEMENINO	\N	PARAGUAYO/A	0972754903	.	\N	\N	\N	SI	NO	1289	Mto. Ursicino Velasco	\N	\N	\N	5	En el lugar donde laSra. Eugenia tiene un copetin donde venda comestibles (Comida rapida).La Sra. Tiene un terreno en la Zona 5-A dentro del barrio.
3579026	Eugenia	Ávalos Arguello	FEMENINO	\N	PARAGUAYO/A	0982745714	UNION DE HECHO	\N	\N	\N	SI	NO	691	Yacare Yrupe	\N	\N	\N	0	La Sra. Cuenta con un copetin ,Manzana 20, Lote 23.Dimension del terreno 12x25, Dimension de la vivienda 5x4, hace un mes esta abierto
1574112	Felipe Santiago	Avalos Arguello	MASCULINO	2069-01-05	PARAGUAYO/A	0994144398	UNION DE HECHO	\N	\N	\N	SI	NO	1416	Ursicino Velasco	\N	\N	\N	0	.
1875509	Higinia	Avalos Arguello	FEMENINO	1973-11-01	PARAGUAYO/A	0983581870	SOLTERO/A	\N	\N	\N	SI	NO	1410	Yvoty Rory c/ Mita Saraki	\N	\N	\N	0	.
1186711	Cirila	Avalos de Espinola	FEMENINO	2061-05-07	PARAGUAYO/A	0971 790 171	CASADO/A	\N	\N	\N	SI	NO	1717	Yacare Pito	\N	\N	\N	3	La familia vivia en San Pedro, Tacuarí
3641348	Luz Margarita	Ávalos de Rojas	FEMENINO	1980-10-06	PARAGUAYO/A	0981 233 621	CASADO/A	\N	\N	\N	SI	NO	2666	Ursicino Velazco	\N	\N	\N	0	.
5333159	Lidia Rosa	Avalos Suarez	FEMENINO	\N	PARAGUAYO/A	0991325208	UNION DE HECHO	\N	\N	\N	SI	NO	1472	Ursicino Velasco	\N	\N	\N	0	Centrifugadora:1.La lleva 12 anos de concubinato con tre hijos menores y un embarazo de 8 meses.Tiene lote y techo propio.El Sr. Juan Ramon Tiene un terreno en la Zona 4 M33/Lote 39.El mismo es una Herencia de su padre quien ya fallecio hace dos meses.El
6500110	Vilma Ramona	Avalos Suarez	FEMENINO	\N	PARAGUAYO/A	0972746622	UNION DE HECHO	\N	\N	\N	SI	NO	1036	Yvoty Rory E/ Mita Saraki	\N	\N	\N	0	Viven dentro del mismo lote y vivienda de la madre.
4370119	Blanca Liliana	Ávalos Vera	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1246	Pajagua Naranja	\N	\N	\N	0	No quiere que se tome foto de la vivienda
6111255	Leticia Soledad	Avalos Vera	FEMENINO	\N	PARAGUAYO/A	0991412743	CASADO/A	\N	\N	\N	SI	NO	737	Mto. Ursicino Velasco	\N	\N	\N	0	.
5702524	Liz Carolina	Avalos Vera	FEMENINO	\N	PARAGUAYO/A	0984893835	UNION DE HECHO	\N	\N	\N	SI	NO	2723	Mto. Ursicino Velasco	\N	\N	\N	0	llevan de 3 años de cocubinato.
4972929	Mirian Raquel	Avalos Vera	FEMENINO	\N	PARAGUAYO/A	0982554226	SOLTERO/A	\N	\N	\N	SI	NO	736	Mto. Ursicino Velasco	\N	\N	\N	0	La vivienda cuenta con un Galpon.
3678045	Pablo Felix	Ávalos Vera	MASCULINO	\N	PARAGUAYO/A	0992345362	SEPARADO/A	\N	\N	\N	SI	NO	2638	Martin Fierro	\N	\N	\N	5	La vivienda cuenta con un galpon en el frente(20x10).
4972914	Rocío Raquel	Avalos Vera	FEMENINO	\N	PARAGUAYO/A	0991221771	UNION DE HECHO	\N	\N	\N	SI	NO	692	Yacare yrupe	\N	\N	\N	5	.
622019	Domingo	Avalos	MASCULINO	2033-12-07	PARAGUAYO/A	0982949379	CASADO/A	\N	\N	\N	SI	NO	1414	Ursicino Velasco	\N	\N	\N	0	El titular y su esposa cobran Tercera Edad.
5907733	María Mercedes	Avalos	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1398	.	\N	\N	\N	0	.
2267586	Mirta Isabel	Ávalos	FEMENINO	1976-09-10	PARAGUAYO/A	0981375593	SOLTERO/A	\N	\N	\N	SI	NO	2151	San Martin de Porres	\N	\N	\N	0	.
6254694	Noemi Soledad	Ávalos	FEMENINO	\N	PARAGUAYO/A	0981375572	UNION DE HECHO	\N	\N	\N	SI	NO	2152	San Martin de Porres	\N	\N	\N	0	.
826041	Victorina Delia	Avalos	MASCULINO	\N	PARAGUAYO/A	0981407913	UNION DE HECHO	\N	\N	\N	SI	NO	689	Yacare yrupe	\N	\N	\N	5	.
2158006	Labriana	Ayala de Domínguez	FEMENINO	2055-06-01	PARAGUAYO/A	0982813578	CASADO/A	\N	\N	\N	SI	NO	1546	Pajagua Naranja	\N	\N	\N	0	.
3419851	Carmen Noelia	Ayala de Ferreira	FEMENINO	\N	PARAGUAYO/A	0982561475	CASADO/A	\N	\N	\N	SI	NO	1811	Minumby y Rancho 8	\N	\N	\N	0	.
929150	Julia Eva	Ayala de Garcia	FEMENINO	2064-03-01	PARAGUAYO/A	0981107399	CASADO/A	\N	\N	\N	SI	NO	1558	Ursicino Velasco	\N	\N	\N	0	Hace 18 anos viven en una casa prestada sober Diaz de Solis.No habita actualmente en su casa por motivos laborales.
3036726	Isabel	Ayala Duarte	FEMENINO	1980-10-04	PARAGUAYO/A	0981375755	UNION DE HECHO	\N	\N	\N	SI	NO	1493	Payagua naranja	\N	\N	\N	0	es la unica forma de quitar la foto(imagen).
2401087	Julián	Ayala Franco	MASCULINO	2065-07-04	PARAGUAYO/A	0993255309	CASADO/A	\N	\N	\N	SI	NO	2	San Felipe y Santiago	\N	\N	\N	0	El Sr. En este momento vive en alquiler se mudo del agua y no regresa porque tiene que cumplir contrato
1127401	Anastacia	Ayala Martínez	FEMENINO	2066-01-06	PARAGUAYO/A	0982629448	UNION DE HECHO	\N	\N	\N	SI	NO	1495	Pajagua Naranja	\N	\N	\N	0	Tiene una construccion para su venta de copetin.
2160690	Aniano	Ayala Martínez	MASCULINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1517	Rancho 8	\N	\N	\N	0	.
5869956	Karina Margarita	Ayala Martínez	FEMENINO	1979-01-09	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1393	Payagua Naranja	\N	\N	\N	0	.
6143757	Daniela	Ayala Vda. De Toledo	MASCULINO	2025-07-09	PARAGUAYO/A	0984323268	VIUDO/A	\N	\N	\N	SI	NO	1442	Ursicino Velasco	\N	\N	\N	0	Cocina a Induccion 1. La titular hace 72 anos que vive en el banado.Actualmente vive con una de sus hijas mayores.
4683812	Liz Adriana	Ayala Villanueva	FEMENINO	1991-12-03	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	55	San Felipe y Santiago	\N	\N	\N	0	.
5352469	Joel Gabriel	Ayala Zuchini	MASCULINO	\N	PARAGUAYO/A	0983868547	SOLTERO/A	\N	\N	\N	SI	NO	1455	Ursicino Velasco	\N	\N	\N	0	Hace un ano esta separados de su pareja , tuvieron una relacion de tres anos,la menor vive con el padre.
5000076	Dominga Belén	Ayala	FEMENINO	1992-04-08	PARAGUAYO/A	0972683293	UNION DE HECHO	\N	\N	\N	SI	NO	1241	Pajagua Naranja	\N	\N	\N	0	.
3496852	Elisa	Ayala	FEMENINO	2060-02-11	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	1248	.	\N	\N	\N	0	.
6002	Epifania	Ayala	FEMENINO	\N	No quiso censar	.	.	\N	\N	\N	SI	NO	2292	.	\N	\N	\N	0	No quiso censarse.
1268944	Fatima Norma	Ayala	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	1880	.	\N	\N	\N	0	.
1888860	Ignacio Ramón	Ayala	MASCULINO	\N	PARAGUAYO/A	0983444149	SOLTERO/A	\N	\N	\N	SI	NO	1444	Ursicino Velasco	\N	\N	\N	0	Placa Electrica 1.El titular del techo mantuvo una relacion de concubinato con la madre de sus hijos por 30 anos , tienen 6 hijos juntos ,5 de ellos siguen viviendo con el padre.
4238244	Leandro	Ayala	MASCULINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	240	Angel Luis	\N	\N	\N	0	.
2467466	Liz Paola	Ayala	FEMENINO	1983-03-10	PARAGUAYO/A	0986608628	UNION DE HECHO	\N	\N	\N	SI	NO	158	Algarrobo	\N	\N	\N	0	.
2118272	María Carolina	Ayala	FEMENINO	\N	PARAGUAYO/A	0986157874	SOLTERO/A	\N	\N	\N	SI	NO	738	Mto. Ursicino Velasco	\N	\N	\N	0	.
4169301	Marina	Ayala	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1402	Pajagua Naranja	\N	\N	\N	0	En el fondo de la casa tiene una casa construida que mider 6x8 y arriba tiene una pieza del mismo tamano .
1832288	Mirta Elizabeth	Ayala	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1191	Fidelina	\N	\N	\N	0	.
1716274	Florencio	Azuaga Jiménez	MASCULINO	1971-11-12	PARAGUAYO/A	0986403768	UNION DE HECHO	\N	\N	\N	SI	NO	771	jakare Yrupe e/Pilcomayo	\N	\N	\N	0	En el salon es donde tienen todas sus comodidades.
1910688	Vicenta Agustína	Báez Cordobés	FEMENINO	\N	PARAGUAYO/A	0971561542	SOLTERO/A	\N	\N	\N	SI	NO	668	23 de Junio	\N	\N	\N	0	Dentro del lote de la titular vive una pareja de jóvenes sin hijos con otro techo. La titular es soltera
1550750	Ramón Guillermo	Báez Cordovez	MASCULINO	2068-10-02	PARAGUAYO/A	096130698	CASADO/A	\N	\N	\N	SI	NO	664	23 de Junio	\N	\N	\N	0	La pareja lleva 16 años de concubinato y dos años de casados
1099389	Epifania Zulma	Báez de Ramírez	FEMENINO	2063-12-07	PARAGUAYO/A	0982273892	CASADO/A	\N	\N	\N	SI	NO	1845	Maria Auxiliadora	\N	\N	\N	0	Tiene una Lavanderia.
5813234	Gilda Patricia	Báez Ramírez	FEMENINO	1990-05-06	PARAGUAYO/A	0982226746	UNION DE HECHO	\N	\N	\N	SI	NO	1799	Bañado Koeti	\N	\N	\N	0	Uno de los miembros se encuentra privado de su libertad.
2535006	Eligio	Báez Zalalla	MASCULINO	1975-03-09	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	1453	.	\N	\N	\N	0	.
5462299	Gladys Elizabeth	Báez	FEMENINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	545	Mtro Urcisino Velazco	\N	\N	\N	0	El papá vive con la hija y dentro de la misma casa, anteriormente contaba con una ampliación de 5x4 el cual se derrumbó de manera parcial
2290204	Ignacia	Báez	FEMENINO	\N	PARAGUAYO/A	0971755038	UNION DE HECHO	\N	\N	\N	SI	NO	225	YVOTY	\N	\N	\N	0	.
3582798	Lucía	Báez	FEMENINO	\N	PARAGUAYO/A	0985341587	UNION DE HECHO	\N	\N	\N	SI	NO	904	Martin Fierro	\N	\N	\N	5	.
2199841	Celia	Balcazar Giménez	FEMENINO	\N	PARAGUAYO/A	0983681002	SOLTERO/A	\N	\N	\N	SI	NO	2535	Mandy Jura	\N	\N	\N	0	.
4562194	Rosa Liz	Barboza Samudio	FEMENINO	\N	PARAGUAYO/A	0972705259	UNION DE HECHO	\N	\N	\N	SI	NO	985	8 de Diciembre	\N	\N	\N	0	Jorge Daniel Massi es nieto del Sr Francisco Cuevas , la casa no esta en uso porque falta techar.
4662913	Hector	Bareiro Brítez	MASCULINO	1989-12-12	PARAGUAYO/A	099453192	UNION DE HECHO	\N	\N	\N	SI	NO	2563	Ko'eti	\N	\N	\N	0	La pareja lleva 4 anos de concubinato tienen techo propio de material no tienen lote propio.Viven en el lote de la madre.
4662911	Isidro	Bareiro Brítez	MASCULINO	1985-08-08	PARAGUAYO/A	0992641990	UNION DE HECHO	\N	\N	\N	SI	NO	2558	Ko'eti	\N	\N	\N	0	La pareja lleva 10 anos de concubinato,no tienen lote propio viven bajo el techo de la madre.
4662927	Nelson	Bareiro Brítez	MASCULINO	1983-12-05	PARAGUAYO/A	0984446874	UNION DE HECHO	\N	\N	\N	SI	NO	2559	Ko'eti	\N	\N	\N	0	La pareja lleva 15 anso de concubinato,viven en el lote de la madre con techo de material independiente.Hace 7 anos que tienen una construcción de material,anteriormente tenian techo aparte.
3730861	Regino	Bareiro Brítez	MASCULINO	1979-07-09	PARAGUAYO/A	0981766565	UNION DE HECHO	\N	\N	\N	SI	NO	1653	Jakare Pito	\N	\N	\N	0	La casa tiene dos piezas con tejas, corredor de chapa zinc y piso con lecherada, la pareja vino del departamento de caaguazu hace 5 años
374324	Domitila	Bareiro de Villalba	FEMENINO	2045-07-05	PARAGUAYO/A	0972147987	CASADO/A	\N	\N	\N	SI	NO	339	Nuevo Amanecer	\N	\N	\N	0	.
3847192	Claudio Barbara	Bareiro Godoy	MASCULINO	1980-04-12	PARAGUAYO/A	0985599578	SOLTERO/A	\N	\N	\N	SI	NO	2093	.	\N	\N	\N	0	.
4111931	María Elena	Bareiro Godoy	FEMENINO	1983-11-05	PARAGUAYO/A	0985599578	UNION DE HECHO	\N	\N	\N	SI	NO	2204	Mbo-ehara	\N	\N	\N	0	.
1198943	Alcides Ramón	Bareiro Méndez	MASCULINO	1973-08-06	PARAGUAYO/A	0981795387	CASADO/A	\N	\N	\N	SI	NO	487	23 de julio c/ Remansito	\N	\N	\N	0	.
2623789	Jorge Daniel	Bareiro Méndez	MASCULINO	\N	PARAGUAYO/A	0981779599	UNION DE HECHO	\N	\N	\N	SI	NO	1005	Mto. Ursicino Velasco	\N	\N	\N	0	Mostrador Vitrina:1.
4407244	Francisco Javier	Barreto Ayala	MASCULINO	\N	PARAGUAYO/A	0986290508	UNION DE HECHO	\N	\N	\N	SI	NO	1192	Fidelina	\N	\N	\N	0	.
1423968	Máxima María	Barreto Ferreira	FEMENINO	\N	PARAGUAYO/A	0981845374	SOLTERO/A	\N	\N	\N	SI	NO	755	Martin Fierro	\N	\N	\N	0	La vivienda fue construida hace 20 anos.
4697638	Liliana Noemi	Barreto Pereira	FEMENINO	1989-12-10	PARAGUAYO/A	0994790405	.	\N	\N	\N	SI	NO	1051	Mar de Plata e/ Nuevo Amanecer	\N	\N	\N	0	Vive dentro del mismo lote y techo que la madre Fátima Elodia Pereira.
4273372	Silvia Natalia	Barreto Pereira	FEMENINO	\N	PARAGUAYO/A	0984206869	UNION DE HECHO	\N	\N	\N	SI	NO	1054	Mar de Plata e/  Nuevo Amanecer	\N	\N	\N	0	Viven dentro del mismo lote y techo que la madre Fátima Elodia Pereira.
3668047	Marcelo Miguel	Barrios Bogado	MASCULINO	\N	PARAGUAYO/A	098877949	CASADO/A	\N	\N	\N	SI	NO	2006	Ñasaindy	\N	\N	\N	3	.
5160032	Shirley Gissell	Barrios Brítez	FEMENINO	\N	PARAGUAYO/A	0986703508	UNION DE HECHO	\N	\N	\N	SI	NO	2044	Ursicino Velasco c/ Sotero Colman	\N	\N	\N	0	Se aplico la antigüedad del marido y hace 7 años que esta aconcubinado.
1082783	Toribia	Barrios Domínguez	FEMENINO	\N	PARAGUAYO/A	0984676003	VIUDO/A	\N	\N	\N	SI	NO	2213	Mto. Ursicino Velasco	\N	\N	\N	0	.
1195239	Lucila	Barrios Duarte	FEMENINO	2064-01-11	PARAGUAYO/A	0985208452	UNION DE HECHO	\N	\N	\N	SI	NO	1855	Villa Maria Auxiliadora	\N	\N	\N	0	Cuenta con Permiso de Ocupación.
4358084	Natalia Lorena	Barrios González	FEMENINO	1985-10-06	PARAGUAYO/A	0984550993	CASADO/A	\N	\N	\N	SI	NO	525	Malvinas	\N	\N	\N	0	Dentro del lote de Natalia vive un hno. Soltero en una extención de 8x4.
3800706	Norma Mabel	Barrios González	FEMENINO	\N	PARAGUAYO/A	0985889150	.	\N	\N	\N	SI	NO	526	Malvinas	\N	\N	\N	0	Vive dentro del lote de su hna Natalia en una vivienda independiente de material bien constituida.
3000079	María Aparecida	Barrios López	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	2352	Nustra Señora de la Asunción	\N	\N	\N	6	.
1188290	Graciela	Barrios Martínez	FEMENINO	\N	PARAGUAYO/A	0981537107	SOLTERO/A	\N	\N	\N	SI	NO	2302	Nuestra Sra. De la Asunción	\N	\N	\N	0	.
4880474	Ángelica	Barrios Peralta	FEMENINO	\N	PARAGUAYO/A	0983248495	SOLTERO/A	\N	\N	\N	SI	NO	2287	Nuestra Sra. De la Asunción	\N	\N	\N	0	.
3730820	Calixto Denis	Barrios Peralta	MASCULINO	1977-09-07	PARAGUAYO/A	0982451365	CASADO/A	\N	\N	\N	SI	NO	2314	Nuestra Sra. De la Asunción	\N	\N	\N	6	.
6217034	Estelvina	Barrios Peralta	FEMENINO	\N	PARAGUAYO/A	0984612616	SOLTERO/A	\N	\N	\N	SI	NO	2335	Nuestra Sra. De la Asunción	\N	\N	\N	6	La titular hace 10 anos que vivce en el Barrio.Su madre es del barrio hace mas de 20 anos que su madre vive en el barrio.La Titular vivio durante 15 anos con su concubino en ciudad este anteriormente ya vivia con su madre en el barrio hace 10 anos que vo
6140942	Gladys Ignacia	Barrios Peralta	FEMENINO	\N	PARAGUAYO/A	0981163441	UNION DE HECHO	\N	\N	\N	SI	NO	2284	Nuestra Sra. De la Asunción	\N	\N	\N	0	.
3704389	Jorge	Barrios Peralta	MASCULINO	\N	PARAGUAYO/A	0982452012	CASADO/A	\N	\N	\N	SI	NO	2283	Nuestra Sra. De la Asunción	\N	\N	\N	0	.
4789074	Adriana Beatriz	Barrios Velazquez	FEMENINO	\N	PARAGUAYO/A	0986615919	UNION DE HECHO	\N	\N	\N	SI	NO	2724	.	\N	\N	\N	0	Vive como encargada en la casa hace 2 años ,es del barrio.La Sra. Lilian no vive en el barrio, tiene casa en Sajonia.
6252439	Juana Margarita	Barrios Velázquez	MASCULINO	2068-11-05	PARAGUAYO/A	0971237766	UNION DE HECHO	\N	\N	\N	SI	NO	979	Malvinas c/ 16 de Agosto	\N	\N	\N	0	.
2728	Alcides	Barrios	MASCULINO	\N	No quiso censar	.	.	\N	\N	\N	SI	NO	2404	.	\N	\N	\N	0	No quiso censarse.
1107843	Felicita	Barrios	FEMENINO	\N	PARAGUAYO/A	0982341874	SOLTERO/A	\N	\N	\N	SI	NO	418	Julio Jara c/ Nuevo Amanecer	\N	\N	\N	4	.
4580067	Gloria Elizabeth	Barrios	FEMENINO	\N	PARAGUAYO/A	0986552243	UNION DE HECHO	\N	\N	\N	SI	NO	2617	Banado Koeti	\N	\N	\N	5	.
6725657	Griselda Isabel	Barrios	FEMENINO	\N	PARAGUAYO/A	0971755456	UNION DE HECHO	\N	\N	\N	SI	NO	1291	Ursicino Velasco	\N	\N	\N	0	Vive dentro del terreno de su abuelo con su pareja en una vivienda precaria conpartida con su mamá.
27021	Nanciancena	Barrios	FEMENINO	\N	No quiso censar	.	.	\N	\N	\N	SI	NO	2505	Pasillo Martin Fierro	\N	\N	\N	0	No quiso censarse.
3384353	Ramón Alberto	Barrios	MASCULINO	\N	PARAGUAYO/A	0971311264	SEPARADO/A	\N	\N	\N	SI	NO	898	Martin Fierro	\N	\N	\N	0	.
6540040	Tania Fernanda	Barrios	FEMENINO	1993-11-06	PARAGUAYO/A	0981537107	UNION DE HECHO	\N	\N	\N	SI	NO	2304	Nuestra Sra. De la Asunción	\N	\N	\N	0	.
6940064	Diana Diana	Basilica	FEMENINO	1993-09-12	PARAGUAYO/A	0972553324	SOLTERO/A	\N	\N	\N	SI	NO	247	San Felipe y Santiago	\N	\N	\N	0	.
4226522	Agustín Gregorio	Bauza Velázquez	MASCULINO	\N	PARAGUAYO/A	097171827	UNION DE HECHO	\N	\N	\N	SI	NO	1300	Mto. Ursicino Velasco	\N	\N	\N	0	.
3677257	Desiderio	Bauza Velázquez	MASCULINO	\N	PARAGUAYO/A	0982819184	UNION DE HECHO	\N	\N	\N	SI	NO	1298	Mto. Ursicino Velasco	\N	\N	\N	0	La flia  vive dentro del mismo lote y vivienda de la madre.
5989594	Margarita Belen	Bauza Velázquez	FEMENINO	1994-09-02	PARAGUAYO/A	098276202	UNION DE HECHO	\N	\N	\N	SI	NO	1297	Mto. Ursicino Velasco	\N	\N	\N	0	En el frente tienen una construccion para vender minutas vive dentro del mismo lote y vivienda de la madre.
2523079	Pedro Ruben	Bazan Barrios	MASCULINO	\N	PARAGUAYO/A	0981784914	UNION DE HECHO	\N	\N	\N	SI	NO	1857	Villa Maria Auxiliadora	\N	\N	\N	0	.
4219909	Liz Magali	Bazán Medina	FEMENINO	\N	PARAGUAYO/A	0961996788	UNION DE HECHO	\N	\N	\N	SI	NO	2104	Mboéhara c/ Stero Colman	\N	\N	\N	0	Hace 9 meses que compro la casa en una mitad la otra es del tio, anteriormente ella vivia en la casa de la madre en Villa 8 de Diciembre.
2471817	Myrian	Beatriz Ibarra	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	978	San Felipe y Santiago	\N	\N	\N	0	La casa e una construcción de bloque pro vida, adelante tienen un galpon( corredor)
573124	Carlos Adolfo	Bello Aceval	MASCULINO	2058-11-06	PARAGUAYO/A	0984459308	.	\N	\N	\N	SI	NO	1056	Mar de plata e/  Nuevo Amanecer	\N	\N	\N	0	Vive dentro del lote de la señora,Fátima Elodia Pereira quien le alquila una habitación con un baño independiente.
3465349	Gladys Saturnina	Benegas Arrua	FEMENINO	1977-08-06	PARAGUAYO/A	0972239097	CASADO/A	\N	\N	\N	SI	NO	1908	Julio Benitez	\N	\N	\N	0	.
2658561	Basilia	Benegas Jímenez	FEMENINO	\N	PARAGUAYO/A	0985200899	.	\N	\N	\N	SI	NO	1919	Urcicino Velazco	\N	\N	\N	0	L a hija Cristina vende empaadas y gaseosas
4172146	Verónica	Benítez Acosta	FEMENINO	\N	PARAGUAYO/A	0991474086	VIUDO/A	\N	\N	\N	SI	NO	1314	Remancito	\N	\N	\N	0	.
5309150	Andrea Isabel	Benítez Adorno	FEMENINO	1990-10-03	PARAGUAYO/A	0972604194	UNION DE HECHO	\N	\N	\N	SI	NO	440	San Cayetano	\N	\N	\N	0	Estan de PAREJA Hace 3 anos.
5309172	María Tomasa	Benítez Adorno	FEMENINO	\N	PARAGUAYO/A	0971875253	UNION DE HECHO	\N	\N	\N	SI	NO	431	El lote no cuenta con patio.	\N	\N	\N	0	432807
3443687	Nancy del Carmen	Benítez Aguilar	FEMENINO	\N	PARAGUAYO/A	0982745818	UNION DE HECHO	\N	\N	\N	SI	NO	637	San Felipe y Santiago esq/ 23 de junio	\N	\N	\N	0	.
6500328	Mirian Raquel	Benítez Aquino	FEMENINO	1990-12-03	PARAGUAYO/A	0982745295	UNION DE HECHO	\N	\N	\N	SI	NO	2650	Nuestra Sra. De la Asunción	\N	\N	\N	0	La pareja lleva 4 anos de concubinato ,anteriormente vivia en la Zona 7 en la casa prestada por la tia,hace 9 meses compraron un lote en Villa Asunción
1961370	Rosa Estela	Benitez Aquino	FEMENINO	\N	PARAGUAYO/A	0985178447	UNION DE HECHO	\N	\N	\N	SI	NO	46	Sn Felipe y Santiago	\N	\N	\N	0	.
6339714	Expedito Rodrigo	Benítez Benítez	MASCULINO	\N	PARAGUAYO/A	0972407219	UNION DE HECHO	\N	\N	\N	SI	NO	966	Ange Luis c/ Virgen de Lourdes	\N	\N	\N	0	.
5319706	Lucio Gabriel	Benítez Bernal	MASCULINO	1990-08-02	PARAGUAYO/A	0994626702	UNION DE HECHO	\N	\N	\N	SI	NO	2385	Maynumby c/ Mandyjura	\N	\N	\N	0	vive en el terreno de su padre y tiene una construccion precaria de 4 x 6
3738666	Graciela María	Benítez Britos	FEMENINO	\N	PARAGUAYO/A	0994145481	SOLTERO/A	\N	\N	\N	SI	NO	1212	Virgen de Lourdes	\N	\N	\N	0	.
3738665	Justo Pastor	Benítez Britos	MASCULINO	1986-09-08	PARAGUAYO/A	0982651943	UNION DE HECHO	\N	\N	\N	SI	NO	489	Remansito c/ 23 de Junio	\N	\N	\N	0	.
2328792	María Bernarda	Benítez Cabañas	FEMENINO	\N	PARAGUAYO/A	0984260168	UNION DE HECHO	\N	\N	\N	SI	NO	192	Mto. Ursicino Velasco	\N	\N	\N	0	Vive dentro de la cas DE la mama que es la Sra. Maria de lo Santo Cabanas.
5527438	Zenaida María	Benítez Cabañas	FEMENINO	\N	PARAGUAYO/A	0985762036	UNION DE HECHO	\N	\N	\N	SI	NO	190	Mto. Ursicino Velazco	\N	\N	\N	0	.
3913295	Aurelia	Benítez Cardozo	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	2570	No proporciona mayores datos. Manifiesta que con e	\N	\N	\N	0	.
1214401	Barbara	Benítez Cardozo	FEMENINO	2062-04-12	PARAGUAYO/A	0981957860	UNION DE HECHO	\N	\N	\N	SI	NO	2338	Villa Asunción	\N	\N	\N	6	.
6569413	María José	Benítez Cardozo	FEMENINO	\N	PARAGUAYO/A	0991941326	UNION DE HECHO	\N	\N	\N	SI	NO	1299	Ursicino Velasco	\N	\N	\N	0	Vive dentro del lote y la vivienda de la mamá con su concubino e hijos. Comparte electrodomésticos con la dueña de casa.
6569394	Verónica Guadalupe	Benítez Cardozo	FEMENINO	1996-10-05	PARAGUAYO/A	0972552007	UNION DE HECHO	\N	\N	\N	SI	NO	1301	Ursicino Velasco	\N	\N	\N	0	Vive dentro del lote de la mamá con su concubino e hijo. Comparte electrodomésticos con la dueña de la casa.
4584739	Elvio David	Benítez Centurión	FEMENINO	\N	PARAGUAYO/A	0972186944	UNION DE HECHO	\N	\N	\N	SI	NO	2235	Mto.Ursicino Velasco	\N	\N	\N	0	Radio y DVD 1.
5388281	Clara Leticia	Benítez Cristaldo	FEMENINO	1992-11-08	PARAGUAYO/A	0982938589	CASADO/A	\N	\N	\N	SI	NO	314	Nuevo Amanecer	\N	\N	\N	0	La casa es la construida por la cooperativa Banado Poty
4381866	María Isabel	Benítez Cristaldo	FEMENINO	\N	PARAGUAYO/A	0986224163	UNION DE HECHO	\N	\N	\N	SI	NO	1099	Nuevo Amanecer	\N	\N	\N	4	.
3189584	Mirian Estela	Benítez Cristaldo	FEMENINO	1977-04-05	PARAGUAYO/A	0982123172	UNION DE HECHO	\N	\N	\N	SI	NO	511	Malvinas	\N	\N	\N	0	.
5527765	Alba Nidia	Benítez Cuba	FEMENINO	\N	PARAGUAYO/A	0994828745	SOLTERO/A	\N	\N	\N	SI	NO	1190	Fidelina	\N	\N	\N	0	.
5070726	Rossana	Benítez Cuba	FEMENINO	\N	PARAGUAYO/A	0985157337	SOLTERO/A	\N	\N	\N	SI	NO	1771	Koety	\N	\N	\N	0	Vive en el mismo terreno de la propietaria y tiene una construccion precaria de 4,50x3 y es madre soltera.
5494365	Sandra Elizabeth	Benítez Cuba	FEMENINO	\N	PARAGUAYO/A	0992784323	SOLTERO/A	\N	\N	\N	SI	NO	1189	Fidelina	\N	\N	\N	0	.
5030883	Basilia Chesebel	Benítez de Arevalos	FEMENINO	\N	PARAGUAYO/A	0971169202	CASADO/A	\N	\N	\N	SI	NO	1736	Ursicino Velasco	\N	\N	\N	0	Cocina 1.
978120	Luciana	Benítez de Echeverria	FEMENINO	\N	PARAGUAYO/A	098274538	CASADO/A	\N	\N	\N	SI	NO	2632	Santa Librada	\N	\N	\N	0	.
1434831	Miriam Concepción	Benítez de Leguizamon	FEMENINO	\N	PARAGUAYO/A	425021	SEPARADO/A	\N	\N	\N	SI	NO	1933	Ursicino Velasco c/Puente Pesoa	\N	\N	\N	0	En el salon tiene ventade ropas usadas.
3749754	María Natividad	Benítez de Santa Cruz	FEMENINO	\N	PARAGUAYO/A	0983963386	VIUDO/A	\N	\N	\N	SI	NO	2300	Nuestra Sra. De la Asunción	\N	\N	\N	5	La flia. Refiere que estaba construyendo su vivienda de material pero por la crecida no pudieron terminar,se observa la base construida.
56	Herminia	Benítez de Vidal	FEMENINO	\N	PARAGUAYO/A	.	CASADO/A	\N	\N	\N	SI	NO	250	San Felipe y Santiago	\N	\N	\N	0	Se nego a que se le quite la foto.
2042349	Amelia Esperanza	Benítez Duarte	FEMENINO	\N	PARAGUAYO/A	0981751979	SOLTERO/A	\N	\N	\N	SI	NO	988	8 de Diciembre	\N	\N	\N	0	Vivienda de material tiene 25 anos de construcción de 7,5 y 4,5 la Pro-Vida de Bloque 20 anos cuya dimension es de 5,30x5,30 , usan cocina y comedor.
3660219	Diana Jazmín	Benítez Duarte	FEMENINO	\N	PARAGUAYO/A	0991171723	UNION DE HECHO	\N	\N	\N	SI	NO	1084	Martin Fierro	\N	\N	\N	0	.
5030797	Edilson Anastacio	Benítez Duarte	FEMENINO	\N	PARAGUAYO/A	0985164195	UNION DE HECHO	\N	\N	\N	SI	NO	1179	Urcicino Velazco	\N	\N	\N	0	dentro del lote hay dos viviendas una utiliza para vivir edilson y su familia, la otra utiliza su papá para vivir, y alquila parte del lugarepara una carnicería, la segunda vivienda tiene una de 14x5 mts
3669944	Eulogio	Benìtez Duarte	MASCULINO	\N	PARAGUAYO/A	0982134482	SOLTERO/A	\N	\N	\N	SI	NO	871	.	\N	\N	\N	0	.
3560648	Griselda Elizabeth	Benítez Duarte	FEMENINO	\N	PARAGUAYO/A	0994124365	UNION DE HECHO	\N	\N	\N	SI	NO	990	8 de Diciembre	\N	\N	\N	0	Cocina y dormitoio todos juntos , tiene una vivienda de Pro-Vida pegada al de material.Tiene una cocina a Gas.
4271589	Hector Javier	Benítez Duarte	MASCULINO	1986-01-07	PARAGUAYO/A	0982857739	UNION DE HECHO	\N	\N	\N	SI	NO	994	Malvinas c/ Maria Elena	\N	\N	\N	0	La pareja estan viviendo en la casa de material con techo de eternit son independiente y estan de pareja hace 7 anos.
2151465	Nicodemus	Benítez Duarte	MASCULINO	\N	PARAGUAYO/A	0985824401	UNION DE HECHO	\N	\N	\N	SI	NO	1213	8 de Diciembre	\N	\N	\N	0	.
2024218	Gloria Elizabeth	Benítez Fariña	FEMENINO	\N	PARAGUAYO/A	0985850817	CASADO/A	\N	\N	\N	SI	NO	2474	Ursicino Velasco c/Sotero Colmean	\N	\N	\N	5	La Sra. Posee una lomiteria y un comedor alquilado a dos cuadras de su casa y cuenta con dos carritos pancheros ,sillas mesas.La mayoria de sus cosas estan en el alquiler.
2024219	Sonia Raquel	Benítez Fariña	FEMENINO	1981-05-03	PARAGUAYO/A	0984518758	CASADO/A	\N	\N	\N	SI	NO	2362	Stero Colman y Nasaindy	\N	\N	\N	0	La casa era de material contaba con dos dormitorios y una pileta de cocina .La ultima crecida tumbo la mitad de la casa por el cual la Sra. Tubo que drreumbar todo para construir de dos pisos pero ya no quiere gastar porque no sabe lo que va pasar por el
5205515	Elida	Benítez Florencio	FEMENINO	1990-11-02	PARAGUAYO/A	0982370794	UNION DE HECHO	\N	\N	\N	SI	NO	444	San Cayetano	\N	\N	\N	0	.
5373379	Gloria Ramona	Benítez Galeano	FEMENINO	1989-06-09	PARAGUAYO/A	0992346313	SOLTERO/A	\N	\N	\N	SI	NO	2523	Ursicino Velasco c/ Mita Saraki	\N	\N	\N	0	Vive en el terreno de la propietaria hace 20 anos.
6360307	Fatima	Benítez Garcia	FEMENINO	\N	PARAGUAYO/A	0986944701	UNION DE HECHO	\N	\N	\N	SI	NO	579	Malvinas	\N	\N	\N	0	.
5448861	Sergio	Benítez Garcia	MASCULINO	\N	PARAGUAYO/A	0982348394	UNION DE HECHO	\N	\N	\N	SI	NO	577	.	\N	\N	\N	0	.
6920751	Sergio Absalom	Benítez Gill	MASCULINO	\N	PARAGUAYO/A	0992915834	UNION DE HECHO	\N	\N	\N	SI	NO	2088	Sotero Colman	\N	\N	\N	5	Cocina y dormitorio todo dentro de la pieza, se utiliza la antigüedad del conyuge Carlos Javier
3906076	Andrésa	Benítez Giménez	FEMENINO	\N	PARAGUAYO/A	0971869331	UNION DE HECHO	\N	\N	\N	SI	NO	276	Ursicino Velasco y Nuevo Amanecer	\N	\N	\N	0	Dentro de este lote hay un techo y 2 Flias.
4543993	Edita	Benítez Giménez	FEMENINO	\N	PÁR	.	SOLTERO/A	\N	\N	\N	SI	NO	916	Martin Fierro	\N	\N	\N	5	La titular esta separada hace 3 meses , esta embarazada . Recibe apoyo economico del padre de sus hijos.
2547773	Fermin	Benítez Giménez	MASCULINO	1970-11-10	PARAGUAYO/A	0972582480	UNION DE HECHO	\N	\N	\N	SI	NO	274	Nuevo Amanecer y los Sauces	\N	\N	\N	0	.
1885407	Ramona	Benítez Giménez	MASCULINO	\N	PARAGUAYO/A	0984840909	UNION DE HECHO	\N	\N	\N	SI	NO	347	Mar del Plata	\N	\N	\N	4	Su hijo Posee ene el mismo terreno una casa de material 6x6 bien constituida.
5353698	Johana	Benítez Godoy	FEMENINO	\N	PARAGUAYO/A	0986870558	SOLTERO/A	\N	\N	\N	SI	NO	282	Nuevo Amanecer	\N	\N	\N	0	Tiene una vivienda precaria.
7422164	Luz Clara	Benítez Godoy	FEMENINO	\N	PARAGUAYO/A	0986870558	UNION DE HECHO	\N	\N	\N	SI	NO	283	Nuevo Amanecer	\N	\N	\N	0	.
3940714	María Ángelica	Benítez Godoy	FEMENINO	\N	PARAGUAYO/A	0992218464	SOLTERO/A	\N	\N	\N	SI	NO	2380	Ursicino Velasco c Rancho 8	\N	\N	\N	0	Cocina Electrica 1.
4718832	Jesica Natalia	Benítez Gómez	FEMENINO	1996-08-07	PARAGUAYO/A	0983442484	SOLTERO/A	\N	\N	\N	SI	NO	710	Mar del Plata	\N	\N	\N	5	Hace 6 meses se separo con su conyuge.
4718888	Juan Angel	Benítez Gómez	MASCULINO	1991-01-02	PARAGUAYO/A	0971261540	UNION DE HECHO	\N	\N	\N	SI	NO	712	Mar del Plata	\N	\N	\N	0	La casa de madera donde vive Juan Angel y su conyuge es de Jesica , tambien el lote que mide 13x50 esta dividido con Juan Angel .
4718961	Karina Elizabeth	Benítez Gómez	FEMENINO	1992-11-05	PARAGUAYO/A	0972654316	UNION DE HECHO	\N	\N	\N	SI	NO	326	Mardel Plata	\N	\N	\N	0	.
1361939	Lucio	Benítez Gonzaga	MASCULINO	\N	PARAGUAYO/A	0971306343	SEPARADO/A	\N	\N	\N	SI	NO	2383	Maynumby c/ Mandujura	\N	\N	\N	0	.
6687136	Dahiana Pamela	Benítez González	FEMENINO	1995-08-05	PARAGUAYO/A	0991190149	UNION DE HECHO	\N	\N	\N	SI	NO	686	Yacare Yrupe	\N	\N	\N	5	.
4530963	Adolfo Ramón	Benítez Guerrero	MASCULINO	\N	PARAGUAYO/A	0992256751	UNION DE HECHO	\N	\N	\N	SI	NO	1448	Bañado Koeti	\N	\N	\N	5	.
1468547	Olinda	Benítez Jara	FEMENINO	\N	PARAGUAYO/A	0972758439	SOLTERO/A	\N	\N	\N	SI	NO	1346	Primavera	\N	\N	\N	0	.
1055719	Ramona	Benítez Jara	MASCULINO	2062-05-06	PARAGUAYO/A	0991948277	UNION DE HECHO	\N	\N	\N	SI	NO	1873	Villa Maria Auxiliadora	\N	\N	\N	0	En este momento se encuentra construyendo en la parte superior de la casa.
3189583	Francisca	Benítez Ledezma	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	1863	Ursicino Velazco	\N	\N	\N	0	Es propietaria del lote y la vivienda. Con ella viven su nieto con su pareja
947829	Elvio Dario	Benítez Lezcano	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	2234	Mto.Ursicino Velasco	\N	\N	\N	0	En la casa tiene un salon para la venta de bebidas y comestibles una dimencion de 8x4 , tambien en la parte superior y tiene 3 dormitorios y un bano ,cada dormitorio mide 4x4. En el lote hay 2 vuiviendas didvididos y eld ueno del lote se llama Jose Prisc
689931	Hernán	Benítez Lezcano	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	2240	El dueno del lote es el Sr. Jose Prisciliano Benit	\N	\N	\N	0	.
4255632	Irene	Benítez Martínez	FEMENINO	\N	PARAGUAYO/A	0986701268	UNION DE HECHO	\N	\N	\N	SI	NO	2041	Ursicino Velasco	\N	\N	\N	0	.
5368234	Karina Mabel	Benítez Martínez	FEMENINO	\N	PARAGUAYO/A	095943543	CASADO/A	\N	\N	\N	SI	NO	2374	Nuestra Señora de la Asuncion	\N	\N	\N	6	el señor tiene marcapasos, no tienen lote propio
2803762	Sonia Raquel	Benítez	FEMENINO	\N	PARAGUAYO/A	0992255296	SOLTERO/A	\N	\N	\N	SI	NO	2313	Nuestra Sra. De la Asunción	\N	\N	\N	6	.
6651375	María Liliana	Benítez Martínez	FEMENINO	\N	PARAGUAYO/A	0986552036	UNION DE HECHO	\N	\N	\N	SI	NO	2376	nuestra sra de la asuncion	\N	\N	\N	6	.
5673055	Norberto	Benítez Martínez	MASCULINO	\N	PARAGUAYO/A	09851186	UNION DE HECHO	\N	\N	\N	SI	NO	2377	NUESTRA SEÑORA DE LA ASUNCION	\N	\N	\N	6	la pareja lleva cinco años de concubinato, no tienen ote propio pero si techo precario
5368232	Liz Marisol	Benítez Matinez	FEMENINO	1979-12-11	PARAGUAYO/A	0986203920	UNION DE HECHO	\N	\N	\N	SI	NO	2616	Nuestra Señora de la Asuncion	\N	\N	\N	6	.
2861956	Bernarda	Benítez Medina	FEMENINO	2050-12-03	PARAGUAYO/A	0982715184	SOLTERO/A	\N	\N	\N	SI	NO	2150	San Martin de Porres	\N	\N	\N	0	Vive dentro del lote de Don Eusebio en una vivienda precaria.
4007372	Morris Liliana	Benítez Mongelos	FEMENINO	1983-08-08	PARAGUAYO/A	0991772984	SEPARADO/A	\N	\N	\N	SI	NO	1842	Santa Librada y Ursicino Velasco	\N	\N	\N	0	Es la casa de la mama pero ella esta totalmente aparte en la planta baja.
4862665	Ever Antonio	Benítez Monges	MASCULINO	1991-03-06	PARAGUAYO/A	0983852600	UNION DE HECHO	\N	\N	\N	SI	NO	2654	Nuestra Sra. De la Asunción	\N	\N	\N	6	La pareja lleva 4 anos de concubinato ,no tienen lote propio viven en el lote de la madre.
6013428	Dalva Giselle	Benítez Moray	FEMENINO	1990-07-07	PARAGUAYO/A	0992213767	.	\N	\N	\N	SI	NO	2176	Chiquero	\N	\N	\N	0	La familia vive en extrema Pobreza
4100062	Daniel	Benítez Ojeda	MASCULINO	\N	PARAGUAYO/A	0971385730	UNION DE HECHO	\N	\N	\N	SI	NO	2067	Sotero Colman c/ Mbo'ehara	\N	\N	\N	0	.
4626024	Hernán	Benítez Ojeda	MASCULINO	\N	.	0961683515	.	\N	\N	\N	SI	NO	2062	Mbo'ehara	\N	\N	\N	0	En este momento está con la mamá, pero la semana que viene estará preparando su casa de madera como siempre tuvo
3212819	José Gabriel	Benítez Ojeda	MASCULINO	\N	PARAGUAYO/A	0981115266	CASADO/A	\N	\N	\N	SI	NO	2367	Mbo'ehara	\N	\N	\N	0	El sr. Gabriel alquila un local en la Villa Esperanza donde tiene una Despensa.
3212882	Lidia Isabel	Benítez Ojeda	FEMENINO	1987-01-06	PARAGUAYO/A	0961683515	CASADO/A	\N	\N	\N	SI	NO	2060	Sotero Colma c/ Mbo'ehara	\N	\N	\N	0	.
2110803	María Concepción	Benítez Palacios	FEMENINO	2063-07-12	PARAGUAYO/A	0972707417	UNION DE HECHO	\N	\N	\N	SI	NO	1163	Urcicino Velazco c/ 26 de Julio	\N	\N	\N	0	La dueña de la casa manifiesta que en el mismo terreno vive su hermano Alberto Benítez con una vivienda preacaria 3x4
4784650	Mario	Benítez Pereira	MASCULINO	\N	PARAGUAYO/A	0984204018	CASADO/A	\N	\N	\N	SI	NO	1392	Bañado Koeti	\N	\N	\N	0	La pareja refiere que conviven hace 4 años, (casados).La familia vive dentro del mismo lote que la madre pero en una construcción aparte.
2041494	Julio Cesar	Benítez Quintana	MASCULINO	\N	PARAGUAYO/A	0981696748	UNION DE HECHO	\N	\N	\N	SI	NO	897	Martin Fierro	\N	\N	\N	0	.
4618968	Christián Federico	Benítez Riquelme	MASCULINO	\N	PARAGUAYO/A	0971594952	UNION DE HECHO	\N	\N	\N	SI	NO	35	San Felipe y Santiago	\N	\N	\N	0	.
5919325	Aida	Benítez Sanabria	FEMENINO	\N	PARAGUAYO/A	0981226068	UNION DE HECHO	\N	\N	\N	SI	NO	1701	Bañado Koeti	\N	\N	\N	5	.
2207820	Fernando Gilberto	Benítez Sánchez	MASCULINO	\N	PARAGUAYO/A	0971551709	SOLTERO/A	\N	\N	\N	SI	NO	1886	Julio Benitez	\N	\N	\N	0	La casa era de los padres luego de fallecer los dos se les quedo a los 3 hnos. dos hombres ,tiene parejas casuales.
3021148	Julio Florentín	Benítez Sánchez	MASCULINO	\N	PARAGUAYO/A	0992741606	SEPARADO/A	\N	\N	\N	SI	NO	604	16 de Agosto	\N	\N	\N	0	.
1475894	Lucíano Miguel	Benítez Sánchez	FEMENINO	2062-06-07	PARAGUAYO/A	0982225209	VIUDO/A	\N	\N	\N	SI	NO	1843	Ursicino Velasco	\N	\N	\N	0	Tiene un negocio para juegos de azar,electronicos y mesa de Villar.
525890	Miguel Felino	Benítez Vega	MASCULINO	2055-08-05	PARAGUAYO/A	0982215769	CASADO/A	\N	\N	\N	SI	NO	2174	Ursicino Velasco	\N	\N	\N	0	.
2117029	María Isabel	Benitez Vera	FEMENINO	\N	PARAGUAYO/A	0994357593	CASADO/A	\N	\N	\N	SI	NO	1835	Santa Librada	\N	\N	\N	0	.
4843170	Alicia Elizabeth	Benítez	FEMENINO	\N	PARAGUAYO/A	0982139742	UNION DE HECHO	\N	\N	\N	SI	NO	2107	Mbo'ehara	\N	\N	\N	0	La construcción es de dos pisos.
3052	Carina	Benítez	FEMENINO	\N	No quiso censar	.	.	\N	\N	\N	SI	NO	2232	.	\N	\N	\N	0	No quiso censarse.
1244283	Clotilde Anastacia	Benítez	FEMENINO	2055-03-06	PARAGUAYO/A	0983838429	SOLTERO/A	\N	\N	\N	SI	NO	2310	Nuestra Sra. De la Asunción	\N	\N	\N	6	.
4662586	Elsi Romina	Benítez	FEMENINO	\N	PARAGUAYO/A	0982786519	UNION DE HECHO	\N	\N	\N	SI	NO	639	San Felipe y Santiago esq/ 23 de Junio	\N	\N	\N	0	La pareja tiene 10 años de  concubinato. Viven en el lote de la titular con techo propio y aparte.
430	Fabiana	Benítez	FEMENINO	\N	PARAGUAYO/A	0972723738	SOLTERO/A	\N	\N	\N	SI	NO	2244	Ursicino Velasco	\N	\N	\N	0	No conoce la dimension, cuenta con permiso de uso y paga arrendamiento, con ella vive uno de sus hijos.No quiere dar sus datos dice no estar interesado en figurar.
4778070	Graciela	Benítez	FEMENINO	\N	PARAGUAYO/A	0972707075	UNION DE HECHO	\N	\N	\N	SI	NO	1474	26 de Julio c/Ursicino Velasco	\N	\N	\N	0	Centrifugadora:1.La pareja lleva 12 anos de concubinato.Tiene dos hijas menores.La pareja vive hace 26 anos en el Barrio.
2577422	Isabel Beatriz	Benítez	FEMENINO	\N	PARAGUAYO/A	0972615047	.	\N	\N	\N	SI	NO	1887	Julio Benitez	\N	\N	\N	0	La casa era de los padres luego de fallecer los dos se les quedo a los 3 hnos. dos hombres ,tiene parejas casuales.
5373381	Juliána	Benítez	MASCULINO	\N	PARAGUAYO/A	0986607281	UNION DE HECHO	\N	\N	\N	SI	NO	1358	Primavera	\N	\N	\N	0	Hace 11 anos que esta con su pareja.
6734048	Liz Noelia	Benítez	FEMENINO	\N	PARAGUAYO/A	0991451176	UNION DE HECHO	\N	\N	\N	SI	NO	1019	Mto Ursicino Velasco	\N	\N	\N	0	.
5373358	Maikel	Benítez	MASCULINO	\N	PARAGUAYO/A	0971631629	UNION DE HECHO	\N	\N	\N	SI	NO	1356	Primavera	\N	\N	\N	0	.
1176012	Margarita	Benítez	FEMENINO	\N	PARAGUAYO/A	0982117166	CASADO/A	\N	\N	\N	SI	NO	915	Maestro Ursicino Velasco	\N	\N	\N	0	Casa de dos pisos.
1042055	Mario Cesar Javier	Benítez	MASCULINO	\N	PARAGUAYO/A	021-424538	VIUDO/A	\N	\N	\N	SI	NO	1725	Ursicino Velasco	\N	\N	\N	0	Es propietariuo del lote y la casa su hija vive en una extencion de 8x8mts., lleva 12 anos acompanada cuenta con un Quincho de 14x4 mts.
4686292	Michela	Benítez	FEMENINO	\N	PARAGUAYO/A	0983716688	SOLTERO/A	\N	\N	\N	SI	NO	1359	Primavera	\N	\N	\N	0	.
4607958	Miguel Alejandro	Benítez	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	576	.	\N	\N	\N	0	La casa era de la mama.
4160136	Patricia Deolinda	Benítez	FEMENINO	\N	PARAGUAYO/A	0986852648	UNION DE HECHO	\N	\N	\N	SI	NO	1840	Santa Librada	\N	\N	\N	0	Es propietario del lote y la casa con ella vive dos de sus hermanos.
3339	Pedro	Benítez	MASCULINO	\N	.	.	.	\N	\N	\N	SI	NO	2492	San Felipe Y Santiago (Recicla)	\N	\N	\N	0	Vivienda Vacía, el dueño falleció hace aproximadamente 2 meses según refiere vecinos
4037675	Raquel Soledad	Benítez	FEMENINO	\N	PARAGUAYO/A	0981316789	UNION DE HECHO	\N	\N	\N	SI	NO	620	Ursicino Velasco	\N	\N	\N	0	.
4276435	Rocío Karina	Benítez	FEMENINO	\N	PARAGUAYO/A	0091707685	CASADO/A	\N	\N	\N	SI	NO	635	San Felipe y Santiago	\N	\N	\N	0	La pareja lleva 20 años de acompañados.
2969903	Rosa Florinda	Benítez	FEMENINO	\N	PARAGUAYO/A	0981846845	.	\N	\N	\N	SI	NO	1480	Payagua Naranja	\N	\N	\N	0	La señora cobra sueldo de tercera edad.
4662987	Rosana Alice	Benítez	FEMENINO	1990-10-01	PARAGUAYO/A	0983761188	UNION DE HECHO	\N	\N	\N	SI	NO	481	23 de Junio	\N	\N	\N	0	.
4305475	Susana Marlene	Benítez	FEMENINO	1985-12-02	PARAGUAYO/A	0981456817	SOLTERO/A	\N	\N	\N	SI	NO	5	San Felipe y Santiago	\N	\N	\N	0	.
1722366	Victor Alexis	Benítez	MASCULINO	\N	PARAGUAYO/A	0991847673	CASADO/A	\N	\N	\N	SI	NO	2375	Nuestra Sra. De la Asunción	\N	\N	\N	6	Secaropas 1.
2580009	Zulma Lovera	Benítez	MASCULINO	1970-04-03	PARAGUAYO/A	0971138049	UNION DE HECHO	\N	\N	\N	SI	NO	327	Mar del Plata	\N	\N	\N	0	Tiene un hijo de 19 totalmente independiente con casa.
5406925	Sergio del Pilar	Benítez	MASCULINO	1992-12-10	PARAGUAYO/A	0971685148	UNION DE HECHO	\N	\N	\N	SI	NO	277	Ursicino Velasco y Nuevo Amanecer	\N	\N	\N	0	Vive bajo el mismo techo de la titular.
5277054	Rodrigo Agustín	Bernal Aguilar	MASCULINO	\N	PARAGUAYO/A	0985336921	CASADO/A	\N	\N	\N	SI	NO	855	Ursicino Velasco	\N	\N	\N	0	El sr. Cuenta con una extenciuon por la casa de la madre.
4350411	Victoria Lorena	Bernal Aguilar	MASCULINO	\N	PARAGUAYO/A	0984202674	CASADO/A	\N	\N	\N	SI	NO	857	Ursicinop Velasco	\N	\N	\N	0	La sra. Posee otra construccion de materiañ 6x6 dentro de su terreno.
5236846	Gustavo Adolfo	Bernal Amarilla	FEMENINO	\N	PARAGUAYO/A	0992255776	UNION DE HECHO	\N	\N	\N	SI	NO	951	Los Sauces	\N	\N	\N	0	La pareja vive en el lote de la Oleria
1217932	Mirta Concepción	Bernal de Barreto	FEMENINO	\N	PARAGUAYO/A	0984445689	CASADO/A	\N	\N	\N	SI	NO	2163	Ursicino Velasco c/ Maria Auxiliadora	\N	\N	\N	0	Compro de la Sra. Benita Campuzano el terreno c/ una casa hace 7 anos.
938978	Irma Beatriz	Bernal de Masi	FEMENINO	2063-02-07	PARAGUAYO/A	0982561984	CASADO/A	\N	\N	\N	SI	NO	705	Algaroba	\N	\N	\N	0	Dentro de la vivienda de la señora Irma Bernal, vive una hija de 28 años de edad, actualmente  se encuentra en concubinato con Roberto Mato el cuál no se encuentra y no tiene los documentos.
1869569	Graciela	Bernal González	FEMENINO	\N	PARAGUAYO/A	0992256564	SOLTERO/A	\N	\N	\N	SI	NO	868	Martin Fierro c/ Ursicino Velasco	\N	\N	\N	0	.
2952382	Esmilce	Bernal Riquelme	FEMENINO	\N	PARAGUAYO/A	0984707051	SOLTERO/A	\N	\N	\N	SI	NO	554	Ursicino Velasco	\N	\N	\N	0	Tiene Corredor.
1722248	María Cristina	Bernal Riquelme	FEMENINO	1971-05-12	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	557	Ursicino Velasco	\N	\N	\N	0	La casa no tiene bano , cocina, comparte con la mama una extensión de pieza de madera.
1479249	José Ramón	Blanco	MASCULINO	1974-09-02	PARAGUAYO/A	0984525229	.	\N	\N	\N	SI	NO	1932	Urcicino Velazco	\N	\N	\N	0	El señor se encuentra alquilando la piez hace 6 meses
5201057	Marilia Raquel	Bobadilla Cuba	FEMENINO	1995-03-03	PARAGUAYO/A	0984808075	CASADO/A	\N	\N	\N	SI	NO	1746	Ursicino Velasco	\N	\N	\N	0	Vive en la parte superior de la casa y mide 8x12 y esta muy bien constituida.
2991966	Margarita	Bobadilla de Cabañas	FEMENINO	1974-11-11	PARAGUAYO/A	0982565710	UNION DE HECHO	\N	\N	\N	SI	NO	173	Nuevo Amanecer e/ Julio Jara	\N	\N	\N	0	.
7801720	Nora Liz	Bobadilla Figueredo	FEMENINO	\N	PARAGUAYO/A	0984662655	SOLTERO/A	\N	\N	\N	SI	NO	2342	Nuestra Sra de la Asunción	\N	\N	\N	6	.
6006148	Tania Gabriela	Bobadilla Figueredo	FEMENINO	1995-02-05	PARAGUAYO/A	0983383872	.	\N	\N	\N	SI	NO	2068	Ursisino Velzco	\N	\N	\N	0	No tienen ni lote ni vivienda, viven en alquiler
4028192	Fernando Virgilio	Bobadilla González	MASCULINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	184	Julio Jara	\N	\N	\N	0	.
733070	Sixto	Bobadilla Leguizamon	MASCULINO	\N	PARAGUAYO/A	.	CASADO/A	\N	\N	\N	SI	NO	934	Nuevo amanecer	\N	\N	\N	4	El señor tiene otra casa y lote en otro lugar y usa como depósito, tiene tambíen un palafito de madera que se construyo aproximadamente  hace 30 años.
4665473	Tamara Alinka	Bobadilla Portillo	FEMENINO	\N	PARAGUAYO/A	0986402381	UNION DE HECHO	\N	\N	\N	SI	NO	350	4 de Octubre	\N	\N	\N	0	La Sra. Tamara dice que el terreno pertenecio a la Abuela.
3512247	Nelly Verónica	Bobadilla Román	FEMENINO	\N	PARAGUAYO/A	0991350632	SOLTERO/A	\N	\N	\N	SI	NO	273	Nuevo Amanecer	\N	\N	\N	0	Dentro de la casa de la titular Nelly Bobadilla vive otro nucleo de Familia.
3811742	Silvio Arnaldo	Bobadilla Román	MASCULINO	\N	PARAGUAYO/A	0992530333	SOLTERO/A	\N	\N	\N	SI	NO	275	Nuevo Amanecer	\N	\N	\N	0	Tiene terreno propio y casa propia tenia pareja , pero actualmente estan separados.
5707250	Andrea Soledad	Bobadilla Vallejos	FEMENINO	\N	PARAGUAYO/A	0982230359	UNION DE HECHO	\N	\N	\N	SI	NO	180	Julio Jara y Nuevo Amanecer	\N	\N	\N	0	.
3596247	Eduardo	Bobadilla Vallejos	MASCULINO	\N	PARAGUAYO/A	0982473609	SOLTERO/A	\N	\N	\N	SI	NO	171	Nuevo Amanecer	\N	\N	\N	0	.
4248621	Petrona Azucena	Bobadilla Vallejos	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	315	Nuevo Amanecer	\N	\N	\N	0	.
4009253	Natalia Dolores	Bobadilla Vargas	FEMENINO	\N	PARAGUAYO/A	0984984908	SOLTERO/A	\N	\N	\N	SI	NO	185	Julio Jara	\N	\N	\N	4	.
965142	Ceferino	Bobadilla Villalba	MASCULINO	2046-08-01	PARAGUAYO/A	0981925635	.	\N	\N	\N	SI	NO	425	Julio Jara	\N	\N	\N	0	vive en un palafito de amdera, tiene una construccion de 2 piezas de 4,4 y 4, 4 la cual alquila.
3275025	Cicilia	Bobadilla	FEMENINO	1971-07-08	PARAGUAYO/A	0982416496	UNION DE HECHO	\N	\N	\N	SI	NO	1775	Pasillo sin nombre 6 y 7	\N	\N	\N	0	La hija de 20 años vive con la mamá y su pareja hace un año
4511544	Juan Carlos	Bobadilla	MASCULINO	1990-02-06	PARAGUAYO/A	0986 153 772	UNION DE HECHO	\N	\N	\N	SI	NO	1778	Pasillo sin nombre	\N	\N	\N	5	Hace 25 años vive en la casa desde que nació y se acuncubinó hac dos años.
3738561	Cynthia Marlene	Bogado Agüero	FEMENINO	\N	PARAGUAYO/A	0985958542	SOLTERO/A	\N	\N	\N	SI	NO	1567	Ursicino Velasco	\N	\N	\N	0	Vive dentro del lote de la mama,cuenta con vivienda propia de material es totalmente independiente a la de la titular.Laura vive pegado a la pared de la hermana en una vivienda precaria de 4x4, con su pareja llevan acompanados 4 anos.
903683	Nicolasa	Bogado Benítez	FEMENINO	2058-10-09	PARAGUAYO/A	0991919504	SOLTERO/A	\N	\N	\N	SI	NO	2069	Mboe'hara	\N	\N	\N	0	.
1099420	Dolores	Bogado Bernal	FEMENINO	\N	PARAGUAYO/A	0972167969	CASADO/A	\N	\N	\N	SI	NO	1194	Fidelina	\N	\N	\N	0	.
2618323	José	Bogado Bernal	MASCULINO	\N	PARAGUAYO/A	0982381917	UNION DE HECHO	\N	\N	\N	SI	NO	1501	Rancho 8	\N	\N	\N	0	Cocina Electrica 1.La pareja lleva 6 anos de concubinato, sin hijos menores se sostienen independientemente.
4189321	Ramón	Bogado Burgos	MASCULINO	1981-10-03	PARAGUAYO/A	0991290716	CASADO/A	\N	\N	\N	SI	NO	2037	Mbo'ehára	\N	\N	\N	0	.
1062093	María Rosa	Bogado de Barrios	FEMENINO	\N	PARAGUAYO/A	0983377949	CASADO/A	\N	\N	\N	SI	NO	2004	Nasaindy	\N	\N	\N	5	.
4313848	Felicia Rossana	Bogado de Duarte	FEMENINO	1979-01-08	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1698	Bañado Poty	\N	\N	\N	5	.
4640290	Vanessa Elizabeth	Bogado González	FEMENINO	\N	PARAGUAYO/A	0981475142	UNION DE HECHO	\N	\N	\N	SI	NO	495	San Felipe c/ 23 de Junio	\N	\N	\N	0	.
850520	Cecilia	Bogado López	FEMENINO	2059-01-07	PARAGUAYO/A	0983415186	SOLTERO/A	\N	\N	\N	SI	NO	1756	.	\N	\N	\N	0	.
6131120	Derlis Javier	Bogado Martínez	MASCULINO	1997-12-06	PARAGUAYO/A	0983411338	UNION DE HECHO	\N	\N	\N	SI	NO	1632	Pajagua Naranja	\N	\N	\N	0	Cocina a Gas 1.Derlis es hijo de Mirta Martinez la pareja vive en esta vivienda hace 6 anos tienen entrada y salida independiente por un pasillo.
4838495	Julio Cesar	Bogado Martínez	MASCULINO	1993-08-03	PARAGUAYO/A	0981761753	SOLTERO/A	\N	\N	\N	SI	NO	1412	Yvoty Rory	\N	\N	\N	0	La casa fue hechada durante la crecida y el Sr. Julio Cesar esta viviendo temporalmente en la casa de la madre.
3736467	Paola Stefani	Bogado Ocampo	FEMENINO	\N	PARAGUAYO/A	0972432019	UNION DE HECHO	\N	\N	\N	SI	NO	1165	Fidelina	\N	\N	\N	0	Tiene una tienda de minicarga y ventas de ropas y jueguetes , librería sobre Ursicino Velasco (Zona 5-B). El terreno le pertenece.
3173634	Librada	Bogado Vda. De Cabrera	FEMENINO	\N	PARAGUAYO/A	.	VIUDO/A	\N	\N	\N	SI	NO	1240	.	\N	\N	\N	0	.
3727091	Wifrido Erico	Bogado Zorrilla	MASCULINO	\N	PARAGUAYO/A	0981397808	CASADO/A	\N	\N	\N	SI	NO	1508	Rancho 8	\N	\N	\N	0	Dentro del lote del titular existen dos techos.
2380043	Carmen Elizabeth	Bogado	FEMENINO	\N	PARAGUAYO/A	0984703280	SOLTERO/A	\N	\N	\N	SI	NO	1682	Virgen de Guadalupe	\N	\N	\N	0	.
2355052	Genara Marta	Bogado	FEMENINO	\N	PARAGUAYO/A	0982290153	UNION DE HECHO	\N	\N	\N	SI	NO	2399	26 de Julio	\N	\N	\N	0	.
6570237	Juana Alicia	Bogado	MASCULINO	\N	PARAGUAYO/A	0982881256	UNION DE HECHO	\N	\N	\N	SI	NO	1683	Virgen de Guadalupe	\N	\N	\N	0	.
4670211	Katterin Andrea	Bogado	FEMENINO	\N	PARAGUAYO/A	0984338321	SOLTERO/A	\N	\N	\N	SI	NO	1500	Rancho 8	\N	\N	\N	0	Cocina Electrica 1. La titular es madre soltera, tiene lote propio, dentro del lote vive otra familia con un techo independiente.Es tio de la titular.
3385376	Natalia Raquel	Bogado	FEMENINO	1978-04-02	PARAGUAYO/A	0981941917	SOLTERO/A	\N	\N	\N	SI	NO	1911	Julio Benitez	\N	\N	\N	0	.
289160	Ramón	Bogado	MASCULINO	\N	PARAGUAYO/A	0984117768	VIUDO/A	\N	\N	\N	SI	NO	2029	Sagrado Corazón de Jesús	\N	\N	\N	0	Es propietaria del lote y la casa con el vive su hija y sus nietos con su pareja.Francisco vive dentro del lote y la casa del papa en una de los piezas.
3759021	Teófilo	Bogado	MASCULINO	\N	PARAGUAYO/A	0982453499	UNION DE HECHO	\N	\N	\N	SI	NO	1027	Mar de plata c/  Nuevo Amanecer	\N	\N	\N	0	La vivienda anteriormente eraa de ladrillos pero se derrumbó durante la crecida del río. Actualmente esta construida una vivienda precaria sin terminación. El titular actualmente vive en la casa de su pareja en la misma. ZONA (M: 47, LOTE:001).
5445383	María Estela	Bogarin	FEMENINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	2451	San Martin de Porres	\N	\N	\N	0	.
1690941	Nilda	Bolanos Sosa	FEMENINO	1972-07-10	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	1691	.	\N	\N	\N	0	.
4710657	Agustína Gladys	Bolanos	MASCULINO	\N	PARAGUAYO/A	0984428965	UNION DE HECHO	\N	\N	\N	SI	NO	1555	Pajagua Naranja	\N	\N	\N	0	.
6143455	Barbara Beatriz	Bolaño	FEMENINO	\N	PARAGUAYO/A	09944560123	UNION DE HECHO	\N	\N	\N	SI	NO	1166	Fidelina	\N	\N	\N	0	La Familia vive en casa de la abuela del conyuge, desde la inundacion.
6807892	Esteban Ramón	Bolaños Ávalos	MASCULINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	1293	.	\N	\N	\N	0	El muchacho tiene un negocio en el lugar. El papa de Esteban tiene ventas de Ropas y Championes.
5373765	Mariel Carolina	Bolaños Ortíz	FEMENINO	\N	PARAGUAYO/A	0962161904	UNION DE HECHO	\N	\N	\N	SI	NO	650	23 de Junio	\N	\N	\N	0	La pareja tiene 4 años de concubinato tienen lote propio y techo precario, en su patio vive su madre con su familia en un techo precario aparte.
2042916	Arnaldo Arnulfo	Bolaños Peralta	MASCULINO	\N	PARAGUAYO/A	0984305757	.	\N	\N	\N	SI	NO	1938	Santa Librada	\N	\N	\N	0	.
1485117	Mirian Estelan	Bolaños Sosa	FEMENINO	\N	PARAGUAYO/A	0981775541	.	\N	\N	\N	SI	NO	2148	Virgen de Guadalupe	\N	\N	\N	0	.
938556	José Rafael	Bolaños	MASCULINO	\N	PARAGUAYO/A	0981502764	CASADO/A	\N	\N	\N	SI	NO	2023	Mbo'ehara	\N	\N	\N	5	.
1709902	Erasmo León	Bordessolles Chaparro	MASCULINO	1971-07-03	PARAGUAYO/A	0972112087	UNION DE HECHO	\N	\N	\N	SI	NO	239	Angel Luis	\N	\N	\N	0	.
4722187	Daisy Lorena	Borja Cardozo	FEMENINO	\N	PARAGUAYO/A	0983393646	UNION DE HECHO	\N	\N	\N	SI	NO	929	Pasillo Angel Luis	\N	\N	\N	0	.
1526213	Bernardino	Borja López	MASCULINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1265	.	\N	\N	\N	0	.
4918392	Elva Ramona	Borja Martínez	FEMENINO	\N	PARAGUAYO/A	0984139416	UNION DE HECHO	\N	\N	\N	SI	NO	291	Ursicino Velasco	\N	\N	\N	0	.
3537421	Gilda Marcelina	Brasel Benítez	FEMENINO	\N	PARAGUAYO/A	0981912099	UNION DE HECHO	\N	\N	\N	SI	NO	541	Martín Fierro c/ Mtro Urcicino Velazco	\N	\N	\N	0	La ocupante manifiesta que el terreno cuenta con una construcción de 96 mts cuadrado de dos plantas
2151482	Ede Elvira	Brassel Benítez	FEMENINO	\N	PARAGUAYO/A	0984 270 485	SOLTERO/A	\N	\N	\N	SI	NO	1870	Ursicino Velazco	\N	\N	\N	0	Vive dentro del lote de su mamá. Ede y Cristhian estan acompañados hace dos años.
2151459	Niño Maríana	Brassel Benítez	MASCULINO	\N	PARAGUAYO/A	0972 170 613	UNION DE HECHO	\N	\N	\N	SI	NO	1868	Ursicino Velazco	\N	\N	\N	0	Es hijo e la propietaria. Vive dentro del lote en una vivienda totalmente independiente y bien constituida.
3537399	Osvaldo Antonio	Brassel Benítez	MASCULINO	1974-07-03	PARAGUAYO/A	0981 127 413	UNION DE HECHO	\N	\N	\N	SI	NO	1866	Ursicino Velazco	\N	\N	\N	0	Es propietario de la vivienda.Actualmente vive en alquiler porque firmo contrato durante la inundación.
6809204	Carlos Daniel	Brítez Ayala	MASCULINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1404	.	\N	\N	\N	0	El Sr. Daniel vive en la casa de su mama con su pareja , tiene una construccion en la parte superior en reparacion.
2118449	Agustín	Brítez Azuaga	MASCULINO	2052-05-03	PARAGUAYO/A	0982848789	SOLTERO/A	\N	\N	\N	SI	NO	925	Nuevo amanecer	\N	\N	\N	4	Canoas- 6
3817438	César Agustín	Brítez Cassera	MASCULINO	\N	PARAGUAYO/A	0985244646	UNION DE HECHO	\N	\N	\N	SI	NO	927	Nuevo amanecer	\N	\N	\N	0	.
3825675	Daisy Dahiana	Brítez Cassera	FEMENINO	1987-11-04	PARAGUAYO/A	0981877990	UNION DE HECHO	\N	\N	\N	SI	NO	420	Julio Jara C/ Amanecer	\N	\N	\N	0	.
294	Angela	Brítez de Cabañas	FEMENINO	\N	PARAGUAYO/A	0992543978	CASADO/A	\N	\N	\N	SI	NO	485	Remancito	\N	\N	\N	0	Vive dentro del terreno de la mama , cuenta con vivienda precaria.
1107840	Juliána	Brítez de Prieto	MASCULINO	\N	PARAGUAYO/A	0982341879	CASADO/A	\N	\N	\N	SI	NO	419	Julio Jara C/Amanecer	\N	\N	\N	0	Vive ene el mismo terreno de la Sra. Felicita Barrios.
1052878	Emilia	Brítez Figueredo	FEMENINO	2065-02-03	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	588	8 de Diciembre	\N	\N	\N	0	Gabriela Acarolina Olivera esta embarazada de 2 meses dentro de la flia. Hay 2 parejas mayores de edad sin techoy terreno.
870822	Julián	Brítez Figueredo	MASCULINO	\N	PARAGUAYO/A	0972739914	SEPARADO/A	\N	\N	\N	SI	NO	1901	Julio Benitez	\N	\N	\N	0	En la Casa de la madre luego quedo el como titular.
3528741	Eva Viviana	Brítez Geraldo	FEMENINO	\N	PARAGUAYO/A	0981872065	UNION DE HECHO	\N	\N	\N	SI	NO	2341	Nuestra Sra de la Asunción	\N	\N	\N	6	Victo y Juan son hijos de Mirta, pareja de Eva
5959918	Liz Noelia	Brítez Geraldo	FEMENINO	1991-07-07	PARAGUAYO/A	0972865950	SOLTERO/A	\N	\N	\N	SI	NO	2344	Nuestra Señora de la Asunción	\N	\N	\N	6	.
3528802	Vicente Alberto	Brítez Geraldo	MASCULINO	\N	PARAGUAYO/A	0986834221	UNION DE HECHO	\N	\N	\N	SI	NO	2346	Nuestra Señora de la Asuncion	\N	\N	\N	6	.
4265933	Gervacia Estela	Brítez Jara	FEMENINO	1984-04-05	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	718	Mar del Plata	\N	\N	\N	5	Vive dentro del lote y de la vivienda de Alberta Ramona Jara.
2804425	Clara	Cabañas	FEMENINO	2067-12-08	PARAGUAYO/A	0971 550 425	UNION DE HECHO	\N	\N	\N	SI	NO	1987	Yacare Yrupe c/ Pilcomayo	\N	\N	\N	5	.
3456011	Norma Beatriz	Cañete	FEMENINO	1979-02-12	PARAGUAYO/A	0972484316	UNION DE HECHO	\N	\N	\N	SI	NO	97	Virgen de Lourdes	\N	\N	\N	3	.
3292013	Maura	Brítez Jara	FEMENINO	\N	PARAGUAYO/A	0981374313	UNION DE HECHO	\N	\N	\N	SI	NO	370	MAR DELPLATA y Virgen de Lourdes	\N	\N	\N	0	La hija de la titular esta concuvinada y tiene un techo precario dentro de lote, pareja sin hijos.Centro del lote tiene una construccion de un Oratorio  llamado Centro de Apostulado  con 15años de construccion y mide 4,30 de frente y 10 de fondo.
4887089	Rossana Ramona	Brítez Maciel	FEMENINO	1989-09-12	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	253	Angel Luis	\N	\N	\N	0	.
5234413	Paola Elizabeth	Brítez Martínez	FEMENINO	\N	PARAGUAYO/A	0971724360	UNION DE HECHO	\N	\N	\N	SI	NO	1256	Urcicino Velzaco e/ pajagua naranja	\N	\N	\N	0	.
2648566	Vicente Ramón	Brítez Martínez	MASCULINO	\N	PARAGUAYO/A	0983473925	CASADO/A	\N	\N	\N	SI	NO	2147	Virgen de Guadalupe	\N	\N	\N	0	La pareja tiene un hijo con Hidrocefalia no se maneja solo.
2214493	De los Santos	Brítez Talavera	MASCULINO	1970-01-11	PARAGUAYO/A	0992668424	SOLTERO/A	\N	\N	\N	SI	NO	371	YVOTY C/ Virgen de Lourdes	\N	\N	\N	0	Dentro del nucleo fliar. De la Sra. De los Santa Britez hay 2 parejas mayores de edad sin hijos ni terreno con un techo precario.
5334752	Jessica Magdalena	Brítez Toledo	FEMENINO	\N	PARAGUAYO/A	0986922389	UNION DE HECHO	\N	\N	\N	SI	NO	1461	Ursicino Velasco	\N	\N	\N	0	La pareja lleva 8 anos de novios y 4 anos de concubinato con un hijo menor.
764039	Altagracia	Brítez	FEMENINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	1612	Ursicino Velasco c/ San Martin de Porres	\N	\N	\N	0	La Sra. Atagracia tiene una despensa con una dimension 7x4 con un galpon frontal.Tambien se dedica a vender asadito.
1182414	Eulalia Almeida	Brítez	FEMENINO	2056-12-02	PARAGUAYO/A	0982163669	SEPARADO/A	\N	\N	\N	SI	NO	1132	Virgen de Lourdes	\N	\N	\N	0	.
7435634	Fermina	Brítez	FEMENINO	\N	PARAGUAYO/A	0972 724 285	SOLTERO/A	\N	\N	\N	SI	NO	1758	Dormitorio y cocina dentro de la pieza.	\N	\N	\N	0	.
1107941	Ignacio	Brítez	MASCULINO	\N	PARAGUAYO/A	0981470497	CASADO/A	\N	\N	\N	SI	NO	973	Algarrobo	\N	\N	\N	0	.
1190758	María Susana	Brítez	FEMENINO	2064-11-08	PARAGUAYO/A	0984901039	SEPARADO/A	\N	\N	\N	SI	NO	140	Las Orquideas	\N	\N	\N	4	En esta vivienda vive con la titular su hija quien es madre soltera de un menor
3211069	Mercedes	Brítez	FEMENINO	\N	PARAGUAYO/A	0992641990	UNION DE HECHO	\N	\N	\N	SI	NO	2557	Ko'eti	\N	\N	\N	0	dentro del lote de la titular vive cuatro nucleos de flia. Uno de ellos bajo el mismo techo y dos de ellos con techo independiente.
164765	Norberto	Britez	MASCULINO	2029-06-06	PARAGUAYO/A	0982752415	VIUDO/A	\N	\N	\N	SI	NO	1237	Pajagua Naranja	\N	\N	\N	0	.
983720	Gilberto	Britos Talavera	MASCULINO	2062-02-02	PARAGUAYO/A	0986 232 126	SOLTERO/A	\N	\N	\N	SI	NO	1937	Martin Fierro	\N	\N	\N	3	Hace un año y cuatro meses que el titular del techo C está separado.
1006642	Jacinta	Britos	FEMENINO	2064-11-09	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	484	Remancito	\N	\N	\N	0	.
1558116	María Lourdes	Brizuela Benítez	FEMENINO	\N	PARAGUAYO/A	0971759306	CASADO/A	\N	\N	\N	SI	NO	2652	Mtro Urcisino velazco	\N	\N	\N	0	.
1356646	Ramón Abrhan	Brizuela Brítez	MASCULINO	\N	PARAGUAYO/A	0992 396 784	VIUDO/A	\N	\N	\N	SI	NO	1844	Ursicino Velazco	\N	\N	\N	0	Tiene una panaderia de más de 35 años de funcionamiento.
1099596	Armando	Brizuela	MASCULINO	2062-08-06	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	1585	Ramon Talavera	\N	\N	\N	6	.
4637927	Claudia Noemi	Bruno Barrios	FEMENINO	\N	PARAGUAYO/A	0984422041	UNION DE HECHO	\N	\N	\N	SI	NO	2373	Nuestra Sra. De la Asunción	\N	\N	\N	6	Aire Portatil,Cinta para Caminar,Cardiobici 1 c/u.
919157	Domingo Beato	Burgos Ramírez	MASCULINO	\N	PARAGUAYO/A	0985749349	SOLTERO/A	\N	\N	\N	SI	NO	2099	.	\N	\N	\N	0	.
4226494	Luz Marina	Caballero Amarilla	FEMENINO	1985-06-05	PARAGUAYO/A	0982423328	SEPARADO/A	\N	\N	\N	SI	NO	777	Jakare Yrupe	\N	\N	\N	0	La vivienda cuenta con un galpon. El lote fue comprdo hace 31 anos ya estaba la construcción.
5639656	Lucía	Caballero Cantero	FEMENINO	1980-04-12	PARAGUAYO/A	0985571003	SOLTERO/A	\N	\N	\N	SI	NO	398	Pasillo Algarobo	\N	\N	\N	0	Tiene su pareja que no se quiso censar.
2536004	Asteria	Caballero de Portillo	FEMENINO	\N	PARAGUAYO/A	0981420404	.	\N	\N	\N	SI	NO	222	Virgen de Lourdes	\N	\N	\N	0	.
5200330	Cinthia Soledad	Caballero González	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	413	Julio Jara y Nuevo Amanecer	\N	\N	\N	0	Viven dentro del terreno de la mama Francisca Gonzales.
4971744	Maríana	Caballero González	FEMENINO	\N	PARAGUAYO/A	0986940724	UNION DE HECHO	\N	\N	\N	SI	NO	414	.	\N	\N	\N	0	Vive dentro del terreno de la mama, tiene una pieza pegada a a la casa de la hna.
4887081	Jonathan Javier	Caballero Ocampos	FEMENINO	\N	PARAGUAYO/A	0986612837	UNION DE HECHO	\N	\N	\N	SI	NO	1124	Virgen de Lourdes	\N	\N	\N	0	.
6956410	Walter Rodrigo	Caballero Ocampos	MASCULINO	\N	PARAGUAYO/A	0994351381	UNION DE HECHO	\N	\N	\N	SI	NO	1125	Virgen de Lourdes	\N	\N	\N	0	.
3661701	Juan José	Caballero Páez	MASCULINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	969	Algarrobo	\N	\N	\N	0	La Flia. Vive dentro de la casa de la madre.
4791943	Liz Ramona	Caballero Salazar	FEMENINO	\N	PARAGUAYO/A	0971766306	UNION DE HECHO	\N	\N	\N	SI	NO	1787	Bañado koeti	\N	\N	\N	5	Es propietaria del lote la casa le fue cedida con ella viven sus hermanos con sus parjas e hijos en extenciones de la vivienda lleva 4 anos con su pareja.
4791944	Richard Daniel	Caballero Salazar	MASCULINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1789	Bañado Koeti	\N	\N	\N	0	Vive dentro de una extencion con su pareja e hija no conoce algunos datos de su pareja llevan en pareja 5 anos , es una pareja inestable.
4791942	Rosalia Isabel	Caballero Salazar	FEMENINO	1996-02-01	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1788	Bañado Koeti	\N	\N	\N	5	Vive dentro del lote y la vivienda de su hermana en una extencion lleva 2 anos con su pareja.
1750717	Miguela	Caballero vda. De Alegre	FEMENINO	\N	PARAGUAYO/A	0994153350	VIUDO/A	\N	\N	\N	SI	NO	392	.	\N	\N	\N	0	La casa tiene 2 salones en frente uno utiliza para merceria y el otro para su bodega.
1035779	Pablino Osmar	Caballero	MASCULINO	2064-07-09	PARAGUAYO/A	0992645505	UNION DE HECHO	\N	\N	\N	SI	NO	1122	Virgen de Lourdes C/ Virgen de Lujan	\N	\N	\N	0	.
2633222	Juan Ramón	Cabañas Arzamienda	MASCULINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	86	Virgen de Lujan y Virgen de Lourdes	\N	\N	\N	3	.
3633737	María Aparecida	Cabañas de Caniza	FEMENINO	\N	PARAGUAYO/A	0972651890	CASADO/A	\N	\N	\N	SI	NO	725	Ursicino Velasco	\N	\N	\N	0	Tiene un copetin bien constituido en lo que ademas tiene un comedor.
2473817	María Delosanta	Cabañas Díaz	FEMENINO	\N	PARAGUAYO/A	0984604769	UNION DE HECHO	\N	\N	\N	SI	NO	189	Mto. Ursiscino Velasco	\N	\N	\N	0	.
6629102	María Dolores	Cabañas Díaz	FEMENINO	\N	PARAGUAYO/A	0991890749	SEPARADO/A	\N	\N	\N	SI	NO	346	AROMITA	\N	\N	\N	0	.
5378709	Martín Ramón	Cabañas Díaz	MASCULINO	\N	PARAGUAYO/A	0986228260	UNION DE HECHO	\N	\N	\N	SI	NO	909	Maestro Ursicino Velasco	\N	\N	\N	0	.
3234870	Luis	Cabañas Domínguez	MASCULINO	\N	PARAGUAYO/A	0986 200 320	.	\N	\N	\N	SI	NO	2243	Urcisino Velazco	\N	\N	\N	0	.
1843083	Angelica	Cabañas Leguizamon	FEMENINO	\N	PARAGUAYO/A	0981695216	UNION DE HECHO	\N	\N	\N	SI	NO	1483	Mainumby c/ Fidelina	\N	\N	\N	0	.
1713628	Mirna Sixta	Cabañas Rodríguez	FEMENINO	\N	PARAGUAYO/A	0985756742	SEPARADO/A	\N	\N	\N	SI	NO	2195	Mbo-ehara c/ Sotero Colman	\N	\N	\N	0	1 corredor
6658086	Gloria Isabel	Cabañas	FEMENINO	\N	PARAGUAYO/A	0983729171	UNION DE HECHO	\N	\N	\N	SI	NO	191	Mto. Ursicino Velasco	\N	\N	\N	0	Vive actualmente en la casa de su abuela Maria en la M49 L25 , mientras levanta su casa .En su lote se observa in cimiento para 2 piezas de 5x4 antes de la inundacion empezo a construir.
1103616	Graciela	Cabañas	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	706	Mar del Plata	\N	\N	\N	5	.
4516606	Laura Elizabeth	Cabañas	FEMENINO	1983-12-09	PARAGUAYO/A	0971 550 927	UNION DE HECHO	\N	\N	\N	SI	NO	1991	Yacare Yrupe c/ Pilcomayo	\N	\N	\N	3	Hace un año esta concubinada con su actual pareja y padre de su hijo en gestación.
4275181	Nilda Concepción	Cabañas	FEMENINO	1984-11-11	PARAGUAYO/A	0971576325	UNION DE HECHO	\N	\N	\N	SI	NO	913	Maestro Ursicino Velasco	\N	\N	\N	0	centrifugadora-1
1744342	Dionisio Ramón	Cabral Villalba	MASCULINO	2043-12-12	PARAGUAYO/A	390192	.	\N	\N	\N	SI	NO	922	Nuevo Amanecer	\N	\N	\N	0	Canoa-1
5164404	Juan Antonio	Cabrera Adorno	MASCULINO	1993-03-03	PARAGUAYO/A	0994275040	UNION DE HECHO	\N	\N	\N	SI	NO	1572	Ursicino Velasco	\N	\N	\N	0	.
3889542	Felicita	Cabrera Báez	FEMENINO	1975-07-05	PARAGUAYO/A	0984 448 397	UNION DE HECHO	\N	\N	\N	SI	NO	2317	Juan León Mallorquín	\N	\N	\N	6	.
5421595	Digna	Cabrera Bogado	FEMENINO	\N	PARAGUAYO/A	0971578944	CASADO/A	\N	\N	\N	SI	NO	1257	Fidelina	\N	\N	\N	0	Digna Cabrera vive con la madre en la misma vivienda comparte bano y cocina.
3449655	Eugenia	Cabrera Bogado	FEMENINO	\N	PARAGUAYO/A	0971159498	UNION DE HECHO	\N	\N	\N	SI	NO	775	26 de Julio	\N	\N	\N	0	.
3704371	Luisa	Cabrera bogado	FEMENINO	\N	PARAGUAYO/A	0981613426	UNION DE HECHO	\N	\N	\N	SI	NO	1489	payagua naranja	\N	\N	\N	0	la vivienda de material esta unida a otra construcción  provida por medio de pequeño tinglado de techo chapa zinc.
4253147	Natalia	Cabrera Bogado	FEMENINO	\N	PARAGUAYO/A	0971357573	UNION DE HECHO	\N	\N	\N	SI	NO	1207	Fidelina	\N	\N	\N	0	Salon-Pasilli. Cuentan con titulo de propiedad ,hace 5 anos comprola casa.
3173706	Ysabel	Cabrera Bogado	FEMENINO	1976-06-11	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	1258	Fidelina	\N	\N	\N	0	.
2843404	Gladys Soledad	Cabrera Brítez	FEMENINO	1974-05-09	PARAGUAYO/A	0994 587 915	UNION DE HECHO	\N	\N	\N	SI	NO	1761	.	\N	\N	\N	0	El baño ccomparte con la señora Fermina, su mamá.
4227077	Viviana Elizabeth	Cabrera Capdevila	FEMENINO	1983-08-10	PARAGUAYO/A	0994837482	UNION DE HECHO	\N	\N	\N	SI	NO	215	Virgen de Lujan y Virgen de Lourdes	\N	\N	\N	0	.
3601037	Miguel Ángel	Cabrera Cardénas	MASCULINO	\N	PARAGUAYO/A	0992526652	UNION DE HECHO	\N	\N	\N	SI	NO	2125	San Martin de Porres	\N	\N	\N	0	Vive dentro de la casa de la mama en una pieza con su esposa e hija.
618455	Catalino	Cabrera Chaparro	MASCULINO	\N	PARAGUAYO/A	0981487433	CASADO/A	\N	\N	\N	SI	NO	2251	Mto. Ursicino Velasco	\N	\N	\N	0	En la Parte superior tiene una construcción que mide 8x5 con dos dormitorios y un bano.
598439	Dionicia	Cabrera de Morel	FEMENINO	2046-06-12	PARAGUAYO/A	0984458949	CASADO/A	\N	\N	\N	SI	NO	1180	Urcicino Velazco	\N	\N	\N	0	Es dueña del lote en el viven sus dos hijos con sus familias
2638307	Alba Rosa	Cabrera de Valdéz	FEMENINO	1974-04-03	PARAGUAYO/A	0982935449	CASADO/A	\N	\N	\N	SI	NO	1368	Yvoty Rory	\N	\N	\N	0	.
1075172	Louisa Beatriz	Cabrera Franco	FEMENINO	\N	PARAGUAYO/A	0991535399	UNION DE HECHO	\N	\N	\N	SI	NO	1372	Yvoty Rory	\N	\N	\N	0	Vitrina: 1.
4730544	Juan Daniel	Cabrera Maidana	MASCULINO	1990-01-01	PARAGUAYO/A	.	SEPARADO/A	\N	\N	\N	SI	NO	1571	.	\N	\N	\N	0	vive dentro del mismo lote de la propietaria y tiene una construccion bien constituida de 6x9.
4077551	Cynthia Elizabeth	Cabrera Ovelar	FEMENINO	1984-01-01	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	1750	Ursicino Velasco	\N	\N	\N	0	.
6196328	Edgar Fabián	Cabrera Ovelar	MASCULINO	\N	PARAGUAYO/A	0982414247	UNION DE HECHO	\N	\N	\N	SI	NO	1530	Mtro Ursicino Velasco	\N	\N	\N	0	la pareja refieren que tienen 3 años de convivencia, tienen una amplia construcción totalmente independiente
4421137	Richard Ariel	Cabrera Ovelar	MASCULINO	1987-07-11	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1245	Yvoty Rory	\N	\N	\N	0	.
4156092	Fernando Daniel	Cabrera	MASCULINO	\N	PARAGUAYO/A	0972609934	SOLTERO/A	\N	\N	\N	SI	NO	1466	Ursicino Velasco	\N	\N	\N	0	El Sr. Fernando vive bajo el mismo techo de la madre quien es la titular del lotey la casa.El mismo esta separado de la madre de sus hijos hace 2 anos mantiene a su flia. A travez de su ingreso como mecanico vive en una de las piezas de la casa de su mad
1105854	Guillermina	Cabrera	FEMENINO	2060-10-02	PARAGUAYO/A	0972835990	UNION DE HECHO	\N	\N	\N	SI	NO	1463	Ursicino Velasco	\N	\N	\N	0	Los titulares viven en el barrio hace 6 anos(compraron la casa) ,anteriormente vivian en Sajonia ,por motivos economicos llegaron al Banado.
977727	Juan Isabelino	Cabrera	MASCULINO	\N	PARAGUAYO/A	0986348461	VIUDO/A	\N	\N	\N	SI	NO	214	Virgen de Lujan y Virgen de Lourdes	\N	\N	\N	0	.
3794330	Rolando	Cabrera	MASCULINO	1982-01-02	PARAGUAYO/A	0986651436	SOLTERO/A	\N	\N	\N	SI	NO	1467	Ursicino Velasco	\N	\N	\N	0	El Sr., Rolando vive bajo el mismo techo de la madre quien es la titular del lote, el mismo esta separado hace dos anos,mantiene a sus hijos atravez se su trabajo de mecanico.
4824912	Vicente Gabriel	Cabrera	MASCULINO	\N	PARAGUAYO/A	0981872138	UNION DE HECHO	\N	\N	\N	SI	NO	1469	Ursicino Velasco	\N	\N	\N	0	Centrifugadora:1. La pareja lleva 4 anos de concubinato, tienen una hija menor ,el joven mantiene su flia.
629563	Gustavo	Cáceres Acosta	MASCULINO	2057-01-09	PARAGUAYO/A	0981508766	CASADO/A	\N	\N	\N	SI	NO	1972	Nasaindy	\N	\N	\N	0	Es propietario del lote no quiere que la saque foto de la casa no responde algunos datos de la hoja censal. No responde datos sobre la vivienda.
1372763	Isabel	Cáceres Alcaraz	FEMENINO	2049-05-11	PARAGUAYO/A	0991693396	SOLTERO/A	\N	\N	\N	SI	NO	1542	Pajagua Naranja	\N	\N	\N	0	La titular cobra Tercera Edad.
5496044	Noelia Elizabeth	Cáceres Aquino	FEMENINO	1996-06-12	PARAGUAYO/A	0986 608911	UNION DE HECHO	\N	\N	\N	SI	NO	1714	Yacare Pito	\N	\N	\N	3	Dormitorio y cocina dentro de la casa.
4484604	Gisel Lorena	Cáceres Cabañas	FEMENINO	1991-03-11	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	797	Virgen de Luján	\N	\N	\N	0	.
4416692	Diego Arnaldo	Cáceres Chávez	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	1252	Pajagua Naranja]	\N	\N	\N	0	.
3484993	Gladys Beatriz	Cáceres Chávez	FEMENINO	1976-11-03	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1390	Payagua Naranja	\N	\N	\N	0	.
4489881	Guillermo Salomon	Cáceres Collar	MASCULINO	\N	PARAGUAYO/A	0994832372	.	\N	\N	\N	SI	NO	1973	Nasaindy	\N	\N	\N	0	Vive dentro del lote de la mama.
4513280	Lurdes Rosana	Cáceres Collar	FEMENINO	\N	PARAGUAYO/A	0971342839	UNION DE HECHO	\N	\N	\N	SI	NO	1975	Nasaindy	\N	\N	\N	0	Vive dentro del lote y la casa de la mama.
4513297	Matilde Baetriz	Cáceres Collar	FEMENINO	\N	PARAGUAYO/A	0982616014	.	\N	\N	\N	SI	NO	1977	Nasaindy	\N	\N	\N	5	Vive dentro del lote y la casa de la mama.
2170702	Hermelinda	Cáceres de Acosta	FEMENINO	\N	PARAGUAYO/A	0984631337	CASADO/A	\N	\N	\N	SI	NO	1613	Ursicino Velasco	\N	\N	\N	0	El terreno pertenecio al Sr. Proscopio Caceres fallecido hace dos meses.La duena actual posee documento que ese terreno le concedio su padre,posee una vivienda construida por la Cooperativa Banado Poty.
2837690	Elba Ramona	Cáceres de Giménez	FEMENINO	\N	PARAGUAYO/A	0994275103	CASADO/A	\N	\N	\N	SI	NO	1140	Virgen de Lujan	\N	\N	\N	0	.
2190866	Magdalena	Cáceres de Miñarro	FEMENINO	\N	PARAGUAYO/A	0982122527	CASADO/A	\N	\N	\N	SI	NO	2359	Nuestra Señora de la Asuncion	\N	\N	\N	6	dentro de la familia hay una pareja, ambos mayores de edad, embarazo de 6 meses
1841109	Valentina	Cáceres de Noceda	FEMENINO	\N	PARAGUAYO/A	0984527871	CASADO/A	\N	\N	\N	SI	NO	1431	Ursicino Velasco	\N	\N	\N	0	Los titulares tienen 3 hijos mayores sin pareja y sin hijos, cada uno cuenta con un techo independiente al techo de los padres.
3850726	Diego Manuel	Cáceres Franco	MASCULINO	1992-12-06	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1426	Ursicino Velasco	\N	\N	\N	0	La pareja lleva dos anos de concubinato no tienen lote propio viven bajo el techo de la madre quien es la titular.
5148296	Cristian Abrahan	Cáceres Gonzalez	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	2742	Los Sauces	\N	\N	\N	0	.
5163866	Liz Paola	Cáceres González	FEMENINO	\N	PARAGUAYO/A	0971172643	SOLTERO/A	\N	\N	\N	SI	NO	341	Los Sauces	\N	\N	\N	0	Cuenta Con un CORREDOR.
1764172	Alejandro	Cáceres Irala	MASCULINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	1330	23 de Junio	\N	\N	\N	0	.
6744017	Angelica	Cáceres Lugo	FEMENINO	1994-07-03	PARAGUAYO/A	0971867942	UNION DE HECHO	\N	\N	\N	SI	NO	1123	Virgen de Lourdes C/ Virgen de Lujan	\N	\N	\N	0	.
3865410	Estelvina	Cáceres Martínez	FEMENINO	\N	PARAGUAYO/A	0983631252	SOLTERO/A	\N	\N	\N	SI	NO	1898	Julio Benitez	\N	\N	\N	0	La Sra. Vive con la mama hace 27 anos con la hna que tiene mas antigüedad que ella en el lugar.
5670287	Silvia Ramona	Cáceres Ortiz	FEMENINO	\N	PARAGUAYO/A	0985797496	UNION DE HECHO	\N	\N	\N	SI	NO	9	San Felipe y Santiago	\N	\N	\N	0	Posee 2 Viviendas
2397351	Celestina Ignacia	Cáceres Ramírez	FEMENINO	1970-06-04	PARAGUAYO/A	0994632412	UNION DE HECHO	\N	\N	\N	SI	NO	1268	.	\N	\N	\N	0	.
3840660	Leoncia	Cáceres Ramírez	FEMENINO	2052-12-09	PARAGUAYO/A	0992684341	SEPARADO/A	\N	\N	\N	SI	NO	324	Mar del Plata	\N	\N	\N	0	.
1992900	Ramón	Cáceres Recalde	MASCULINO	\N	PARAGUAYO/A	0984797167	UNION DE HECHO	\N	\N	\N	SI	NO	2457	Ursicino Velasco	\N	\N	\N	0	En la Actualidad el propietario vive en alquiler en San Lorenzo.Se mudo por la crecida y en estos momentos vive el hno.Sebastian Caceres.
1327914	Brigida Dominga	Cáceres Robles	FEMENINO	2065-08-10	PARAGUAYO/A	0985465783	SOLTERO/A	\N	\N	\N	SI	NO	1656	Virgen de Guadalupe	\N	\N	\N	0	Cocina a Inducción 1.
999730	De los Santos	Cáceres Robles	MASCULINO	2056-01-11	PARAGUAYO/A	.	VIUDO/A	\N	\N	\N	SI	NO	1251	Pajagua Naranja	\N	\N	\N	0	.
2665262	Leopoldina	Cáceres Robles	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1259	Fidelina	\N	\N	\N	0	La vivienda es de la Cooperativa Banado Poty tiene una extencion de madera donde viven los hijos mayores.
3679132	Elva Carolina	Cáceres	FEMENINO	\N	PARAGUAYO/A	0972147955	CASADO/A	\N	\N	\N	SI	NO	502	San Felipe y Santiago	\N	\N	\N	0	.
285	Liliana	Cáceres	FEMENINO	\N	No quiso censar	.	.	\N	\N	\N	SI	NO	1065	Ursicino Velasco	\N	\N	\N	0	No quiso censarse.
4481643	Osvaldo Proscopio	Cáceres	MASCULINO	1987-11-07	PARAGUAYO/A	0984552129	UNION DE HECHO	\N	\N	\N	SI	NO	1615	Ursicino Velasco	\N	\N	\N	0	El bano comparte con la madre porque el suyo no se completo por la inundacion ,viven independientemente ,en una extension a la casa materna.La extension fue construida por propio medio de la pareja.
1278509	Benita	Campusano	FEMENINO	\N	PARAGUAYO/A	481686	.	\N	\N	\N	SI	NO	2162	Ursicino Velasco c/ Maria Auxiliadora	\N	\N	\N	0	.
80013763	CAMSAT - Obras Sociales	.	.	\N	.	.	.	\N	\N	\N	SI	NO	2468	Maestro Ursicino Velasco E/ San Felipe y S	\N	\N	\N	0	El lote le pertenece a Camsat, fue comprado en el año 1996. Cuenta con 2 edificaciones: 1 vivienda destinado al cesero de 71 m2, 3 salones de 189 m2 destinado a 2 aulas para la guarderia y 1 aula destinado a formaacion profesional. El representante legal
331	CAMSAT - Predio Deportivo	.	.	\N	.	.	.	\N	\N	\N	SI	NO	2487	Mto. Ursicino Velasco e/ San Felipe y Santiago	\N	\N	\N	0	El predio pertenece a CANSAT desde el ano 2007.Esta destinado a actividades deportivas y recreativas de la comunidad, el lote cuenta con una superficie de 8095 .Representante Legal EL Padre Pedro Velasco.
23	CAMSAT - RECICLA	.	.	\N	.	.	.	\N	\N	\N	SI	NO	149	San Felipe y Santiago	\N	\N	\N	0	Tiene2 oficinas y 2 piezas para los cuidadores de 5x4 c/u..Este predio es de la Organización CAMSAT, utilizado para una unidad productiva de Reciclado.
37002	Camsat - Unidad de Salud familiar	.	.	\N	.	.	.	\N	\N	\N	SI	NO	1287	Maestro Ursicino Velasco	\N	\N	\N	0	1 Camilla, 1 Estante, 1 Lavatorio de Consultorio, 1 mesa.
52008	Albino	Can	MASCULINO	\N	.	0984976865	.	\N	\N	\N	SI	NO	1006	Mar de plata	\N	\N	\N	0	La casa cuenta con un galpón. El dueño de la casa es pescador y se encuentra de viaje p/pescar, no conseguí manera de comunicarme con el mismo. ( Datos proporcionados por un vecino).
295	Cancha 3 de Mayo	.	.	\N	.	.	.	\N	\N	\N	SI	NO	476	Remancito	\N	\N	\N	0	.
1419	Cancha Rancho 8	.	.	\N	.	.	.	\N	\N	\N	SI	NO	2607	Rancho 8	\N	\N	\N	0	.
5721069	Alba María	Candia Avalos	FEMENINO	\N	PARAGUAYO/A	0983229038	SEPARADO/A	\N	\N	\N	SI	NO	572	Mavinas	\N	\N	\N	0	.
6666860	Arnaldo Gabriel	Candia Avalos	FEMENINO	1995-11-10	PARAGUAYO/A	0986224633	UNION DE HECHO	\N	\N	\N	SI	NO	570	Malvinas	\N	\N	\N	0	.
4354011	Emilce Daria	Candia Avalos	FEMENINO	1985-03-06	PARAGUAYO/A	0982254913	UNION DE HECHO	\N	\N	\N	SI	NO	286	Angel Luis	\N	\N	\N	0	.
5726817	Lidia Elizabeth	Candia Avalos	FEMENINO	1994-04-04	PARAGUAYO/A	0985742807	SOLTERO/A	\N	\N	\N	SI	NO	569	MaLVINAS	\N	\N	\N	0	La mama fallecio hace 15 meses y el papa les dejo la casa a los dos. La casa cuenta con un Oratorio de 3x 3.
3318	Domingo	Candia	MASCULINO	\N	.	.	SOLTERO/A	\N	\N	\N	SI	NO	28	Frente de Recicla	\N	\N	\N	0	.
4559555	Eva María	Candia	FEMENINO	\N	PARAGUAYO/A	0992590092	UNION DE HECHO	\N	\N	\N	SI	NO	206	Aromita	\N	\N	\N	0	.
6103528	Tatiana Beatriz	Candia	FEMENINO	\N	PARAGUAYO/A	0992757481	UNION DE HECHO	\N	\N	\N	SI	NO	1606	Nuestra Senora de la Asunción	\N	\N	\N	6	Actualmente esta en el refugio de la S.E.N., por motivos de la inundación.
3723908	Silvia Teresa	Cano Godoy	FEMENINO	\N	PARAGUAYO/A	0971892609	UNION DE HECHO	\N	\N	\N	SI	NO	1025	Mar de plata	\N	\N	\N	4	.
757343	Juan Emilio	Cano Piriz	MASCULINO	2061-03-07	PARAGUAYO/A	0981135927	CASADO/A	\N	\N	\N	SI	NO	2250	El sr. Juan Gabriel vive en la Zona 3 y fue censad	\N	\N	\N	0	.
618305	Florinda	Cantero Caballero	FEMENINO	2037-10-06	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	818	Vecinos Unidos	\N	\N	\N	0	.
4801929	Marcos José	Cantero Rodríguez	MASCULINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1160	Fidelina	\N	\N	\N	0	Vivian en su terreno propio pero despues de la inundacion viven con su madre.
1962391	Victoriana	Cantero Vda de López	FEMENINO	\N	PARAGUAYO/A	0982321821	VIUDO/A	\N	\N	\N	SI	NO	308	Virgen de Lourdes c/ Oleros	\N	\N	\N	0	.
1935395	De los Santos Ramón	Cañete Espínola	MASCULINO	2055-01-01	PARAGUAYO/A	00985286382	.	\N	\N	\N	SI	NO	2111	.	\N	\N	\N	0	sus casas se fundieron en la crecida
4560890	Silvia Benita	Cañete Vera	FEMENINO	1992-02-10	PARAGUAYO/A	0992478422	SOLTERO/A	\N	\N	\N	SI	NO	2066	Sotero Colman	\N	\N	\N	0	.
3209768	Isabelino	Cañete	FEMENINO	\N	PARAGUAYO/A	.	SEPARADO/A	\N	\N	\N	SI	NO	1478	Payagua Naranja	\N	\N	\N	0	El señor tenia su terreno al lado de la señora, hace tiempo hubo un inscendio y destruyó su casa muy precaria, desde lo sucedido vive con la señora.
4958414	Gladys Magali	Cañiza Espínola	FEMENINO	1991-12-10	PARAGUAYO/A	0992355877	UNION DE HECHO	\N	\N	\N	SI	NO	2731	Virgen de Lourdes y Aromita	\N	\N	\N	0	.
3825674	José Manuel	Capdevila Alonso	MASCULINO	\N	PARAGUAYO/A	0991725536	UNION DE HECHO	\N	\N	\N	SI	NO	423	Julio Jara	\N	\N	\N	0	Vive dentro del terreno de la mama Mirian Alonso , su construccion esta pegada a ala vivienda principal.
4517876	Yamili María José	Capdevila Alonso	MASCULINO	1990-02-12	PARAGUAYO/A	0983757903	UNION DE HECHO	\N	\N	\N	SI	NO	422	Julio Jara	\N	\N	\N	0	Vive en una casa de material debajo al de sus padres.
3923281	Liliana	Capdevila Arguello	FEMENINO	\N	PARAGUAYO/A	0972846063	UNION DE HECHO	\N	\N	\N	SI	NO	1328	Yacare Yrupe	\N	\N	\N	0	.
2699838	María Estela	Capdevila Duarte	FEMENINO	\N	PARAGUAYO/A	0971215356	UNION DE HECHO	\N	\N	\N	SI	NO	1302	Mto. Ursicino Velasco	\N	\N	\N	0	.
5260593	Diana Graciela	Capdevila	FEMENINO	\N	PARAGUAYO/A	0983753229	UNION DE HECHO	\N	\N	\N	SI	NO	1378	Yvoty Rory	\N	\N	\N	0	Vive dentro del lote de la mama en una vivienda de material bien constituida.
2146678	Manuela Graciela	Capdevila	FEMENINO	\N	PARAGUAYO/A	0991688927	SOLTERO/A	\N	\N	\N	SI	NO	1377	Yvoty Rory	\N	\N	\N	0	Es propietaria del lote al fondo del lote viven una hija con su esposo e hijos , por otro lado tambien vive dentro del mismo lote un hijo de 28 soltero en una vivienda precaria.
3918744	Mirta Elizabet	Capdevila	FEMENINO	\N	PARAGUAYO/A	0994707582	UNION DE HECHO	\N	\N	\N	SI	NO	397	Algarobo c/ Urcisino Velasco	\N	\N	\N	0	.
1901	Capilla María Auxiliadora	.	FEMENINO	\N	.	.	.	\N	\N	\N	SI	NO	1331	Urisicino Velasco	\N	\N	\N	0	.
9999902	Capilla San Miguel	.	.	\N	.	.	.	\N	\N	\N	SI	NO	2364	Ursicino Velasco	\N	\N	\N	0	Iglesia:Frente:9,40 Fondo:23,70.
80033029	Capilla Santa Ana	.	.	\N	.	0994700425	.	\N	\N	\N	SI	NO	1090	Vecinos Unidos	\N	\N	\N	0	.
1198973	Marta Raquel	Cardénas de Ávalos	FEMENINO	2067-10-12	PARAGUAYO/A	0984 433 980	CASADO/A	\N	\N	\N	SI	NO	2588	27 de Mallorquin	\N	\N	\N	0	.
308490	Zulma	Cardénas de Cabrera	FEMENINO	\N	PARAGUAYO/A	0991199927	CASADO/A	\N	\N	\N	SI	NO	2124	San Martin de Porres	\N	\N	\N	0	.
2175104	Graciela Auxiliadora	Cardénas	FEMENINO	\N	PARAGUAYO/A	0984644219	SEPARADO/A	\N	\N	\N	SI	NO	2533	Mandyjura y Rancho 8	\N	\N	\N	0	Dentro del lote de la titular viven tres nucleos de famillias,Bajo el mismo techo en distintos dormitorios.
4908135	Jorge Francisco	Cardénas	MASCULINO	1989-03-10	PARAGUAYO/A	0994840364	UNION DE HECHO	\N	\N	\N	SI	NO	2536	Mandyjura yRancho 8	\N	\N	\N	0	1 Placa Electrica.
4363251	Rocio Noemi	Cardénas	FEMENINO	\N	PARAGUAYO/A	0992254915	UNION DE HECHO	\N	\N	\N	SI	NO	2541	Mandyjura y Rancho 8	\N	\N	\N	0	La pareja lleva cinco años de concubinato,Utilizan un dormitorio dentro de la casa  de la titular.
4996774	Dalma Vanessa	Cardozo Acosta	FEMENINO	\N	PARAGUAYO/A	0982814394	UNION DE HECHO	\N	\N	\N	SI	NO	1239	Pajagua Naranja	\N	\N	\N	0	.
1825534	Hermelinda	Cardozo Adorno	FEMENINO	\N	PARAGUAYO/A	0991300097	SOLTERO/A	\N	\N	\N	SI	NO	862	Ursicino Velasco	\N	\N	\N	0	La titular lleva dos meses separada de su marido por violencia domestica , el mismo sigue viviendo bajo el mismo techo.La casa es dos pisos la titular vive arriba y se ex amrido abajo.
2130047	Ramona Isabel	Cardozo de Benitez	MASCULINO	2061-02-07	PARAGUAYO/A	0972552007	SOLTERO/A	\N	\N	\N	SI	NO	1296	Ursicino Velasco	\N	\N	\N	0	El propietario del terreno actualmente se encuentra trabajando en argentina aproximadamente viene cada 4 meses, dentro de la propiedad viven tres hijos con sus parejas.
7579935	Guillermina	Cardozo Fernández	FEMENINO	\N	PARAGUAYO/A	0983241384	SOLTERO/A	\N	\N	\N	SI	NO	2602	Rancho 8	\N	\N	\N	0	Dentro del lote viven tres nucleos de familia y dos techos.
6569614	Eugenio	Cardozo Godoy	MASCULINO	2063-01-01	PARAGUAYO/A	0971390415	UNION DE HECHO	\N	\N	\N	SI	NO	365	SIN NOMBRE	\N	\N	\N	0	.
3252946	Rosalina	Cardozo Inchausti	FEMENINO	\N	PARAGUAYO/A	0986 267 546	UNION DE HECHO	\N	\N	\N	SI	NO	2005	Pilcomayo	\N	\N	\N	0	.
3981445	Pedro Daniel	Cardozo Rodríguez	MASCULINO	\N	PARAGUAYO/A	0972649901	UNION DE HECHO	\N	\N	\N	SI	NO	1094	Fidelina	\N	\N	\N	0	La vivienda esta construida por pro vida de bloque no tiene cocina.
5054236	Leticia Gissela	Cardozo Vega	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	514	Ursicino Velasco	\N	\N	\N	0	Vive dentro del lote de la mama en una extensión de la vivienda.
4636605	Analia Tamara	Cardozo	FEMENINO	\N	PARAGUAYO/A	0971741574	UNION DE HECHO	\N	\N	\N	SI	NO	1367	Yvoty Rory	\N	\N	\N	0	.
1209092	Gladys Hilda	Cardozo	FEMENINO	2061-07-09	PARAGUAYO/A	0982512390	UNION DE HECHO	\N	\N	\N	SI	NO	524	Malvinas C/ 16 de Agosto	\N	\N	\N	0	.
2034516	Mirtha Ramona	Cardozo	FEMENINO	1972-04-04	PARAGUAYO/A	0994284052	SOLTERO/A	\N	\N	\N	SI	NO	383	YVOTY	\N	\N	\N	0	La Sra. Cuenta con un lote propio en la zona 4, atrás de recicla no pudo construir todavia su vivienda por problemas economicos.Vive temporalmente en la casa de su hija.
4770602	Rosaura Noemi	Cardozo	FEMENINO	\N	PARAGUAYO/A	0985452444	SOLTERO/A	\N	\N	\N	SI	NO	730	Mto. Ursicino Velasco	\N	\N	\N	0	Vive dentro del lote del abuelo, pero tienen una casa precaria aparte.
3921782	Yahari	Carmen	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1172	Urcicino Velazco	\N	\N	\N	0	el hijo vive dentro del lote de Inocencia Prieto al fondo en una vivienda precaria
4566274	Luz Marina	Carmona Godoy	FEMENINO	1985-04-04	PARAGUAYO/A	0984698396	UNION DE HECHO	\N	\N	\N	SI	NO	322	Los sauces	\N	\N	\N	0	.
3224	Maxim	Casa Parroqioal de la Fraternidad Misionera	.	\N	.	.	.	\N	\N	\N	SI	NO	2750	.	\N	\N	\N	0	.
1729	Casa Vacia	.	.	\N	.	.	.	\N	\N	\N	SI	NO	2455	.	\N	\N	\N	0	Vivienda precaria desabitada vecinos y desconocen nombre del propietario.
18	Casa Vacia	.	.	\N	.	.	.	\N	\N	\N	SI	NO	114	Oleros	\N	\N	\N	0	Vivienda desavitada en construcción.
4718	Casa Vacía - Capitan Benítez	.	.	\N	.	.	.	\N	\N	\N	SI	NO	2409	.	\N	\N	\N	0	Según vecinos el propietario no frecuenta habitualmente la vivienda, el propietario no vive en la casa, vecinos desconocen datos.
3610438	Roque	Casa Vacía - Guanes	MASCULINO	\N	PARAGUAYO/A	0981711858	.	\N	\N	\N	SI	NO	547	.	\N	\N	\N	0	La vivienda desabitada hace tres meses. Actualmete vive en la zona 3, en la casa de su pareja
5107283	Marcos Josafat	Casa Vacía - Guillen González	MASCULINO	\N	.	0986416375	.	\N	\N	\N	SI	NO	1521	Rancho 8	\N	\N	\N	0	Actualmente la casa esta en construccion ,no pudieron continuar antes por la crecida.
378	Clotilde	Casa Vacia - Lugo vda de Baezquen	FEMENINO	\N	.	.	.	\N	\N	\N	SI	NO	633	La casa esta vacia , el dueno vive en Sajonia.	\N	\N	\N	0	.
3250	Susana	Casa Vacía - Vidal Benítez	FEMENINO	\N	.	.	.	\N	\N	\N	SI	NO	2524	Pasillo sin nombre	\N	\N	\N	0	Casa abandonada. Según vecinos la casa lleva 10 años desabitada. Vive en el bañado en la zona 2, en la casa de la madre. Se censó en el lote 29 de la manzana 37.
4326	Felicia	Casa Vacia - Villalba	FEMENINO	\N	.	.	.	\N	\N	\N	SI	NO	2499	Oleros	\N	\N	\N	0	Según vecinos la propietaria vive en Itaugua, desde la primera inundacion desconocemos datos de la misma
1046033	Mauro	Casco Olmedo	MASCULINO	\N	PARAGUAYO/A	0991981989	VIUDO/A	\N	\N	\N	SI	NO	559	Ursicino Velasco e/ 8 de Diciembre	\N	\N	\N	0	.
3306847	Antonio Ramón	Casco	MASCULINO	\N	PARAGUAYO/A	0971752926	UNION DE HECHO	\N	\N	\N	SI	NO	2136	Agosto Poty	\N	\N	\N	0	.
2229440	Irina Valentina	Casco	FEMENINO	\N	PARAGUAYO/A	0971653273	.	\N	\N	\N	SI	NO	2135	Agosto Poty	\N	\N	\N	0	.
2473013	María Zulema	Casco	FEMENINO	\N	PARAGUAYO/A	0983285889	.	\N	\N	\N	SI	NO	2134	Agosto Poty	\N	\N	\N	0	.
3665993	Nilsa Calolina	Cassera Cabrera	FEMENINO	\N	PARAGUAYO/A	0992296037	SOLTERO/A	\N	\N	\N	SI	NO	1075	Mar de plata	\N	\N	\N	0	vive dentro del lote de la madre, tiene una construcción debajo del palafito.
2113951	Silvia	Cassera Cabrera	FEMENINO	2069-03-11	PARAGUAYO/A	0972244584	SOLTERO/A	\N	\N	\N	SI	NO	1064	Mar de Plata	\N	\N	\N	0	Actualmente viven en un alquiler, apenas termine el contrato piensan volver a su casa, refiere la titular.
2919340	Cristina	Cassera de Báez	MASCULINO	1977-12-07	PARAGUAYO/A	0982746384	CASADO/A	\N	\N	\N	SI	NO	1067	Mar de plata	\N	\N	\N	0	.
6182595	Juan Ramón	CaStellano Godoy	MASCULINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	880	Nuevo Amanecer	\N	\N	\N	0	La casa e material tiene una extensión de madera precaria
6155553	Pedro Simón	Castellano Godoy	MASCULINO	1994-09-06	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	882	Nuevo Amanecer	\N	\N	\N	0	.
7348214	María Victoria	Castellano	FEMENINO	\N	PARAGUAYO/A	0986837155	SOLTERO/A	\N	\N	\N	SI	NO	382	YVOTY	\N	\N	\N	0	La titular esta separada hace mas de un ano ,5 meses .La misma duena del lote .Su cunada esta embarazada de 7 meses , dentro del lote de la  titular vive su madre y hermana menor con otro techo precario.
5264308	María Laura	Castillo Avalos	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1397	.	\N	\N	\N	0	.
3635612	Mirna Isabel	Castillo Morales	FEMENINO	\N	PARAGUAYO/A	0981354751	UNION DE HECHO	\N	\N	\N	SI	NO	1028	Mar de plata c/ Nuevo Amanecer	\N	\N	\N	0	La vivienda cuenta con un galpón. La niña María Rolón vive con la familia hace 8 años pero no es pariente sanguinea.
607066	Francisco Hernán	Castro Román	MASCULINO	2045-04-06	PARAGUAYO/A	0983745407	.	\N	\N	\N	SI	NO	1528	Ursicino Velasco	\N	\N	\N	0	.
1140466	Cristina	Cazal Aponte	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	415	Julio Jara c/ Amanecer	\N	\N	\N	0	.
1593102	Pablo	Cazal Aponte	MASCULINO	2054-02-03	PARAGUAYO/A	0991214798	SOLTERO/A	\N	\N	\N	SI	NO	417	Julio Jara C/ Amanecer	\N	\N	\N	0	Vivedentro en el mismo predio del papa en el caul hay 3 construccione svivienda Pro-Vida 6x4 y la vivienda precaria es de 4x4 en cada una de ellas viven sus hijos solteros.
4273565	Isidro	Cazal Cuba	MASCULINO	1984-04-04	PARAGUAYO/A	0986147581	CASADO/A	\N	\N	\N	SI	NO	1032	Mar de plata c/ Nuevo Amancer	\N	\N	\N	0	En el frente de la vivienda tiene un Galpón.
2949955	Alejandra Fabiána	Cazal	FEMENINO	\N	PARAGUAYO/A	0982838156	SOLTERO/A	\N	\N	\N	SI	NO	741	Sin nombre (Ursicino Velasco)	\N	\N	\N	0	.
1069113	Centro Educativo Cristiano Nande Jara Roga	.	.	\N	.	.	.	\N	\N	\N	SI	NO	1281	.	\N	\N	\N	0	Es una construccion muy bien constituida son Aulas por lo menos 12.
3189575	Julio Cesar	Centurión Benítez	MASCULINO	\N	PARAGUAYO/A	0981998760	SOLTERO/A	\N	\N	\N	SI	NO	701	San Cayetano	\N	\N	\N	0	En la vivienda vive su sobrino Miguel Adorno . El titular tiene otra cosa y lote en la Zona 5-A en la manzana 30 Lote 5 , el titular no vive en la casa.
6732164	Tamara	Centurión Chamorro	FEMENINO	1999-08-03	PARAGUAYO/A	0971385502	UNION DE HECHO	\N	\N	\N	SI	NO	1401	.	\N	\N	\N	0	La familia vive dentro del mismo lote de la madre, refieren que conviven hace tres(3) años. Tienen una construcción precaria independiente.
2192611	Ana Alice	Centurión de Núñez	FEMENINO	\N	PARAGUAYO/A	0982403289	CASADO/A	\N	\N	\N	SI	NO	496	San Felipe y Santiago	\N	\N	\N	0	.
4988213	Jessica Daihana	Centurión Ramírez	FEMENINO	\N	PARAGUAYO/A	0971356201	CASADO/A	\N	\N	\N	SI	NO	2629	Maria Auxiliadora	\N	\N	\N	0	Jessica es la encargada en estos momentos de la casa.
4121951	Beata	Centurión	FEMENINO	2048-05-12	PARAGUAYO/A	0972877978	SOLTERO/A	\N	\N	\N	SI	NO	677	Yacaré Yrupé	\N	\N	\N	0	.
3215372	Benita	Centurión	FEMENINO	2040-03-05	PARAGUAYO/A	0981809239	.	\N	\N	\N	SI	NO	32	San Felipe y Santiago	\N	\N	\N	0	.
2921999	Nancy Beatriz	Chamorro Chávez	FEMENINO	1977-02-06	PARAGUAYO/A	0992537564	CASADO/A	\N	\N	\N	SI	NO	1046	Yvoty Rory	\N	\N	\N	0	.
1479265	María Mercedes	Chamorro de Arce	FEMENINO	\N	PARAGUAYO/A	0981487477	SOLTERO/A	\N	\N	\N	SI	NO	246	San Felipe y Santiago 1080	\N	\N	\N	0	.
1479286	Inocencio	Chamorro Duarte	MASCULINO	\N	PARAGUAYO/A	0981244964	VIUDO/A	\N	\N	\N	SI	NO	2246	Ursicino Velasco c Sotero Colman	\N	\N	\N	0	Actualmente se encuentra desabitada el propietario vive en Sajonia y manifiesta que cuenta con titulo de Propiedad.
1949423	Eulalia	Chamorro Vda. De Ortiz	FEMENINO	2052-12-02	PARAGUAYO/A	0985383401	VIUDO/A	\N	\N	\N	SI	NO	1063	Ursicino Velasco	\N	\N	\N	0	.
5411	Chancheria	.	.	\N	.	.	.	\N	\N	\N	SI	NO	1052	El Sr. Hector Daniel Sosa es el encaragado de la C	\N	\N	\N	0	.
1206588	Basilia	Chaparro de Sánchez	FEMENINO	\N	PARAGUAYO/A	0981579478	CASADO/A	\N	\N	\N	SI	NO	757	26 de Julio	\N	\N	\N	5	La casa es de 2 plantas con construcción total de 117 M2
1818930	Dominga	Chaparro Rivas	FEMENINO	\N	PARAGUAYO/A	0985349284	SOLTERO/A	\N	\N	\N	SI	NO	760	26 de Julio	\N	\N	\N	0	Vive con la Hermana
3951031	Pablo Darío	Chávez Benítez	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	381	Cristhian Ferreira y Mar del Plata	\N	\N	\N	0	La vivienda cuenta con 9 piezas entre ellas con una sala y una piscina en el patio.La casa esta en refaccion vive en la casa del padre vuelve dentro de 15 dias aprox.
6068593	Herminia Florentín	Chávez Daspet	FEMENINO	1989-05-11	PARAGUAYO/A	0981705606	UNION DE HECHO	\N	\N	\N	SI	NO	940	Olero yt Virgen de Lourdes	\N	\N	\N	0	.
3235447	Celeste María Soledad	Chávez de Florentín	FEMENINO	1979-12-12	PARAGUAYO/A	0982 493 893	CASADO/A	\N	\N	\N	SI	NO	2621	Ursicino Velazco	\N	\N	\N	0	La vivienda es de dos plantas (construccion hormigonada) en la parte superior cuentan con dos piezas de 8x8 m2.
5044615	María Celeste	Chávez Florentín	FEMENINO	\N	PARAGUAYO/A	0982469889	UNION DE HECHO	\N	\N	\N	SI	NO	2574	8 de Diciembre	\N	\N	\N	0	.
1511105	María Teresa	Chávez Garrigoza	FEMENINO	2066-08-10	PARAGUAYO/A	0981984797	CASADO/A	\N	\N	\N	SI	NO	303	Virgen de Lourdes c/ Aromita	\N	\N	\N	0	Seca Ropas 1
2493723	Nestor de la Cruz	Chávez Garrigoza	MASCULINO	\N	PARAGUAYO/A	0984246858	CASADO/A	\N	\N	\N	SI	NO	373	Mar del Plata	\N	\N	\N	0	La hija del titular cuenta con un lote propio en la zona 3 manzana 46 sobre la calle Mar del Plata.
2492854	Tito Gustavo	Chávez Garrigoza	MASCULINO	1976-06-02	PARAGUAYO/A	0983306291	CASADO/A	\N	\N	\N	SI	NO	378	Yvoty y Aromita	\N	\N	\N	0	Tiene una sala y un corredor dentro de la vivienda. La flia. Vivio 2 anos y medio fuera del barrio por la inundación.
2047091	Anastacia	Chávez Martínez	FEMENINO	\N	PARAGUAYO/A	0981 872 138	UNION DE HECHO	\N	\N	\N	SI	NO	1913	Martín Fierro	\N	\N	\N	0	.
4323997	Marcelina	Chávez Monges	FEMENINO	\N	PARAGUAYO/A	0984698755	SOLTERO/A	\N	\N	\N	SI	NO	2010	Nasaindy	\N	\N	\N	5	.
3792159	Vilma Lorena	Chávez Monges	FEMENINO	\N	PARAGUAYO/A	0983515819	SOLTERO/A	\N	\N	\N	SI	NO	2009	Nasaindy	\N	\N	\N	0	Es hija de la propietaria del lote cuenta con vivienda propia de material lote constituida.
638939	Claudio	Chávez oviedo	MASCULINO	\N	PARAGUAYO/A	0985849168	.	\N	\N	\N	SI	NO	36	San Felipe y Santiago	\N	\N	\N	0	La flia cuenta con una despensa y juegos electronicos.
234878	Rufino	Chávez Valdéz	MASCULINO	\N	PARAGUAYO/A	0984672141	CASADO/A	\N	\N	\N	SI	NO	376	Cristhian Ferreira y Mar del Plata	\N	\N	\N	0	El titutlar vive en el barrio hace mas de 61 anos y tiene 56 de matrimonio. El Sr. Tambien tiene un Oleria en la zona 3.
999731	Eladio	Chávez	MASCULINO	\N	PARAGUAYO/A	0981278467	SOLTERO/A	\N	\N	\N	SI	NO	2453	Ursicino Velasco c/Sotero Colman	\N	\N	\N	0	.
2042120	Juana Beatriz	Chávez	MASCULINO	2064-08-01	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	1590	Ramon Talavera	\N	\N	\N	0	.
7022959	Maríane	Chávez	FEMENINO	\N	PARAGUAYO/A	0994546230	SOLTERO/A	\N	\N	\N	SI	NO	2675	Nuestra Sra. De la Asunción	\N	\N	\N	6	Cocina A Gas 1.
3736148	Paula Beatriz	Chávez	FEMENINO	\N	PARAGUAYO/A	0982883301	UNION DE HECHO	\N	\N	\N	SI	NO	1591	Ramon Talavera	\N	\N	\N	0	.
3372020	Pedro Luis	Ciancio Ayala	MASCULINO	\N	PARAGUAYO/A	0982655048	UNION DE HECHO	\N	\N	\N	SI	NO	2322	Nuestra Sra. De la Asunción	\N	\N	\N	6	La pareja lleva 13 anos de concubinato.
3603414	Ana Beatriz	Ciancio Duarte	FEMENINO	1978-03-12	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	2725	Pajagua Naranja	\N	\N	\N	0	Viven en el lote de su madre y tiene una casa precaria.
3603415	Pedro Pablo	Ciancio Duarte	MASCULINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	1243	.	\N	\N	\N	0	.
1743138	Eliseo	Ciancio Varela	MASCULINO	\N	PARAGUAYO/A	0984331287	UNION DE HECHO	\N	\N	\N	SI	NO	1957	Urcicino Velazco	\N	\N	\N	0	El Señor tiene un inquilinato con 3 piezas alquiladas, es su fuente de ingreso.
4022	Club Atletico Boqueron	.	.	\N	.	.	.	\N	\N	\N	SI	NO	2483	.	\N	\N	\N	0	.
2467543	Cesar Ramón	Collar Venegas	MASCULINO	\N	PARAGUAYO/A	0983494595	UNION DE HECHO	\N	\N	\N	SI	NO	1312	Remancito	\N	\N	\N	0	La vivienda cuenta con un galpon en la parte posterior.
6219587	Angela Beatriz	Colmán Amarilla	FEMENINO	1992-02-10	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	67	San Felipe	\N	\N	\N	3	Dispone de otro predio en Zona 6.
6046567	María Elizabeth	Colmán Amarilla	FEMENINO	1989-08-12	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1511	Pajagua Naranja	\N	\N	\N	5	La familia vive en extrema pobreza
4829275	Sandra Vanesa	Colmán Chávez	FEMENINO	\N	PARAGUAYO/A	0984431722	UNION DE HECHO	\N	\N	\N	SI	NO	1589	Ramon Talavera	\N	\N	\N	0	.
4818016	Jessica Natalia	Colmán Colmán	FEMENINO	\N	PARAGUAYO/A	0984942214	SOLTERO/A	\N	\N	\N	SI	NO	1481	Mainumby	\N	\N	\N	0	La Sra. Jessica hace 4 anos que esta separada ,vive en el lote de madre con techo aparte.Su construccion esta pegada a la construccion de la madre.
4676843	Julio Ramón	Colmán Colmán	MASCULINO	\N	PARAGUAYO/A	0983655816	UNION DE HECHO	\N	\N	\N	SI	NO	1479	.	\N	\N	\N	0	La pareja lleva 7 anos de concubinato.Viven en el lote de los padres con una vivienda de Pro-Vida.No tienen salida independiente, las construcciones estan pegadas con una sola entrada y salida.Se manejan independientemente las dos flias.
5129038	Patricia	Colmán Galeano	FEMENINO	1990-11-09	PARAGUAYO/A	0994671679	UNION DE HECHO	\N	\N	\N	SI	NO	2482	Sotero Colman	\N	\N	\N	0	Tiene cocina y dormitorio dentro de la pieza, se utiliza la antigüedad de una de las conyuges
771725	Claudio	Colmán Gauto	MASCULINO	2057-12-08	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	2173	.	\N	\N	\N	0	Cocina 1.
1175490	Cecilia Susana	Colmán Sanabria	FEMENINO	\N	PARAGUAYO/A	0986438363	SOLTERO/A	\N	\N	\N	SI	NO	1220	Fidelina	\N	\N	\N	0	Cocina en brazero afuera de la vivienda.
1395367	María Felicita	Colmán Sanabria	FEMENINO	2063-01-11	PARAGUAYO/A	0983808371	CASADO/A	\N	\N	\N	SI	NO	1477	Mainumby	\N	\N	\N	0	Centrifugadora:1.La pareja tiene 30 anos anteriormente tenian una casa precaria en el mismo lote.Hace 5 anos que tienen una casa de material.La titular cuenta con una Despensa ,dentro del lote de la titular hay 3 nucleos de flia., en tres techos diferent
2940971	Adriana	Colmán	FEMENINO	1970-08-09	PARAGUAYO/A	0986502011	UNION DE HECHO	\N	\N	\N	SI	NO	748	26 de Julio Vecinos Unidos	\N	\N	\N	0	.
3534529	Miriam Graciela	Colmán	FEMENINO	\N	PARAGUAYO/A	0991616218	UNION DE HECHO	\N	\N	\N	SI	NO	787	Virgen de Lujan	\N	\N	\N	0	.
2430066	Nicolás	Colmán	MASCULINO	2063-10-09	PARAGUAYO/A	0971382235	CASADO/A	\N	\N	\N	SI	NO	1532	Rancho 8	\N	\N	\N	0	Centrifugadora 1.
7508434	Yessica Mariela	Colmán	FEMENINO	\N	PARAGUAYO/A	0992584958	SOLTERO/A	\N	\N	\N	SI	NO	1210	Virgen de Lourdes	\N	\N	\N	0	La Srta. Yessica actualmente vive en alquiler y tiene un contrato de 6 meses mas ,  la casa esta en reparaciones porque durante la crecida se le robo parte del techo.
1182600	Comité de pescadores del Pto Yasy y	.	.	\N	.	0982752415	.	\N	\N	\N	SI	NO	1231	Yvoty Rory	\N	\N	\N	0	El comité fue fundado n el año 2008, Organizan turnos de pesca, se reunen cada 15 días, cuenta con 27 miembros. El representante legal es el Señor Franciso Britez Armoa, con C.I nro. 1182600 (pdte), No tiene  una construcción edificada ademaás del galpón
983139	Ursulina	Contrera	FEMENINO	2056-01-04	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	2716	Primavera	\N	\N	\N	0	.
7993	Cooperativa de Vivienda Bañado Poty	.	.	\N	.	0962231243	.	\N	\N	\N	SI	NO	2394	.	\N	\N	\N	0	.
1137521	Susana	Cordobés de Jara	FEMENINO	2059-09-07	PARAGUAYO/A	0985468025	CASADO/A	\N	\N	\N	SI	NO	2203	Mbo-ehara	\N	\N	\N	0	Ramona Clara Colman es suegra de Juan José Jara.
6551948	Rodrigo Miguel	Cordobés Santa cruz	MASCULINO	1997-04-07	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	2120	Sin Nombre	\N	\N	\N	0	.
5087438	Ana Victoria	Cordobes Santacruz	FEMENINO	\N	PARAGUAYO/A	0994838616	UNION DE HECHO	\N	\N	\N	SI	NO	1376	Yvoty Rory	\N	\N	\N	0	.
811110	Roberto	Cordobés	MASCULINO	\N	PARAGUAYO/A	.	VIUDO/A	\N	\N	\N	SI	NO	2179	Sin Nombre	\N	\N	\N	0	Su nieto se enuentra con el por orden judicial, su carpinteria se encuentra en la Manzana 2 hace mas de 30 años en la casa de la madre.
5725632	Osvaldo Javier	Cordobéz Santacruz	MASCULINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	2117	sin nombre	\N	\N	\N	0	.
4871125	Verónica Graciela	Cordoves Suarez	FEMENINO	\N	PARAGUAYO/A	0992 726 054	UNION DE HECHO	\N	\N	\N	SI	NO	1920	Martin Fierro	\N	\N	\N	0	La Pareja lleva dos años de concubinato. Tienen lote propio  y techo de material.
1301208	Matilde	Cordovez	FEMENINO	\N	PARAGUAYO/A	0994354269	UNION DE HECHO	\N	\N	\N	SI	NO	462	23 de Junio	\N	\N	\N	0	Dentro de la vivienda de Na Matilde Cordovez vive su hijo soltero con hija de 5 anos.
4517411	Pabla Francisca	Coronel Arévalos	FEMENINO	\N	PARAGUAYO/A	0986357314	SOLTERO/A	\N	\N	\N	SI	NO	2636	Martin Fierro	\N	\N	\N	5	Cocina y Bano comparten con su madre Pabla Emiliana Arevalos.Hace 5 meses estan en el Barrio.
4547710	Rossana Noemi	Coronel Arévalos	FEMENINO	\N	PARAGUAYO/A	0972289557	UNION DE HECHO	\N	\N	\N	SI	NO	2637	Martin Fierro	\N	\N	\N	5	Cocina a Gas 1.
2574287	Domingo	Coronel Sánchez	MASCULINO	\N	PARAGUAYO/A	0982915853	UNION DE HECHO	\N	\N	\N	SI	NO	822	23 de Junio	\N	\N	\N	0	.
2841659	Leandro	Coronel	MASCULINO	\N	PARAGUAYO/A	0991 935 914	SOLTERO/A	\N	\N	\N	SI	NO	1904	Pilcomayo	\N	\N	\N	0	El titular Leandro Coronel presta parte de su lote a un nucleo familiar hace un año.
3693060	Lourdes Estela	Corrales	FEMENINO	\N	PARAGUAYO/A	0982437402	UNION DE HECHO	\N	\N	\N	SI	NO	2216	Chiquero	\N	\N	\N	0	.
2202265	María Dolores	Cristaldo de Benítez	FEMENINO	\N	PARAGUAYO/A	0985352390	CASADO/A	\N	\N	\N	SI	NO	510	Malvinas	\N	\N	\N	0	.
4343614	Isaura	Cristaldo Garcete	FEMENINO	\N	PARAGUAYO/A	0994635300	UNION DE HECHO	\N	\N	\N	SI	NO	2649	Ramon Talavera	\N	\N	\N	0	La señora Isaura no es dueña del lote, ni del terreno, vive en el lote poque la dueña le dio permiso par que haga su casa precaria hace 6 meses porque no tenía donde hice. El terreno pertenece a Adriana Elvira Romero. La sra Adriana manifiesta que no tie
3243789	María Estela	Cristaldo Giménez	FEMENINO	\N	PARAGUAYO/A	0983369045	SOLTERO/A	\N	\N	\N	SI	NO	53	San Felipe	\N	\N	\N	3	Su hijo tiene techo aparte en el mismo lote.
2969083	Pedro Salvador	Cristaldo Giménez	MASCULINO	\N	PARAGUAYO/A	0994635170	UNION DE HECHO	\N	\N	\N	SI	NO	64	San Felipe y Santiago	\N	\N	\N	3	Tiene construida la casa de la cooperativa.
1507661	Herminio	Cuandu Bernal	MASCULINO	\N	PARAGUAYO/A	0981194076	.	\N	\N	\N	SI	NO	1885	Julio Benitez	\N	\N	\N	0	El Sr. Cobre Tercera Edad, el hijo Herminio Asuncion posee una construccion de madera aparte del padre y hno.
5614656	Genara	Cuba Benítez	FEMENINO	1988-05-02	PARAGUAYO/A	098629966	SEPARADO/A	\N	\N	\N	SI	NO	814	26 de Julio	\N	\N	\N	3	.
4995669	María Roque	Cuba Benítez	FEMENINO	\N	PARAGUAYO/A	0986134378	.	\N	\N	\N	SI	NO	2092	sin nombre	\N	\N	\N	5	.
2209425	Fermina	Cuba de Bobadilla	FEMENINO	1970-07-07	PARAGUAYO/A	0982277580	CASADO/A	\N	\N	\N	SI	NO	293	Mto. Ursicino Velasco c/ Nuevo Amanecer	\N	\N	\N	4	.
2815698	Florentína	Cuba Dias	FEMENINO	\N	PARAGUAYO/A	0992468208	UNION DE HECHO	\N	\N	\N	SI	NO	1188	Fidelina	\N	\N	\N	0	.
1387652	Juan Ramón	Cuba Díaz	MASCULINO	2064-02-02	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	813	26 de Julio	\N	\N	\N	3	.
2190232	Lucía	Cuba Díaz	FEMENINO	2068-11-10	PARAGUAYO/A	0971185396	UNION DE HECHO	\N	\N	\N	SI	NO	1769	Koety	\N	\N	\N	0	.
3458392	Panfilo	Cuba Díaz	MASCULINO	1974-10-06	PARAGUAYO/A	0982899035	CASADO/A	\N	\N	\N	SI	NO	1197	Fidelina (Santa Ana)	\N	\N	\N	0	.
6311341	Cristina Mabel	Cuba González	FEMENINO	1993-12-11	PARAGUAYO/A	0986148293	UNION DE HECHO	\N	\N	\N	SI	NO	2476	Vecinos Unidos	\N	\N	\N	0	.
3872893	Natividad	Cuba Ozuna	FEMENINO	\N	PARAGUAYO/A	0986539303	SOLTERO/A	\N	\N	\N	SI	NO	2726	Julio Jara c/ Nuevo Amanecer	\N	\N	\N	0	La madre refiere que extravio las cedulas de sus hijas.
5373359	Jacqueline	Cuba Romero	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1420	Bañado Koeti	\N	\N	\N	5	.
6934380	Ada Luz	Cuba	FEMENINO	1987-08-01	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	317	Nuevo Amanecer	\N	\N	\N	0	.
3189573	Cristina	Cuba	FEMENINO	2028-05-12	PARAGUAYO/A	0982277580	SOLTERO/A	\N	\N	\N	SI	NO	427	Julio Jara	\N	\N	\N	0	.
6774162	Teresa Lujan	Cuba	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	167	Nuevo Amanecer	\N	\N	\N	0	.
4193993	Albino	Cubas Díaz	MASCULINO	1982-09-01	PARAGUAYO/A	0971181145	UNION DE HECHO	\N	\N	\N	SI	NO	1424	Bañado Koeti	\N	\N	\N	5	.
2130969	Ignacio	Cubas Martínez	MASCULINO	1970-08-03	PARAGUAYO/A	0984871014	SOLTERO/A	\N	\N	\N	SI	NO	887	Martin Fierro	\N	\N	\N	0	El titular tiene una vivienda adquirida por la Cooperativa . Vive con su hijo hace mas de 40 años.
5317088	Albino David	Cubas Pereira	MASCULINO	\N	PARAGUAYO/A	094550320	SOLTERO/A	\N	\N	\N	SI	NO	385	YVOTY y Virgen de Lourdes	\N	\N	\N	0	Cuenta con lote propio y techo precario, no tiene pareja ni hijos.
1467429	Amanda Concepción	Cubas	FEMENINO	\N	PARAGUAYO/A	.	SEPARADO/A	\N	\N	\N	SI	NO	166	Nuevo Amanecer	\N	\N	\N	0	.
3658466	Viviana Elizabeth	Cubas	FEMENINO	\N	PARAGUAYO/A	0985776941	UNION DE HECHO	\N	\N	\N	SI	NO	590	Malvinas	\N	\N	\N	0	La cocina y el comedor van juntos en una extension de madera -casita de 4x3 m2.
5069536	Fabrizio Alejandro	Cubilla Aliente	MASCULINO	\N	PARAGUAYO/A	0982625140	SOLTERO/A	\N	\N	\N	SI	NO	1939	Ursicino Velasco	\N	\N	\N	0	.
4672176	Dario Daniel	Cubilla Cáceres	MASCULINO	1985-07-07	PARAGUAYO/A	0991894827	SOLTERO/A	\N	\N	\N	SI	NO	1544	Pajagua Naranja	\N	\N	\N	0	Vive en la vivienda de la titular con su pareja.Tienen un hijo recien nacido que no tiene nombre aún.
5494778	Tania Elizabeth	Cubilla López	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1543	Pajagua Naranja	\N	\N	\N	0	.
6843768	María Gabriela	Cubilla Martínez	FEMENINO	\N	PARAGUAYO/A	0971137996	UNION DE HECHO	\N	\N	\N	SI	NO	2200	Mbo-ehara c/ Sotero Colman	\N	\N	\N	0	.
1504087	Germán Patricio	Cubilla Mongelos	MASCULINO	\N	PARAGUAYO/A	0981 932 934	CASADO/A	\N	\N	\N	SI	NO	2019	En la parte superior hay tres piezas muy bien cons	\N	\N	\N	0	.
2152772	Juan Lucas	Cubilla Mongelos	MASCULINO	\N	PARAGUAYO/A	0983347778	UNION DE HECHO	\N	\N	\N	SI	NO	2133	.	\N	\N	\N	0	.
973917	Juana Inocencia	Cubilla Mongelós	MASCULINO	2063-08-08	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	2094	Ursicino Velazco	\N	\N	\N	0	.
2925598	Justa Emilis	Cubilla Mongelós	FEMENINO	\N	PARAGUAYO/A	0986714450	UNION DE HECHO	\N	\N	\N	SI	NO	2090	Ursicino Velazco	\N	\N	\N	0	.
5023327	Estelvina	Cubilla Villalba	FEMENINO	1988-08-04	PARAGUAYO/A	0972801870	UNION DE HECHO	\N	\N	\N	SI	NO	823	Vecinos Unidos	\N	\N	\N	0	.
1214393	María Elena	Cubilla	FEMENINO	2055-06-06	PARAGUAYO/A	0991350183	SEPARADO/A	\N	\N	\N	SI	NO	2123	.	\N	\N	\N	0	.
3983214	Pastora Concepción	Cubilla	FEMENINO	1981-02-07	PARAGUAYO/A	0982146021	SOLTERO/A	\N	\N	\N	SI	NO	2407	Mandyjura	\N	\N	\N	0	Dentro del terreno de la propietaria vive una de sus hijas de 19 años con su pareja con una hija de dos meeses en una vivienda precaria.
1920473	Vicente Milceades	Cubilla	MASCULINO	2068-06-04	PARAGUAYO/A	0971680421	SOLTERO/A	\N	\N	\N	SI	NO	1547	Pajagua Naranja	\N	\N	\N	0	Secarropas 1. El titular vive con su pareja y su hija ,su pareja es titular en otra vivienda donde viven sus hijos menores con un hermano mayor.
4672387	Milciades Ariel	Cubillas López	MASCULINO	\N	PARAGUAYO/A	0981361103	UNION DE HECHO	\N	\N	\N	SI	NO	1545	Pajagua Naranja	\N	\N	\N	0	El titular compro la vivienda hace 3 anos.
1922261	Isabel	Cuellar de Velázquez	FEMENINO	\N	PARAGUAYO/A	0991989820	CASADO/A	\N	\N	\N	SI	NO	702	Mar del Plat y Virgen de Lourdes	\N	\N	\N	0	Tiene una SALA.
4551272	Pamela Nataly	Cuellar Sosa	FEMENINO	1987-05-07	PARAGUAYO/A	0992750581	UNION DE HECHO	\N	\N	\N	SI	NO	48	OLEROS	\N	\N	\N	3	.
2495651	Máximo	Cuellar	MASCULINO	\N	PARAGUAYO/A	0982649782	CASADO/A	\N	\N	\N	SI	NO	1946	Ursicino Velasco c/ Puente Pesoa	\N	\N	\N	0	El sr. Posee dentro del lote otra pieza de material 3,50x5 y otro 5x6.
5307955	Roberto Daniel	Cuellar	MASCULINO	1993-08-03	PARAGUAYO/A	0984887681	UNION DE HECHO	\N	\N	\N	SI	NO	703	Mar del Plata y Virgen de Lourdes	\N	\N	\N	0	Tiene Corredor adentro.
6187543	David	Cuenca Valdéz	MASCULINO	1993-05-09	PARAGUAYO/A	0993584472	SOLTERO/A	\N	\N	\N	SI	NO	1777	Bañado Koeti	\N	\N	\N	0	David y Nancy son propietarios del terreno y la vivienda, la compra fue compartida llevan pagando mas de 2 anos.
1903842	Andreza	Cueva Vda de Florentín	FEMENINO	2046-10-10	PARAGUAYO/A	.	VIUDO/A	\N	\N	\N	SI	NO	446	San Cayetano	\N	\N	\N	0	Dentro del lote de Andreza vive un Hijo soltero en una vivienda precaria aparte
6623954	Mirtha	Cueva	FEMENINO	1990-03-10	PARAGUAYO/A	0992590046	UNION DE HECHO	\N	\N	\N	SI	NO	1012	Mar de plata	\N	\N	\N	0	.
3338411	María Ángelica	Cuevas Acosta	FEMENINO	2052-01-10	PARAGUAYO/A	0984737789	UNION DE HECHO	\N	\N	\N	SI	NO	2327	Nuestra Sra. De la Asunción	\N	\N	\N	6	.
1205683	Celedonia	Cuevas Basualdo	FEMENINO	2064-03-03	PARAGUAYO/A	0986886733	.	\N	\N	\N	SI	NO	1069	Mar de Plata	\N	\N	\N	0	.
2391568	Francisco	Cuevas Martínez	MASCULINO	\N	PARAGUAYO/A	0981973370	UNION DE HECHO	\N	\N	\N	SI	NO	983	8 de Diciembre	\N	\N	\N	0	La vivienda es de madera de planta alta es un palafito la construida por la pastoral hace 30 anos , el bano es comun , no tiene cocina.
3019451	Patricia Ramona	Cuevas	FEMENINO	1990-06-07	PARAGUAYO/A	0971725370	SOLTERO/A	\N	\N	\N	SI	NO	210	Aromita	\N	\N	\N	0	.
721400	Carlos	Dapollo	MASCULINO	2051-08-10	PARAGUAYO/A	0982565852	SOLTERO/A	\N	\N	\N	SI	NO	919	Maestro Ursicino Velasco	\N	\N	\N	0	Canoa-1
6639698	Julia Mabel	Davalos Román	FEMENINO	\N	PARAGUAYO/A	0984541415	UNION DE HECHO	\N	\N	\N	SI	NO	1091	San Felipe y Santiago	\N	\N	\N	3	La pareja lleva 5 anos de concubinato cuentan con un terreno propio en la Zona 7 que mide 15x15, despues de la ultima inundacion ya no pudieron construir.
4692722	Pablo Francisco	Dávalos	MASCULINO	1970-02-03	PARAGUAYO/A	0986159959	.	\N	\N	\N	SI	NO	2329	Nuestra Sra. De la Asunción	\N	\N	\N	6	Cocina A gas 1.
4655079	María Fabiola	De León Barreto	FEMENINO	1986-09-06	PARAGUAYO/A	0981913396	UNION DE HECHO	\N	\N	\N	SI	NO	337	Mar del Plata	\N	\N	\N	0	Su vivienda era del Suegro.
1711902	Francisco	de León	MASCULINO	2037-03-04	PARAGUAYO/A	0986859411	VIUDO/A	\N	\N	\N	SI	NO	1773	Bañado Koeti	\N	\N	\N	0	.
4943459	Lorena Analiz	Del Valle Huerta	FEMENINO	1990-10-11	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	2249	Mtro. Urcisino Velazco	\N	\N	\N	0	En el salón tienen sus negocios de venta de bebidas y comidas rápidas. Ingreso suficiente.
4187082	Gladis Fabiola	Delacruz Cardozo	FEMENINO	1991-10-07	PARAGUAYO/A	0981262538	UNION DE HECHO	\N	\N	\N	SI	NO	520	Malvinas	\N	\N	\N	0	.
4186522	Lidia Soledad	Delacruz Cardozo	FEMENINO	\N	PARAGUAYO/A	0983427526	UNION DE HECHO	\N	\N	\N	SI	NO	527	Malvinas c/ 16 de Agosto	\N	\N	\N	0	.
3687953	Angélica Concepción	Delgado	FEMENINO	\N	PARAGUAYO/A	0982915268	UNION DE HECHO	\N	\N	\N	SI	NO	2658	Nuestra Sra. De la Asunción	\N	\N	\N	6	Dentro del techo de la titular viven dos parejas ,una de ellas con un menor de 4 meses y la otra con embarazo.
1267346	Gabriela de Jesús	Delvalle de Díaz	FEMENINO	2068-10-03	PARAGUAYO/A	0971283859	CASADO/A	\N	\N	\N	SI	NO	600	Malvinas c/ 16 de Agosto	\N	\N	\N	0	Fabrica de baldosones y polleria los domingos 3,30x4 ,Polleria 4x3,50.
723549	Dolores	Delvalle de Rojas	FEMENINO	\N	PARAGUAYO/A	0983 869 412	CASADO/A	\N	\N	\N	SI	NO	1931	Martin Fierro	\N	\N	\N	3	Dentro del techo de la titular vivien dos nucleos familiares, quienes se manejan de manera independiente.
2000210	Sindulfo	Delvalle Galeano	MASCULINO	\N	PARAGUAYO/A	0984221735	UNION DE HECHO	\N	\N	\N	SI	NO	2038	Ursicino Velasco c/ Nasayndi	\N	\N	\N	0	.
2396414	Eulogio	Delvalle Rolón	MASCULINO	2067-11-03	PARAGUAYO/A	0981 493 011	UNION DE HECHO	\N	\N	\N	SI	NO	1925	Martin Fierro	\N	\N	\N	5	El titular posee detrás de su casa un terreno de 50 m. de frente y 100 m. de fondo que utiliza para criar animales. (cerdos, gallinas y cabras)
3893141	Catalina	Delvalle Sanabria	FEMENINO	\N	PARAGUAYO/A	0991	UNION DE HECHO	\N	\N	\N	SI	NO	1976	Martin Fierro	\N	\N	\N	0	.
535991	Paula	Demo Fonte	FEMENINO	\N	PARAGUAYO/A	0984136660	UNION DE HECHO	\N	\N	\N	SI	NO	1556	Ursicino Velasco	\N	\N	\N	0	Es propietario del lote con ella vive uno de sus hijos en otra vivienda de material bien constituida y totalmente independiente.
6193388	Cheila Beatriz	Denis Cazal	FEMENINO	\N	PARAGUAYO/A	0994188961	UNION DE HECHO	\N	\N	\N	SI	NO	743	Mto. Ursicino Velasco e/ Martin Fierro	\N	\N	\N	0	Vive dentro del mismo lote, en una ampliacion de la vivienda.
5355133	Lidia Judith	Denis Cazal	FEMENINO	\N	PARAGUAYO/A	0992433237	UNION DE HECHO	\N	\N	\N	SI	NO	744	Mto. Ursicino Velasco e/ Martin Fierro	\N	\N	\N	0	Vive dentro del mismo lote y techo que ekl del padre Martin Denis.
1606086	María Teresa	Denis Garay	FEMENINO	\N	PARAGUAYO/A	0962304853	UNION DE HECHO	\N	\N	\N	SI	NO	1582	Ramon Talavera	\N	\N	\N	0	Actulamente vive en una casa precaria de 3x3.Esta construyendo un Hormigon de 5x8 ,es mama de la propietaria del lote.
2705	Eugenia	Denis	FEMENINO	\N	.	.	.	\N	\N	\N	SI	NO	2405	.	\N	\N	\N	0	No quiso censarse.
1266563	Martin	Denis	MASCULINO	2054-11-11	PARAGUAYO/A	0991635315	SOLTERO/A	\N	\N	\N	SI	NO	742	Ursicino Velasco	\N	\N	\N	4	.
2993180	Luz Marina	Diana de González	FEMENINO	\N	PARAGUAYO/A	0986698936	CASADO/A	\N	\N	\N	SI	NO	2277	.	\N	\N	\N	0	.
4393719	María Celeste	Diana Galeano	FEMENINO	1987-02-06	PARAGUAYO/A	0986639995	.	\N	\N	\N	SI	NO	116	.	\N	\N	\N	0	.
2992563	Rocio Raquel	Diana Vera	FEMENINO	\N	PARAGUAYO/A	0984889206	CASADO/A	\N	\N	\N	SI	NO	2276	Nuestra Sra. De la Asunción	\N	\N	\N	0	.
2883636	Ramona	Diana	MASCULINO	2061-01-09	PARAGUAYO/A	0985625875	UNION DE HECHO	\N	\N	\N	SI	NO	597	Malvinas	\N	\N	\N	0	.
4155393	Vicente Ramón	Diana	MASCULINO	\N	PARAGUAYO/A	0981651416	SOLTERO/A	\N	\N	\N	SI	NO	598	Malvinas c/ 16 de Agosto	\N	\N	\N	0	.
1420172	Cirilo	Díaz Alvarenga	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	1907	Julio Benitez	\N	\N	\N	0	.
2949953	Cirilo Alberto	Díaz Chávez	MASCULINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	1692	San Martin de Porres	\N	\N	\N	0	Era la casa de la madre donde nacio y al fallecer quedo el y la hna.
1490832	Miryan Graciela	Díaz Chávez	FEMENINO	1971-01-07	PARAGUAYO/A	0981351480	UNION DE HECHO	\N	\N	\N	SI	NO	1902	Julio Benitez	\N	\N	\N	0	234 mts2. tiene de losa y dos pisos.
2949954	Rossana Raquel	Díaz Chávez	FEMENINO	\N	PARAGUAYO/A	0971103932	UNION DE HECHO	\N	\N	\N	SI	NO	1693	San Martin de Porres	\N	\N	\N	0	Era la casa de la madre donde nacio.
2815724	Melchora	Díaz de Cuba	FEMENINO	2044-06-01	PARAGUAYO/A	0981136598	CASADO/A	\N	\N	\N	SI	NO	1198	Fidelina (Santa Ana)	\N	\N	\N	0	.
4486928	Gabriela Ilusión	Díaz delvalle	FEMENINO	\N	PARAGUAYO/A	.	SEPARADO/A	\N	\N	\N	SI	NO	19	San Felipe y Santiago	\N	\N	\N	3	.
4887090	Juan de Jesus	Díaz Delvalle	MASCULINO	1992-07-08	PARAGUAYO/A	0991671840	UNION DE HECHO	\N	\N	\N	SI	NO	602	Malvinas c/ 16 de Agosto	\N	\N	\N	0	.
5033443	Aida Florencia	Díaz González	FEMENINO	\N	PARAGUAYO/A	0971566863	UNION DE HECHO	\N	\N	\N	SI	NO	961	Nuevo Amanecer	\N	\N	\N	0	.
5373326	Ana Rosalia	Díaz González	FEMENINO	1995-09-08	PARAGUAYO/A	0982497902	CASADO/A	\N	\N	\N	SI	NO	348	Los Sauces	\N	\N	\N	0	.
5968357	Catalina	Díaz González	FEMENINO	1998-06-04	Praguaya	0991882247	UNION DE HECHO	\N	\N	\N	SI	NO	699	Nuevo amanecer	\N	\N	\N	4	.
3552254	Flora Abelina	Díaz González	FEMENINO	\N	PARAGUAYO/A	.	SEPARADO/A	\N	\N	\N	SI	NO	264	Nuevo Amanecer	\N	\N	\N	0	La vivienda esta aparte en el lote del titular una casa bien hecha de madera.
5234945	Isabelino	Díaz González	FEMENINO	1990-03-03	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	263	Nuevo Amanecer	\N	\N	\N	0	.
5234949	Simón	Díaz González	MASCULINO	1992-02-04	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	265	Nuevo Amanecer	\N	\N	\N	0	La vivienda esta aparte en el lote del titular una casa bien hecha de madera.
3710924	Francisco Ceferino	Díaz Martínez	MASCULINO	\N	PARAGUAYO/A	0994119752	CASADO/A	\N	\N	\N	SI	NO	756	Martin Fierro c/Mto. Ursicino Velasco	\N	\N	\N	0	.
4312168	Hugo Victorino	Díaz Martínez	MASCULINO	1987-02-12	PARAGUAYO/A	0994124796	UNION DE HECHO	\N	\N	\N	SI	NO	889	Martin Fierro	\N	\N	\N	0	La pareja vive bajo el techo del Señor Ignacio , llevan 8 meses de concubinato.
4312169	José Manuel	Díaz Martínez	MASCULINO	\N	PARAGUAYO/A	0971149126	UNION DE HECHO	\N	\N	\N	SI	NO	759	Martin Fierro c/Mto. Ursicino Velasco	\N	\N	\N	0	.
3825653	Carlos Alberto	Díaz Riveros	MASCULINO	\N	PARAGUAYO/A	0982249198	UNION DE HECHO	\N	\N	\N	SI	NO	900	Martin Fierro	\N	\N	\N	0	Su padre vive con el en una extencion de 4x4 totalmente independiente c/ ducha y baño , trabaja para mantenerse.
963794	María Cecilia	Díaz Rotela	FEMENINO	\N	PARAGUAYO/A	0982273751	UNION DE HECHO	\N	\N	\N	SI	NO	893	Martin Fierro	\N	\N	\N	0	El Sr. Timoteo cuenta con un terreno en la Zon 5 B , especificamente dentro de la Asociacion de Carreros .Mide 30 mts x7,30 mts., la flia cuenta con una despensa, tambien la titular se dedica a la costura.
1221968	Fidelina	Díaz Trinidad	FEMENINO	2068-12-01	PARAGUAYO/A	0984263151	UNION DE HECHO	\N	\N	\N	SI	NO	947	Mar del Plata	\N	\N	\N	0	La dueña del lote y la casa vive en alquiler desde la inundacion ,no volvio a su casa por temor al proyecto actualmente esta alquilando a Veronica Portillo hace 1 año.
1898795	María Magdalena	Díaz Vda. De Palacios	FEMENINO	\N	PARAGUAYO/A	0982456440	VIUDO/A	\N	\N	\N	SI	NO	967	Algarrobo	\N	\N	\N	0	.
914	Julio	Diez Pérez	MASCULINO	\N	.	.	.	\N	\N	\N	SI	NO	1636	Pajagua Naranja	\N	\N	\N	0	.
5471554	Herminia Andrea	Doldan Escobar	FEMENINO	\N	PARAGUAYO/A	0983426113	SEPARADO/A	\N	\N	\N	SI	NO	43	San Felipe y Santiago	\N	\N	\N	0	Con sala y corredor.
1042056	Antonia	Doldan	FEMENINO	\N	PARAGUAYO/A	0982869239	UNION DE HECHO	\N	\N	\N	SI	NO	1848	Maria Auxiliadora	\N	\N	\N	0	.
4210656	Isidro Usvaldo	Doldan	MASCULINO	\N	PARAGUAYO/A	0986602935	UNION DE HECHO	\N	\N	\N	SI	NO	1849	Villa Maria Auxiliadora	\N	\N	\N	0	.
1745611	Maximo Dionisio	Doldán	MASCULINO	2053-08-04	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	148	San Felipe y Santiago	\N	\N	\N	3	.
5168454	Milagros Monserrath	Domesqui Marecos	FEMENINO	1994-03-08	PARAGUAYO/A	0982171610	UNION DE HECHO	\N	\N	\N	SI	NO	1387	Bañado Koeti	\N	\N	\N	0	.
3318214	Hector Daniel	Domínguez Ayala	MASCULINO	\N	PARAGUAYO/A	0985852468	UNION DE HECHO	\N	\N	\N	SI	NO	1549	Pajagua Naranja	\N	\N	\N	0	.
4171089	Humberto Raul	Domínguez Ayala	MASCULINO	\N	PARAGUAYO/A	0983849016	SOLTERO/A	\N	\N	\N	SI	NO	1548	Pajagua Naranja	\N	\N	\N	0	.
4177546	Santa Justina	Domínguez Chaparro	FEMENINO	\N	PARAGUAYO/A	0985734994	UNION DE HECHO	\N	\N	\N	SI	NO	1743	Ursicino Velasco	\N	\N	\N	0	Cocina a Gas 1.
5679092	Hugo Javier	Domínguez Domínguez	MASCULINO	\N	PARAGUAYO/A	0993530256	UNION DE HECHO	\N	\N	\N	SI	NO	1430	.	\N	\N	\N	0	Hugo Javier es nieto del titular.
1428760	Asuncion	Domínguez Morel	FEMENINO	\N	PARAGUAYO/A	0972648202	UNION DE HECHO	\N	\N	\N	SI	NO	1583	Ramon Talavera	\N	\N	\N	6	.
5709821	Blanca Elizabeth	Domínguez Morel	FEMENINO	1990-02-07	PARAGUAYO/A	0982203216	SOLTERO/A	\N	\N	\N	SI	NO	1703	Virgen de Guadalupe	\N	\N	\N	0	.
4466439	Cristobal	Domínguez Morel	MASCULINO	\N	PARAGUAYO/A	0983791135	SOLTERO/A	\N	\N	\N	SI	NO	1450	Virgen de Guadalupe	\N	\N	\N	0	.
5572731	Elisa Mariela	Domínguez Morel	FEMENINO	\N	PARAGUAYO/A	0986460635	.	\N	\N	\N	SI	NO	1702	Virgen de Guadalupe	\N	\N	\N	0	.
4372192	Marta Valeria	Domínguez Morel	FEMENINO	1984-12-03	PARAGUAYO/A	0985715166	UNION DE HECHO	\N	\N	\N	SI	NO	1705	Virgen de Guadalupe	\N	\N	\N	0	.
4555957	Venancio Valentin	Domínguez Morel	MASCULINO	1988-01-04	PARAGUAYO/A	.	CASADO/A	\N	\N	\N	SI	NO	1741	Ursicino Velasco	\N	\N	\N	0	Tiene una construccion de Hormigon que mide 6x12.
1088305	José Zenon	Domínguez Recalde	MASCULINO	\N	PARAGUAYO/A	0981399037	SOLTERO/A	\N	\N	\N	SI	NO	1600	Nuestra Senora de la Asunción	\N	\N	\N	0	Actualmente el propietario vive en el refugio S.E.M.,aproximadamente 6 meses.
4744005	Alfredo Andrés	Domínguez Rolón	MASCULINO	1990-12-03	PARAGUAYO/A	0981567885	UNION DE HECHO	\N	\N	\N	SI	NO	1569	Ursicino Velasco	\N	\N	\N	0	Vive en una construccion precaria de 8x5 ,hace 5 anos que estan en pareja.
4634538	Reinaldo	Domínguez Sales	MASCULINO	\N	.	.	SOLTERO/A	\N	\N	\N	SI	NO	1806	Bañado Koeti	\N	\N	\N	0	.
4989720	Eleuterio Alberto	Domínguez	MASCULINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	1229	.	\N	\N	\N	0	.
4654949	Miguel Angel	Domínguez	MASCULINO	1997-08-09	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1753	Ursicino Velasco	\N	\N	\N	0	.
7578383	Natalia Rocío	Domínguez	FEMENINO	\N	PARAGUAYO/A	0971716088	UNION DE HECHO	\N	\N	\N	SI	NO	44	San Felipe y Santiago	\N	\N	\N	0	.
2685460	Ramón Catalino	Domínguez	MASCULINO	\N	PARAGUAYO/A	0981127904	SEPARADO/A	\N	\N	\N	SI	NO	1993	Nasaindy	\N	\N	\N	0	Hace 7 meses no habita refiere que es una zona muy inundable.
5904537	Marien Mabel	Dosantos	FEMENINO	\N	PARAGUAYO/A	0983683220	UNION DE HECHO	\N	\N	\N	SI	NO	512	.	\N	\N	\N	0	.
5698187	Ruben Dario	Dosantos	MASCULINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	95	Virgen de Lourdes	\N	\N	\N	0	Vive en lote y techo de su madrina.
3899177	Germán	Duarte Adorno	MASCULINO	1995-12-05	PARAGUAYO/A	0991722254	UNION DE HECHO	\N	\N	\N	SI	NO	568	San Cayetano	\N	\N	\N	0	.
5373337	Cinthia Yohana	Duarte Alen	FEMENINO	\N	PARAGUAYO/A	0983362940	UNION DE HECHO	\N	\N	\N	SI	NO	2673	Nuestra Sra. De la Asunción	\N	\N	\N	6	Placa Electrica 1.
5373335	Dahiana Navily	Duarte Alen	FEMENINO	\N	PARAGUAYO/A	0984921855	UNION DE HECHO	\N	\N	\N	SI	NO	2672	Nuestra Sra. De la Asunción	\N	\N	\N	6	.
6112648	Karen Tatiana	Duarte Alen	FEMENINO	\N	PARAGUAYO/A	0986739319	SOLTERO/A	\N	\N	\N	SI	NO	2667	Nuestra Sra. De la Asunción	\N	\N	\N	6	La titular esta separada hace 4 meses despues de una relacion de 2 anos ,anteriormente vivia en la Zona 5 ,en la casa de su madre,hace 7 anos compro el terreno en Villa Asución.
2137082	Zulma	Duarte Ayala	FEMENINO	\N	PARAGUAYO/A	0971561542	SOLTERO/A	\N	\N	\N	SI	NO	1095	Martin Fierro	\N	\N	\N	0	Zulma es propietaria del lote. Dentro de su vivenda vive una de sus hijas con su pareja e hijos.
5010862	David Daniel	Duarte Bareiro	MASCULINO	\N	PARAGUAYO/A	0984841474	SOLTERO/A	\N	\N	\N	SI	NO	2564	Mandy Jura	\N	\N	\N	0	Dentro del lote del titular vieven su hno con su pareja.La pareja lleva 1 ano de concubinato con un embarazo 7 meses.Tienen un techo precario de 4x4.Cocina a Gas 1
4495213	Irma	Duarte Benítez	FEMENINO	\N	PARAGUAYO/A	0986452389	SOLTERO/A	\N	\N	\N	SI	NO	995	Detrás de Reecicla	\N	\N	\N	3	Dormitorio, cocina todo dentro de la vivienda. Utiliza el agua y el baño con la vecina.
1751221	Ramón Fulgencio	Duarte Cabrera	MASCULINO	\N	PARAGUAYO/A	0961318283	CASADO/A	\N	\N	\N	SI	NO	1823	Mandyjura	\N	\N	\N	0	salon para venta de comestibles
1220431	Vidal	Duarte Cabrera	MASCULINO	\N	PARAGUAYO/A	0984811833	SOLTERO/A	\N	\N	\N	SI	NO	2116	Ursicino Velasco	\N	\N	\N	0	.
3657681	Cristhian Ulices	Duarte Collar	MASCULINO	\N	PARAGUAYO/A	0991452565	UNION DE HECHO	\N	\N	\N	SI	NO	1073	Martin Fierro	\N	\N	\N	0	Vive dentro de una vivienda precaria , tiene una vivienda de materialque no ocupa porque es bajo y cuando llueve se junta agua es dueno del lote.
4376142	Juan Gabriel	Duarte Collar	MASCULINO	1985-09-08	PARAGUAYO/A	0992541873	UNION DE HECHO	\N	\N	\N	SI	NO	537	Martin Fierro	\N	\N	\N	0	Juan Gabriel Duarte Collar se encuentra privado de su libertad según fliares. Saldra pronto. Actualmente la casa esta desabitada la Sra. Antonia vive en la casa de un familiar.
2255142	Benigno	Duarte Cortaza	MASCULINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	471	.	\N	\N	\N	0	.
1735900	Lucía	Duarte Cortazar	FEMENINO	\N	PARAGUAYO/A	0971255073	CASADO/A	\N	\N	\N	SI	NO	539	Martin Fierro c/ Ursicino Velasco	\N	\N	\N	0	.
1034637	Lucíano	Duarte Cortazar	FEMENINO	2055-07-01	PARAGUAYO/A	0992302807	CASADO/A	\N	\N	\N	SI	NO	1208	Fidelina	\N	\N	\N	3	En el mismno lote tienen dos casitas separadas pero pertenecen al titular.
5994308	María Verónica	Galeano Martínez	FEMENINO	\N	PARAGUAYO/A	072761662	UNION DE HECHO	\N	\N	\N	SI	NO	1592	Ramon Talavera	\N	\N	\N	0	.
1490836	Marta Ofelia	Duarte de Benítez	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	992	MALVINAS C/ Maria Elena	\N	\N	\N	0	El Sr Eulogio trabaja en su propia casa como tecnico-Reperador de refrigeracion .La vivienda tiene otra pieza de material
2336969	Miguela Elizabeth	Duarte de Benítez	FEMENINO	\N	PARAGUAYO/A	0972209992	SEPARADO/A	\N	\N	\N	SI	NO	1735	Ursicino Velasco	\N	\N	\N	0	Tiene una construccion en la parte superior que mide 13x25 mts. Que esta muy bien constituida, los salones se usan para la venta de ropas y de comida rapida.
1384742	Teresa de Jesús	Duarte de Cruz	FEMENINO	\N	PARAGUAYO/A	0981 682 515	CASADO/A	\N	\N	\N	SI	NO	2593	Ursicino Velazco	\N	\N	\N	0	.
4795151	Verónica	Duarte de Quintana	FEMENINO	1984-12-01	PARAGUAYO/A	0985598251	UNION DE HECHO	\N	\N	\N	SI	NO	20	RECICLA	\N	\N	\N	1	.
5852778	Barsaba	Duarte Galeano	MASCULINO	2045-11-12	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	1408	Pajagua Naranja	\N	\N	\N	0	.
4994133	Yoana Elizabeth	Duarte González	FEMENINO	1989-01-04	PARAGUAYO/A	0986690374	SEPARADO/A	\N	\N	\N	SI	NO	1762	Koety	\N	\N	\N	0	Vive dentro del lote de la mama en una extencion de madera.
5144540	Ruth Paola	Duarte Morel	FEMENINO	\N	PARAGUAYO/A	0986 521 711	UNION DE HECHO	\N	\N	\N	SI	NO	2614	Ursicino Velazco	\N	\N	\N	0	Este nucleo familiar vive en la casa del titular, Federico Scheunemann
6247927	Adela	Duarte Ramírez	FEMENINO	1994-09-03	PARAGUAYO/A	0991232838	.	\N	\N	\N	SI	NO	473	23 de JUNIO	\N	\N	\N	0	.
4551270	Alicia	Duarte Ramírez	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	479	23 de Junio	\N	\N	\N	0	.
5593860	Rosa Benigna	Duarte Ramírez	FEMENINO	1988-01-03	PARAGUAYO/A	0981187842	UNION DE HECHO	\N	\N	\N	SI	NO	648	23 de Junio	\N	\N	\N	0	La pareja lleva 11 años de concubinato, sin hijos.
2337035	Osvaldo	Duarte Rios	MASCULINO	\N	PARAGUAYO/A	0984292461	.	\N	\N	\N	SI	NO	1994	Nasaindy	\N	\N	\N	5	.
6326562	Johana Beatriz	Duarte Rivas	FEMENINO	\N	PARAGUAYO/A	0985451238	UNION DE HECHO	\N	\N	\N	SI	NO	1810	Manduyura	\N	\N	\N	0	Placa Electrica, La pareja tiene un año de concubinato, tienen lote propio, techo precario
5614275	Natalia Soledad	Duarte Rivas	FEMENINO	1997-07-04	PARAGUAYO/A	0984811833	UNION DE HECHO	\N	\N	\N	SI	NO	2119	Ursicino Velasco	\N	\N	\N	0	Vive dentro del lote del tio en una extencion de la vivienda.
43014	Benigno	Duarte	MASCULINO	1980-12-01	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	2467	Calle sin nombre(Recicla)	\N	\N	\N	0	El titular no cuenta con elèctrodomesticos , quedo con varias secuelas despùes de un accidente. No tiene cèdula, según refiere la hermana Verònica Duarte Ramirez  extravìo y desconoce el nùmero.
3297010	Cecilio	Duarte	MASCULINO	\N	PARAGUAYO/A	0994717995	SOLTERO/A	\N	\N	\N	SI	NO	51	San Felipe y Santiago	\N	\N	\N	3	El Sr. Anteriormente en la zona 7.
4067291	Cintia Carolina	Duarte	FEMENINO	\N	PARAGUAYO/A	0991952738	UNION DE HECHO	\N	\N	\N	SI	NO	1808	Bañado koeti	\N	\N	\N	3	.
996772	Dalmacia Capdevila	Duarte	FEMENINO	2053-01-11	PARAGUAYO/A	0972450423	SOLTERO/A	\N	\N	\N	SI	NO	216	Virgen de Lujan y Virgen de Lourdes	\N	\N	\N	0	.
3306402	Daria	Duarte	FEMENINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	1609	Nuestra Senora de la Asunción	\N	\N	\N	6	Actualmente esta en el refugio de la S.E.N., por motivos de la inundación.
5189127	Dayanira	Duarte	FEMENINO	1994-09-07	PARAGUAYO/A	0994699379	UNION DE HECHO	\N	\N	\N	SI	NO	1110	.	\N	\N	\N	0	Vive dentro del lote de Elva Duarte tiene una vivienda precaria. Se mantiene totalmente aparte.
4879279	Elva	Duarte	FEMENINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	1109	Angel Luis	\N	\N	\N	0	Es duena del terreno tiene una vivienda de material dos de sus hijos viven con ella adultos con hijos.
7477330	Elvio David	Duarte	MASCULINO	\N	PARAGUAYO/A	0985430436	UNION DE HECHO	\N	\N	\N	SI	NO	538	Martin Fierro c/ Ursicino Velasco	\N	\N	\N	0	.
1125526	Rumilda	Duarte	FEMENINO	2065-07-02	PARAGUAYO/A	0984127885	SOLTERO/A	\N	\N	\N	SI	NO	1852	Villa Maria Auxiliadora	\N	\N	\N	0	.
2319171	Heriberta	Duré Gaona	FEMENINO	\N	PARAGUAYO/A	0985829267	UNION DE HECHO	\N	\N	\N	SI	NO	1985	Nasaindy	\N	\N	\N	5	Es propietaria del lote y una de las viviendas , su hija tiene una construccion pegada a su casa desconoce los datos de la hija y la pareja, los nietos. Ricardo vive dentro del lote de la mama en una extencion de 4x4 es soltero sin hijos.
3794326	Graciela Noemí	Echeverria de González	FEMENINO	\N	PARAGUAYO/A	0983286949	SEPARADO/A	\N	\N	\N	SI	NO	1710	Virgen de Guadalupe	\N	\N	\N	0	.
1706335	Guillermina	Echeverría de Ramírez	FEMENINO	2045-10-02	PARAGUAYO/A	0982191309	CASADO/A	\N	\N	\N	SI	NO	453	Mtro Urcicino Velazco	\N	\N	\N	0	.
1215837	Felicita	Echeverria de Rodríguez	FEMENINO	\N	PARAGUAYO/A	0981886169	CASADO/A	\N	\N	\N	SI	NO	2011	Nasaindy	\N	\N	\N	5	La sra. Posee una lomiteria que se encuentra funcionando en el prdio de la hna 3 anos pero la lomiteria funciona hace 6 anos y aparte su marido tiene un taller de uso para carpinteria aparte la hija se dedica a la profesion de peluqueria.
3892012	Lilian Beatriz	Echeverría	FEMENINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	454	Mtro Urcicino Velzaco	\N	\N	\N	0	Lilian vive dentro del lote y de la vivienda de la mamá
1902912	Saturnina Celeste	Echeverría	FEMENINO	\N	PARAGUAYO/A	0983257148	SEPARADO/A	\N	\N	\N	SI	NO	836	Ursicino Velazco c/ Martín Fierro	\N	\N	\N	0	.
1506	Edelma	.	FEMENINO	\N	No quiso censar	.	.	\N	\N	\N	SI	NO	2401	.	\N	\N	\N	0	No quiso censarse.
4813916	Natalia Belén	Encina Candado	FEMENINO	\N	PARAGUAYO/A	0994717985	UNION DE HECHO	\N	\N	\N	SI	NO	267	Ursicino Velasco	\N	\N	\N	0	.
2317975	María Joséfa	Encina de Alegre	FEMENINO	\N	PARAGUAYO/A	0983141142	.	\N	\N	\N	SI	NO	2075	Pasillo Sin Nombre	\N	\N	\N	0	Es propietaria del lote y una de las casas, dentro del lote vive uno de sus hijos con su esposa e hijos.
3237449	Sebastiana	Encina de Amarilla	FEMENINO	\N	PARAGUAYO/A	.	CASADO/A	\N	\N	\N	SI	NO	826	Vecino Unidos	\N	\N	\N	3	Tiene un terreno en Ñemby.
6624271	Jenifer Fabiola	Encina Escobar	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	2228	.	\N	\N	\N	0	En este momento esta viviendo c/ sus padres. Ella es la dueña. Su padre le compró su Terreno.
6552023	Rossana Elizabeth	Encina Escobar	FEMENINO	\N	PARAGUAYO/A	0983326015	SOLTERO/A	\N	\N	\N	SI	NO	2079	Pasillo Sin Nombre	\N	\N	\N	0	Actualmente vive en la casa de la mama porque la casa esta en reconstrucción total.142,50 m2. Terraza 360x470 m2.
1661214	Orlando	Encina Pereira	MASCULINO	2067-05-11	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	266	Ursicino Velasco	\N	\N	\N	0	.
4813944	Elba Diana	Encina Vealázquez	FEMENINO	\N	PARAGUAYO/A	0986308486	UNION DE HECHO	\N	\N	\N	SI	NO	333	P/4 de Octubre	\N	\N	\N	0	.
967921	Nelida	Enciso de Acosta	FEMENINO	2060-07-08	PARAGUAYO/A	0992875811	.	\N	\N	\N	SI	NO	318	14 de Octubre	\N	\N	\N	0	.
2839494	José Domingo	Escobar Achucarro	MASCULINO	2050-04-08	PARAGUAYO/A	.	SEPARADO/A	\N	\N	\N	SI	NO	403	.	\N	\N	\N	0	.
1423947	Bernardo	Escobar Agüero	MASCULINO	2062-03-04	PARAGUAYO/A	0982 981 841	SEPARADO/A	\N	\N	\N	SI	NO	2233	Sotero Colman	\N	\N	\N	0	.
2334555	María Sixta	Escobar Agüero	FEMENINO	2069-06-05	PARAGUAYO/A	no tiene	UNION DE HECHO	\N	\N	\N	SI	NO	2293	Ramón Talavera	\N	\N	\N	0	.
3381612	Blanca Rosana	Escobar de Encina	FEMENINO	1978-11-06	PARAGUAYO/A	0984620406	CASADO/A	\N	\N	\N	SI	NO	2086	Sotero Colman	\N	\N	\N	0	.
405	Adriana	Martínez	FEMENINO	\N	No quiso censar	.	.	\N	\N	\N	SI	NO	2481	.	\N	\N	\N	0	No quiso censarse.
4570344	Natividad	Escobar Diana	FEMENINO	2017-08-09	PARAGUAYO/A	0982828842	UNION DE HECHO	\N	\N	\N	SI	NO	975	16 de Agosto	\N	\N	\N	0	La casa es de bloque Pro-Vida la otra construcción es de material.
2380045	Calixto	Escobar Florentín	MASCULINO	1970-04-03	PARAGUAYO/A	72240447	SOLTERO/A	\N	\N	\N	SI	NO	1912	Ursicino Velasco  C/Julio Benitez	\N	\N	\N	0	El piso abajo es baldosa y arriba es de ceramica. La casa es de dos pisos.
1511107	Patrocinia	Escobar Florentín	FEMENINO	2068-08-05	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	1638	Pajagua Naranja	\N	\N	\N	0	.
2514399	Rubén	Escobar Florentín	MASCULINO	\N	PARAGUAYO/A	0994447381	.	\N	\N	\N	SI	NO	2164	Ursicino Velasco c/ Julio Benitez	\N	\N	\N	0	.
4669432	Ricardo	Escobar Leiva	MASCULINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	2129	San Martin de Porres	\N	\N	\N	0	.
1628699	Ynocencia	Escobar Vda. De Heberth	FEMENINO	\N	PARAGUAYO/A	0984961109	VIUDO/A	\N	\N	\N	SI	NO	614	Ursicino Velasco y San Felipe y Santiago	\N	\N	\N	0	Tiene un Salon de Belleza. Dentro de este lote viven dos Flias , con dos techos . La titular es viuda y su hijo mayor con su concubina y un hijo menor
1907232	Antonia	Escobar	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	2734	YVOTY	\N	\N	\N	0	.
2256556	Arminda	Escobar	FEMENINO	2043-09-08	PARAGUAYO/A	0983416337	VIUDO/A	\N	\N	\N	SI	NO	1045	Yvoty Rory 805	\N	\N	\N	0	.
2298311	Edelira	Escobar	FEMENINO	\N	PARAGUAYO/A	0982197117	VIUDO/A	\N	\N	\N	SI	NO	1055	Ursicino Velasco	\N	\N	\N	0	.
4517813	José Miguel	Escobar	MASCULINO	1986-08-02	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	2236	Sotero Colman	\N	\N	\N	0	.
316	Escuela Sotero Colmán	.	.	\N	.	.	.	\N	\N	\N	SI	NO	2168	Ursicino Velasco	\N	\N	\N	0	10 aulas, un patio en el medio y una cancha al costado 2 aulas mas uno para comedor y otro la dirección un sector de la escuela es de dos pisos ,cuenta con 14 docentes incluyendo Directora y Vice.La escuela trabaja en ambos turnos.
4513670	Digna Beatriz	Escurra Cabrera	FEMENINO	1988-12-09	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1373	Yvoty Rory	\N	\N	\N	0	Vive dentro del lote de la mama con su pareja e hijos dentro de la misma vivienda.
809150	Estela	Escurra	FEMENINO	2056-02-01	PARAGUAYO/A	.	VIUDO/A	\N	\N	\N	SI	NO	2031	Mbo'ehára	\N	\N	\N	0	La mitad de la vivienda es mitad techo y mitad hormigón
4520435	Celeste Guillermina	Esteche Pereira	FEMENINO	1986-03-06	PARAGUAYO/A	0961753994	.	\N	\N	\N	SI	NO	61	San Felipe y Santiago	\N	\N	\N	3	.
4984916	Deolinda	Espinola Avalos	FEMENINO	\N	PARAGUAYO/A	0985 491 607	CASADO/A	\N	\N	\N	SI	NO	1716	Yacare Pito	\N	\N	\N	0	.
5966180	Nilda Elizabeth	Espinola Avalos	FEMENINO	1992-06-03	PARAGUAYO/A	0982 842 027	SOLTERO/A	\N	\N	\N	SI	NO	1718	Yacare Pito	\N	\N	\N	3	.
1981792	Mercedes Eustaquia	Espinola Garcete	FEMENINO	\N	PARAGUAYO/A	0984841057	CASADO/A	\N	\N	\N	SI	NO	792	Virgen de Lourdes	\N	\N	\N	0	Nery tiene una casa totalmente independiente dentro del lote de la titular, la misma es de material vive en techo aparte desde los 20 tiene: Heladera -TV Cable y enseres de dormitorio(cama, ropero, televisor).
2036975	Crecensia	Espinola Gómez	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	105	.	\N	\N	\N	0	.
4919562	Fatima Adriana	Espinola López	FEMENINO	1989-09-06	PARAGUAYO/A	0991759957	UNION DE HECHO	\N	\N	\N	SI	NO	719	Mar del Plata	\N	\N	\N	5	Tiene un Gallinero.
5758808	Adolfina	Espinola Medina	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	58	Calle sin nombre	\N	\N	\N	0	.
5046981	María José	Espinola Medina	FEMENINO	\N	PARAGUAYO/A	072333623	UNION DE HECHO	\N	\N	\N	SI	NO	232	Nuevo Amanecer	\N	\N	\N	0	.
1488567	Eligia	Espínola Núñez	FEMENINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	1605	Payagua Naranja	\N	\N	\N	5	.
1633339	Marina	Espinola Núñez	FEMENINO	2069-03-03	PARAGUAYO/A	0984406524	UNION DE HECHO	\N	\N	\N	SI	NO	2715	Pajagua Naranja	\N	\N	\N	5	.
4198437	Diana Elizabeth	Espinola Quintana	FEMENINO	\N	PARAGUAYO/A	0981292408	CASADO/A	\N	\N	\N	SI	NO	1155	Vecinos Unidos	\N	\N	\N	0	.
5461049	Miguel Angel	Espinola Sánchez	MASCULINO	\N	PARAGUAYO/A	0983 913615	UNION DE HECHO	\N	\N	\N	SI	NO	1617	San Luis	\N	\N	\N	0	.
3596601	Bernarda	Espínola	FEMENINO	1976-11-02	PARAGUAYO/A	425427	CASADO/A	\N	\N	\N	SI	NO	2446	Ursicino Velasco c/ Santa Librada	\N	\N	\N	0	Siempre vivo en el barrio.
3700966	Delia Denis	Espínola	FEMENINO	1981-04-11	PARAGUAYO/A	0994984173	UNION DE HECHO	\N	\N	\N	SI	NO	2191	Sotero Colman	\N	\N	\N	0	1 corredor
4754	Jonathan Moisés	Espinoza B	MASCULINO	1999-08-02	PARAGUAYO/A	0972634486	UNION DE HECHO	\N	\N	\N	SI	NO	667	Los Sauces	\N	\N	\N	0	Tiene una extension de terciada a continuacion de la casa del padre tienendos entradas diferentes.
6823762	Hugo Orlando	Espinoza Benegas	MASCULINO	\N	PARAGUAYO/A	0971396566	UNION DE HECHO	\N	\N	\N	SI	NO	1910	Julio Benitez	\N	\N	\N	0	Posee una extencion de madera por la casa de la madre y es aconcubinado hace 6 anos.
5034396	Lider Nicolás	Espinoza Benítez	MASCULINO	\N	PARAGUAYO/A	0984359851	UNION DE HECHO	\N	\N	\N	SI	NO	669	Sauce	\N	\N	\N	0	.
1970728	Nicolás	Espinoza Duarte	MASCULINO	1970-10-09	PARAGUAYO/A	0986407657	SEPARADO/A	\N	\N	\N	SI	NO	666	Los Sauce	\N	\N	\N	0	El Sr. Se censo solo porque tiene pareja circunstanciales.
3983297	Silvia Beatriz	Espinoza Pereira	FEMENINO	\N	PARAGUAYO/A	0972799714	UNION DE HECHO	\N	\N	\N	SI	NO	411	Mar del Plata	\N	\N	\N	0	Vive dentro del terreno de la mama.
3889374	Karen Viviana	Espinoza Recalde	FEMENINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	199	.	\N	\N	\N	0	.
4484300	Reina Carolina	Espinoza Rojas	FEMENINO	\N	PARAGUAYO/A	0992925309	UNION DE HECHO	\N	\N	\N	SI	NO	907	Maestro Ursicino Velasco	\N	\N	\N	0	.
5443722	Andrea Belén	Esquivel Amarilla	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1020	Mar de plata	\N	\N	\N	0	Es una casa de madera muy precaria.
5443718	Lorena Grisel	Esquivel Amarilla	FEMENINO	1994-02-08	PARAGUAYO/A	0971 158226	UNION DE HECHO	\N	\N	\N	SI	NO	1984	Pasillo Martin fierro	\N	\N	\N	3	Cocina y dormitorio, todo dentro de la pieza.
2671007	Nancy Mirian	Esquivel de Ortega	FEMENINO	2060-01-05	PARAGUAYO/A	0985145177	CASADO/A	\N	\N	\N	SI	NO	561	8 de Diciembre	\N	\N	\N	0	La casa tiene en el fondo del patio una pieza de 4x6 con corredor.
3527117	Liz Blasida	Esquivel de Quintana	FEMENINO	1981-03-02	PARAGUAYO/A	0981742667	.	\N	\N	\N	SI	NO	2406	Ursicino Velasco	\N	\N	\N	0	.
5758884	Bruno de Jesús	Esquivel Maciel	MASCULINO	\N	PARAGUAYO/A	0993 547 884	UNION DE HECHO	\N	\N	\N	SI	NO	2014	Pilcomayo c/ yacare	\N	\N	\N	0	llevan cuatro años de concubinato. Viven dentro del lote de su madre en una extensión de pieza de material, que mide 3 m de frente y 4 m de fondo.
5858124	Laura Eliza	Esquivel Maciel	FEMENINO	\N	PARAGUAYO/A	0971671387	UNION DE HECHO	\N	\N	\N	SI	NO	2391	.	\N	\N	\N	0	.
1257267	Alberta	Esquivel Ortega	FEMENINO	2066-07-08	PARAGUAYO/A	0991194444	SOLTERO/A	\N	\N	\N	SI	NO	766	Pilcomayo 3632	\N	\N	\N	0	.
1771852	Nicolása	Esquivel Ortega	MASCULINO	2064-10-09	PARAGUAYO/A	0982829533	UNION DE HECHO	\N	\N	\N	SI	NO	1308	Remancito c/ Mto. Ursicino Velasco	\N	\N	\N	0	.
1921518	Rodolfo Tomas	Esquivel Ortega	MASCULINO	\N	PARAGUAYO/A	0971191066	SOLTERO/A	\N	\N	\N	SI	NO	753	Martin Fierro	\N	\N	\N	5	.
3650215	Ana María	Esquivel Ramírez	FEMENINO	\N	PARAGUAYO/A	0986613895	CASADO/A	\N	\N	\N	SI	NO	546	Mtro Urcisino Velazco	\N	\N	\N	0	Tiene una vivienda amplia de dos pisos
4501843	Julio César	Esquivel Ramírez	MASCULINO	\N	PARAGUAYO/A	0982530266	CASADO/A	\N	\N	\N	SI	NO	2115	Ursicino Velasco	\N	\N	\N	0	Vive dentro de la casa de la mama en una pieza con su pareja e hijos llevan casados 5 anos.
4487360	Nancy Irene	Esquivel Ramírez	FEMENINO	1987-09-05	PARAGUAYO/A	0984322239	CASADO/A	\N	\N	\N	SI	NO	583	8 de Diciembre	\N	\N	\N	0	Tiene un galpon.
907045	Hermelinda	Esquivel	FEMENINO	2056-04-06	PARAGUAYO/A	0982843450	SOLTERO/A	\N	\N	\N	SI	NO	536	San Felipe y Santiago	\N	\N	\N	0	Hermelinda tiene una cosa de dos pisos amplia donde viven sus hijos con sus parejas , en total hay 2 parejas.
4342338	Liliana Elizabeth	Esquivel	FEMENINO	1986-11-09	PARAGUAYO/A	0986615325	UNION DE HECHO	\N	\N	\N	SI	NO	1578	Ramon Talavera	\N	\N	\N	0	.
337	Establo de Vacas	.	.	\N	.	.	.	\N	\N	\N	SI	NO	2486	Sotero Colman	\N	\N	\N	0	El establo es del señor Arce, que se censo en su casa, es un armaje con techo
4312063	Herminia	Esteche de Olmedo	FEMENINO	1984-03-02	PARAGUAYO/A	0961507941	CASADO/A	\N	\N	\N	SI	NO	65	San Felipe y Santiago	\N	\N	\N	0	.
4805855	Elizabeth Saturnina	Esteche Florentín	FEMENINO	\N	PARAGUAYO/A	0993532775	UNION DE HECHO	\N	\N	\N	SI	NO	358	Virgen de Lujan C/ Aromita	\N	\N	\N	0	.
5373339	María Magdalena	Esteche Perreira	FEMENINO	\N	PARAGUAYO/A	0983698735	UNION DE HECHO	\N	\N	\N	SI	NO	802	San Felipe y Santiago	\N	\N	\N	0	La vivienda es de Bloque pro vida, está pegada a una de material con techo de zinc. El terreno era de la abuela que falleció hace 9 años, los hijos no tienen cédula de identidad, ni siquiera en proceso de solicitud, vive con su pareja actual hace 2 años
1983472	Maríano	Esteche	FEMENINO	1971-05-01	PARAGUAYO/A	0971222579	CASADO/A	\N	\N	\N	SI	NO	611	Ursicino Velazco y San Felipe y Santiago	\N	\N	\N	0	La pareja lleva viviendo en el bañado tacumbú más de 40 años y como casados 21 años
3431680	Victor	Esteche	MASCULINO	\N	PARAGUAYO/A	0982565617	.	\N	\N	\N	SI	NO	2741	4 de Octubre e/ Yvoty	\N	\N	\N	4	Actualmente el sr. Esteche no vive en la casa, esta alquilando la casa a Nancy Franco hace 3 meses.
3234704	María Agustína	Estigarribia de Varela	FEMENINO	\N	PARAGUAYO/A	0995685377	SEPARADO/A	\N	\N	\N	SI	NO	1679	Pasillo IVU	\N	\N	\N	0	.
921463	Limpia Concepción	Estigarribia Medina	FEMENINO	\N	PARAGUAYO/A	0981804882	SOLTERO/A	\N	\N	\N	SI	NO	2551	Mallorquin 1842	\N	\N	\N	0	Actualmente vive en ñemby se mudo de la crecida,y su vivienda se encuentra en reconstruccion.
3664354	Maríana	Estigarribia Mendoza	FEMENINO	2058-02-06	PARAGUAYO/A	0991937360	UNION DE HECHO	\N	\N	\N	SI	NO	575	.	\N	\N	\N	0	La Sra. Hace anos que vive en el mismo predio pero ahora en una casa de la Cooperativa dejando su casa mas antigua a uno de sus hijos.
1068319	Virgilia Natividad	Estigarribia Román	FEMENINO	\N	PARAGUAYO/A	0993546859	UNION DE HECHO	\N	\N	\N	SI	NO	1174	Urcicino Velzaco	\N	\N	\N	0	.
4600113	María Fermina	Estigarribia Velázquez	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	2628	Nuestra Señora de la Asuncion	\N	\N	\N	6	1 Radio grabadora.
4117834	María Ramona	Estigarribia Velázquez	FEMENINO	\N	PARAGUAYO/A	0992476792	SEPARADO/A	\N	\N	\N	SI	NO	1895	Julio Benitez	\N	\N	\N	0	Usa electrodosmeticos de la madre.
5480173	Edith Johana	Fabián Martínez	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	298	Yvoty c/ Las Orquideas	\N	\N	\N	0	.
5027999	Lina Dorotea	Fabián Martínez	FEMENINO	\N	PARAGUAYO/A	0982603121	UNION DE HECHO	\N	\N	\N	SI	NO	207	Aromita	\N	\N	\N	0	.
3450130	Fatima Liz	Famoso de Aguilera	FEMENINO	1984-07-05	PARAGUAYO/A	0982205833	CASADO/A	\N	\N	\N	SI	NO	843	Ursicino Velasco	\N	\N	\N	0	.
3450135	Domitila	Famoso de Arevalos	FEMENINO	1981-09-05	PARAGUAYO/A	0982638741	UNION DE HECHO	\N	\N	\N	SI	NO	842	Ursicino Velasco	\N	\N	\N	0	La Sra. Tiene un sobrino de 23 años que vive en su lote hace 13 años con una construccion de material piso de ceramica.Fotos :2
3449251	Bernardina	Famoso de Martínez	FEMENINO	2059-12-03	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	2537	Mandy Jura	\N	\N	\N	0	Es propietaria del lote y viven sus hijos con sus flias.
1481049	Petrona	Farfan Espinola	FEMENINO	\N	PARAGUAYO/A	0971302992	UNION DE HECHO	\N	\N	\N	SI	NO	1332	Urisicino Velasco C/ Yvoty Rory	\N	\N	\N	0	.
4835932	Cesar Luis	Farfan	MASCULINO	\N	PARAGUAYO/A	0991191518	UNION DE HECHO	\N	\N	\N	SI	NO	461	San Cayetano	\N	\N	\N	0	.
4634859	Leticia Rafaela	Farina Rodríguez	FEMENINO	\N	PARAGUAYO/A	0982842783	SOLTERO/A	\N	\N	\N	SI	NO	1276	Ursicino Velasco	\N	\N	\N	0	La Sra. Leticia tiene una extencion en frente de la casa de su mama de 8x4 de Hormigon y esta muy bien constituida.
3736571	Norma de Jesús	Fariña Domínguez	FEMENINO	1984-10-12	PARAGUAYO/A	0981867651	SOLTERO/A	\N	\N	\N	SI	NO	1108	26 de julio	\N	\N	\N	0	La señora Norma no tiene cocina, el baño comparte con el dueño de la casa, es encargada de la pieza.
5091870	Adriana Soledad	Fariña Rodríguez	FEMENINO	1991-05-01	PARAGUAYO/A	0982893965	UNION DE HECHO	\N	\N	\N	SI	NO	1178	Urcicino Velazco	\N	\N	\N	0	.
3831699	Juan Ramón Dario	Fariña Rodríguez	MASCULINO	\N	PARAGUAYO/A	0982750695	SOLTERO/A	\N	\N	\N	SI	NO	1177	Urcicino Velazco	\N	\N	\N	0	.
887518	Estela Mary	Fariña	FEMENINO	2062-07-08	PARAGUAYO/A	0986906215	UNION DE HECHO	\N	\N	\N	SI	NO	2043	Ursicino Velasco c/ Sotero Colman	\N	\N	\N	0	La sra. Tiene una lomiteria con un carrito y un puesto de comida, cocina para el Proyecto Ikatu.
1107526	María Concepción	Fariña	FEMENINO	\N	PARAGUAYO/A	0982498688	SOLTERO/A	\N	\N	\N	SI	NO	2089	Sotero Colman	\N	\N	\N	0	.
6012369	Juana Graciela	Fernández Acosta	MASCULINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1238	Fidelina	\N	\N	\N	0	Una cocina de 4 hornallas. La Sra. Juana es hgija del SR. Heriberto vive en la propiedad en una piecita.
1075336	Olinda Igancia	Fernández Alcantara	FEMENINO	\N	PARAGUAYO/A	0992857484	CASADO/A	\N	\N	\N	SI	NO	1742	Ursicino Velasco	\N	\N	\N	0	La vivienda cuenta con un corredor dentro de la vivienda.Era de dos pisos en la ultima inundación entraron a robar y quemaron la planta alta.
4300415	Norma Beatriz	Fernández Amarilla	FEMENINO	\N	PARAGUAYO/A	0981952268	UNION DE HECHO	\N	\N	\N	SI	NO	2727	8 de Diciembre	\N	\N	\N	0	La casa tiene una Galería que hace de patio interno.
975940	Alba Fidelina	Fernández Bogado	FEMENINO	\N	PARAGUAYO/A	0984404459	CASADO/A	\N	\N	\N	SI	NO	2021	Mbo'ehara	\N	\N	\N	5	.
7030497	Tania Soledad	Fernández Escobar	FEMENINO	\N	PARAGUAYO/A	0983977481	UNION DE HECHO	\N	\N	\N	SI	NO	1514	Pajagua Naranja	\N	\N	\N	5	la Familia vive en extrema pobreza
1583965	Teresa	Fernández Fernández	FEMENINO	2067-09-01	PARAGUAYO/A	0985727091	SOLTERO/A	\N	\N	\N	SI	NO	1176	Urcicino Velazco	\N	\N	\N	0	dentro de la vivienda están dos madres solteras
5080926	Olda Rocío	Fernández Fleitas	FEMENINO	\N	PARAGUAYO/A	0983468858	UNION DE HECHO	\N	\N	\N	SI	NO	405	Algarrobo	\N	\N	\N	0	Posee dos construcciones en el mismo terreno.
3308647	Gustavo	Fernandez Gonzalez	MASCULINO	\N	PARAGUAYO/A	0981548014	UNION DE HECHO	\N	\N	\N	SI	NO	2738	Mtro Ursicino Velasco	\N	\N	\N	0	.
1827268	Victor Hugo	Fernández González	MASCULINO	\N	PARAGUAYO/A	0981824037	UNION DE HECHO	\N	\N	\N	SI	NO	2178	Mto. Ursicino Velasco	\N	\N	\N	0	La vivienda cuenta con dos corredores en la parte posterior y frontal.
5991245	Leandra Viviana	Fleitas Fernández	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	2706	San Felipe y Santiago c/ Recicla	\N	\N	\N	3	.
1557821	Rosalia	Fernández Lezcano	FEMENINO	2064-04-09	PARAGUAYO/A	0991580815	SEPARADO/A	\N	\N	\N	SI	NO	1782	Bañado Koeti	\N	\N	\N	5	Es propietario del lote, dentro de el vive su hijo con su pareja e hijos en una vivienda de material totalmente independiente.
5719386	Dahyana Salustiana	Fernández Martínez	FEMENINO	1992-08-06	PARAGUAYO/A	0992802515	SOLTERO/A	\N	\N	\N	SI	NO	1199	Fidelina	\N	\N	\N	0	La vivienda pertenece a Ceferino Fernandez, padre de Dahyana , pero quizo que su hija fuera censada como titular.
5719380	Mabel Carolina	Fernández Martínez	FEMENINO	\N	PARAGUAYO/A	0984869579	UNION DE HECHO	\N	\N	\N	SI	NO	26	San Felipe y Santiago	\N	\N	\N	3	.
3527371	Nilda Stephanie	Fernández Méndez	FEMENINO	1989-08-01	PARAGUAYO/A	422326	SEPARADO/A	\N	\N	\N	SI	NO	2193	Mbo-ehara	\N	\N	\N	0	La casa es de dos pisos, el techo tbm es de hormigon
2248583	Heriberto Silvano	Fernández	MASCULINO	\N	PARAGUAYO/A	0981832261	VIUDO/A	\N	\N	\N	SI	NO	1230	Fidelina	\N	\N	\N	0	Ernesto ,hijo deHeriberto, dueno del terreno, vive dentro del terreno en una pieza totalmente independiente en su pieza funciona dormitorio, cocina, comedor.La pareja solventan sus gastos aparte.
3173648	Julia Emilia	Fernández	FEMENINO	\N	PARAGUAYO/A	.	SEPARADO/A	\N	\N	\N	SI	NO	850	SIN NOMBRE	\N	\N	\N	0	.
2986550	María Gloria	Fernández	FEMENINO	\N	PARAGUAYO/A	00982711858	SOLTERO/A	\N	\N	\N	SI	NO	92	Virgen de Lourdes	\N	\N	\N	3	Tiene Venta de Minutas.
4074108	Yeny Carolina	Fernández	FEMENINO	1984-03-07	PARAGUAYO/A	0984253679	UNION DE HECHO	\N	\N	\N	SI	NO	1865	Villa Maria Auxiliadora	\N	\N	\N	0	.
4999596	Mariela Asuncion	Ferraris	FEMENINO	\N	PARAGUAYO/A	0981217802	UNION DE HECHO	\N	\N	\N	SI	NO	1193	Fidelina	\N	\N	\N	0	No tiene costado (casa completaa lo ancho).
4895925	Mirta Magdalena	Ferreira Aquino	FEMENINO	\N	PARAGUAYO/A	0994944864	UNION DE HECHO	\N	\N	\N	SI	NO	1352	Primavera	\N	\N	\N	0	Vive dentro del lote de la mama ,estan construyendo su casa ,anteriormente vivia en la Zona 5-A.
6693367	Aliana	Ferreira Bogado	FEMENINO	1992-06-06	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	121	Nuevo Amanecer	\N	\N	\N	0	Laa Sra. Aliana es encargada de al casa no duena. La propietaria trabaja en Argentina su nombre es Christian Gabriel Salcedo.
3906795	Bernarda	Ferreira Bogado	FEMENINO	1975-11-06	PARAGUAYO/A	095350343	SOLTERO/A	\N	\N	\N	SI	NO	407	Julio Jara y Nuevo Amanecer	\N	\N	\N	0	.
7119426	Daisy	Ferreira Bogado	FEMENINO	\N	PARAGUAYO/A	0984479693	SOLTERO/A	\N	\N	\N	SI	NO	1196	Fidelina	\N	\N	\N	0	.
4618219	Nicolás Dario	Ferreira Bogado	MASCULINO	1984-06-12	PARAGUAYO/A	0998447969	SOLTERO/A	\N	\N	\N	SI	NO	1195	Fidelina	\N	\N	\N	0	Su hija vive con el pero aun no esta reconocida( Hace 4 anos).
1956835	Aurora	Ferreira de Caballero	FEMENINO	\N	PARAGUAYO/A	0986160070	UNION DE HECHO	\N	\N	\N	SI	NO	1343	Primavera	\N	\N	\N	0	La duena de la casa reside en la ciudad de Luque como encargada de una casa y en el terreno censado vive como cuidador de la casa el Sr. Martin Ferreira.
4275984	Zunilda	Ferreira de Cardozo	FEMENINO	\N	PARAGUAYO/A	0984328057	CASADO/A	\N	\N	\N	SI	NO	723	Ursicino Velasco 3471	\N	\N	\N	0	.
2151480	Alicia	Ferreira Giménez	FEMENINO	\N	PARAGUAYO/A	0972432939	UNION DE HECHO	\N	\N	\N	SI	NO	1802	Rancho 8	\N	\N	\N	0	centrifugadora 1
4650029	Mariel Gissela	Ferreira Giménez	FEMENINO	\N	PARAGUAYO/A	0991471230	SOLTERO/A	\N	\N	\N	SI	NO	1490	Mainumby	\N	\N	\N	0	Su casa ocupa todo el ancho(no tiene costado).
4344882	Lorena Virina	Ferreira Martínez	FEMENINO	\N	PARAGUAYO/A	0982112755	UNION DE HECHO	\N	\N	\N	SI	NO	2128	San Martin de Porres	\N	\N	\N	0	Estan de pareja hace 2 anos.
4976347	Jorge Lucíano	Ferreira Santos	MASCULINO	1993-06-08	PARAGUAYO/A	0991853833	UNION DE HECHO	\N	\N	\N	SI	NO	1008	Mar de Plata	\N	\N	\N	0	.
3567318	María Fatima	Ferreira Troche	FEMENINO	1979-07-10	PARAGUAYO/A	0961458189	UNION DE HECHO	\N	\N	\N	SI	NO	259	Virgen de Lourdes	\N	\N	\N	0	Tiene en frente un Galpón.
2935579	Gladys Zunilda	Ferreira Vda. De Ciancio	FEMENINO	\N	PARAGUAYO/A	0991882827	VIUDO/A	\N	\N	\N	SI	NO	1421	Mainumby	\N	\N	\N	0	.
3376570	Vicenta	Ferreira Vda. De Orrego	FEMENINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	1270	.	\N	\N	\N	0	.
4589420	Miguel Isaías	Ferreira Yapari	MASCULINO	1987-09-06	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	690	Virgen de Lujan c/ Aromita	\N	\N	\N	0	.
1741207	Alejandra	Ferreira	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	1664	.	\N	\N	\N	0	Nacio y se crio en la misma casa.
1009154	Antonio	Ferreira	MASCULINO	2060-04-05	PARAGUAYO/A	0992381341	SOLTERO/A	\N	\N	\N	SI	NO	1369	Bañado Akati	\N	\N	\N	0	.
4220539	Cirila Ramona	Ferreira	FEMENINO	1978-07-07	PARAGUAYO/A	0976371966	UNION DE HECHO	\N	\N	\N	SI	NO	2662	Nuestra Sra. De la Asunción	\N	\N	\N	6	.
4220538	Daniela Isabel	Ferreira	MASCULINO	1975-03-01	PARAGUAYO/A	0994147453	SOLTERO/A	\N	\N	\N	SI	NO	2316	Nuestra Sra. De la Asunción	\N	\N	\N	6	Aire Portatil 1
3370575	Felipa	Ferreira	FEMENINO	\N	PARAGUAYO/A	0981234666	.	\N	\N	\N	SI	NO	1838	Santa Librada	\N	\N	\N	0	Es propietaria del lote y la vivienda.Cuenta con permiso de ocupación.
1324406	María Elizabeth	Ferreira	FEMENINO	\N	PARAGUAYO/A	0981374694	UNION DE HECHO	\N	\N	\N	SI	NO	694	Yacare Yrupe	\N	\N	\N	5	Los dos hijos tiene su vivienda a parte , pero por ser menores de 25 fueron censados con su madre.
679603	Mario Rufino	Ferreira	MASCULINO	2057-10-07	PARAGUAYO/A	0984130311	.	\N	\N	\N	SI	NO	1665	Virgen de Guadalupe	\N	\N	\N	0	.
1800112	Matilde	Ferreira	FEMENINO	\N	PARAGUAYO/A	0994152808	UNION DE HECHO	\N	\N	\N	SI	NO	1074	Mar de plata	\N	\N	\N	0	Casa de dos plantas.
289	Martin	Fierro	MASCULINO	\N	.	.	.	\N	\N	\N	SI	NO	1072	Ursicino Velasco	\N	\N	\N	0	No quiso censarse.
1575346	Estela	Figueredo Candia	FEMENINO	2066-10-01	PARAGUAYO/A	0983254577	VIUDO/A	\N	\N	\N	SI	NO	1554	Pjagua Naranja	\N	\N	\N	0	.
2151472	Ana Beatriz	Figueredo Guillen	FEMENINO	\N	PARAGUAYO/A	0981598604	CASADO/A	\N	\N	\N	SI	NO	11	San Felipe y Santiago	\N	\N	\N	0	Dentro de la vivienda vive su hijo con su pareja ambos mayores de edad sin hijos, los cuáles se mantienen solos.
2024723	Felipa Edelmira	Figueredo Recalde	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	2167	Ursicino Velasco c/ Julio Benitez	\N	\N	\N	0	.
1558112	Ramona Elizabeth	Figueredo Recalde	MASCULINO	1971-07-09	PARAGUAYO/A	0986794576	.	\N	\N	\N	SI	NO	1929	.	\N	\N	\N	0	.
1110987	Lucia	Figueredo	FEMENINO	2068-04-03	.	0984308438	SEPARADO/A	\N	\N	\N	SI	NO	2336	NUESTRA SEÑORA DE LA ASUNCION	\N	\N	\N	6	ACTUALMENTE LA TITULAR VVE EN UNA CASA PRESTADA EN SAJONIA. POR CAUSA DE LA INUNDACION
893793	Vicenta	Figueredo	FEMENINO	\N	PARAGUAYO/A	0995686796	SOLTERO/A	\N	\N	\N	SI	NO	2340	Nuestra Sra. De la Asunción	\N	\N	\N	6	.
6533508	Carmen Elizabeth	Fleitas Benítez	FEMENINO	\N	PARAGUAYO/A	0971139880	UNION DE HECHO	\N	\N	\N	SI	NO	1215	8 de Diciembre	\N	\N	\N	0	La pareja esta junta hace 2 anos vive con el tio en la misma vivienda.Centrifugadora 1, Cocina Electrica 1.
4134615	Ana Beatriz	Fleitas de Quintana	FEMENINO	\N	PARAGUAYO/A	0982708914	CASADO/A	\N	\N	\N	SI	NO	1968	Sotero Colman	\N	\N	\N	0	.
3832931	José	Fleitas DemoFonte	MASCULINO	1982-04-01	PARAGUAYO/A	0971940718	UNION DE HECHO	\N	\N	\N	SI	NO	1557	Ursicino Velasco	\N	\N	\N	0	Vive dentro del lote de la mama con su pareja e hijos, llevan en concubinato 6 anos.Segun vecinos se mudo hace tres meses por el tema del censo.
4305488	Rodrigo Manuel	Fleitas Ferreira	MASCULINO	\N	PARAGUAYO/A	0972432939	UNION DE HECHO	\N	\N	\N	SI	NO	1821	Rancho 8	\N	\N	\N	0	La pareja lleva 8 años de concubinato, tienen 3 hijos menores, poseen lote propio y techo precario
2105472	Eladio	Fleitas Pereira	MASCULINO	\N	PARAGUAYO/A	.	CASADO/A	\N	\N	\N	SI	NO	1541	Pajagua Naranja	\N	\N	\N	0	.
4933206	Emilce Adriana	Florentín Almeida	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1131	Virgen de Lujan	\N	\N	\N	0	.
3235563	María de la Cruz	Florentín Almeida	FEMENINO	1979-03-05	PARAGUAYO/A	0981421624	SOLTERO/A	\N	\N	\N	SI	NO	801	Virgen de Lujan c/ Aromita	\N	\N	\N	0	.
3724751	Mirian Concepcion	Florentín Almeida	FEMENINO	\N	PARAGUAYO/A	0994141832	UNION DE HECHO	\N	\N	\N	SI	NO	100	.	\N	\N	\N	3	Vende Remedios Yuyos.
4160151	Sandra Mercedes	Florentín Almeida	FEMENINO	1987-06-08	PARAGUAYO/A	0994141832	.	\N	\N	\N	SI	NO	2416	Sin Nombre	\N	\N	\N	0	.
3981362	Fernando Ariel	Florentín Ameida	MASCULINO	1987-01-08	PARAGUAYO/A	0983597417	UNION DE HECHO	\N	\N	\N	SI	NO	367	Virgen de Lourdes c/ Oleros	\N	\N	\N	0	.
2529614	Luciano	Florentín Cáceres	MASCULINO	\N	PARAGUAYO/A	0981602315	SOLTERO/A	\N	\N	\N	SI	NO	2332	Nuestra Sra. De la Asunción	\N	\N	\N	6	El titular es padre soltero hace 8 anos que esta separado de la madre de sus hijos.Actualmente refiere que esta sin pareja.
2312882	Adelina	Florentín de Chavez	FEMENINO	1975-05-02	PARAGUAYO/A	0983 336 525	CASADO/A	\N	\N	\N	SI	NO	2701	8 de Diciembre	\N	\N	\N	0	.
1524732	Delia	Florentín de Escobar	FEMENINO	2050-12-01	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	1635	Pajagua Naranja	\N	\N	\N	0	La señora posee una despensa bien surtida, donde tambien vende empanadas y milanesas, el señor cobra tercera edad
1233133	Enriqueta	Florentín de Ortíz	FEMENINO	\N	PARAGUAYO/A	0991185753	CASADO/A	\N	\N	\N	SI	NO	201	Mtro Ursicino Velazco esq. Nuevo Amanecer	\N	\N	\N	4	.
2104430	Laura Annavel	Florentín de Rojas	FEMENINO	1975-10-08	PARAGUAYO/A	0986667086	CASADO/A	\N	\N	\N	SI	NO	306	Virgen de Lourdes c/ Oleros	\N	\N	\N	0	.
1058081	Benigno	Florentín Guillen	MASCULINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	2065	Sotero Colman	\N	\N	\N	0	.
5248894	Liz Celeste	Florentín Sanabria	FEMENINO	\N	PARAGUAYO/A	.	SEPARADO/A	\N	\N	\N	SI	NO	767	26 de Julio Z-5b	\N	\N	\N	0	La señora vive con la hermana, La señora Maura Florentín, hace 3 años.
6359181	María Ligia	Florentín Sanabria	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	815	26 de Julio	\N	\N	\N	5	Casa construida por un Casa para mi País.
3184663	Maura Catalina	Florentín Sanabria	FEMENINO	\N	PARAGUAYO/A	0972684066	SEPARADO/A	\N	\N	\N	SI	NO	764	26 de Julio	\N	\N	\N	5	Casa de la cooperativa.
853635	Bernardo	Florentín	MASCULINO	2052-08-10	PARAGUAYO/A	0984651652	SOLTERO/A	\N	\N	\N	SI	NO	1130	Virgen de Lujan	\N	\N	\N	0	.
2856139	Cecilia	Florentín	FEMENINO	\N	PARAGUAYO/A	0984977752	SOLTERO/A	\N	\N	\N	SI	NO	62	Angel de Lourdes	\N	\N	\N	3	No maneja la dimension de su terreno, la hija Laura Florentin vive dentro del mismo lote con otro techo precario.
2104440	Clara Liliana	Florentín	FEMENINO	\N	PARAGUAYO/A	0992959281	UNION DE HECHO	\N	\N	\N	SI	NO	1139	Virgen de Lujan	\N	\N	\N	0	Tiene una Heladeria.
3610484	Clemente	Florentín	MASCULINO	\N	PARAGUAYO/A	09086636656	CASADO/A	\N	\N	\N	SI	NO	79	Angel Luis	\N	\N	\N	0	.
5047769	Gladys Marlene	Florentín	FEMENINO	1982-01-04	PARAGUAYO/A	0986606495	SEPARADO/A	\N	\N	\N	SI	NO	1678	Virgen de Guadalupe	\N	\N	\N	0	No se puede quitar bien las fotos, es un pasillo muy chico tiene dos Pro-Vida juntas.
3009406	María Ursula	Florentín	FEMENINO	\N	PARAGUAYO/A	.	VIUDO/A	\N	\N	\N	SI	NO	976	San Felipe y Santiago	\N	\N	\N	0	El señor Eladio David González, es nieto de la Doña Úrsula, vive en el lote dentro de una casa prearia.
4435902	Erika Natalicia	Flores Ayala	FEMENINO	\N	PARAGUAYO/A	0991359289	UNION DE HECHO	\N	\N	\N	SI	NO	739	Mto. Ursicino Velasco	\N	\N	\N	0	.
4518214	Nelsi Teresa	Flores Leiva	FEMENINO	1987-03-04	PARAGUAYO/A	0986760650	UNION DE HECHO	\N	\N	\N	SI	NO	1206	Fidelina	\N	\N	\N	0	Anteriormente vivian en el barrio pero en alquiler, antes de la inundacion la casa era de material.Salon-Pasillo.
3958001	Zunilda Noemi	Flores Leiva	FEMENINO	\N	PARAGUAYO/A	0971737825	UNION DE HECHO	\N	\N	\N	SI	NO	1205	Fidelina	\N	\N	\N	0	.
5649834	Richard David	Franco Barrios	MASCULINO	\N	PARAGUAYO/A	0986140191	.	\N	\N	\N	SI	NO	2648	Villa Asunción	\N	\N	\N	6	En este momento esta viviendo en lo de la madre , se mudo por la inundación,apenas el agua baje bien piensa volver.
4526323	Miriam Lorena	Franco Cáceres	FEMENINO	\N	PARAGUAYO/A	081662295	UNION DE HECHO	\N	\N	\N	SI	NO	505	San Felipe C/ 23 de Junio	\N	\N	\N	0	.
2447218	Felipe	Franco Cantero	MASCULINO	2052-01-05	PARAGUAYO/A	0994489347	CASADO/A	\N	\N	\N	SI	NO	223	YVOTY	\N	\N	\N	0	.
1492904	Amado	Franco Centurión	MASCULINO	2066-07-09	PARAGUAYO/A	0994986813	UNION DE HECHO	\N	\N	\N	SI	NO	2258	Juan Leon Mallorquin	\N	\N	\N	0	.
3935738	Joséfina	Franco Centurión	MASCULINO	1981-08-02	PARAGUAYO/A	0981567353	CASADO/A	\N	\N	\N	SI	NO	856	Ursicino Velasco	\N	\N	\N	0	La Flia. Cuenta con una panaderia de 6x6, Funcionando.
3935725	Joséta Lourdes	Franco Centurión	MASCULINO	\N	PARAGUAYO/A	0986700249	.	\N	\N	\N	SI	NO	73	Angel Luis	\N	\N	\N	0	Tiene 2 piezas mas 1 construcción.
4270953	Angela	Franco Colmán	FEMENINO	1982-10-02	PARAGUAYO/A	0984810586	CASADO/A	\N	\N	\N	SI	NO	374	Virgen de Lourdes Esq/ Mar del Plata	\N	\N	\N	0	La titular tiene una vivienda de la cooperativa (COBAT). Llevan 18 anos de pareja y 16 anos de casados por civil.
682775	Calixto Abel	Franco Colmán	MASCULINO	\N	PARAGUAYO/A	0992973023	.	\N	\N	\N	SI	NO	549	Ursicino Velasco e/ Malvina	\N	\N	\N	0	.
4154297	Liliana Catalina	Franco Colmán	FEMENINO	\N	PARAGUAYO/A	0984852751	UNION DE HECHO	\N	\N	\N	SI	NO	224	YVOTY	\N	\N	\N	0	.
4757595	Sara Noemi	Franco Colmán	FEMENINO	\N	PARAGUAYO/A	0982118152	.	\N	\N	\N	SI	NO	616	Ursicino Velasco/ San Felipe y Santiago	\N	\N	\N	0	El Sr.Pedro con su conyuge , son encargados de la guarderia .
5272872	Vicente	Franco Colmán	MASCULINO	\N	PARAGUAYO/A	0982452174	CASADO/A	\N	\N	\N	SI	NO	1417	Bañado Koeti	\N	\N	\N	0	.
1872044	Gladys Elizabeth	Franco de Cáceres	FEMENINO	\N	PARAGUAYO/A	0981640236	CASADO/A	\N	\N	\N	SI	NO	1425	Ursicino Velasco	\N	\N	\N	0	Secarropas: 1.
2617220	Teodolina	Franco de Maylin	FEMENINO	\N	PARAGUAYO/A	0986703392	SEPARADO/A	\N	\N	\N	SI	NO	593	Malvinas	\N	\N	\N	0	.
2950256	Victoriana	Franco de Paredes	MASCULINO	\N	PARAGUAYO/A	097188885	CASADO/A	\N	\N	\N	SI	NO	159	URSICINO VELASCO	\N	\N	\N	0	.
3761810	Edgar Eduardo	Franco Doldán	MASCULINO	1983-03-11	PARAGUAYO/A	0986373642	UNION DE HECHO	\N	\N	\N	SI	NO	1307	Ursicino Velasco	\N	\N	\N	0	vive dentro del lote de su tía felicita en una vivienda de madera, tiene una construcción de material que no está terminada.
2207561	Juana	Franco Doldan	MASCULINO	\N	PARAGUAYO/A	0972539692	UNION DE HECHO	\N	\N	\N	SI	NO	2708	28 y Mallorquín	\N	\N	\N	0	La sra. Estubo ausente en el país unos años, por problemas laborales y de salud.
4914325	Juan De la Cruz	Franco Gómez	MASCULINO	1993-03-05	PARAGUAYO/A	0985151656	UNION DE HECHO	\N	\N	\N	SI	NO	2259	.	\N	\N	\N	0	.
2232197	Miguela	Franco Jara	FEMENINO	1971-05-02	PARAGUAYO/A	0971801685	CASADO/A	\N	\N	\N	SI	NO	268	Nuevo Amanecer	\N	\N	\N	0	Cada uno de los hijos tiene un casa de material en el mismo lote.
3427532	Victor Benjamin	Franco López	MASCULINO	\N	PARAGUAYO/A	0972178628	UNION DE HECHO	\N	\N	\N	SI	NO	1428	Ursicino Velasco	\N	\N	\N	0	Arriba de la construccion tienen un palafito con dos dormitorios (8x9).
3402181	Victor Manuel	Franco Martínez	MASCULINO	\N	PARAGUAYO/A	0991217618	UNION DE HECHO	\N	\N	\N	SI	NO	550	Malvinas	\N	\N	\N	0	.
3336924	Graciela	Franco Meza	FEMENINO	\N	PARAGUAYO/A	0984457911	UNION DE HECHO	\N	\N	\N	SI	NO	750	Martin Fierro-1025	\N	\N	\N	0	La Sra. Andrea vive enla casa de la tia con sus dos hijos que la esta construida al costado de la casa de su tia es un casita muy precaria.
6980945	Silvia Patricia	Franco Moline	FEMENINO	\N	PARAGUAYO/A	0982711858	UNION DE HECHO	\N	\N	\N	SI	NO	93	Virgen de Lourdes	\N	\N	\N	0	.
214936	Adolfina	Franco Ortiz	FEMENINO	\N	PARAGUAYO/A	426388	UNION DE HECHO	\N	\N	\N	SI	NO	2196	Mbo-ehara c/ Sotero Colman	\N	\N	\N	0	La señora tiene una despensa compartida con la hija, la sra posee una construcción de dos plantas, las paredes son de hormigon y techo de teja
4752236	Braulia Celestina	Franco Ramírez	FEMENINO	\N	PARAGUAYO/A	0971264083	UNION DE HECHO	\N	\N	\N	SI	NO	515	Ursicino Velasco	\N	\N	\N	0	.
4252809	María Carolina	Franco Ruíz Díaz	FEMENINO	1982-06-01	PARAGUAYO/A	0983435986	UNION DE HECHO	\N	\N	\N	SI	NO	1754	Koety	\N	\N	\N	0	.
3865249	Ana María	Franco	FEMENINO	\N	PARAGUAYO/A	0985643887	UNION DE HECHO	\N	\N	\N	SI	NO	1671	Pasillo Sin Nombre	\N	\N	\N	0	Su hijo de 39 anos vive en el mismo terreno en una casa precaria la nena censada con la mama es su hija.
4971373	Elvira Cristina	Franco	FEMENINO	\N	PARAGUAYO/A	0972762004	SOLTERO/A	\N	\N	\N	SI	NO	2084	Moéhara	\N	\N	\N	0	.
4455058	Luz Marina	Franco	FEMENINO	\N	PARAGUAYO/A	0971584363	UNION DE HECHO	\N	\N	\N	SI	NO	895	Martin Fierro	\N	\N	\N	0	Tiene una casa de un techo p/ mi pais con un corredor y en el fondo 2 piezas de madera donde habitan sus hijos . 7x5.
5202259	Nancy Diana	Franco	FEMENINO	1991-05-07	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	321	P/4 de Octubre e/ Yvoty	\N	\N	\N	0	Tiene Cocina Y comedor. El terreno es de la Sra. Bernardina Zarate y Victor Esteche. L ajefe de hogar alquila Hace 3 meses.
41	Pabla	Franco	FEMENINO	\N	.	.	.	\N	\N	\N	SI	NO	1216	Angel de la guarda	\N	\N	\N	0	No quiso censarse.
3267734	Eleuteria	Fretes Torales	FEMENINO	1975-08-08	PARAGUAYO/A	0985246335	UNION DE HECHO	\N	\N	\N	SI	NO	522	Malvina c/16 de Agosto	\N	\N	\N	0	.
3777766	Lucina	Fretes Torales	FEMENINO	1982-12-12	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	523	Malvinas C/ 16 de Agosto	\N	\N	\N	0	Vive en el mismo terreno de la duena de casa de la M40 L10 y cuenta con una vivienda precaria y estan hace 9 anos.
5308169	Zunilda	Fretes Torales	FEMENINO	\N	PARAGUAYO/A	0981968325	UNION DE HECHO	\N	\N	\N	SI	NO	490	Remancito c/ 23 de Junio	\N	\N	\N	0	La duena de casa manifiesta que cuenta con una construccion de hormigon sin terminar de dimension 6x4.
3704849	zunilda Rosa	Frutos Romero	FEMENINO	\N	PARAGUAYO/A	0981538277	CASADO/A	\N	\N	\N	SI	NO	506	Maria Elena e/ María Elena	\N	\N	\N	0	.
3792201	Fundición de Hierros - Marcos Benítez	.	.	\N	.	0981402182	.	\N	\N	\N	SI	NO	1285	.	\N	\N	\N	0	En el lote hay una construccion de un tinglado que funcionaba como fundicion de hierro ,actualmente ya no funciona ni vive nadie.
4601546	Cynthia Noemi	Gadda Colarte	FEMENINO	1989-11-04	PARAGUAYO/A	0991206577	.	\N	\N	\N	SI	NO	2105	Ursicino Velazco	\N	\N	\N	0	.
4601561	Laura Raquel	Gadda Colarte	FEMENINO	\N	PARAGUAYO/A	0992390285	CASADO/A	\N	\N	\N	SI	NO	2108	Ursicino Velazco	\N	\N	\N	0	.
4601560	Marcos Antonio	Gadda Colarte	MASCULINO	\N	PARAGUAYO/A	0985391906	SOLTERO/A	\N	\N	\N	SI	NO	2113	Ursicino Velazco	\N	\N	\N	0	.
3283364	César Enrique	Gadda Mongelos	MASCULINO	1986-05-08	PARAGUAYO/A	0971910079	UNION DE HECHO	\N	\N	\N	SI	NO	2270	Juan León Mallorquin	\N	\N	\N	0	.
3283362	Sonia Elizabeth	Gadda Mongelos	FEMENINO	1985-10-06	PARAGUAYO/A	098272270	CASADO/A	\N	\N	\N	SI	NO	2271	Juan León Mallorquin	\N	\N	\N	0	.
3283360	Victor Javier	Gadda Mongelos	MASCULINO	\N	PARAGUAYO/A	0985642589	UNION DE HECHO	\N	\N	\N	SI	NO	2269	Juan León Mallorquin	\N	\N	\N	0	.
1209668	César Napoleón	Gadda Noguera	MASCULINO	\N	PARAGUAYO/A	423467	VIUDO/A	\N	\N	\N	SI	NO	2279	Nuestra Sra. De la Asunción	\N	\N	\N	0	La sra. Neuza Villamayor (c.i.:3487364),fallecio el 25-4-17.Esposa del Sr. Cesar.
1084991	Enrique Anibal	Gadda Villamayor	MASCULINO	\N	PARAGUAYO/A	0992 247 972	CASADO/A	\N	\N	\N	SI	NO	2321	Nuestra Señora de la Asunción	\N	\N	\N	6	.
1428767	Marcos Antonio	Gadda Villamayor	MASCULINO	\N	PARAGUAYO/A	0992355006	UNION DE HECHO	\N	\N	\N	SI	NO	2098	Ursicino Velázquez	\N	\N	\N	0	un salón funciona como Heladería y Hamburguesería de 7mx3,5m
3518340	Neusa Estefania	Gadda Villamayor	FEMENINO	1980-08-07	PARAGUAYO/A	0983457097	CASADO/A	\N	\N	\N	SI	NO	2280	Nuestra Sra. De la Asunción	\N	\N	\N	0	Vivienda de dos platas,la parte superior esta en construción ,(falta techar),la flia tiene un copetin frente a su vivienda de 5x4m2.
3831258	Sandra Carolina	Galeano Cazal	FEMENINO	\N	PARAGUAYO/A	0983190725	UNION DE HECHO	\N	\N	\N	SI	NO	950	Ursicino Velazco y Nuevo Amanecccer	\N	\N	\N	0	corredor del frente tiene 9x4
4157082	Tomasa	Galeano de Escurra	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	1247	.	\N	\N	\N	0	.
1030462	Ana Encarnación	Galeano de Génes	FEMENINO	\N	PARAGUAYO/A	0972486207	VIUDO/A	\N	\N	\N	SI	NO	2281	Calle'i Santiago	\N	\N	\N	0	.
859532	Fulgencio	Galeano Escurra	MASCULINO	\N	PARAGUAYO/A	0981275572	UNION DE HECHO	\N	\N	\N	SI	NO	2288	Juan Leon Mallorquin	\N	\N	\N	0	Dentro del lote del titular viven dos nucleos de flias mas y dos techos.Hace un ano que el hijo del titular esta separado despues de una relacion de casados por dos anos.
2138015	Alberta	Galeano Farfan	FEMENINO	\N	PARAGUAYO/A	0971767919	UNION DE HECHO	\N	\N	\N	SI	NO	1093	San Cayetano	\N	\N	\N	0	En este lote viven dos familias con dos techos independientes.
5373378	Romina Maximilina	Galeano Farfan	FEMENINO	\N	PARAGUAYO/A	0971767919	UNION DE HECHO	\N	\N	\N	SI	NO	805	San Cayetano	\N	\N	\N	0	La pareja lleva 4 años de concubinato, tienen lote y techo propio
4813621	Katherina Valeria	Galeano Genes	FEMENINO	1990-12-03	PARAGUAYO/A	0982990681	UNION DE HECHO	\N	\N	\N	SI	NO	2690	Juan Leon Mallorquín	\N	\N	\N	0	la pareja lleva dos años de concubinato, no tienen lote i techo propio, viven bajo el techo del titular
6133251	Aníbal	Galeano Giménez	MASCULINO	\N	.	0983464953	.	\N	\N	\N	SI	NO	1829	Bañado Koeti	\N	\N	\N	5	.
3579041	Rafael ArcÁngel	Galeano González	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	2156	Virgen de Guadalupe	\N	\N	\N	0	Actualmente el propietario del lote y la vivienda se encuentra privado de su libertad según manifestación de vecinos saldra en Agosto.La vivienda se encuentra desocupada.
3347268	Pedro Miguel	Galeano Jiménez	MASCULINO	\N	PARAGUAYO/A	0983174304	CASADO/A	\N	\N	\N	SI	NO	1126	Virgen de Lourdes	\N	\N	\N	0	.
7109199	Sandra Carolina	Galeano Jiménez	FEMENINO	1998-11-07	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	964	Angel Luis c/ Virgen de Lourdes	\N	\N	\N	0	.
4544631	Claudio Lorena	Galeano Martínez	MASCULINO	\N	PARAGUAYO/A	0972455739	UNION DE HECHO	\N	\N	\N	SI	NO	1465	pajagua naranja	\N	\N	\N	0	.
6256783	María Elena	Galeano Martínez	FEMENINO	\N	PARAGUAYO/A	0994944670	UNION DE HECHO	\N	\N	\N	SI	NO	1482	payagua naranja	\N	\N	\N	0	La dueña del lote esta trabajando en Esoaña, se llama Emerita Martinez hace 9 años.
4745541	Cynthia Pamela	Galeano Medina	FEMENINO	1991-08-05	PARAGUAYO/A	0981716050	CASADO/A	\N	\N	\N	SI	NO	2205	Mbo-ehara	\N	\N	\N	0	seca ropas 1
872227	Lucíana	Galeano Pigurno	FEMENINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	1263	Yvoty Rory	\N	\N	\N	0	Hace 1 mes construyendo la vivienda.Posee una radio.
2665955	Eusebio Dejesús	Galeano	MASCULINO	\N	PARAGUAYO/A	0993544308	SOLTERO/A	\N	\N	\N	SI	NO	2149	San Martin de Porres	\N	\N	\N	0	Es propietario del lote ,presto parte del lote a la Sra. Bernarda para que construya una vivienda precaria.
1068431	Miguel	Galeano	MASCULINO	2051-08-05	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	580	8 de Diciembre	\N	\N	\N	4	Alfredo David Galeano tiene su pieza aparte de los padres
3368535	Ramona	Galeano	MASCULINO	\N	PARAGUAYO/A	0983626411	UNION DE HECHO	\N	\N	\N	SI	NO	115	Virgen de Lujan /Oleros	\N	\N	\N	3	.
4560876	Victor Daniel	Galloso Gamarra	MASCULINO	\N	PARAGUAYO/A	0986503997	UNION DE HECHO	\N	\N	\N	SI	NO	1366	Yvoty Rory	\N	\N	\N	0	.
6129965	Sergio Ariel	Gamarra Aquino	MASCULINO	\N	PARAGUAYO/A	0994846462	UNION DE HECHO	\N	\N	\N	SI	NO	638	Mar del Plata	\N	\N	\N	0	Al terreno se le puso un solo numero pero son dos lotes distintos.
3338726	Barcilisa	Gamarra Báez	FEMENINO	\N	PARAGUAYO/A	0981180223	UNION DE HECHO	\N	\N	\N	SI	NO	848	SIN NOMBRE	\N	\N	\N	3	.
2144269	Aparicia	Gamarra Chaparro	FEMENINO	\N	PARAGUAYO/A	0984748725	SOLTERO/A	\N	\N	\N	SI	NO	1864	Villa Maria Auxiliadora	\N	\N	\N	0	.
3806293	Rosita	Gamarra Cristaldo	FEMENINO	\N	PARAGUAYO/A	0982904738	UNION DE HECHO	\N	\N	\N	SI	NO	617	San Felipe y Santiago	\N	\N	\N	0	La pareja tiene 5 años de concubinato.
613380	Olga Beatriz	Gamarra de Fariña	FEMENINO	\N	PARAGUAYO/A	423987	CASADO/A	\N	\N	\N	SI	NO	2261	Nuestra Sra. De la Asuncion	\N	\N	\N	0	Cuenta con un quincho en la parte posterior de la vivienda.
888429	María Pabla	Gamarra de Gayoso	FEMENINO	\N	PARAGUAYO/A	0983 138 426	VIUDO/A	\N	\N	\N	SI	NO	2239	Urcisino Velazco c/ Sotero Colman	\N	\N	\N	0	La señora vivia con la madre y el esposo.
1209666	Martha Beatriz	Gamarra de Trepowski	FEMENINO	1974-04-01	PARAGUAYO/A	091967408	VIUDO/A	\N	\N	\N	SI	NO	6	San Felipe y Santiago	\N	\N	\N	0	.
4922021	Antonia Ramona	Gamarra Oviedo	FEMENINO	\N	PARAGUAYO/A	0972624702	UNION DE HECHO	\N	\N	\N	SI	NO	2606	Nuestra Señora de la Asuncion	\N	\N	\N	6	.
5296851	Juana María	Gamarra Oviedo	MASCULINO	1994-12-06	PARAGUAYO/A	0982677612	UNION DE HECHO	\N	\N	\N	SI	NO	2612	Nuestra Señora de la Asuncion	\N	\N	\N	6	.
5420924	Griselda Beatriz	Gamarra Pérez	FEMENINO	1990-09-04	PARAGUAYO/A	0985643687	UNION DE HECHO	\N	\N	\N	SI	NO	594	Malvinas	\N	\N	\N	0	La vivienda tiene una construccion de 4x3 que utiliza como cocina , comedor. Tiene un corredor.
4746041	Gustavo Rodrigo	Gamarra Rojas	MASCULINO	\N	PARAGUAYO/A	0985831668	UNION DE HECHO	\N	\N	\N	SI	NO	2210	Sin Nombre	\N	\N	\N	0	Bano Compartido.
3503831	Juan Carlos	Gamarra Rojas	MASCULINO	1980-02-06	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1992	Nasaindy	\N	\N	\N	5	La casa se construyo hace 1 mes.
3967464	Gregoria	Gamarra vda de Varela	FEMENINO	2057-12-03	PARAGUAYO/A	.	VIUDO/A	\N	\N	\N	SI	NO	1159	Urcicino Velazco	\N	\N	\N	0	Vive dentro de una vivienda dde bloque de cemento, y tiene una construcción de 6x9 de dimension que están terminando
2154184	Elena Elizabeth	Gamarra Vera	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	986	8 de Diciembre	\N	\N	\N	0	El lotetiene bano comun letrina cocina y dormitorio todosadentro de la casa.
1675543	José Domingo	Gamarra Zarza	MASCULINO	1972-05-03	PARAGUAYO/A	0982260368	CASADO/A	\N	\N	\N	SI	NO	2211	Mto. Ursicino Velasco	\N	\N	\N	0	La sra. Amelia tiene un kiosko ,tambien tiene venta de helados es su fuente de ingreso.El salón tiene un medida de 3,50x5 con un pequeño deposito de madera.
5024224	Daisy Paola	Gamarra	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	636	Mar del Plata	\N	\N	\N	0	Al terreno se le puso un solo numero pero son dos lotes distintos.
6053105	Liz Marlene	Gamarra	FEMENINO	\N	PARAGUAYO/A	0982483681	UNION DE HECHO	\N	\N	\N	SI	NO	1062	Mar de plata e/ Nuevo Amanecer	\N	\N	\N	0	.
4861683	Noelia Liset	Garay Vazquez	FEMENINO	\N	PARAGUAYO/A	0985133873	UNION DE HECHO	\N	\N	\N	SI	NO	643	Angel Luis	\N	\N	\N	0	Vive con la flia Marta hace 2 anos ,es su hna espiritual.
6348067	Diego Antonio	Garcete Fernández	MASCULINO	\N	PARAGUAYO/A	0994750702	UNION DE HECHO	\N	\N	\N	SI	NO	2489	Recicla	\N	\N	\N	0	El lote no tiene una divisoria visible y al lado vive la abuela.
6541263	Miguela Rocío	Garcete Toledo	FEMENINO	\N	PARAGUAYO/A	0982158174	UNION DE HECHO	\N	\N	\N	SI	NO	933	Angel Luis C/ Virgen de Lourdes	\N	\N	\N	0	.
3193363	Martina	Garcete	FEMENINO	2057-01-01	PARAGUAYO/A	0982522223	SOLTERO/A	\N	\N	\N	SI	NO	1586	Ramon Talavera	\N	\N	\N	6	Lote Vacio.La propietaria actualmente vive en el lote de un vecino hace 6 meses, por motivos de la crecida del agua.
4747963	Rocío Guadalupe	Garcia Almada	FEMENINO	\N	PARAGUAYO/A	0994763809	.	\N	\N	\N	SI	NO	1936	Urcicicino Velazco	\N	\N	\N	0	Todos los datos del techo y paredes don iguales a la del dueño ya que alquila la pieza
5496691	Alicia Romina	Garcia Aranda	FEMENINO	\N	PARAGUAYO/A	0992356380	UNION DE HECHO	\N	\N	\N	SI	NO	1224	Fidelina	\N	\N	\N	0	El bano comparte con los padres, vive en una extencion de la casa con su pareja.
2021689	Liboria	Garcia Aranda	FEMENINO	\N	PARAGUAYO/A	0983790440	CASADO/A	\N	\N	\N	SI	NO	1222	Fidelina	\N	\N	\N	0	.
48176379	Cesar Daniel	Garcia Benítez	MASCULINO	\N	PARAGUAYO/A	0972551271	UNION DE HECHO	\N	\N	\N	SI	NO	111	OLEROS	\N	\N	\N	3	.
4027515	Laura Luana	Garcia Benítez	FEMENINO	\N	PARAGUAYO/A	0983838429	UNION DE HECHO	\N	\N	\N	SI	NO	2312	Nuestra Sra. De la Asunción	\N	\N	\N	6	.
1433329	Sonia Elizabeth	Garcia de Ramírez	FEMENINO	\N	PARAGUAYO/A	0982805206	CASADO/A	\N	\N	\N	SI	NO	1948	Ursicino Velasco	\N	\N	\N	0	La sra posee una extencion a continuacion de la casa de la madre.
1096574	Seferina	Garcia de Rojas	FEMENINO	\N	PARAGUAYO/A	0986109718	CASADO/A	\N	\N	\N	SI	NO	1738	Ursicino Velasco	\N	\N	\N	0	Casa de dos plantas.
1034663	Felicita	Garcia de Vera	FEMENINO	\N	PARAGUAYO/A	0992748770	CASADO/A	\N	\N	\N	SI	NO	1167	Uricicino velazco	\N	\N	\N	0	.
7000979	Cinthia Carolina	Garcia Díaz	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1137	.	\N	\N	\N	0	.
2336024	Gabriela	Garcia Domínguez	FEMENINO	\N	PARAGUAYO/A	0982364945	SOLTERO/A	\N	\N	\N	SI	NO	1525	Urcicino Velasco	\N	\N	\N	5	.
1343445	Anatalia Antonia	García Espinola	FEMENINO	2060-09-07	PARAGUAYO/A	098250955	.	\N	\N	\N	SI	NO	1515	Jacare Pito	\N	\N	\N	5	.
5007584	Mariela Elizabeth	Garcia Fernanadez	FEMENINO	1989-04-10	PARAGUAYO/A	0982148303	UNION DE HECHO	\N	\N	\N	SI	NO	1860	Villa Maria Auxiliadora	\N	\N	\N	0	.
3735453	Abrahan	Garcia Insaurralde	MASCULINO	\N	PARAGUAYO/A	0983712448	SOLTERO/A	\N	\N	\N	SI	NO	2450	Sin Nombre Zona 7	\N	\N	\N	0	.
339792	Juan Andrés	Garcia Leguizamon	MASCULINO	\N	PARAGUAYO/A	0982121223	CASADO/A	\N	\N	\N	SI	NO	1859	Villa Maria Auxiliadora	\N	\N	\N	0	.
4324531	Agueda Carolina	Garcia Vera	FEMENINO	1985-05-02	PARAGUAYO/A	0981397553	UNION DE HECHO	\N	\N	\N	SI	NO	1362	Yvoty rory	\N	\N	\N	0	La pareja tiene 6 anos de concubinato.Christian tiene una hija de 9 anos.
4675042	Ingrid Dahiana	García Vera	FEMENINO	1988-02-05	PARAGUAYO/A	0984512118	UNION DE HECHO	\N	\N	\N	SI	NO	1824	Rancho 8	\N	\N	\N	0	.
4675024	Ismael	Garcia Vera	MASCULINO	\N	PARAGUAYO/A	0992467791	UNION DE HECHO	\N	\N	\N	SI	NO	1816	Rancho 8	\N	\N	\N	0	La pareja lleva 3 años de concubinato, tienen lote propio, su casa se derrumbo
377393	Fidelina	Garcia	FEMENINO	\N	PARAGUAYO/A	426081	SOLTERO/A	\N	\N	\N	SI	NO	1947	Ursicino Velasco	\N	\N	\N	0	La sra. Cobra 3ra. Edad,era casa de la madre.
564	Leoncio	Garcia	MASCULINO	\N	.	.	.	\N	\N	\N	SI	NO	2692	Villa Asunción	\N	\N	\N	6	No se obtuvo datos porque no se pudo contactar con el titular, ya que el mismo se mudó temporalmente, su lote ya se encuentra inundado
2505479	Lucía del Carmen	Garcia	FEMENINO	1973-11-05	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1445	Bañado Koeti	\N	\N	\N	5	.
4669793	Mari Albina	García	FEMENINO	1983-01-03	PARAGUAYO/A	83239962	UNION DE HECHO	\N	\N	\N	SI	NO	1602	Jacare Pito	\N	\N	\N	5	.
2943398	Sergio Gustavo	Garcia	MASCULINO	\N	PARAGUAYO/A	0985934098	SEPARADO/A	\N	\N	\N	SI	NO	1952	.	\N	\N	\N	0	.
4645618	Tomasa	Garcia	FEMENINO	\N	PARAGUAYO/A	.	VIUDO/A	\N	\N	\N	SI	NO	1650	Jakare Pito	\N	\N	\N	3	Tiene una vivienda construida por los voluntarios de
2869090	Zulma	Garcia	FEMENINO	\N	PARAGUAYO/A	0991989104	CASADO/A	\N	\N	\N	SI	NO	1730	Ursicino Velasco	\N	\N	\N	5	Cuenta con un Taller de Reparaciones de Electrodomesticossobre la calle de 5x4 mts.
1468004	Timotea	Garrigoza de Chávez	FEMENINO	\N	PARAGUAYO/A	0982 902 864	CASADO/A	\N	\N	\N	SI	NO	2577	8 de Diciembre	\N	\N	\N	0	La titular y su cónyuge cobran tercera edad.
2136784	Lorenzo	Gavilán Amarilla	MASCULINO	2057-10-08	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	628	San Felipe y Santiago	\N	\N	\N	0	.
5418673	Enzo Eduardo	Gavilán Peralta	MASCULINO	1990-11-04	PARAGUAYO/A	0982452012	UNION DE HECHO	\N	\N	\N	SI	NO	2286	Nuestra Sra. De la Asunción	\N	\N	\N	0	.
5046647	Rossana Elizabeth	Gavilan Willigs	FEMENINO	1991-09-06	PARAGUAYO/A	0986430554	SOLTERO/A	\N	\N	\N	SI	NO	715	Mar del Plata	\N	\N	\N	5	Vive en lote de Monica Beatriz le cedio una parte del fondo de su lote, no es familiar.
3650746	Sofia	Gayoso	FEMENINO	\N	PARAGUAYO/A	0986582541	UNION DE HECHO	\N	\N	\N	SI	NO	118	Virgen de Lourdes	\N	\N	\N	3	.
1474295	Ladis Ramona	Génes de Alcaraz	FEMENINO	2065-08-01	PARAGUAYO/A	0981 953 025	CASADO/A	\N	\N	\N	SI	NO	2303	Urcisino Velazco	\N	\N	\N	0	.
3189588	Sonia Beatriz	Genes Esquivel	FEMENINO	1988-09-07	PARAGUAYO/A	0971569245	UNION DE HECHO	\N	\N	\N	SI	NO	768	Pilcomayo	\N	\N	\N	0	.
1013413	Ermelinda	Génes Galeano	FEMENINO	2064-12-01	PARAGUAYO/A	0982101943	SOLTERO/A	\N	\N	\N	SI	NO	2291	Nuestra Sra. De la Asunción	\N	\N	\N	0	.
1337234	Pascual	Génes Galeano	MASCULINO	2066-10-04	PARAGUAYO/A	0972486207	CASADO/A	\N	\N	\N	SI	NO	2282	Nuestra Sra. De la Asunción	\N	\N	\N	0	.
3818991	Elida	Genes Gavilán	FEMENINO	\N	PARAGUAYO/A	0981267853	CASADO/A	\N	\N	\N	SI	NO	1070	Ursicino Velasco	\N	\N	\N	0	.
3196235	Cristiane Raquel	Génes	FEMENINO	1978-12-08	PARAGUAYO/A	094717132	SOLTERO/A	\N	\N	\N	SI	NO	2290	Juan Leon Mallorquin	\N	\N	\N	0	Cocina Electrica 1.
6363445	Ana Laura	Genez Baez	FEMENINO	\N	PARAGUAYO/A	.	DIVORCIADO/A	\N	\N	\N	SI	NO	2699	.	\N	\N	\N	0	vive dentro del lote y la casa de la mamá, lleva 3 años de concubinato, comparte electrodomésticos con la mamá
1961007	Concesa	Geraldo Vda. De Brítez	FEMENINO	2042-08-04	PARAGUAYO/A	0986834221	VIUDO/A	\N	\N	\N	SI	NO	2158	Mto. Ursicino Velasco	\N	\N	\N	0	El titular se dedica al alquiler de habitaciones, cuenta con 12 habitaciones independientes.Tambien cria animales(vacas, gallinas, cerdos).
2569942	Alicia	Gill	FEMENINO	1975-04-02	PARAGUAYO/A	0972275311	SOLTERO/A	\N	\N	\N	SI	NO	2085	Sotero Colman	\N	\N	\N	0	Baño y cocina utilza on la familia
6927651	Gabriela Elizabeeth	Giménes Ramírez	FEMENINO	\N	PARAGUAYO/A	0972641267	UNION DE HECHO	\N	\N	\N	SI	NO	799	Virgen de Lujan	\N	\N	\N	0	.
2667846	Ramona	Giménez Arce	MASCULINO	\N	PARAGUAYO/A	.	SEPARADO/A	\N	\N	\N	SI	NO	987	San Felipe y Santiago	\N	\N	\N	0	El lote tiene una vivienda de material al frente y otro bloque pro vida pegada entre si, además tiene extensión de madera.
5870085	Pedro Damian	Giménez Cáceres	MASCULINO	\N	PARAGUAYO/A	0972452264	.	\N	\N	\N	SI	NO	354	Los Sauces e/Virgen de Lujan	\N	\N	\N	0	Corredor.
4631609	Gabriela Beatriz	Giménez Cañete	FEMENINO	1986-08-04	PARAGUAYO/A	0981265041	CASADO/A	\N	\N	\N	SI	NO	2215	Mto. Ursicino Velasco	\N	\N	\N	0	.
1224053	Sebastiana	Giménez de Ferreira	FEMENINO	2053-05-01	PARAGUAYO/A	0982927662	CASADO/A	\N	\N	\N	SI	NO	1488	Mainumby	\N	\N	\N	0	.
1352374	Alina	Giménez de Recalde	FEMENINO	\N	PARAGUAYO/A	0985535444	CASADO/A	\N	\N	\N	SI	NO	1335	Ursicino Velasco	\N	\N	\N	0	Es duena del lote dentro de el vive uno de sus hijos con su pareja e hijos en una vivienda de material bien constituida.
4130344	Ana María	Giménez Ezequiel	FEMENINO	\N	PARAGUAYO/A	0991429028	UNION DE HECHO	\N	\N	\N	SI	NO	1111	Angel Luis	\N	\N	\N	0	Vive dentro del lote de Elva Duarte tiene una vivienda precaria. Se mantiene totalmente aparte.
1295882	Juan de Dios	Giménez González	MASCULINO	2067-08-03	PARAGUAYO/A	0991739122	CASADO/A	\N	\N	\N	SI	NO	2294	Nuestra Sra. De la Asunción	\N	\N	\N	0	.
4293035	José Luis	Giménez Miño	MASCULINO	\N	PARAGUAYO/A	0984227187	UNION DE HECHO	\N	\N	\N	SI	NO	1144	Aromita	\N	\N	\N	0	Vive en la vivienda de su Hno. Dentro del terreno de su mama Maria Mino.
2521182	Sonia Elizabeth	Giménez Morel	FEMENINO	1978-01-01	PARAGUAYO/A	0983569505	SOLTERO/A	\N	\N	\N	SI	NO	2331	Nuestra Sra. De la Asunción	\N	\N	\N	6	La titular hace una no esta separada es madre soltera despues de una relación de 2 anos.
7116474	Leticia Elizabeth	Giménez Ramírez	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	355	.	\N	\N	\N	0	.
4814781	Lorena Mabel	Giménez Ríos	FEMENINO	\N	PARAGUAYO/A	0991904130	UNION DE HECHO	\N	\N	\N	SI	NO	2297	Nuestra Sra. De la Asunción	\N	\N	\N	0	Cocina a Gas 1.
3941678	Blasida	Giménez Toledo	FEMENINO	\N	PARAGUAYO/A	0972420818	UNION DE HECHO	\N	\N	\N	SI	NO	654	Virgen de Lourdes E/ San Felipe y Santiago	\N	\N	\N	0	La flia actualmente no vive en esta casa , se encuentran en alquiler mientras se termina parte de la contruccion.
6194941	Melissa Andrea	Giménez Zaracho	FEMENINO	\N	PARAGUAYO/A	0991251078	UNION DE HECHO	\N	\N	\N	SI	NO	2080	Sin Nombre	\N	\N	\N	0	.
6194942	Verónica Natalia	Giménez Zaracho	FEMENINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	941	OLERO	\N	\N	\N	0	.
1623421	Dora	Giménez Zarate	FEMENINO	\N	PARAGUAYO/A	0983937089	UNION DE HECHO	\N	\N	\N	SI	NO	81	.	\N	\N	\N	0	.
1701213	Ramona	Giménez Zarate	MASCULINO	2041-04-03	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	1879	Ursicino Velasco c/ Julio Benitez	\N	\N	\N	0	La Sra. Cobra Tercera Edad.
717585	Wigberto	Giménez Zarate	MASCULINO	\N	PARAGUAYO/A	0981484189	SEPARADO/A	\N	\N	\N	SI	NO	1878	.	\N	\N	\N	0	La hija vive con el pero en este momento se encuentra en alquiler porque se fue del agua y tiene que cumplir contrato ella es ancida en el barrio.
1344341	Modesta Fatima	Giménez Zoilan	FEMENINO	\N	PARAGUAYO/A	0992423347	CASADO/A	\N	\N	\N	SI	NO	1136	Virgen de Lujan	\N	\N	\N	0	.
5310717	Daisy Tamara	Giménez	FEMENINO	\N	PARAGUAYO/A	0983955019	UNION DE HECHO	\N	\N	\N	SI	NO	1959	28 y Juan Leon Mallorquin	\N	\N	\N	0	.
3582912	Edith Elizabeth	Giménez	FEMENINO	\N	PARAGUAYO/A	0971507900	UNION DE HECHO	\N	\N	\N	SI	NO	2000	Nasaindy	\N	\N	\N	5	.
1214392	Evangelista	Giménez	FEMENINO	2058-05-06	PARAGUAYO/A	0986698208	.	\N	\N	\N	SI	NO	1961	Ursicino Velasco y 28 Proyectadas	\N	\N	\N	0	.
4310	Irma	Giménez	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	57	.	\N	\N	\N	3	.
2270895	Juan Ángel	Giménez	MASCULINO	\N	PARAGUAYO/A	0981942646	CASADO/A	\N	\N	\N	SI	NO	2214	Mto. Ursicino Velasco	\N	\N	\N	0	.
1312108	María Victoria	Giménez	FEMENINO	\N	PARAGUAYO/A	0992462288	SOLTERO/A	\N	\N	\N	SI	NO	1964	Ursicino Velasco	\N	\N	\N	0	Era la casa de la madre hace 52 anos que vive en el lugar, nacio en la misma.
6353537	Natalia Ramona	Giménez	FEMENINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	2324	Nuestra Sra. De la Asunción	\N	\N	\N	6	La titular estuvo aconcubinado en este tiempo compraron un,lote y construyeron,actualmente la misma ersta separada hace 9 meses sin hijos,con lote propio y techo,la misma maneja con su ingreso.
2041518	Santa Margarita	Giménez	FEMENINO	\N	PARAGUAYO/A	0981316805	SOLTERO/A	\N	\N	\N	SI	NO	474	23 de Junio	\N	\N	\N	0	Dentro del lote d eSanta vive su nieta de 16 anos de edad es una vivienda precaria con su hija de 9 meses.
2367394	Teresita DeJesus	Giménez	FEMENINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	1496	Ursicino Velasco e/ Rancho 8	\N	\N	\N	0	.
4423564	Mariela	Godoy Aranda	FEMENINO	\N	PARAGUAYO/A	0994685458	SOLTERO/A	\N	\N	\N	SI	NO	1492	Mainumby	\N	\N	\N	0	.
2933947	Norma Beatriz	Godoy de Benítez	FEMENINO	\N	PARAGUAYO/A	0986870558	CASADO/A	\N	\N	\N	SI	NO	281	Nuevo Amanecer	\N	\N	\N	0	.
1240624	Felicita Ramona	Godoy Doldán	FEMENINO	\N	PARAGUAYO/A	0986403307	SEPARADO/A	\N	\N	\N	SI	NO	1306	Ursicino Velasco	\N	\N	\N	0	El terreno pertenecio a los padres de la señora Felicita, fueron los antiguos pobladores.
1786672	Silvina	Godoy Recalde	FEMENINO	\N	PARAGUAYO/A	0981486931	UNION DE HECHO	\N	\N	\N	SI	NO	294	.	\N	\N	\N	0	Casa de 2 pisos (palafito).
2617933	Bernarda	Godoy	FEMENINO	\N	PARAGUAYO/A	0981119559	CASADO/A	\N	\N	\N	SI	NO	200	Nuevo Amanecer	\N	\N	\N	0	.
2949945	María Isabel	Godoy	FEMENINO	1970-02-06	PARAGUAYO/A	0986686528	VIUDO/A	\N	\N	\N	SI	NO	696	Nuevo Amanecer	\N	\N	\N	0	.
1961805	Zulema	Godoy	FEMENINO	\N	PARAGUAYO/A	.	VIUDO/A	\N	\N	\N	SI	NO	945	.	\N	\N	\N	0	.
1496058	Mercedes	Gómes Pérez	FEMENINO	\N	PARAGUAYO/A	0992750711	UNION DE HECHO	\N	\N	\N	SI	NO	2592	.	\N	\N	\N	0	Es propietario del lote en el mismo vive su hijo con su pareja e hijo.
3775847	Brígido Gustavo	Gómez Arce	MASCULINO	1975-08-06	PARAGUAYO/A	0984925660	SOLTERO/A	\N	\N	\N	SI	NO	606	San Cayetano	\N	\N	\N	5	El titular vive solo, no cuenta con electrodomésticos, su vivienda es precaria
2116022	Victor Hugo	Gómez Arce	MASCULINO	\N	PARAGUAYO/A	0981377365	UNION DE HECHO	\N	\N	\N	SI	NO	477	Remancito	\N	\N	\N	0	.
2251148	Néstor Daniel	Gómez Cubilla	MASCULINO	1975-02-05	PARAGUAYO/A	0985956593	UNION DE HECHO	\N	\N	\N	SI	NO	2126	Ursicino Velazco	\N	\N	\N	0	.
830711	Ramona Edith	Gomez de Peralta	MASCULINO	2053-08-04	PARAGUAYO/A	481530	CASADO/A	\N	\N	\N	SI	NO	1956	.	\N	\N	\N	0	La sra. Usa todas las cosas del hijo.
2104437	Mirna	Gómez de Santos	FEMENINO	\N	PARAGUAYO/A	0981508954	CASADO/A	\N	\N	\N	SI	NO	2159	Mto Ursicino Velasco	\N	\N	\N	0	.
2035915	Justiniano	Gómez Espinola	MASCULINO	1971-06-09	PARAGUAYO/A	0983790092	VIUDO/A	\N	\N	\N	SI	NO	749	Sin nombre (Ursicino Velasco)	\N	\N	\N	0	.
4941371	Andrea Soledad	Gómez Estigarribia	FEMENINO	1989-06-01	PARAGUAYO/A	0992654765	UNION DE HECHO	\N	\N	\N	SI	NO	1175	Urcicino Velazco	\N	\N	\N	0	Vive en el mismo techo que su madre, y tiene una construcción de 4x6 mts, bien constituida y están en concubinato desde hace 5 años
4492214	Lourdes Elizabeth	Gómez González	FEMENINO	1987-05-01	PARAGUAYO/A	0982537513	UNION DE HECHO	\N	\N	\N	SI	NO	1655	Virgen de Guadalupe	\N	\N	\N	0	La pareja vive junto hace 11 anos, el Sr. Gustavo tiene 30 anos habitando el barrio su nro. De telefono es 0982-968-458.
2520547	Monica Beatriz	Gómez González	FEMENINO	\N	PARAGUAYO/A	0985303245	VIUDO/A	\N	\N	\N	SI	NO	713	Mar del Plata	\N	\N	\N	5	.
2314099	Victor Pastor	Gómez Hermosilla	MASCULINO	\N	PARAGUAYO/A	0971628759	CASADO/A	\N	\N	\N	SI	NO	1732	Ursicino Velasco	\N	\N	\N	0	.
3930442	Angelina	Gómez Reyes	FEMENINO	1978-05-01	PARAGUAYO/A	0984509737	UNION DE HECHO	\N	\N	\N	SI	NO	844	Sin Nombre	\N	\N	\N	0	.
4639940	Felipa Ramona	Gómez Rojas	FEMENINO	1974-02-05	PARAGUAYO/A	0984692595	UNION DE HECHO	\N	\N	\N	SI	NO	534	San Felipe y Santiago	\N	\N	\N	0	.
3748609	Juan María	Gómez Zarate	MASCULINO	1988-08-12	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	649	Angel Luis 1147	\N	\N	\N	0	En frente tiene un corredor con pileta.
2487009	Elsa	Gómez	MASCULINO	1976-03-06	PARAGUAYO/A	0991760789	SEPARADO/A	\N	\N	\N	SI	NO	2595	Rancho 8	\N	\N	\N	0	.
2289572	Julia Constancia	Gómez	FEMENINO	\N	PARAGUAYO/A	0986801187	SOLTERO/A	\N	\N	\N	SI	NO	2138	Agosto Poty	\N	\N	\N	0	.
2647858	Ladislaa	Gómez	FEMENINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	104	San Felipe y Santiago	\N	\N	\N	0	.
6023265	Liz Fabiola	Gómez	FEMENINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	845	Sin Nombre	\N	\N	\N	0	.
2213093	Miguel Angel Martin	Gómez	MASCULINO	1972-01-12	PARAGUAYO/A	0971383118	UNION DE HECHO	\N	\N	\N	SI	NO	747	Ursicino Velasco	\N	\N	\N	0	En el lote hay un salon con un almacen muy grande y complejo , tiene 2 Vitrina.
1303789	Elena	González Acosta	FEMENINO	\N	PARAGUAYO/A	0991797594	SOLTERO/A	\N	\N	\N	SI	NO	1634	Pajagua Naranja	\N	\N	\N	0	Las dos Hermanas llegaron juntas al barrio y compraron el terreno,dividiendose las dos una de ellas hizo una extencion cuando nacio su hija y decidieron dividir sus viviendas.
6560624	Génesis Karen	González Acosta	FEMENINO	1997-08-11	PARAGUAYO/A	0986741862	UNION DE HECHO	\N	\N	\N	SI	NO	2103	Chiquero	\N	\N	\N	0	La sra. Se encuentra embarazada.
2983987	Isabelino	González Acosta	FEMENINO	2069-02-07	PARAGUAYO/A	0994820293	SOLTERO/A	\N	\N	\N	SI	NO	1630	Pajagua Naranja	\N	\N	\N	0	Cocina Electrica 1.La pieza de Isabelino se cayo por la inudacion que todavia no se reparo.Los ocupantes de la vivienda no tienen electrodomesticos.
3550557	Rosalba	González Aguirre	FEMENINO	1982-03-09	PARAGUAYO/A	0983836550	SEPARADO/A	\N	\N	\N	SI	NO	1625	Pajagua Naranja	\N	\N	\N	3	Tiene su terreno en recicla al cual se mudara dentro dos meses aprox.
1429020	Inocencia	González Alen	FEMENINO	\N	PARAGUAYO/A	0982814544	.	\N	\N	\N	SI	NO	465	Martin Fierro	\N	\N	\N	0	.
4858641	Sofia Herminia	González Almada	FEMENINO	\N	PARAGUAYO/A	0986867122	SOLTERO/A	\N	\N	\N	SI	NO	1100	Julio Jara	\N	\N	\N	0	Vive dentro del lote y la parte de arriba de la vivienda de la Sra. Francisca cuenta con terreno pero no con vivienda era nuera de la duena del lote y la vivienda.
5591223	Nemesis Delfina	González Almiron	FEMENINO	\N	PARAGUAYO/A	0992656475	UNION DE HECHO	\N	\N	\N	SI	NO	1118	Virgen de Lourdes C/ Virgen de Lujan	\N	\N	\N	0	El ocupante de la misma cuenta con una vivienda de loza 4,50x4,50 mts.
4466223	Aurelia Leticia	González Aquino	FEMENINO	\N	PARAGUAYO/A	0975 263 392	UNION DE HECHO	\N	\N	\N	SI	NO	1713	Yacare Pito	\N	\N	\N	3	La señora Aurelia ademas, se dedica a la venta de ropas usadas y productos de limpieza.
5079352	Viviana Carolina	González Aranda	FEMENINO	1988-04-11	PARAGUAYO/A	0994606602	CASADO/A	\N	\N	\N	SI	NO	2110	Nasaindy	\N	\N	\N	0	Cocina a Gas 1.
4160145	Luis Miguel	González Benítez	MASCULINO	1985-09-06	PARAGUAYO/A	0982310648	SOLTERO/A	\N	\N	\N	SI	NO	2225	Mto.Ursicino Velasco	\N	\N	\N	0	.
4626638	Ruth Carolina	González Cabañas	FEMENINO	1988-06-04	PARAGUAYO/A	0976176608	UNION DE HECHO	\N	\N	\N	SI	NO	1058	Ursicino Velasco	\N	\N	\N	0	Vive dentro del lote de la suegra tiene su vivienda totalmente independiente.
3613557	Gregoria Soledad	González Chávez	FEMENINO	1981-09-05	PARAGUAYO/A	0984 436 868	UNION DE HECHO	\N	\N	\N	SI	NO	1900	Pilcomayo	\N	\N	\N	0	.
5184536	Francisca Ramona	González Corrales	FEMENINO	\N	PARAGUAYO/A	0991266500	UNION DE HECHO	\N	\N	\N	SI	NO	1363	Yvoty Rory	\N	\N	\N	0	.
6182842	Irma Mercedes	González Corrales	FEMENINO	\N	PARAGUAYO/A	0991916774	UNION DE HECHO	\N	\N	\N	SI	NO	101	San Felipe y Santiago	\N	\N	\N	3	Comedor Grande.
3449868	Norma Beatriz	González Corrales	FEMENINO	1974-12-09	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	2347	Nuestra Sra de la Asunción	\N	\N	\N	6	.
5373453	Liz María	González Cristaldo	FEMENINO	1992-03-04	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	30	San Felipe y Santiago	\N	\N	\N	0	.
2124208	Aurelia	González de Acosta	FEMENINO	2056-05-03	PARAGUAYO/A	0981740840	CASADO/A	\N	\N	\N	SI	NO	1468	payagua naranja	\N	\N	\N	0	La señora Aurelia González comenta que tiene un nieto que está en la penitenciaria, se llama Julio César García, no tienen su número de cédula.
2316605	Magdalena Emilce	González de Aguilera	FEMENINO	\N	PARAGUAYO/A	0983896343	SOLTERO/A	\N	\N	\N	SI	NO	211	Aromita	\N	\N	\N	0	.
3817872	Cynthia Noemi	González de Báez	FEMENINO	1982-09-11	PARAGUAYO/A	0981595515	CASADO/A	\N	\N	\N	SI	NO	464	23 de Junio	\N	\N	\N	5	.
2194810	Karina Monserrath	González de Benitez	FEMENINO	\N	PARAGUAYO/A	0972217441	CASADO/A	\N	\N	\N	SI	NO	509	Malvinas	\N	\N	\N	0	.
3219574	Ernesta	González de Escobar	FEMENINO	\N	PARAGUAYO/A	0982578590	CASADO/A	\N	\N	\N	SI	NO	1512	Rancho 8	\N	\N	\N	0	Placa Electrica 1.
3621778	Lilian Maricel	González de Quiroga	FEMENINO	1976-03-12	PARAGUAYO/A	0983 453387	CASADO/A	\N	\N	\N	SI	NO	1689	Yacare Pito	\N	\N	\N	3	La casa es de planta alta. Arriba tienen dos sormitorios y abajo dos dormitorios mas.
1240605	Margarita	González de Román	FEMENINO	\N	PARAGUAYO/A	0972952739	CASADO/A	\N	\N	\N	SI	NO	388	.	\N	\N	\N	0	La casa cuenta con un mini tinglado y una construccion sin terminar.
1526807	Marta	González de Santa Cruz	FEMENINO	\N	PARAGUAYO/A	0994944808	CASADO/A	\N	\N	\N	SI	NO	881	Martin Fierro	\N	\N	\N	5	.
3219072	Nancy Raquel	González de Zarate	FEMENINO	1980-07-01	PARAGUAYO/A	.	SEPARADO/A	\N	\N	\N	SI	NO	2187	Sotero Colman	\N	\N	\N	0	la familia veve en una extrema pobreza
5305685	Bernardita Simona	González Duarte	FEMENINO	1988-12-03	PARAGUAYO/A	'0982795295	SOLTERO/A	\N	\N	\N	SI	NO	1610	Nuestra Senora de la Asunción	\N	\N	\N	6	Actualmente esta en el refugio de la S.E.N., por motivos de la inundación.
6018814	Clara Mabel	González Duarte	FEMENINO	1989-12-08	PARAGUAYO/A	0982745295	SOLTERO/A	\N	\N	\N	SI	NO	2363	Nuestra Señora de la Asuncion	\N	\N	\N	6	la titular esta separada ace un año, el padre de los niños le pasa mensualidad
4053288	Corina Fatima	González Farina	FEMENINO	1983-06-10	PARAGUAYO/A	0991279095	CASADO/A	\N	\N	\N	SI	NO	1666	Virgen de Guadalupe	\N	\N	\N	0	El Sr. Pedro Antonio tiene 38 anos de asentamiento en el barrio. Dona Corina tiene la vivienda construida por la Cooperativa Banado Poty Duena del lote Gloria Elizabeth Benitez Delgado que manifiesta que estaba en proceso de venta hace 6 anos aproximadam
4053205	Fernando	González Farina	MASCULINO	\N	PARAGUAYO/A	0982320050	UNION DE HECHO	\N	\N	\N	SI	NO	1648	Tape Pyahu	\N	\N	\N	0	Cocina Electrica 1.
4506360	Mara Eliza	González Farina	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1280	.	\N	\N	\N	0	.
3183258	María Celina	González Farina	FEMENINO	\N	PARAGUAYO/A	0986960996	UNION DE HECHO	\N	\N	\N	SI	NO	1646	Tape Pyahu	\N	\N	\N	0	Cocina Electrica 1.Comedor con Sala.
1242045	Eva Mabel	González García	FEMENINO	2069-01-12	PARAGUAYO/A	0971 444 745	SOLTERO/A	\N	\N	\N	SI	NO	2584	Mallorquin	\N	\N	\N	0	La titular posee documento del terreno
5223693	José Manuel	González García	MASCULINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1290	Ursicino Velasco	\N	\N	\N	0	vive dentro del terreno de su abuelo en una vivienda precaria con su mamá y sus hermana tiene pareja e hijos.
4879467	Sergio Daniel	González Garcia	MASCULINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1446	Bañado Koeti	\N	\N	\N	0	.
4992855	Cecilia Diana	González Gonzalez	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1763	Koety	\N	\N	\N	0	Vive en una vivienda precaria 4x6 y estan en pareja hace 1 ano.
2011341	Lina	González González	FEMENINO	\N	PARAGUAYO/A	0983176766	CASADO/A	\N	\N	\N	SI	NO	2175	Mto. Ursicino Velasco	\N	\N	\N	0	El sr. Gustavo Fernández tiene una construcción totalmente independiente de dos pisos donde vive solo y tiene una hija que se queda con el los fines de semanas.Se maneja independiente a la familia, su contrucción data de hace 10 años.
2080503	Francisco Javier	González Guerrero	MASCULINO	\N	PARAGUAYO/A	.	SEPARADO/A	\N	\N	\N	SI	NO	2076	.	\N	\N	\N	5	tiene una construción sin concluuir que es para una pieza
5388389	Guillermo Daniel	González Hebert	MASCULINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	879	Martin Fierro	\N	\N	\N	0	.
6285835	Luz Clara	González Ibarra	FEMENINO	1997-12-06	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	981	San Felipe y Santiago	\N	\N	\N	0	Vive en una extensión de madera de la casa de bloque.
5373328	Viviana Elizabeth	González Ibarra	FEMENINO	\N	PARAGUAYO/A	0983793817	SOLTERO/A	\N	\N	\N	SI	NO	984	San Felipe y Santiago	\N	\N	\N	3	Vive en el fondo del lote en una casa precaria, el baño lo comparte el de la familia.  Ella se maneja independiente con sus dos(2) hijos.
1191112	Celsa	González Martínez	FEMENINO	2068-07-10	PARAGUAYO/A	0992735469	CASADO/A	\N	\N	\N	SI	NO	323	Mar del Plata	\N	\N	\N	0	Anteriormente vivian en la zona 5A.
3181008	Alfredo	González Mendoza	MASCULINO	\N	PARAGUAYO/A	0982 756 182	SOLTERO/A	\N	\N	\N	SI	NO	1772	.	\N	\N	\N	0	.
2126195	Agustín	González Meza	MASCULINO	\N	PARAGUAYO/A	0984412787	SOLTERO/A	\N	\N	\N	SI	NO	932	Nuevo Amanecer	\N	\N	\N	0	El señor Agustin es el propietario del lote y la vivienda, su hija y su familia  viven dentro del mismo lote y vivienda.
1112849	Isabelino	González Meza	FEMENINO	2054-08-06	PARAGUAYO/A	.	CASADO/A	\N	\N	\N	SI	NO	233	Nuevo Amanecer	\N	\N	\N	0	.
5781480	María Antonia	González Núñez	FEMENINO	\N	PARAGUAYO/A	0983 785 480	UNION DE HECHO	\N	\N	\N	SI	NO	1969	Martin Fierro	\N	\N	\N	3	El señor Silvio Echeverria nació en el Bañado. Antes vivía en la zona 5 A.
1217142	Valerio	González Ocampos	MASCULINO	2062-10-01	PARAGUAYO/A	0983696629	CASADO/A	\N	\N	\N	SI	NO	621	San Felipe y Santiago	\N	\N	\N	0	La pareja lleva 36 años de matrimonio. Ambos mayores de edad.
383	Roberto	González Oviedo	MASCULINO	1977-05-06	PARAGUAYO/A	0982935779	.	\N	\N	\N	SI	NO	389	.	\N	\N	\N	0	Anteriormente vivia con la madre, pero la misma ha fallecido hace 3 meses.
612923	Vidal Norberto	González Parra	MASCULINO	\N	PARAGUAYO/A	0983953987	SOLTERO/A	\N	\N	\N	SI	NO	2230	Mto.Ursicino Velasco	\N	\N	\N	0	2 miembros de la flia.(hijos) se encuentrran estudiando en Espana.
3219140	Alicia Elizabeth	González Ramírez	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	2056	.	\N	\N	\N	0	Vive con la mamá y utiliza todos los electrodomesticos de la madre, ya que su situación es de extrema pobreza
4290122	Augusto Pastor	González Rojas	MASCULINO	\N	PARAGUAYO/A	0985133981	UNION DE HECHO	\N	\N	\N	SI	NO	1341	PAR	\N	\N	\N	0	.
5213866	Dalva Joséfina	González Román	FEMENINO	1991-04-03	PARAGUAYO/A	0983344842	SOLTERO/A	\N	\N	\N	SI	NO	1337	Primavera	\N	\N	\N	0	Es propietaria del lote donde viven ella, su hermana e hijos, dentro de la misma vivienda
5861797	Marcia Lorena	González Román	FEMENINO	\N	PARAGUAYO/A	0972571544	UNION DE HECHO	\N	\N	\N	SI	NO	1338	Primavera	\N	\N	\N	0	Vive dentro del lote de la hermana en una pieza dentro de su vivienda
4585661	Mariela	González Román	FEMENINO	1989-04-03	PARAGUAYO/A	0971315419	UNION DE HECHO	\N	\N	\N	SI	NO	1339	Primavera	\N	\N	\N	0	Vive dentro del lote y techo de su hermana
4632256	Lucía	González Samaniego	FEMENINO	1993-05-07	PARAGUAYO/A	0971269590	UNION DE HECHO	\N	\N	\N	SI	NO	930	Nuevo amanecer	\N	\N	\N	0	.
5932909	Nidia Lidia	González Santos	FEMENINO	\N	PARAGUAYO/A	0982369093	SOLTERO/A	\N	\N	\N	SI	NO	2319	Nuestra Sra. De la Asunción	\N	\N	\N	6	.
3585241	Ana María	González Sosa	FEMENINO	\N	PARAGUAYO/A	0986880251	UNION DE HECHO	\N	\N	\N	SI	NO	530	16 de Agosto y Malvinas	\N	\N	\N	0	Posee una Construccion de 6x4 dentro del lote , ene total tiene dos construcciones.
1191730	Juliána Rosa	González Sosa	MASCULINO	\N	PARAGUAYO/A	0981508837	UNION DE HECHO	\N	\N	\N	SI	NO	255	San Felipe y Santiago	\N	\N	\N	0	.
5436539	María Griselda	González Soto	FEMENINO	\N	PARAGUAYO/A	0983869541	CASADO/A	\N	\N	\N	SI	NO	2569	Mandy Jura	\N	\N	\N	0	.
7059725	Telma Jovina	González Tifi	FEMENINO	1996-07-07	PARAGUAYO/A	0994758235	UNION DE HECHO	\N	\N	\N	SI	NO	1627	Pajagua Naranja	\N	\N	\N	0	Delivery en Farmacia.3 anos de Concubinato.
1091249	Marcelina	González Vda. De Diez Pérez	FEMENINO	\N	PARAGUAYO/A	.	VIUDO/A	\N	\N	\N	SI	NO	2735	Pajagua Naranja	\N	\N	\N	0	Tiene Sala-Comedor.
2681615	Nancy Francisca	González Velázquez	FEMENINO	1975-04-10	PARAGUAYO/A	0983147780	CASADO/A	\N	\N	\N	SI	NO	521	Ursicino Velasco c/Malvinas	\N	\N	\N	0	Tiene otra vivienda al fondo del lote en donde vive sus sobrinos.
4113968	Amalia Fabiola	González Vera	FEMENINO	\N	PARAGUAYO/A	0982887546	UNION DE HECHO	\N	\N	\N	SI	NO	164	Nuevo Amanecer 1060	\N	\N	\N	0	.
2356963	Blanca Liliana	González Vera	FEMENINO	\N	PARAGUAYO/A	0971223359	SEPARADO/A	\N	\N	\N	SI	NO	1353	Ursicino Velasco	\N	\N	\N	0	Es una de las propietarias del lote y de la casa, anteriormente vivía en la casa de la suegra con su marido e hijos, hace 6 meses se separó y vive en la casa de su madre que le había heredado también a ella . Actualmente ocupa una de las piezas en el dom
1651104	María Luz	González Vera	FEMENINO	2065-02-10	PARAGUAYO/A	0991515567	UNION DE HECHO	\N	\N	\N	SI	NO	1041	Yvoty Rory	\N	\N	\N	0	Casa de dos plantas.
1920736	Mercedes Elvia	González Vera	FEMENINO	2069-08-09	PARAGUAYO/A	0982321441	SOLTERO/A	\N	\N	\N	SI	NO	1347	Ursicino Velasco	\N	\N	\N	0	El lote pertenecio a la madre, luego de fallecer la madre heredaron los hermanos, viven con ellosunas de sus hermanas con sus hijos. También sus hijos con sus parejas e hijos.
3484404	Noelia Evelyn	González Vera	FEMENINO	\N	PARAGUAYO/A	0972686102	SEPARADO/A	\N	\N	\N	SI	NO	1623	Ursicino Velasco y Pajagua Naranja	\N	\N	\N	0	Era la casa de la mama el cual dividio a dos de sus hijas siendo ellas las que realizaran las mejoras de los destrozos ocasionados por la crecida.
3173678	Arcenio	González	FEMENINO	\N	PARAGUAYO/A	0982843777	CASADO/A	\N	\N	\N	SI	NO	434	San Cayetano	\N	\N	\N	0	.
7660663	Arnaldo Andrés	González	MASCULINO	\N	PARAGUAYO/A	0984925660	UNION DE HECHO	\N	\N	\N	SI	NO	608	San Cayetano esq. Ursicino Velazco	\N	\N	\N	0	La Familia es de muy escasos recursos, no tienen baño propio, utilizan el baño del vecino.
1826834	Cecilia Mariza	González	FEMENINO	2069-12-04	PARAGUAYO/A	0986550101	UNION DE HECHO	\N	\N	\N	SI	NO	1765	Koety	\N	\N	\N	0	.
1530218	Eludiana	González	FEMENINO	\N	PARAGUAYO/A	0971277501	SOLTERO/A	\N	\N	\N	SI	NO	591	Malvinas	\N	\N	\N	0	La vivienda tiene una extension de madera de 5x6 m2.
1184871	Esmilce	González	FEMENINO	\N	.	0971656294	SOLTERO/A	\N	\N	\N	SI	NO	2132	Agosto Poty	\N	\N	\N	0	.
3471345	Estela	González	FEMENINO	\N	PARAGUAYO/A	0981494454	UNION DE HECHO	\N	\N	\N	SI	NO	670	23 de Junio	\N	\N	\N	0	dentro de este techo vive una pareja de jóvenes con una hija sin terreno ni techo
4996151	Fernando Ariel	González	MASCULINO	\N	PARAGUAYO/A	0971893988	UNION DE HECHO	\N	\N	\N	SI	NO	1043	Yvoty Rory	\N	\N	\N	0	Vive dentro del mismo lote y construccion que la madre 3 anos de convivencia.
497202	Francisca	González	FEMENINO	2047-12-10	PARAGUAYO/A	0971101038	SOLTERO/A	\N	\N	\N	SI	NO	1540	Pajagua Naranja	\N	\N	\N	0	.
2090972	Francisca	González	FEMENINO	\N	PARAGUAYO/A	0984460978	CASADO/A	\N	\N	\N	SI	NO	412	Julio Jara	\N	\N	\N	0	.
2949946	Gloria Liz	González	FEMENINO	1972-07-06	PARAGUAYO/A	0982886957	UNION DE HECHO	\N	\N	\N	SI	NO	1767	Koety	\N	\N	\N	0	.
915	Inocencio	González	MASCULINO	\N	No quiso censar	.	.	\N	\N	\N	SI	NO	1645	.	\N	\N	\N	0	No quiso censarse.
5479193	Jessica Romina	González	FEMENINO	1992-08-03	PARAGUAYO/A	0992727700	SOLTERO/A	\N	\N	\N	SI	NO	1349	Ursicino Velasco	\N	\N	\N	0	Vive dentro de la casa de la mama en una pieza.
5459485	Jorge Daniel	González	MASCULINO	1996-12-06	PARAGUAYO/A	0971888299	UNION DE HECHO	\N	\N	\N	SI	NO	1351	Ursicino Velasco	\N	\N	\N	0	Vive dentro e la casa de la mamá en la pieza.
5639865	Karen Melisa	González	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	102	San Felipe y Santiago	\N	\N	\N	0	Anteriormente vivian en la zona6.
4994139	Liz Marlene	González	FEMENINO	1986-07-12	PARAGUAYO/A	0983691611	UNION DE HECHO	\N	\N	\N	SI	NO	1760	Koety	\N	\N	\N	0	Vive en el mismo terreno y tiene una construccion de material de 4x5 y la construccion data de 10 anos.
4134598	Marcos	González	MASCULINO	1971-04-05	PARAGUAYO/A	0984979694	UNION DE HECHO	\N	\N	\N	SI	NO	2357	Nuestra Señora de la Asuncion	\N	\N	\N	6	el titular tiene problemas de corazón, son una familia de muy escasos recursos
2575889	María Mirta	González	FEMENINO	\N	PARAGUAYO/A	0981474341	UNION DE HECHO	\N	\N	\N	SI	NO	2387	Mainumby	\N	\N	\N	0	Dentro de la vivienda vive la hija de 24 años con un hijo, tiene lote pero comparte vivienda con la mama por el momento
3680620	Miguela Catalina	González	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	752	Martin Fierro	\N	\N	\N	0	.
5491931	Nancy	González	FEMENINO	1989-10-11	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	659	Yoty y Virgen de Lourdes	\N	\N	\N	0	Casa en construccion .
3488987	Noelia Rocío	González	FEMENINO	1983-06-03	PARAGUAYO/A	0986669924	SOLTERO/A	\N	\N	\N	SI	NO	1633	Pajagua Naranja	\N	\N	\N	0	T
4738900	Norma Beatriz	González	FEMENINO	1986-06-06	PARAGUAYO/A	0992345649	CASADO/A	\N	\N	\N	SI	NO	466	Martin Fierro	\N	\N	\N	0	.
4542404	Olga María	González	FEMENINO	1982-01-05	PARAGUAYO/A	0992691345	UNION DE HECHO	\N	\N	\N	SI	NO	582	8 de Diciembre	\N	\N	\N	0	El terreno tiene 2 viviendas Pro-Vida 2 piezas de material.
1421931	Pablo Vicente	González	MASCULINO	\N	PARAGUAYO/A	0986117768	SOLTERO/A	\N	\N	\N	SI	NO	107	San Felipe y Santiago	\N	\N	\N	3	La mdre del jefe de hogar esta viviendo en su casa , pero la Sra. Ursula cuenta con un terreno en la zona 4.
5239482	Regina De Jesús	González	FEMENINO	1996-08-04	PARAGUAYO/A	0986411142	UNION DE HECHO	\N	\N	\N	SI	NO	85	Virgen de Lujan y Virgen de Lourdes	\N	\N	\N	0	.
4030146	Rocío Marlene	González	FEMENINO	\N	PARAGUAYO/A	0992375433	SOLTERO/A	\N	\N	\N	SI	NO	1076	Martin Fierro	\N	\N	\N	0	Vive dentro del lote de Cristhian , Cuenta con una vivienda precaria donde viven sus hijos , su nuera y su nieta.
3678564	Rocío Natalia	González	FEMENINO	\N	PARAGUAYO/A	0986830008	UNION DE HECHO	\N	\N	\N	SI	NO	516	Ursicino Velasco	\N	\N	\N	0	.
1727410	Román	González	MASCULINO	2068-09-03	PARAGUAYO/A	0984208700	SOLTERO/A	\N	\N	\N	SI	NO	1604	Nuestra Senora de la Asunción	\N	\N	\N	6	Actualmente vive en el Refugio por motivo de la inundación.
4139671	Santa Aurora	González	FEMENINO	1980-04-04	PARAGUAYO/A	0985280289	CASADO/A	\N	\N	\N	SI	NO	2366	Juan Leon Mallorquin y Nuestra Sra de la Asuncion	\N	\N	\N	6	.
2155659	Santiago Esteban	González	MASCULINO	\N	PARAGUAYO/A	0982763686	UNION DE HECHO	\N	\N	\N	SI	NO	1518	Rancho 8	\N	\N	\N	0	Vive en el mismo lote del propietario en una construccion precaria de 12x7.
7407108	Teofilo	González	MASCULINO	\N	PARAGUAYO/A	0971 673 494	SOLTERO/A	\N	\N	\N	SI	NO	1676	Yacare Pito	\N	\N	\N	0	El señor Teofilo Gonzalez tiene el beneficio de la tercera edad que maneja su hermana, Tomasa García.
2105682	Wilfrida	González	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	262	Nuevo Amanecer c/ Julio Jara	\N	\N	\N	4	.
4452700	Luis Enrique	Gosling Vargas	MASCULINO	1977-11-03	PARAGUAYO/A	0994352661	UNION DE HECHO	\N	\N	\N	SI	NO	342	Virgen de Lujan	\N	\N	\N	0	.
2151714	María Lucía	Guanes Brítez	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	463	23 de Junio	\N	\N	\N	0	.
2431	Guarderia Divino Nino	.	.	\N	.	.	.	\N	\N	\N	SI	NO	2403	Vecinos Unidos	\N	\N	\N	0	Actualmente la guarderia provee a los niños de una merienda  dos vecs a la semana. La guarderia abarca a niños de 5 a 7 años.
4917	Guardería Polvito de Estrella	.	.	\N	.	.	.	\N	\N	\N	SI	NO	181	Actualmente no esta en funcionamiento.Funciona com	\N	\N	\N	0	.
2568459	Lidia Elizabeth	Guayuan Cáceres	FEMENINO	1976-06-11	PARAGUAYO/A	0982213719	UNION DE HECHO	\N	\N	\N	SI	NO	404	Algarrobo	\N	\N	\N	0	La construccion es de 2 pisos.
3995405	Melanie Giannina	Guccione Benítez	FEMENINO	1996-05-01	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	2586	Nuestra Sra de la Asuncion	\N	\N	\N	6	.
759220	Teofilo Esteban	Guerrero Rolón	MASCULINO	2055-05-03	PARAGUAYO/A	0982228287	CASADO/A	\N	\N	\N	SI	NO	2262	Nuestra Sra. De la Asuncion	\N	\N	\N	0	.
2039427	Marcos Antonio	Guillen Duarte	MASCULINO	\N	PARAGUAYO/A	0981474677	UNION DE HECHO	\N	\N	\N	SI	NO	2545	Mandy Jura	\N	\N	\N	0	.
4803551	Celso Antonio	Guzmán Amaro	MASCULINO	\N	PARAGUAYO/A	0983393646	UNION DE HECHO	\N	\N	\N	SI	NO	353	4 de Octubre	\N	\N	\N	0	LA vivienda no tiene bano , cocina , utilizan un corredor.
4187160	Laura Marina	Guzmán Amaro	FEMENINO	1984-08-08	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	352	4 de Octubre	\N	\N	\N	0	.
3961073	Ignacio	Guzmán Fernández	MASCULINO	\N	PARAGUAYO/A	0992688557	SOLTERO/A	\N	\N	\N	SI	NO	138	Las Orquideas c/ Aromita	\N	\N	\N	0	Esta vivienda se encuentra a lado del LOTE 20
4326255	Oscar	Guzmán Fernández	MASCULINO	\N	PARAGUAYO/A	0983786796	.	\N	\N	\N	SI	NO	142	Las Orquideas Esq/ Aromita	\N	\N	\N	0	.
752428	Juan Alberto	Guzmán Rosetti	MASCULINO	2059-11-01	PARAGUAYO/A	0992461092	SOLTERO/A	\N	\N	\N	SI	NO	127	Nuevo Amanecer	\N	\N	\N	0	.
2897557	Adriana Paola	Guzmán	FEMENINO	\N	PARAGUAYO/A	.	SEPARADO/A	\N	\N	\N	SI	NO	124	Nuevo amanecer	\N	\N	\N	0	.
6048470	Clara Bernarda	Haedo González	FEMENINO	\N	PARAGUAYO/A	0992481564	UNION DE HECHO	\N	\N	\N	SI	NO	1889	Julio Benitez	\N	\N	\N	0	.
2162999	Lucíana	Haedo	FEMENINO	\N	PARAGUAYO/A	0971660781	UNION DE HECHO	\N	\N	\N	SI	NO	123	Nuevo Amanecer	\N	\N	\N	4	.
4669660	Cinthia Katerin	Haneman Capdevila	FEMENINO	\N	.	0985988738	.	\N	\N	\N	SI	NO	1861	Villa Maria Auxiliadora	\N	\N	\N	0	Usa uno de sus salones para su peluqueria.Tiene un Salón de Belleza.
4815520	Natalia Noemí	Hebert de Servín	FEMENINO	1991-03-06	PARAGUAYO/A	0961175941	CASADO/A	\N	\N	\N	SI	NO	2747	Rancho 8	\N	\N	\N	0	.
3471578	Cecilia	Hebert Escobar	FEMENINO	\N	PARAGUAYO/A	0985561299	SOLTERO/A	\N	\N	\N	SI	NO	260	Angel Luis	\N	\N	\N	0	Construcción de Hormigon.Vive en la casa de la madre en laZona 4, la construcción del hormigon paro desde la creciente.
4641685	Cristian Daniel	Hebert Escobar	MASCULINO	\N	PARAGUAYO/A	0982958926	UNION DE HECHO	\N	\N	\N	SI	NO	615	Ursicino Velasco/ San Felipe y Santiago	\N	\N	\N	0	Este núcleo tiene 6 años como pareja, los dos nacieron y se criaron en el bañado.
4645947	Edgar Javier	Hebert Escobar	MASCULINO	1989-12-03	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	249	San Felipe y Santiago	\N	\N	\N	0	Patio Baldio Notiene ninuguna construcción.
4786846	Shirley Elizabeth	Hebert Irala	FEMENINO	1993-05-01	PARAGUAYO/A	0971789504	SOLTERO/A	\N	\N	\N	SI	NO	2600	Rancho 8	\N	\N	\N	0	La Sra. Esta separada hace 2 anos de su 1ra relacion donde tuvo dos hijas.El padre le provee una mensualidad para su manutensión, el padre del ultimo hijo menor no le provee de ninguna mensualidad.Es madre soltera se separo del padre del ultimo hijo hace
1108925	Sixta Salvadora	Hebert	FEMENINO	2048-06-08	PARAGUAYO/A	0982369997	SOLTERO/A	\N	\N	\N	SI	NO	1506	rancho 8	\N	\N	\N	0	.
5263600	Emilce	Heladeria-Valdéz Fretes	FEMENINO	1991-05-06	PARAGUAYO/A	0972178894	.	\N	\N	\N	SI	NO	543	Mtro Urcicino Velazco	\N	\N	\N	0	La construcción se utiliza para venta de helados y venta de mercería. No se utiliza para vivienda, Sólo para comercio, la heladería funciona hace 3 años
3113	Hogar Comunitario	.	.	\N	.	.	.	\N	\N	\N	SI	NO	2071	El Centro Comunitario fue fundado por la comision	\N	\N	\N	0	.
1221495	María Graciela	Huerta de Delvalle	FEMENINO	\N	PARAGUAYO/A	.	CASADO/A	\N	\N	\N	SI	NO	1955	Urcicino Velazco c/Julio Benitez	\N	\N	\N	0	La Sra hace cinco años que vive en el barrio, pero siempre en alquiler
29881	Iglesia de Dios en el Paraguay OASIS de esperanza	.	.	\N	.	0982558363	.	\N	\N	\N	SI	NO	2223	.	\N	\N	\N	0	Supervisor, Cesar Paredes (pastor)C.I: 733084
2403	Iglesia Evangélica Peniel	.	.	\N	.	0982762102	.	\N	\N	\N	SI	NO	1037	Mar de plata esqu. Nuevo Amanecer	\N	\N	\N	0	El lote pertenece a la Iglesia persorenia juridica 2403. El represante legal es el señor Ireneo Pérez.
80030829	Iglesia Nazarena Apostolica Cristiana	.	.	\N	.	0971264346	.	\N	\N	\N	SI	NO	2379	Ursicino Velasco	\N	\N	\N	0	La Iglesia Nazarena tiene varias construcciones dentro del lote.1 Salón que se utiliza para las reuniones de cultos.
1775186	Carlos Ramón	Iglesia Pereira	MASCULINO	\N	PARAGUAYO/A	0984144025	SOLTERO/A	\N	\N	\N	SI	NO	544	Mtro Urcicino Velazco c/ Martín Fierro	\N	\N	\N	0	El terreno no cuenta con patio, sólo cuenta con la contrucción que se mencionan en la misma
1389009	Lorenzo Dario	Insaurralde Colmán	MASCULINO	\N	PARAGUAYO/A	0983347951	SOLTERO/A	\N	\N	\N	SI	NO	1282	.	\N	\N	\N	0	Actualmente vive en alquiler y tiene un contrato de 1 ano mas en la casa nadie vive porque en reparaciones.
3865555	María Soledad	Insaurralde de Ocampo	FEMENINO	1982-12-01	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	595	Malvinas	\N	\N	\N	0	Tiene un pasillo de 1,70 x 8.Los datos de la familia fue proporcionado por la hija mayor de 18 anos.
3736370	Mirta Zoraida	Insaurralde Núñez	FEMENINO	\N	PARAGUAYO/A	0985579921	UNION DE HECHO	\N	\N	\N	SI	NO	1507	Rancho 8	\N	\N	\N	0	Con la flia vive el suegro la titular, el mismo recibe sueldo del estado.La pareja lleva 15 anos de concubinato,el marido nacio en el barrio.
3421418	Luz Marina	Insaurralde	FEMENINO	\N	PARAGUAYO/A	0992629014	SOLTERO/A	\N	\N	\N	SI	NO	2154	Virgen de Guadalupe	\N	\N	\N	0	.
2967563	María Castorina	Insfrán Barrios	FEMENINO	\N	PARAGUAYO/A	0982166370	UNION DE HECHO	\N	\N	\N	SI	NO	408	Julio Jara c/ Nuevo Amanecer	\N	\N	\N	4	.
4460564	Blanca Liz	Insfrán Caceres	FEMENINO	1983-04-04	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	377	Virgen de Lujan y Mar del Plata	\N	\N	\N	0	Hace 10 anos que viven en el lote propio , la casa tiene una construccion de 16 anos Anteriormente la pareja vivia en Santa Ana.
1078677	Anastacio	Insfrán Cuevas	FEMENINO	\N	PARAGUAYO/A	0984126444	UNION DE HECHO	\N	\N	\N	SI	NO	918	Angel Luis	\N	\N	\N	0	.
1301944	Antonio Patricio	Insfrán Cuevas	MASCULINO	2064-12-07	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	1116	.	\N	\N	\N	0	.
4637646	Sandra Yohana	Insfrán Torres	FEMENINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	1770	La casa es aparte.	\N	\N	\N	0	.
4662986	Andrea Natalia	Irala Benítez	FEMENINO	\N	PARAGUAYO/A	0971289413	UNION DE HECHO	\N	\N	\N	SI	NO	655	23 de Junio	\N	\N	\N	0	Tienen vivienda de la cooperativa. Esta pareja tienen viviendo y tienen propio, tienen 5 años de concubinato.
6542577	Jessica Noemi	Irala Benítez	FEMENINO	\N	PARAGUAYO/A	0984369270	UNION DE HECHO	\N	\N	\N	SI	NO	640	San Felipe y Santago c/ 23 de Junio	\N	\N	\N	0	La pareja tiene 3 años de concubinato. Tienen un techo propio con la construcción de material dentro del lote de la titular.
3855198	Sebastiana	Irala de Scheunemann	FEMENINO	\N	PARAGUAYO/A	.	CASADO/A	\N	\N	\N	SI	NO	2618	Ursicino Velazco	\N	\N	\N	0	.
2837254	Lucy	Irala Escobar	FEMENINO	1973-08-08	PARAGUAYO/A	0971789504	UNION DE HECHO	\N	\N	\N	SI	NO	2598	Rancho 8	\N	\N	\N	0	Placa Electrica 1.
4087369	Ninfa Graciela	Irala Escobar	FEMENINO	1983-02-07	PARAGUAYO/A	0983904237	SOLTERO/A	\N	\N	\N	SI	NO	1520	Rancho 8	\N	\N	\N	0	Placa Induccion 1.Este nucleo familiar vive dentro del lote de la titular con un techo de material pegado a la casa de la titular.
5721748	Angela Tomasa	Irala González	FEMENINO	\N	PARAGUAYO/A	0972 823 606	UNION DE HECHO	\N	\N	\N	SI	NO	1903	Pilcomayo	\N	\N	\N	0	La pareja lleva tres años de concubinato.
784290	Gerónimo	Irala Ortíz	MASCULINO	\N	PARAGUAYO/A	0982745818	SOLTERO/A	\N	\N	\N	SI	NO	644	San Felipe y Santiago esq/ 23 de Junio	\N	\N	\N	0	.
1659343	Jorgelina Beatriz	Irala	MASCULINO	2067-09-09	PARAGUAYO/A	0994846197	UNION DE HECHO	\N	\N	\N	SI	NO	1513	Rancho 8	\N	\N	\N	0	Centrifugadora y Placa Electrica 1 c/u.
2363687	Eva María	Irigoyen Oviedo	FEMENINO	\N	PARAGUAYO/A	0991455351	UNION DE HECHO	\N	\N	\N	SI	NO	2355	Nuestra Sra de la Asunción	\N	\N	\N	6	.
5731891	Galdys Fanny	Irigoyen Oviedo	FEMENINO	\N	PARAGUAYO/A	0994769631	UNION DE HECHO	\N	\N	\N	SI	NO	885	Nuevo Amanecer	\N	\N	\N	4	.
3530444	Juan Carlos	Irigoyen Oviedo	MASCULINO	\N	PARAGUAYO/A	0994266606	CASADO/A	\N	\N	\N	SI	NO	2372	Nuestra Sra  de la Asuncion	\N	\N	\N	6	dentro del lote del,titular, viven 3 familias cada uno con techo indepediente. Dentro del techo del titular vive su hijo mayor con pareja un año de concubinato, sin hijos, no tienen lote propio, pero si una pieza de 3x4.
2235812	Olga Beatriz	Irigoyen Oviedo	FEMENINO	\N	PARAGUAYO/A	0984159730	UNION DE HECHO	\N	\N	\N	SI	NO	2378	Nuestra Sra. De la Asunción	\N	\N	\N	6	.
5616326	Liz Gabriela	Irigoyen	FEMENINO	\N	PARAGUAYO/A	0991455351	SOLTERO/A	\N	\N	\N	SI	NO	2360	Nuestra Sra de la Asunción	\N	\N	\N	6	Cocina Electrica 1.
6082880	Juan Emanuel	Jacquet Zaval	MASCULINO	\N	PARAGUAYO/A	0991393283	UNION DE HECHO	\N	\N	\N	SI	NO	1505	Rancho 8	\N	\N	\N	0	Centrifugadora 1.La pareja lleva 4 anos de concubinato viven en lote de la madre quien es la titular.
1990660	Balvina	Jara Aguayo	FEMENINO	\N	PARAGUAYO/A	0972234676	UNION DE HECHO	\N	\N	\N	SI	NO	1310	Ursicino Velasco	\N	\N	\N	0	tiene una vivienda muy amplia de dos pizos utilizados para alquilar para negocios y 5 dormitorios tambien estan alquilados.
5362175	Diego Ernesto	Jara Capdevila	MASCULINO	1991-02-07	PARAGUAYO/A	0985208235	SOLTERO/A	\N	\N	\N	SI	NO	1361	Yvoty Rory	\N	\N	\N	0	.
3604349	Julián	Jara Cardenas	MASCULINO	\N	PARAGUAYO/A	0981935277	SOLTERO/A	\N	\N	\N	SI	NO	1803	Bañado Koeti	\N	\N	\N	5	.
2026335	Félix Reinaldo	Jara Chávez	MASCULINO	1971-05-01	PARAGUAYO/A	0983722961	SOLTERO/A	\N	\N	\N	SI	NO	375	YVOTY Esq/ Virgen de Lourdes	\N	\N	\N	0	La hna. Mayor del titular nacio en el barrio . Luego trajo a su hno a ala edad de 6 anos a vivir con ella.
2384867	Juan Evangelista	Jara Chávez	MASCULINO	2055-03-06	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	938	Virgen de lujan	\N	\N	\N	0	.
4213605	Edson Armando	Jara Denis	MASCULINO	\N	PARAGUAYO/A	0981216870	UNION DE HECHO	\N	\N	\N	SI	NO	2534	Vecinos Unidos	\N	\N	\N	0	.
3802917	Lidia Estefana	Jara Denis	FEMENINO	\N	PARAGUAYO/A	0983886490	CASADO/A	\N	\N	\N	SI	NO	2554	Mandy Jura	\N	\N	\N	0	.
7538683	Nancy Mabel	Jara Fernández	FEMENINO	1998-03-03	PARAGUAYO/A	0972796773	UNION DE HECHO	\N	\N	\N	SI	NO	330	Mto. Ursicino Velasco	\N	\N	\N	0	Compro el lote hace 1 mes para su casa en la Asocicion de Carreros.
5182598	Hermelinda	Jara Florentín	FEMENINO	1998-05-02	PARAGUAYO/A	0971300635	UNION DE HECHO	\N	\N	\N	SI	NO	993	San Felipe y Santiago	\N	\N	\N	3	La pareja lleva 3 años de concubinato, tienen lote propio y techo precario.
1962392	Eulogia	Jara González	FEMENINO	\N	PARAGUAYO/A	0982259395	CASADO/A	\N	\N	\N	SI	NO	289	Ursicino Velasco	\N	\N	\N	0	.
4003750	Limpia Concepción	Jara Mencia	FEMENINO	1990-08-12	PARAGUAYO/A	.	CASADO/A	\N	\N	\N	SI	NO	153	Ursicino Velasco c/ 8 de Diciembre	\N	\N	\N	0	.
6662797	Alberta Ramona	Jara	FEMENINO	\N	PARAGUAYO/A	.	VIUDO/A	\N	\N	\N	SI	NO	716	Mar del Plata e/ Virgen de Lujan	\N	\N	\N	5	.
1523	Deysi Noemi	Jara	FEMENINO	\N	No quiso censar	.	.	\N	\N	\N	SI	NO	2555	.	\N	\N	\N	0	No quiso censarse.
2818	Félix Ramón	Jara	MASCULINO	2057-02-07	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	1088	23 de Junio	\N	\N	\N	0	Vive dentro del lote de la nuera con su hijo que vivienda precaria tiene otro hijo que vive del mismo lote en otra vivienda precaria.
773896	Julia	Jiménez Mereles	FEMENINO	2055-08-02	PARAGUAYO/A	0982564232	UNION DE HECHO	\N	\N	\N	SI	NO	1747	Ursicino Velasco	\N	\N	\N	0	Hector tiene una construccion aparte se maneja de manera independiente.
3260928	Heriberto Ramón	Jiménez	MASCULINO	\N	PARAGUAYO/A	00991890747	UNION DE HECHO	\N	\N	\N	SI	NO	34	San Felipe y Santiago	\N	\N	\N	0	Esposa e Hija trabajan en Espana.
3787529	Sivio Daniel	Jiménez	MASCULINO	1976-02-09	PARAGUAYO/A	0985334012	SOLTERO/A	\N	\N	\N	SI	NO	989	San Felipe Y Santiago	\N	\N	\N	3	El titular del techo vive en el lote de la madre. Es padre soltero de tres(3) niñas. Es beneficiado con la casa de la cooperativa Bañadp poty.
6013	Juan Ramón	.	MASCULINO	\N	.	.	.	\N	\N	\N	SI	NO	2682	Pasillo Sin nombre	\N	\N	\N	0	Según manifestaciones de vecinos el propietario del lote y casa, no frecuenta abitualmente. Desconocen el porqué y tienen una construcción precaria de 3x3.
1584000	Luicia	Landí Fernández	FEMENINO	1973-01-02	PARAGUAYO/A	0981788728	UNION DE HECHO	\N	\N	\N	SI	NO	2599	Nuestra Señora de la Asuncion	\N	\N	\N	6	Placa Electrica 1.
5138965	Nelson	Larrosa González	MASCULINO	1986-12-10	PARAGUAYO/A	0971949006	UNION DE HECHO	\N	\N	\N	SI	NO	491	Remancito c/ 23 de Junio	\N	\N	\N	0	.
5927641	Miryan	Ledezma	FEMENINO	1985-10-09	PARAGUAYO/A	0984823537	UNION DE HECHO	\N	\N	\N	SI	NO	825	Vecinos Unidos	\N	\N	\N	0	En la crecida del Rio, se le extraviaron todos los documentos de sus hijos.
2653349	Félix Dionisio	Leguizamon Amarilla	MASCULINO	\N	PARAGUAYO/A	0985582953	SOLTERO/A	\N	\N	\N	SI	NO	695	Yacare Yrupe	\N	\N	\N	5	.
981674	Dionisio	Leguizamon Estigarribia	MASCULINO	\N	PARAGUAYO/A	0983188478	SOLTERO/A	\N	\N	\N	SI	NO	697	Yacare Yrupe	\N	\N	\N	5	.
2344826	María Magdalena	Leguizamon Estigarribia	FEMENINO	\N	PARAGUAYO/A	0983250970	SOLTERO/A	\N	\N	\N	SI	NO	2513	Yacare Yrupe	\N	\N	\N	5	Cocina a Gas 1.
3542130	Claudia Carolina	Leguizamón Romero	FEMENINO	\N	PARAGUAYO/A	0982647724	UNION DE HECHO	\N	\N	\N	SI	NO	1899	.	\N	\N	\N	0	.
5874711	Ángel Luciano	Leiva Soria	MASCULINO	\N	PARAGUAYO/A	0992395702	UNION DE HECHO	\N	\N	\N	SI	NO	2260	Dr. Juan Leon Mallorquin	\N	\N	\N	0	Luciana Soria no quiso dar datos de su nro. De cedula.
176961	Juana Estanislaa	Leiva vda. De Ayala	MASCULINO	\N	PARAGUAYO/A	0982728323	VIUDO/A	\N	\N	\N	SI	NO	1355	Ursicino Velasco	\N	\N	\N	0	.
2961112	Barsilicia	Leiva	FEMENINO	1972-01-08	PARAGUAYO/A	0986378331	UNION DE HECHO	\N	\N	\N	SI	NO	1068	Ursicino Velasco	\N	\N	\N	0	Espropietaria del lote , viven dentro de una vivienda de material bien constituida dentro del lote hay dos viviendas de madera donde viven su suegra en una y su cunada en otra.
5409991	Diana Antonia	Leiva	FEMENINO	1989-08-07	PARAGUAYO/A	0982770773	SOLTERO/A	\N	\N	\N	SI	NO	2165	Ursicino Velasco c/ Julio Benitez	\N	\N	\N	0	La mamá le regalo la casa a los hijos ella era la esposa de uno de el cual se caso con otra dejandole su parte a ella y su hija.
2169724	Sabina Beatriz	Leiva	FEMENINO	\N	PARAGUAYO/A	0994 684 826	SOLTERO/A	\N	\N	\N	SI	NO	2458	Mandyjura	\N	\N	\N	0	.
876388	Mario Gregorio	Lezcano Lezcano	MASCULINO	\N	PARAGUAYO/A	0992678203	SOLTERO/A	\N	\N	\N	SI	NO	796	Virgen de Lujan	\N	\N	\N	0	.
4370729	Lizi Carolina	Lezcano Morai	FEMENINO	1985-03-04	PARAGUAYO/A	0992256663	UNION DE HECHO	\N	\N	\N	SI	NO	646	23 de Junio	\N	\N	\N	0	La pareja lleva 11 años de concubinato.
5151959	Natalia María	Lezcano Morai	FEMENINO	\N	PARAGUAYO/A	0992463244	SOLTERO/A	\N	\N	\N	SI	NO	1086	Martin Fierro c/ 23 de Junio	\N	\N	\N	0	.
5151940	Cristian David	Lezcano Moray	MASCULINO	\N	PARAGUAYO/A	0982496163	UNION DE HECHO	\N	\N	\N	SI	NO	480	23de Junio	\N	\N	\N	0	.
4923213	Andrea Verenisse	Lezcano Rojas	FEMENINO	1989-11-08	PARAGUAYO/A	0991841112	UNION DE HECHO	\N	\N	\N	SI	NO	733	Mto. Ursicino Velasco	\N	\N	\N	0	.
4486496	Lilian Victoria	Lezcano Rojas	FEMENINO	1987-01-04	PARAGUAYO/A	0992738280	CASADO/A	\N	\N	\N	SI	NO	1077	Martin Fierro c/Ursicino Velasco	\N	\N	\N	0	.
4627438	Richard Ramón	Lezcano	MASCULINO	\N	PARAGUAYO/A	0981480226	UNION DE HECHO	\N	\N	\N	SI	NO	2547	Mandy Jura	\N	\N	\N	0	.
2541965	Juana María	Lima Ramírez	MASCULINO	1974-10-04	PARAGUAYO/A	0981378298	UNION DE HECHO	\N	\N	\N	SI	NO	1080	Martin Fierro	\N	\N	\N	0	.
4052799	Nilda Isabel	Lima Ramírez	FEMENINO	1980-06-12	PARAGUAYO/A	0971312075	UNION DE HECHO	\N	\N	\N	SI	NO	682	Yacare Yrupe	\N	\N	\N	0	.
6689158	Claudia	Limenza Ramos	FEMENINO	\N	PARAGUAYO/A	0991660601	SOLTERO/A	\N	\N	\N	SI	NO	2585	Rancho 8	\N	\N	\N	0	Actulamente vive bajo el mismo techo de la madre,utiliza dos de sus piezas precarias no tiene lote propio,hace mas de un ano que esta separada del padre de sus hijos.El padre le pasa una mensualidad de 200.000 Gs.
5321852	Fabiola Celeste	Limenza Ramos	MASCULINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	2582	Rancho 8	\N	\N	\N	0	Actualmente los hijos se encuentran en un hogar de niños internados por orden Judicial,la pareja 11 anos de concubinato ,tienen un techo precario dentro del lote de madre.
633621	María Eloisa	López  Vda de Quintana	FEMENINO	2061-05-05	PARAGUAYO/A	021422829	VIUDO/A	\N	\N	\N	SI	NO	1890	Urcisino Velasco casi Julio Benitez	\N	\N	\N	0	La señora cobra la jubilacion de su marido. Cuenta con una casa de dos pisos y un quincho.
2207272	Gregorio Bernabe	López Achucarro	MASCULINO	2066-11-06	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	1719	Yacare Pito	\N	\N	\N	0	la vivienda es de madera, construida por los voluntarios de un techo para mi pais con una extensión precaria que utilizan como depósito. En la planilla de Barrio Temporal figura el nombre de Gregorio Lopez
4516928	Estela Marys	López Amarilla	FEMENINO	1990-12-09	PARAGUAYO/A	.	CASADO/A	\N	\N	\N	SI	NO	175	Julio Jara y Nuevo Amanecer	\N	\N	\N	0	.
4517058	Mirna Teresa	Lopez Amarilla	FEMENINO	1988-06-04	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	176	Julio Jara y Nuevo Amanecer	\N	\N	\N	0	.
3242542	Cecilia Marlene	López Aponte	FEMENINO	\N	PARAGUAYO/A	0983451992	UNION DE HECHO	\N	\N	\N	SI	NO	386	Ursicino Velasco	\N	\N	\N	0	.
2215082	Mirian Rosana	López Aponte	FEMENINO	1976-12-06	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	1563	Ursicino Velasco	\N	\N	\N	0	Cuenta con una vivienda propia, actualmente su vivienda esta en reconstruccion, vive  provisoriamente en la casa de Juana con su madre e hijo.
5460951	Verónica Antonia	López Baez	FEMENINO	\N	PARAGUAYO/A	0992650255	UNION DE HECHO	\N	\N	\N	SI	NO	2689	Maria Auxiliadora	\N	\N	\N	0	.
448884	Eduvigis	López Benítez	FEMENINO	\N	PARAGUAYO/A	0984433196	VIUDO/A	\N	\N	\N	SI	NO	1214	vecinos Unidos c/ Santa Ana	\N	\N	\N	0	.
6580595	Edgar Hernán	López Britez	MASCULINO	1994-02-10	PARAGUAYO/A	0994146859	UNION DE HECHO	\N	\N	\N	SI	NO	366	Yvoty c/ Virgen de Lourdes	\N	\N	\N	0	vive bajo el mismo techo y lote que la sra De Lo Santa Britez
3204457	Marina Isabel	López Cantero	FEMENINO	\N	PARAGUAYO/A	0991670663	UNION DE HECHO	\N	\N	\N	SI	NO	139	Las Orquideas	\N	\N	\N	4	.
678567	Inocente Adolfo	López Chávez	MASCULINO	\N	PARAGUAYO/A	.	CASADO/A	\N	\N	\N	SI	NO	656	Mar del Plata y Virgen de Lourdes	\N	\N	\N	0	Dentro del techo del titular viven con sus respectivas parejas las hijas mayores . Una de las parejas tiene un menor de edad.
1295116	Fulgencia	López de Franco	FEMENINO	\N	PARAGUAYO/A	0972516139	CASADO/A	\N	\N	\N	SI	NO	1427	Ursicino Velasco	\N	\N	\N	0	.
3639703	Sonia Raquel	López de Mareco	FEMENINO	\N	PARAGUAYO/A	0986871334	CASADO/A	\N	\N	\N	SI	NO	1616	Ursicino Velasco	\N	\N	\N	4	.
3341143	María Margarita	López de Martínez	FEMENINO	\N	PARAGUAYO/A	0983662040	SOLTERO/A	\N	\N	\N	SI	NO	785	26 de Julio	\N	\N	\N	3	.
1214381	Maura	López de Peralta	FEMENINO	\N	PARAGUAYO/A	0985528752	CASADO/A	\N	\N	\N	SI	NO	2024	Mbo'ehara	\N	\N	\N	0	.
543338	Alcira	López de Silva	FEMENINO	\N	PARAGUAYO/A	3282970	CASADO/A	\N	\N	\N	SI	NO	2264	Nuestra Sra. De la Asuncion	\N	\N	\N	0	.
5124390	Hugo Arnaldo	López Espinola	MASCULINO	\N	PARAGUAYO/A	83692821	CASADO/A	\N	\N	\N	SI	NO	1611	Payagua naranja	\N	\N	\N	3	.
4218976	María Liz	López Esquivel	FEMENINO	\N	PARAGUAYO/A	0972 845 278	SOLTERO/A	\N	\N	\N	SI	NO	1896	Pilcomayo C/ Yacare Yrupe	\N	\N	\N	0	La titular del techo B se mantiene a travez de una mensualidad que el padre de las hijas les provee.
3189579	Orlando Daniel	López Esquivel	MASCULINO	\N	PARAGUAYO/A	0972 754 951	SOLTERO/A	\N	\N	\N	SI	NO	1893	Pilcomayo c/ Yacare	\N	\N	\N	0	el titular es soltero con una hija que vive con la madre dentro del barrio Bañado Tacumbu, zona 5.
3388930	Liz Marina	López Ferreira	FEMENINO	1980-09-06	PARAGUAYO/A	0983176643	CASADO/A	\N	\N	\N	SI	NO	2410	Virgen de Lujan c Mar del Plata	\N	\N	\N	0	.
2816474	Aristides	López Franco	MASCULINO	\N	PARAGUAYO/A	.	CASADO/A	\N	\N	\N	SI	NO	56	RECICLA	\N	\N	\N	0	.
4393755	Cirila	López Gauto	FEMENINO	\N	PARAGUAYO/A	0985389674	UNION DE HECHO	\N	\N	\N	SI	NO	132	AROMITA	\N	\N	\N	0	Vive en alquiler,alquila de la Sra. Bernardina Zarate Godoy.
5870114	Vanessa Beatriz	López Gómez	FEMENINO	\N	PARAGUAYO/A	0986928953	SOLTERO/A	\N	\N	\N	SI	NO	535	San Felipe c/ 23 de Junio	\N	\N	\N	0	.
3671083	Mirian Marlene	López Morinigo	FEMENINO	\N	PARAGUAYO/A	0991333239	CASADO/A	\N	\N	\N	SI	NO	500	San Felipe y Santiago	\N	\N	\N	0	.
2337217	Marco	López Ocampo	MASCULINO	\N	PARAGUAYO/A	.	VIUDO/A	\N	\N	\N	SI	NO	1010	Mar de Plata	\N	\N	\N	0	.
4484940	Josia Josue	López Osorio	MASCULINO	\N	PARAGUAYO/A	0986618531	UNION DE HECHO	\N	\N	\N	SI	NO	401	ALGAROBO	\N	\N	\N	0	.
3816307	Ramona Noemi	López Pereira	MASCULINO	\N	PARAGUAYO/A	0984960573	UNION DE HECHO	\N	\N	\N	SI	NO	1221	Yvoty Rory	\N	\N	\N	0	Vive dentro del mismo lote de los padres, pero en una construcción aparte.
2453568	Liboria	López Rodas	FEMENINO	2059-12-06	PARAGUAYO/A	0984418244	UNION DE HECHO	\N	\N	\N	SI	NO	108	San Felipe y Santiago	\N	\N	\N	3	Dentro del lote hay una sola vivienda en el cual vive Liboria y su hijo de 22 anos ,actualmente estan separados pero viven en la misma casa (la pareja).
4673409	Rolando Miguel	López Rodríguez	MASCULINO	\N	PARAGUAYO/A	0986268082	UNION DE HECHO	\N	\N	\N	SI	NO	1164	Fidelina	\N	\N	\N	0	Comparte Datos con Maria Gladys Cabral (5-A1).
3341082	Estelvina	López Sanabria	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	786	26 de Julio	\N	\N	\N	0	.
1631321	Honoria	López Sanabria	FEMENINO	1972-02-03	PARAGUAYO/A	0972833971	UNION DE HECHO	\N	\N	\N	SI	NO	1384	Primavera	\N	\N	\N	0	.
3512818	Rosa Sonia	López Sanabria	FEMENINO	\N	PARAGUAYO/A	0983241318	UNION DE HECHO	\N	\N	\N	SI	NO	1261	Fidelina y 26 de Julio	\N	\N	\N	0	.
4982233	Sonia Mabel	López Sanabria	FEMENINO	\N	PARAGUAYO/A	0986819258	SOLTERO/A	\N	\N	\N	SI	NO	1262	Fidelina y 26 de Julio	\N	\N	\N	0	La cocina y bano comparte con la Sra. Rosa .Sonia Sanabria es su sobrina vive en el lote hace 2 anos.
3287406	Francisca Soledad	López Santa Cruz	FEMENINO	1981-04-10	PARAGUAYO/A	0992996482	UNION DE HECHO	\N	\N	\N	SI	NO	761	Martin Fierro	\N	\N	\N	0	.
1723537	José Librado	López Trinidad	MASCULINO	\N	PARAGUAYO/A	0981482372	VIUDO/A	\N	\N	\N	SI	NO	1862	Villa Maria Auxiliadora	\N	\N	\N	0	Una de las hijas ya esta aconcubinada hace 3 anos y tiene una construccion de 5x4 en el lote del padre.
3941380	Cinthia Hermenegilda	López	FEMENINO	1989-10-03	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	817	26 de Julio	\N	\N	\N	0	Ahora mismo esta en lo de su mama . Se mudo a ala casa de su mama de manera temporal porque las lluvias continuas inundaron sus lotes según vecinos esta en consumo de drogas.
3960460	Felicita	López	FEMENINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	784	26 de Julio	\N	\N	\N	0	Vive con la hermana de 19 años.
1487071	Juliána	López	MASCULINO	\N	PARAGUAYO/A	0971231046	UNION DE HECHO	\N	\N	\N	SI	NO	1451	Virgen de Guadalupe	\N	\N	\N	0	.
1526575	Mirta Beatriz	López	FEMENINO	\N	Paaguaya	0983 805 900	CASADO/A	\N	\N	\N	SI	NO	1826	Urcisio Velazco con Virgen de Guadalupe	\N	\N	\N	0	Tiene una Lavanderia y tambien los fines de semana venden Pollo al Spiedo.
1589833	Norma Elisa	López	FEMENINO	\N	PARAGUAYO/A	0981848181	SOLTERO/A	\N	\N	\N	SI	NO	774	Jakare Yrupe	\N	\N	\N	0	.
775305	Pablo Jovino	López	MASCULINO	\N	PARAGUAYO/A	0994 117 148	CASADO/A	\N	\N	\N	SI	NO	1894	Pilcomayo C/ Yacare Yrupe	\N	\N	\N	0	.
942755	Ramón Roberto	López	MASCULINO	2063-07-06	PARAGUAYO/A	0982909105	SEPARADO/A	\N	\N	\N	SI	NO	1030	Mto. Ursicino Velasco	\N	\N	\N	0	.
783889	Rossana  Mercedes	López	FEMENINO	1975-04-01	PARAGUAYO/A	0973104774	CASADO/A	\N	\N	\N	SI	NO	2388	Ursicino Velasco y 26 de Julio	\N	\N	\N	0	.
1398924	Seferina	López	FEMENINO	\N	PARAGUAYO/A	0971784113	UNION DE HECHO	\N	\N	\N	SI	NO	2013	Nasaindy	\N	\N	\N	5	Es propietario del lote en la casa cada uno de sus hijos tiene una pieza.
5464332	María Gloria	Lovera Alfonso	FEMENINO	1992-06-11	PARAGUAYO/A	0985686416	CASADO/A	\N	\N	\N	SI	NO	1364	Yvoty Rory	\N	\N	\N	0	Es propietari de lote donde viven con sus cunados e hijos.
2215080	Dario Daniel	Lovera Benítez	MASCULINO	\N	PARAGUAYO/A	0982647424	SEPARADO/A	\N	\N	\N	SI	NO	948	Mar del Plata	\N	\N	\N	0	Blanca Elizabeth , la hija esta embarazada.
5172283	Denis Arturo	Lovera Benítez	MASCULINO	1991-01-10	PARAGUAYO/A	0991195107	UNION DE HECHO	\N	\N	\N	SI	NO	335	Mar del Plata	\N	\N	\N	0	Vive con la mama.
5200350	Elena Elizabeth	Lovera Benítez	FEMENINO	1993-07-07	PARAGUAYO/A	0983135118	UNION DE HECHO	\N	\N	\N	SI	NO	227	YVOTY	\N	\N	\N	0	.
1453036	Félix Anibal	Lovera Benítez	MASCULINO	\N	PARAGUAYO/A	071188277	CASADO/A	\N	\N	\N	SI	NO	325	Mar del Plata	\N	\N	\N	0	Anteriormente vivia en la Zona 6B.
2580010	María Cristina	Lovera Benítez	FEMENINO	\N	PARAGUAYO/A	0982199060	CASADO/A	\N	\N	\N	SI	NO	1438	Ursicino Velasco	\N	\N	\N	0	.
2580011	María Zunilda	Lovera Benítez	FEMENINO	2069-01-08	PARAGUAYO/A	0984203871	UNION DE HECHO	\N	\N	\N	SI	NO	334	Mar del Plata	\N	\N	\N	0	Con un corredorcito.
3318000	Fermina	Lovera Rios	FEMENINO	1970-11-10	PARAGUAYO/A	0972502028	UNION DE HECHO	\N	\N	\N	SI	NO	998	8 de Diciembre	\N	\N	\N	0	La Familia vive en la Astilleria ,propiedad del S. Raul Caballero.
4945223	Alexis Massi	Lovera	MASCULINO	1990-12-07	PARAGUAYO/A	0981952987	CASADO/A	\N	\N	\N	SI	NO	202	Mtro Urcicino Velazco esq. Nuevo Amanecer	\N	\N	\N	0	.
3784193	Andrés Rodrigo	Lovera	MASCULINO	1989-04-02	PARAGUAYO/A	0983926519	SOLTERO/A	\N	\N	\N	SI	NO	1724	Ursicino Velasco	\N	\N	\N	0	.
5546217	Carolina Beatriz	Lovera	FEMENINO	\N	PARAGUAYO/A	0971774304	.	\N	\N	\N	SI	NO	134	AROMITA	\N	\N	\N	0	.
4983953	Daisy Yoana	Lovera	FEMENINO	\N	PARAGUAYO/A	0972299931	CASADO/A	\N	\N	\N	SI	NO	1588	Ramon Talavera	\N	\N	\N	0	.
5108178	Liz Jazmín	Lovera	FEMENINO	\N	PARAGUAYO/A	0992493056	UNION DE HECHO	\N	\N	\N	SI	NO	162	Nuevo Amanecer	\N	\N	\N	0	.
5373329	María Laura	Lovera	FEMENINO	\N	PARAGUAYO/A	0991188951	UNION DE HECHO	\N	\N	\N	SI	NO	280	Ursicino Velasco	\N	\N	\N	0	.
392428	Salvador	Lovera	MASCULINO	2043-06-08	PARAGUAYO/A	0981248854	CASADO/A	\N	\N	\N	SI	NO	336	Mar del Plata	\N	\N	\N	0	Tiene casa de la Cooperativa de viviendas.
3204913	Gilberto Ramón	Lugo Bogado	MASCULINO	\N	PARAGUAYO/A	0992441533	UNION DE HECHO	\N	\N	\N	SI	NO	2030	Sagrado Corazón de Jesús	\N	\N	\N	0	Vive dentro del lote y la casa del abuelo, esta casado hace 7 anos comparte electrodomesticos con la duena del lote.
2191652	Victor Ramón	Lugo Recalde	MASCULINO	\N	PARAGUAYO/A	0981 225 257	UNION DE HECHO	\N	\N	\N	SI	NO	1858	Ursicino Velazco	\N	\N	\N	0	.
4536627	Arnildo	Maciel Benítez	MASCULINO	1986-11-09	PARAGUAYO/A	0984342919	UNION DE HECHO	\N	\N	\N	SI	NO	1321	Remancito	\N	\N	\N	0	La flia. Actualmente  vive con los padres , mientras se termina la construccion de su vivienda por la crecida no pudieron terminar la construccion.
6141332	Mabel	Maciel Brítez	FEMENINO	\N	PARAGUAYO/A	0992399431	UNION DE HECHO	\N	\N	\N	SI	NO	1087	23 de Junio	\N	\N	\N	0	.
5234740	Jessica	Maciel Centurión	FEMENINO	1997-07-01	PARAGUAYO/A	0992672318	UNION DE HECHO	\N	\N	\N	SI	NO	832	Vecinos Unidos	\N	\N	\N	0	La misma manifiesta haber comprado de la madre , tiene un solo Nro. Porque se observa devision fisica , ella esta aconcubinada hace 6 años y es totalmente independiente a ala madre . Tiene una vivienda precaria construida totalmente independiente . Tubo
4061262	Faustina	Martínez Duarte	FEMENINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	890	Martin Fierro	\N	\N	\N	0	Dos de los hijos mayores de la titular no cuentan con C.I. , aparte son adictos al Crack.
3028946	Nidia	Maciel Centurión	FEMENINO	\N	PARAGUAYO/A	0981841125	UNION DE HECHO	\N	\N	\N	SI	NO	2201	Mbo-ehara	\N	\N	\N	0	es dueña del lote, el lote tiene dos casas, en una de las casas vive su hijo con un su pareja
3028941	Sergia	Maciel Centurión	FEMENINO	\N	PARAGUAYO/A	0972582538	UNION DE HECHO	\N	\N	\N	SI	NO	831	Vecinos Unidos	\N	\N	\N	0	.
2890702	Marían	Maciel de Esquivel	FEMENINO	1975-11-11	PARAGUAYO/A	0992 381 217	CASADO/A	\N	\N	\N	SI	NO	2008	Pilcomayo c/ Yacare Yrupe	\N	\N	\N	0	Dentro del mismo lote bajo su techo vive su hija de 16 años con su hija de 8 meses.
2658526	Enriqueta	Maciel de Martínez	FEMENINO	\N	PARAGUAYO/A	0982595349	CASADO/A	\N	\N	\N	SI	NO	1317	Remancito	\N	\N	\N	0	La Sra. Enriqueta se encuentra actualmente viviendo fuera del pais , por motivados laborales su flia. Si habita la vivienda.
821176	Amalia	Maciel de Romero	FEMENINO	\N	PARAGUAYO/A	0972136476	CASADO/A	\N	\N	\N	SI	NO	609	Ursicino Velazco y San Felipe y Santiago	\N	\N	\N	0	Dentro de este lote aparte de la titular vive su hijo mayor con su concubina, tienen una relación de 6 años sin hijos
6042393	Carolina	Maciel González	FEMENINO	1978-03-12	PARAGUAYO/A	0981390058	UNION DE HECHO	\N	\N	\N	SI	NO	1700	Bañado Poty	\N	\N	\N	5	.
1506588	José de la Cruz	Maciel Ledesma	MASCULINO	2045-03-05	PARAGUAYO/A	424980	CASADO/A	\N	\N	\N	SI	NO	839	Ursicino Velasco c/ Martin Fierro	\N	\N	\N	0	el señor cobra por la tercera edad
1969168	Panfila	Maciel Ledezma	FEMENINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	2305	Ramón Talavera	\N	\N	\N	0	.
661160	Rosalina	Maciel Ledezma	FEMENINO	2054-04-09	PARAGUAYO/A	0981292306	UNION DE HECHO	\N	\N	\N	SI	NO	231	Angel Luis	\N	\N	\N	0	Ninfa Romero embarazada 6 meses.
2678330	Francisca	Maciel Ovelar	FEMENINO	2053-10-10	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1059	Mar de plata e/ Nuevo Amanecer	\N	\N	\N	0	Uno de loss miembros se encuentra privado de su libertad.
5276497	Nancy Claudia	Maciel	FEMENINO	\N	PARAGUAYO/A	0985379086	UNION DE HECHO	\N	\N	\N	SI	NO	445	San Cayetano	\N	\N	\N	0	.
1257581	Pilnio Cesar	Maciel	MASCULINO	\N	PARAGUAYO/A	0981881315	SOLTERO/A	\N	\N	\N	SI	NO	387	.	\N	\N	\N	0	Tiene un salon que utiliza como dormitorio en este momento en el mismo lote.
3228557	Nancy Carolina	Maidana Balbuena	FEMENINO	\N	PARAGUAYO/A	0994546182	UNION DE HECHO	\N	\N	\N	SI	NO	1533	Rancho 8	\N	\N	\N	0	Placa Electrica 1. La pareja lleva 12 anos de concubinato.
1961025	Aureliano	Maidana Benítez	MASCULINO	\N	PARAGUAYO/A	.	CASADO/A	\N	\N	\N	SI	NO	2217	.	\N	\N	\N	0	.
5573363	Adriana	Maidana Bernal	FEMENINO	\N	PARAGUAYO/A	0991823431	UNION DE HECHO	\N	\N	\N	SI	NO	876	Martin Fierro C/ Ursicino Velasco	\N	\N	\N	0	.
5329762	Mariela	Maidana Bernal	FEMENINO	\N	PARAGUAYO/A	0991217526	UNION DE HECHO	\N	\N	\N	SI	NO	891	Martin Fierro	\N	\N	\N	0	.
6009242	Sonia Beatriz	Maidana Bernal	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	878	.	\N	\N	\N	0	.
899423	Ana María	Maidana de Adorno	FEMENINO	2057-12-06	PARAGUAYO/A	.	CASADO/A	\N	\N	\N	SI	NO	1295	Ursicono Velasco c/ Remancito	\N	\N	\N	0	La señora es hija de la dueña del terreno y cuenta con una vivienda precaria de 5x4.
4029565	Richard Luis	Maidana Escobar	MASCULINO	1983-11-05	PARAGUAYO/A	0983303671	CASADO/A	\N	\N	\N	SI	NO	1304	Remancito c/ Mto. Ursicino Velasco	\N	\N	\N	0	.
1383650	Ana Dejesús	Maidana Vda. De Cabrera	FEMENINO	\N	PARAGUAYO/A	0983363842	VIUDO/A	\N	\N	\N	SI	NO	1570	Ursicino Velasco	\N	\N	\N	0	.
1223135	Antoliano	Maidana	MASCULINO	2043-06-02	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	1809	Bañado Koeti	\N	\N	\N	0	.
750	Bernardo	Maidana	MASCULINO	\N	No quiso censar	.	.	\N	\N	\N	SI	NO	1807	Bañado koeti	\N	\N	\N	5	No quiso censarse.
4851738	Carlos Alberto	Maidana	MASCULINO	1990-12-07	PARAGUAYO/A	0991981178	UNION DE HECHO	\N	\N	\N	SI	NO	1057	Ursicino Velasco	\N	\N	\N	0	Viven dentro del mismo lote de la mama en una pieza.
3983137	María Claudia	Maidana	FEMENINO	\N	PARAGUAYO/A	0971 186 816	UNION DE HECHO	\N	\N	\N	SI	NO	2507	Urcisino Velazco	\N	\N	\N	0	.
1056451	María Antonia	Maldonado de Villalba	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1405	Bañado Koeti	\N	\N	\N	5	.
3572620	Isidora Ramona	Maldonado Valiente	FEMENINO	\N	PARAGUAYO/A	0982698806	CASADO/A	\N	\N	\N	SI	NO	507	.	\N	\N	\N	0	.
1747702	Milciades	Mareco Roldán	MASCULINO	\N	PARAGUAYO/A	0982 279 669	UNION DE HECHO	\N	\N	\N	SI	NO	2295	Urcisino Velazco	\N	\N	\N	0	.
525	Arminda	Mareco	FEMENINO	\N	PARAGUAYO/A	.	VIUDO/A	\N	\N	\N	SI	NO	2746	Nuestra Sra. De la Asunción	\N	\N	\N	0	.
3631720	Elisa Valdovina	Marecos Martínez	FEMENINO	\N	PARAGUAYO/A	0983301541	UNION DE HECHO	\N	\N	\N	SI	NO	1997	Nasaindy	\N	\N	\N	0	Vive dentro del lote y la casa de la mama con su pareja e hijos.
3617139	Lidia Beatriz	Marecos Ramírez	FEMENINO	1977-11-02	PARAGUAYO/A	0985649973	UNION DE HECHO	\N	\N	\N	SI	NO	1686	.	\N	\N	\N	0	La Sra. Tiene la Casa de la Cooperativa.
2961115	Silvia Raquel	Marecos Ramírez	FEMENINO	1975-03-11	PARAGUAYO/A	0983375119	UNION DE HECHO	\N	\N	\N	SI	NO	1386	Bañado Koeti	\N	\N	\N	0	.
5435357	Juliána Carolina	Marin Ruíz Díaz	MASCULINO	1991-12-08	PARAGUAYO/A	0991266938	UNION DE HECHO	\N	\N	\N	SI	NO	1535	San Luis y Rancho 8	\N	\N	\N	0	La pareja lleva 6 anos de concubinato.La misma cuenta con una extencion de una pieza y un dormitorio proximo al de la madre ,quien es la titular del lote.Las dimensiones del dormitorio son de 4 x 6.
4666167	Liliana Joséfina	Marín Ruiz Díaz	FEMENINO	1986-01-05	PARAGUAYO/A	83661997	UNION DE HECHO	\N	\N	\N	SI	NO	1608	Jacare Pito	\N	\N	\N	0	.
458931	María Teresa	Martínez Alegre	FEMENINO	2054-08-03	PARAGUAYO/A	0986288905	SOLTERO/A	\N	\N	\N	SI	NO	1641	Jakare Pito	\N	\N	\N	0	.
3942526	Carlos Agustín	Martínez Alonzo	MASCULINO	\N	PARAGUAYO/A	0983802094	UNION DE HECHO	\N	\N	\N	SI	NO	970	Algarrobo	\N	\N	\N	0	Vivienda indepediente dentro del terreno de la mama
17	GuIllermo	Martínez Barrrios	MASCULINO	\N	PARAGUAYO/A	0972479216	UNION DE HECHO	\N	\N	\N	SI	NO	103	Oleros	\N	\N	\N	0	Hace 2 meces estan viviendo en la zona.Vivia en alquiler en TABLADA.
2900780	Francisca Ramona	Martínez Blanco	FEMENINO	\N	PARAGUAYO/A	0994823671	VIUDO/A	\N	\N	\N	SI	NO	2188	.	\N	\N	\N	0	.
4702439	Juan Carlos	Martínez Cardozo	MASCULINO	1985-08-02	PARAGUAYO/A	0992531864	UNION DE HECHO	\N	\N	\N	SI	NO	584	San Cayetano	\N	\N	\N	0	La construcción de material (LADRILLO), se derrumbó a acusa de la inundación, actualmente  tienen una vivienda precaria.
6915075	Miguel Angel	Martínez Cardozo	MASCULINO	\N	PARAGUAYO/A	0982226271	UNION DE HECHO	\N	\N	\N	SI	NO	581	San Cayetano	\N	\N	\N	0	La señora Eva Vera tieneun embarazo de 7 meses, y llevan concubinados 7 años.
4224029	Yohanna Herminia	Martínez Chávez	FEMENINO	\N	PARAGUAYO/A	0986551289	CASADO/A	\N	\N	\N	SI	NO	698	Yacare Yrupe	\N	\N	\N	0	.
2125695	María Ester	Martínez de Cubilla	FEMENINO	\N	PARAGUAYO/A	0986198464	.	\N	\N	\N	SI	NO	2199	Mbo-ehara	\N	\N	\N	0	Es la unica forma de tomar la foto porque no posee patio
638354	Elodia Ramona	Martínez de Marecos	FEMENINO	2052-06-10	PARAGUAYO/A	0982186707	.	\N	\N	\N	SI	NO	1996	Nasaindy	\N	\N	\N	3	Es propietaria del lote y la casa con ella vive sus hijos con su pareja e hija.
1440830	María Virginia	Martínez de Rojas	FEMENINO	\N	PARAGUAYO/A	0971765181	SEPARADO/A	\N	\N	\N	SI	NO	1112	Virgen de Lourdes c/ Angel Luis	\N	\N	\N	0	.
6981847	Gabriel Ivan	Martínez Duarte	MASCULINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1097	Martin Fierro	\N	\N	\N	0	Vive dentro del lote de la mama en una vivienda precaria con su pareja e hija.
6535156	Juan Ramón	Martínez Duarte	MASCULINO	\N	PARAGUAYO/A	0985 204 639	SOLTERO/A	\N	\N	\N	SI	NO	1917	Martin Fierro	\N	\N	\N	0	.
6659844	Monica Leticia	Martinez Duarte	FEMENINO	1997-04-05	PARAGUAYO/A	0991541769	UNION DE HECHO	\N	\N	\N	SI	NO	2698	Martin Fierro	\N	\N	\N	0	vive dentro del lote y la casa de la mamá, lleva 1 año y cuatro meses, comparte electrodomésticos
6290155	Alicia Ramona	Martínez Famoso	FEMENINO	1980-11-01	PARAGUAYO/A	0994127931	UNION DE HECHO	\N	\N	\N	SI	NO	2538	Mandy Jura	\N	\N	\N	0	Sandra esta acompanada hace 4 anos con Cristobal vive en una pieza dentro de la casa de la mamá.
6290169	Julia Concepción	Martínez Famoso	FEMENINO	1990-06-03	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	2464	Mondyjura	\N	\N	\N	0	vivie dentro del lote de la mamá, en una extención de madera.
2919370	Francisco Luciano	Martínez Franco	MASCULINO	\N	PARAGUAYO/A	0992479705	UNION DE HECHO	\N	\N	\N	SI	NO	2198	Mbo-ehara c/ Sotero Colman	\N	\N	\N	0	.
1017492	Jorgelina	Martínez Franco	MASCULINO	2062-10-02	PARAGUAYO/A	0971711263	UNION DE HECHO	\N	\N	\N	SI	NO	758	Martin Fierro c/Mto. Ursicino Velasco	\N	\N	\N	0	.
2919342	Juan de la Cruz	Martínez Franco	MASCULINO	1976-12-09	PARAGUAYO/A	0971926113	CASADO/A	\N	\N	\N	SI	NO	2051	Ursicino Velasco	\N	\N	\N	0	.
3264366	Néstor	Martínez Galeano	MASCULINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	2307	Nuestra Sra. De la Asunción	\N	\N	\N	0	La vivienda cuenta con un galpon en el frente.
1882666	Carlos José	Martínez González	MASCULINO	\N	PARAGUAYO/A	0971 896 991	UNION DE HECHO	\N	\N	\N	SI	NO	2677	.	\N	\N	\N	0	.
3031782	Máxima	Martínez González	FEMENINO	\N	PARAGUAYO/A	.	CASADO/A	\N	\N	\N	SI	NO	297	Yvoty c/ Las Orquideas	\N	\N	\N	0	.
1001766	Vidal	Martínez Guimenez	MASCULINO	2046-04-11	PARAGUAYO/A	0984543934	VIUDO/A	\N	\N	\N	SI	NO	1004	Mto. Ursicino Velasco	\N	\N	\N	0	Cuenta con dos salones para Despensa y Merceria.
4959133	Luz Jorgelina	Martínez Jiménez	FEMENINO	\N	PARAGUAYO/A	0985990343	UNION DE HECHO	\N	\N	\N	SI	NO	751	26 de Junio	\N	\N	\N	0	.
3616867	Felicia	Martínez López de López	FEMENINO	1978-07-07	PARAGUAYO/A	0981789871	CASADO/A	\N	\N	\N	SI	NO	2036	Ursicino Velasco	\N	\N	\N	0	Posee un salon de 6x4 de dimension donde realiza los tarbajos de costuras.
2167465	Blanca Ceferina	Martínez López	FEMENINO	\N	PARAGUAYO/A	0981721546	UNION DE HECHO	\N	\N	\N	SI	NO	1497	Rancho 8	\N	\N	\N	0	La vivienda pertenecia a la mama Carlos Maria Ferreira.
3652638	Georgina	Martínez López	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	2016	Nasaindy	\N	\N	\N	0	Vive dentro del lote y la casa de la mama con la pareja e hijos.
4363953	Laura Eugenia	Martínez López	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	2017	Nasaindy	\N	\N	\N	0	Vive dentro del lote y la casa de la mama con la pareja e hijos.
1554046	María Magdalena	Martínez López	FEMENINO	\N	PARAGUAYO/A	0976157322	SOLTERO/A	\N	\N	\N	SI	NO	1576	Ramon Talavera	\N	\N	\N	0	.
3218202	Silvina	Martínez López	FEMENINO	1980-06-02	PARAGUAYO/A	0982532050	UNION DE HECHO	\N	\N	\N	SI	NO	2015	Nasaindy	\N	\N	\N	0	Vive dentro del lote y la casa de la mama con su pareja e hijo.
723591	Alejandro	Martínez Martínez	MASCULINO	2057-09-09	PARAGUAYO/A	0991300097	SOLTERO/A	\N	\N	\N	SI	NO	863	Ursicino Velasco	\N	\N	\N	0	El Sr. Alejandro vive bajo el mismo techo de ex pareja . El mismo se caracteriza por ser violento y amenaza con sacarle el techo a su ex pareja y su hijo menor de edad .
3533372	Raquel Soledad	Martínez Martínez	FEMENINO	\N	PARAGUAYO/A	0983203562	CASADO/A	\N	\N	\N	SI	NO	618	San Felipe y Santiago	\N	\N	\N	0	.
4934255	Candida Concepcion	Martínez Medina	FEMENINO	\N	PARAGUAYO/A	0981883877	UNION DE HECHO	\N	\N	\N	SI	NO	924	Pasillo Angel Luis	\N	\N	\N	0	.
4360980	Camila Aldana	Martinez Mendez	FEMENINO	1994-01-07	PARAGUAYO/A	0991446859	UNION DE HECHO	\N	\N	\N	SI	NO	2713	Ñasayndi	\N	\N	\N	0	.
2196248	Verónica Rossana	Martínez Noceda	FEMENINO	\N	PARAGUAYO/A	0992219103	UNION DE HECHO	\N	\N	\N	SI	NO	1626	Pajagua Naranja	\N	\N	\N	3	.
5549609	José Carlos	Martínez Noguera	MASCULINO	1995-06-12	PARAGUAYO/A	0983181817	UNION DE HECHO	\N	\N	\N	SI	NO	1537	.	\N	\N	\N	0	Conviven hace dos anos.
2396383	Félix	Martínez Núñez	MASCULINO	2066-12-01	PARAGUAYO/A	0983779878	UNION DE HECHO	\N	\N	\N	SI	NO	305	Virgen de Lourdes esq. Yvoty A1	\N	\N	\N	3	.
2022017	Ilda Concepción	Martínez Núñez	FEMENINO	\N	PARAGUAYO/A	0982432131	SOLTERO/A	\N	\N	\N	SI	NO	2121	Ursicino Velasco c/ San Martin de Porres	\N	\N	\N	0	.
3468931	Marta Beatriz	Martínez Núñez	FEMENINO	\N	PARAGUAYO/A	0991941204	UNION DE HECHO	\N	\N	\N	SI	NO	458	Ursicino Velasco	\N	\N	\N	0	.
1728234	Mirtha Graciela	Martínez Núñez	FEMENINO	2068-12-06	PARAGUAYO/A	0981761733	UNION DE HECHO	\N	\N	\N	SI	NO	1631	Pajagua Naranja	\N	\N	\N	0	El Sr. Viviano compro el lote con casa cuando era soltero y fue ampliando con el tiempo.
3598720	Sixto Gregorio	Martínez Núñez	MASCULINO	\N	PARAGUAYO/A	0971784684	CASADO/A	\N	\N	\N	SI	NO	243	Virgen de Lourdes C/Angel Luis	\N	\N	\N	0	.
5659097	Clara Liz	Martínez Palma	FEMENINO	1992-08-07	PARAGUAYO/A	0982463205	UNION DE HECHO	\N	\N	\N	SI	NO	258	Virgen de Lourdes	\N	\N	\N	0	.
4822149	Olivia Celeste	Martínez Palma	FEMENINO	2017-06-04	PARAGUAYO/A	0991599051	CASADO/A	\N	\N	\N	SI	NO	652	Ursicino Velasco y Virgen d Lourdes	\N	\N	\N	0	Aprox. La muralla del terreno tiene 2 mts. De altura y con una dimension de 50x50 mts.2. y el terreno esta vacio.
4013012	Vicente Ismael	Martínez Palma	.	1982-06-04	PARAGUAYO/A	0971548895	CASADO/A	\N	\N	\N	SI	NO	772	26 de Julio	\N	\N	\N	0	La casa es de dos plantas con una contrucción total de 131Mº
7712196	Rocío Jazmín	Martínez Pérez	FEMENINO	1999-04-01	PARAGUAYO/A	0992938552	.	\N	\N	\N	SI	NO	380	YVOTY C/ Las Orquideas	\N	\N	\N	0	La pareja hace 4 anos se acompano tienen su lote en la zona 4 manzna 43 no pudieron construir mas en sus terrenos porque el agua sigue en el lugar por esa razón siguen viviendo en el lote de su hermano quien es el titular.
6256390	Camila Soledad	Martínez Ramírez	FEMENINO	\N	PARAGUAYO/A	0992738721	UNION DE HECHO	\N	\N	\N	SI	NO	2001	Pajagua Naranja	\N	\N	\N	0	.
5373373	Félix Jonathan	Martínez Sosa	MASCULINO	\N	PARAGUAYO/A	0985419311	UNION DE HECHO	\N	\N	\N	SI	NO	99	Virgen de Lourdes	\N	\N	\N	0	.
2022782	Eliodora	Martínez Vera	FEMENINO	2044-03-07	PARAGUAYO/A	0984632010	VIUDO/A	\N	\N	\N	SI	NO	1897	Julio Benitez	\N	\N	\N	0	Cobra Tercera Edad.
2918429	Ignacio	Martínez Vera	MASCULINO	1976-01-02	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	1711	Yacare Pito	\N	\N	\N	3	Celular del hijo: (0972) 791802
2308613	Rossmary	Martínez Vera	FEMENINO	\N	PARAGUAYO/A	0984404613	UNION DE HECHO	\N	\N	\N	SI	NO	2169	.	\N	\N	\N	0	.
6007017	Jhony Daniel	Martínez Viveros	MASCULINO	\N	PARAGUAYO/A	0984310291	UNION DE HECHO	\N	\N	\N	SI	NO	254	Angel Luis	\N	\N	\N	0	Patio Baldio.
4815392	Shirley Rocío	Martínez Viveros	FEMENINO	\N	PARAGUAYO/A	0984363685	CASADO/A	\N	\N	\N	SI	NO	714	Algarrobo c/ Ursicino Velasco	\N	\N	\N	0	La señora Shirley Martinez vive dentro del lote de la señora Rosa Viveros, hace 6 años en una construcción aparte. Están casados hace 1 año, pero llevan como pareja hace 6 años.
2663065	Bernardino	Martínez	MASCULINO	1976-07-01	PARAGUAYO/A	0981340783	UNION DE HECHO	\N	\N	\N	SI	NO	1389	Bañado Koeti	\N	\N	\N	0	La vivienda cuenta con un corredor en el frente.
4542414	Carlos Andrés	Martínez	MASCULINO	1993-11-04	PARAGUAYO/A	0982265241	UNION DE HECHO	\N	\N	\N	SI	NO	1983	Nasaindy	\N	\N	\N	5	Poseen documentacion del terreno a nombre de Eduardo Villamayor suegro del jefa de hogar.
2949957	Cristian Ruben	Martínez	MASCULINO	1979-03-09	PARAGUAYO/A	.	SEPARADO/A	\N	\N	\N	SI	NO	894	Martin Fierro	\N	\N	\N	0	El Sr. Cristian se encuentra recluidop en Tacumbu. En la casa no habita nadie.
1315	David Isaías	Martínez	MASCULINO	\N	No quiso censar	.	.	\N	\N	\N	SI	NO	1523	Rancho 8	\N	\N	\N	0	No quiso censarse.
379923	Fidelina	Martínez	FEMENINO	\N	PARAGUAYO/A	0971 987 920	.	\N	\N	\N	SI	NO	2504	Urcisino Velazco	\N	\N	\N	0	.
1230734	Francisca	Martínez	FEMENINO	\N	PARAGUAYO/A	0981683833	CASADO/A	\N	\N	\N	SI	NO	2059	Ursicino Velasco c/ Sotero Colman	\N	\N	\N	0	.
2152774	Gregoria	Martínez	FEMENINO	1970-03-09	PARAGUAYO/A	0982112755	SOLTERO/A	\N	\N	\N	SI	NO	2127	San Martin de Porres	\N	\N	\N	0	.
2618322	Ignacia	Martínez	FEMENINO	2062-01-02	PARAGUAYO/A	.	CASADO/A	\N	\N	\N	SI	NO	33	San Felipe y Santiago	\N	\N	\N	3	.
4985650	José Ronaldo	Martínez	MASCULINO	\N	PARAGUAYO/A	0983975408	UNION DE HECHO	\N	\N	\N	SI	NO	288	Virgen de Lourdes	\N	\N	\N	0	.
241930	José Victoriano	Martínez	MASCULINO	2037-06-03	PARAGUAYO/A	0991797796	CASADO/A	\N	\N	\N	SI	NO	2197	Mbo-ehara	\N	\N	\N	0	Es señor es jubilado yla sra cobra el sueldo de la tercera edad
1214459	Juan Pablo	Martínez	MASCULINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	1254	.	\N	\N	\N	0	.
2672088	Justino	Martínez	MASCULINO	\N	PARAGUAYO/A	0961442800	UNION DE HECHO	\N	\N	\N	SI	NO	892	Martin Fierro	\N	\N	\N	0	El Sr. Posee dos viviendas en un lote un anatigua donde vive con su esposa e hijas menores y otra de la Cooperativa donde viven sus hijos varones . Tiene do terrenos mas en el barrio uno 11x30, y otro 8x9.
3280132	María Enriqueta	Martínez	FEMENINO	\N	.	.	.	\N	\N	\N	SI	NO	2368	Nuestra Señora de la Asuncion	\N	\N	\N	6	.
5031801	María Patricia Fabián	Martínez	FEMENINO	\N	PARAGUAYO/A	0972707661	UNION DE HECHO	\N	\N	\N	SI	NO	208	Aromita	\N	\N	\N	0	.
1140233	Máxima Elizabeth	Martínez	FEMENINO	\N	PARAGUAYO/A	0985965220	CASADO/A	\N	\N	\N	SI	NO	1255	.	\N	\N	\N	0	.
3807379	Mercedes	Martínez	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1286	.	\N	\N	\N	0	La Sra. Nancy tiene una casa de madera en el lote de su mama donde vive con su hija Agustina , la casa de madera es muy precaria.
6564058	Odina	Martínez	FEMENINO	2017-01-11	PARAGUAYO/A	0971 550 927	SOLTERO/A	\N	\N	\N	SI	NO	1998	Yacare Yrupe c/ Pilcomayo	\N	\N	\N	5	La titular del techo D, LOTE 14, Mz 26, padece de problemas mentales pero esta medicada, tambien el hijo mayor.
2722	Victorina	Martínez	MASCULINO	\N	.	.	.	\N	\N	\N	SI	NO	763	Martin Fierro	\N	\N	\N	0	La Sra Victorina se encuentra privada de su libertad , la casa temporalmente esta vacia y no se pudo obtener datos de la misma.
5592109	Victorino	Martínez	MASCULINO	\N	PARAGUAYO/A	0984354902	SOLTERO/A	\N	\N	\N	SI	NO	665	Yvoty	\N	\N	\N	0	.
9890	Walberto Ramón	Martínez	MASCULINO	1977-12-07	PARAGUAYO/A	.	SEPARADO/A	\N	\N	\N	SI	NO	296	YVOTY C/Las Orquideas	\N	\N	\N	0	.
2013	Maríano	Massiel	FEMENINO	\N	No quiso censar	.	.	\N	\N	\N	SI	NO	1279	Sin Nombre	\N	\N	\N	0	No quiso censarse.
1597004	Sergio	Maylin Sánchez	MASCULINO	2066-07-11	PARAGUAYO/A	0982180318	SEPARADO/A	\N	\N	\N	SI	NO	603	16 de Agosto	\N	\N	\N	0	.
4842648	Isabel	Medina Acosta	FEMENINO	\N	PARAGUAYO/A	0972692115	UNION DE HECHO	\N	\N	\N	SI	NO	1914	Julio Benitez	\N	\N	\N	0	.
2678028	Rosalia	Medina Almeida	FEMENINO	2058-04-09	PARAGUAYO/A	0991681585	SOLTERO/A	\N	\N	\N	SI	NO	926	Pasillo Angel Luis	\N	\N	\N	0	.
6309690	Yessica Rosalba	Medina Almeida	FEMENINO	1991-04-09	PARAGUAYO/A	0991452342	SOLTERO/A	\N	\N	\N	SI	NO	928	Pasillo Angel Luis	\N	\N	\N	0	.
1108635	Estela Bernarda	Medina Barro	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1200	Fidelina	\N	\N	\N	0	La extencion de madera era de material antes de la inundacion.
7154051	Ana Francisca	Medina Castillo	FEMENINO	\N	PARAGUAYO/A	0986526682	UNION DE HECHO	\N	\N	\N	SI	NO	1970	Ñasayndy	\N	\N	\N	0	.
2569703	Elena	Medina de Bareiro	FEMENINO	\N	PARAGUAYO/A	0982278257	CASADO/A	\N	\N	\N	SI	NO	364	Oleros	\N	\N	\N	0	Tiene viviendas y utiliza para habitar, uno utiliza para las vacas y chanchos, la olería tiene 50x20 mts y el Horno 6x4 mt, los trabajadores viven de forma permanente.
1873973	Bernarda	Medina de Bazán	FEMENINO	\N	PARAGUAYO/A	0982 293 676	CASADO/A	\N	\N	\N	SI	NO	2579	8 de Diciembre	\N	\N	\N	0	.
4725499	Josi Paola Medina	Medina Echeverria	FEMENINO	1994-12-05	PARAGUAYO/A	0991414791	VIUDO/A	\N	\N	\N	SI	NO	456	Ursicino Velasco	\N	\N	\N	0	Vive dentro del lote de la Abuela en una estancia de madera con su pareja e hijos.
6066403	Cinthia Yamila	Medina Medina	FEMENINO	\N	PARAGUAYO/A	0984211060	UNION DE HECHO	\N	\N	\N	SI	NO	359	San Felipe y Santiago	\N	\N	\N	0	.
4993304	Leticia Noemí	Medina Silvano	FEMENINO	1994-11-09	PARAGUAYO/A	0994485006	UNION DE HECHO	\N	\N	\N	SI	NO	783	26 de julio	\N	\N	\N	3	Vive con la mamá tiene una externa de 4x5 y se maneja sola su mamá es funcionario Público.
6716363	Constancia Carolina	Medina	FEMENINO	\N	PARAGUAYO/A	0984530658	CASADO/A	\N	\N	\N	SI	NO	1804	Bañado Koeti	\N	\N	\N	5	.
785792	Eulalia	Medina	FEMENINO	\N	PARAGUAYO/A	0972733267	.	\N	\N	\N	SI	NO	59	.	\N	\N	\N	0	.
4300032	Salvador Mario Alfonso	Medina	MASCULINO	\N	PARAGUAYO/A	098296681	SEPARADO/A	\N	\N	\N	SI	NO	773	Jakare Yrupe	\N	\N	\N	0	.
504065	Juliána	Melgarejo Benítez	MASCULINO	2051-01-04	PARAGUAYO/A	0992637688	SEPARADO/A	\N	\N	\N	SI	NO	2052	Ursicino Velasco	\N	\N	\N	0	Es propietario de la casa y el lote su hijo Alberto vive con ella con su flia.
549	Miguel	Melgarejo Chávez	MASCULINO	\N	.	.	.	\N	\N	\N	SI	NO	722	.	\N	\N	\N	0	El titular no viev en la casa , esta totalmente vacia. El Sr. No quizo censarse pero igual se recaudo algunos datos.
3639807	Edgar Miguel	Melgarejo Ovelar	MASCULINO	\N	PARAGUAYO/A	0983114115	UNION DE HECHO	\N	\N	\N	SI	NO	2730	8 de Diciembre	\N	\N	\N	0	.
1476042	Germania	Melgarejo	FEMENINO	2066-11-05	PARAGUAYO/A	0986793097	UNION DE HECHO	\N	\N	\N	SI	NO	1473	payagua naranja	\N	\N	\N	0	.
1590243	Irene	Melgarejo	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1242	Pajagua Naranja	\N	\N	\N	0	.
4703631	Liz Karina	Melgarejo	FEMENINO	1985-08-01	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1233	Pajagua Naranja	\N	\N	\N	0	.
4011665	Cynthia Carolina	Melida Duarte	FEMENINO	\N	PARAGUAYO/A	0991309809	UNION DE HECHO	\N	\N	\N	SI	NO	1853	Villa Maria Auxiliadora	\N	\N	\N	0	.
1545374	María Doris	Mencia de Valenzuela	FEMENINO	2064-05-08	PARAGUAYO/A	0981555359	.	\N	\N	\N	SI	NO	1935	Ursicino Velasco c/28 y Juan Leon Mallorquin	\N	\N	\N	0	La Sra. Posee una casa de la Cooperativa.
1453101	Lucia Nilda	Méndez Araujo	FEMENINO	\N	PARAGUAYO/A	0982233361	SEPARADO/A	\N	\N	\N	SI	NO	2192	Mbo-ehara	\N	\N	\N	0	.
4226469	Luz Diana	Méndez Araujo	FEMENINO	1986-04-04	PARAGUAYO/A	0972517235	SOLTERO/A	\N	\N	\N	SI	NO	1988	Nasaindy	\N	\N	\N	5	Vive dentro del lote de la mama con su pareja e hijos.
1198923	Juana Beatriz	Méndez de Bareiro	MASCULINO	\N	PARAGUAYO/A	0961622751	CASADO/A	\N	\N	\N	SI	NO	301	Yvoty c/ Las Orquideas	\N	\N	\N	0	.
2279178	Norma Estela	Méndez de Ferreira	FEMENINO	\N	PARAGUAYO/A	0971998517	CASADO/A	\N	\N	\N	SI	NO	687	Virgen de Lujan casi Aromita	\N	\N	\N	0	.
2491649	Lourdes Concepcion	Méndez González	FEMENINO	\N	PARAGUAYO/A	0992465132	UNION DE HECHO	\N	\N	\N	SI	NO	1031	.	\N	\N	\N	0	.
3215887	Wilfrido Javier	Méndez González	MASCULINO	\N	PARAGUAYO/A	.	CASADO/A	\N	\N	\N	SI	NO	1406	Pajagua Naranja	\N	\N	\N	0	.
4360979	Diego Alexander	Méndez Gucciones	MASCULINO	\N	PARAGUAYO/A	0981878734	UNION DE HECHO	\N	\N	\N	SI	NO	1989	Nasaindy	\N	\N	\N	5	Vive dentro del lote de la mama en una vivienda de material de 4x4, llevan 6 anos de concubinatos.
1191716	Ramón	Méndez Quiroga	MASCULINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	1013	Mto Ursicino Velasco	\N	\N	\N	0	Tiene una despensa que es unos de los antiguos de Barrio tiene en la parte superior una apieza que mide 4x4 y tambien en la parte superior tiene un deposito donde guarda susmercaderias.Tiene una Vitrina.
4488177	Marina Concepción	Méndez Ramires	FEMENINO	1988-03-05	PARAGUAYO/A	0984758684	UNION DE HECHO	\N	\N	\N	SI	NO	2143	Virgen de Guadalupe	\N	\N	\N	0	Vive dentro del lote de la mama en una vivienda precaria.
4553257	María Elena	Méndez Ramírez	FEMENINO	\N	PARAGUAYO/A	0992468923	.	\N	\N	\N	SI	NO	2144	Agosto poty	\N	\N	\N	0	.
484468	Irma	Méndez Vda de Avalos	FEMENINO	\N	PARAGUAYO/A	0982306212	VIUDO/A	\N	\N	\N	SI	NO	1018	Mto Ursicino Velasco	\N	\N	\N	0	.
639071	Angel Dionisio	Méndez	MASCULINO	2052-08-10	PARAGUAYO/A	0982315777	CASADO/A	\N	\N	\N	SI	NO	1423	Ursicino Velasco	\N	\N	\N	0	Angel Dionisio cobra Tercera Edad.
863079	Marta Beatriz	Méndez	FEMENINO	2068-03-11	.	.	.	\N	\N	\N	SI	NO	1014	Mto Ursicino Velasco	\N	\N	\N	0	Casa de Hormigon de dos plantas.
2333050	Miriam Rosa	Méndez	FEMENINO	1973-06-09	PARAGUAYO/A	0991414734	UNION DE HECHO	\N	\N	\N	SI	NO	2208	Ñasaindy	\N	\N	\N	0	tiene una hija que vive en el mismo lote con su pareja e hijhos, la sra es duña de lote
964974	Restituto	Mendieta	MASCULINO	2041-09-12	.	.	.	\N	\N	\N	SI	NO	2102	.	\N	\N	\N	0	.
6559312	Javier	Mendiola	MASCULINO	\N	PARAGUAYO/A	0994275247	SEPARADO/A	\N	\N	\N	SI	NO	968	Angel Luis c/ Virgen de Lourdes	\N	\N	\N	0	.
1214407	Alcira Rosalia	Mendoza Hermosilla	FEMENINO	1971-05-08	PARAGUAYO/A	0981426389	CASADO/A	\N	\N	\N	SI	NO	2034	Sagrado Corazón de Jesús	\N	\N	\N	0	.
918	Amalia	Mendoza	FEMENINO	\N	No quiso censar	.	.	\N	\N	\N	SI	NO	1644	.	\N	\N	\N	0	No quiso censarse.
3528937	Rubén Darío	Mendoza	MASCULINO	1980-10-10	PARAGUAYO/A	0981276787	SOLTERO/A	\N	\N	\N	SI	NO	874	Nuevo Amanecer	\N	\N	\N	0	Casa de Dos pisos
2207285	Zulma	Mendoza	FEMENINO	\N	PARAGUAYO/A	0981963805	UNION DE HECHO	\N	\N	\N	SI	NO	1552	Pajagua Naranja	\N	\N	\N	0	.
5852795	Griselda Beatriz	Mercado López	FEMENINO	\N	PARAGUAYO/A	0984246985	UNION DE HECHO	\N	\N	\N	SI	NO	213	Virgen de Lourdes	\N	\N	\N	0	.
4976469	Valeria Beatriz	Mereles Riquelme	FEMENINO	\N	PARAGUAYO/A	0972537288	UNION DE HECHO	\N	\N	\N	SI	NO	1228	Fidelina	\N	\N	\N	0	Declara que compra la vivienda ,vive en alquiler en Barrio San Pablo, la casa en reparacion, ocupa despues de la Semana Santa.
2574260	Liz Catalina	Mereles Saiz	FEMENINO	\N	PARAGUAYO/A	0986253296	CASADO/A	\N	\N	\N	SI	NO	361	Oleros	\N	\N	\N	3	.
836836	Mirtha Magdalena	Mereles Servían	FEMENINO	\N	PARAGUAYO/A	0981481432	UNION DE HECHO	\N	\N	\N	SI	NO	917	Maestro Ursicino Velasco	\N	\N	\N	0	.
3904950	Bienvenida	Mereles Vera	FEMENINO	2065-10-03	PARAGUAYO/A	0992643693	UNION DE HECHO	\N	\N	\N	SI	NO	2644	Pajagua Naraja	\N	\N	\N	0	.
4639786	Cinthia Mercedes	Mereles Vera	FEMENINO	\N	PARAGUAYO/A	0981307874	SEPARADO/A	\N	\N	\N	SI	NO	2440	Ursicino Velasco y Maria Auxiladora	\N	\N	\N	0	La sra. Se encuentra fuera del pais por motivos laborales, la construcción es dos pisos y en la construcción del vive otro familia, tiene la custodia judicial de la hna, los datos fueron proporcionados por la tia en comunicación telefonica.
3524471	Liz Tatania Elizabeth	Mereles	FEMENINO	1981-12-05	PARAGUAYO/A	0972741513	UNION DE HECHO	\N	\N	\N	SI	NO	707	Algarobo	\N	\N	\N	0	.
2941633	Martina	Mereles	FEMENINO	1971-11-11	PARAGUAYO/A	0981307874	UNION DE HECHO	\N	\N	\N	SI	NO	2444	Ursicino Velasco y Maria Auxiladora	\N	\N	\N	0	La sra. Es la hna.de la duena y cuidadora de la casa.
1840	José Dolores	Mesa Hermosilla	MASCULINO	\N	PARAGUAYO/A	0985275062	SOLTERO/A	\N	\N	\N	SI	NO	2393	Mita Saraki	\N	\N	\N	0	Cocina Electrica 1.
1010649	María Benancia	Mesa Hermosilla	FEMENINO	2062-12-04	PARAGUAYO/A	0985 853 475	UNION DE HECHO	\N	\N	\N	SI	NO	1847	Ursicino Velazco	\N	\N	\N	0	Es propietaria del lote y de la casa independiente en la cual viven su hija y su pareja.
1139	Jacinta	Meza Silvero	FEMENINO	\N	PARAGUAYO/A	0981964048	UNION DE HECHO	\N	\N	\N	SI	NO	1603	Nuestra Senora de la Asunción	\N	\N	\N	6	Actualmente esta familia vive en la S.E.N., no se mudan por temor a la subida del agua hace 3 meses vivern en el Refugio.
3753909	María Ángelica	Meza Valdez	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	2539	Mandy Jura	\N	\N	\N	0	Placa Electrica 1.
5373369	Juana Soledad	Millan	MASCULINO	\N	PARAGUAYO/A	0971217084	UNION DE HECHO	\N	\N	\N	SI	NO	469	Martin Fierro	\N	\N	\N	0	Viven dentro del lote de Teodora en una vivienda precaria.
10867	Ministerio Evangélico la Buena Semilla	.	.	\N	.	0984 846 813	.	\N	\N	\N	SI	NO	2315	Pasillo sin nombre	\N	\N	\N	0	La Iglesia está en el barrio hace 10 años. En el salón se llevan a cabo los cultos, reuniones de oración, estudios bíblicos para adultos y niños.
3543499	Herminia Beatriz	Miñarro Cáceres	FEMENINO	1988-01-10	PARAGUAYO/A	0986476097	UNION DE HECHO	\N	\N	\N	SI	NO	2361	Nuestra Señora de la Asuncion	\N	\N	\N	6	.
2480888	Miguel Angel	Miñarro	MASCULINO	\N	PARAGUAYO/A	0985455278	UNION DE HECHO	\N	\N	\N	SI	NO	693	Nuevo Amanecer	\N	\N	\N	4	El propietario del lote, al fondo del terreno vive un hijo mayor de edad en una vivienda precaria.
3382120	María Mirta	Miño	FEMENINO	\N	PARAGUAYO/A	0986839120	SOLTERO/A	\N	\N	\N	SI	NO	1142	Aromita	\N	\N	\N	0	.
5546215	Johana Beatriz	Miranda Lovera	FEMENINO	\N	PARAGUAYO/A	0972818632	CASADO/A	\N	\N	\N	SI	NO	658	Nta. Senora de Lourdes	\N	\N	\N	0	La vivienda era de su mama.
6040858	Fatima Monserrat	Mochet Fernández	FEMENINO	\N	PARAGUAYO/A	0985360378	UNION DE HECHO	\N	\N	\N	SI	NO	2623	Ramon Talavera	\N	\N	\N	0	.
3852187	María Beatriz	Molas de Ortiz	FEMENINO	\N	PARAGUAYO/A	0972742575	CASADO/A	\N	\N	\N	SI	NO	442	San Cayetano	\N	\N	\N	0	.
7379354	Vicenta Dominga	Molas	FEMENINO	1997-03-03	PARAGUAYO/A	0972463020	UNION DE HECHO	\N	\N	\N	SI	NO	443	San Cayetano	\N	\N	\N	0	.
3201416	Mabel	Molinas Duarte	FEMENINO	\N	PARAGUAYO/A	0981265293	SOLTERO/A	\N	\N	\N	SI	NO	2488	Sotero Colman	\N	\N	\N	0	En este momento se encuentra en la casa de la suegra, porque en su techo en este momento hay agua. La casa es precaria
883931	Nicasia	Mongay	FEMENINO	\N	PARAGUAYO/A	0991676183	UNION DE HECHO	\N	\N	\N	SI	NO	1509	Payagua Naranja	\N	\N	\N	0	La casa es de la señora Patrocinia Escobar y ella vive aquí hace 1 año, La dueña vive en el interior. La señora vive alquilando desde hace 50 años, la inquilina desconoce las dimensiones y año de asentamiento
849490	Margarita	Mongelos Davalos	FEMENINO	\N	PARAGUAYO/A	0972283090	SOLTERO/A	\N	\N	\N	SI	NO	1841	Santa Librada	\N	\N	\N	0	La casa es de dos pisos y ella esta totalmente independiente de su hija que se maneja abajo.
1226989	Rosa Elizabeth	Mongelos de Gadda	FEMENINO	\N	PARAGUAYO/A	0984155846	VIUDO/A	\N	\N	\N	SI	NO	2268	Juan León Mallorquin	\N	\N	\N	0	.
757780	Isidora	Monges Cardozo	FEMENINO	\N	.	0991633543	SEPARADO/A	\N	\N	\N	SI	NO	2045	Estero Colmán y Mbo'ehára	\N	\N	\N	0	.
5462108	Pablo César	Mónges Zaracho	MASCULINO	\N	PARAGUAYO/A	0986285190	UNION DE HECHO	\N	\N	\N	SI	NO	2386	Ursicino Velasco	\N	\N	\N	0	Vive dentro del lote del papa con su pareja e hijos.cuenta con vivienda precaria de 4x4.
5262215	Constancio Concepción	Monges	MASCULINO	1996-07-12	PARAGUAYO/A	0994277866	UNION DE HECHO	\N	\N	\N	SI	NO	2655	Nuestra Sra. De la Asunción	\N	\N	\N	6	Cocina Electrica 1.
1418953	Enriqueta	Monges	FEMENINO	2067-11-07	PARAGUAYO/A	0991411410	CASADO/A	\N	\N	\N	SI	NO	2020	Mbo'ehara	\N	\N	\N	5	.
838323	Juana Pabla	Monges	MASCULINO	\N	PARAGUAYO/A	0984698755	SEPARADO/A	\N	\N	\N	SI	NO	2007	Nasaindy	\N	\N	\N	5	Es propietara del lote y una de las casas ,c/u de sus hijos tiene una casa ,Vidal es uno de sus hijos cuneta con una vibienda de material 4x6, es una vivienda totalmente pendiente.
2051512	María Justa	Monges	FEMENINO	2064-06-02	PARAGUAYO/A	0971258465	UNION DE HECHO	\N	\N	\N	SI	NO	2653	Nuestra Sra. De la Asunción	\N	\N	\N	6	Hace 8 anso que la titular vive en el barrio.
2949949	Milciades	Mónges	MASCULINO	2063-01-06	PARAGUAYO/A	0994220687	UNION DE HECHO	\N	\N	\N	SI	NO	2381	Ursicino Velasco	\N	\N	\N	0	Carlos y Marlene cuenta on vivienda precaria de 5x4 de dimencion,Pablo Florentin cuenta con una extencion de material de 5x4.debido a su condicion la extencion esta adecuada y construida con los cuidados necesarios para un nino con Leucemia.
1976898	Ramona	Mónges	MASCULINO	\N	PARAGUAYO/A	0992-465252	UNION DE HECHO	\N	\N	\N	SI	NO	1872	Urcisino Velasco	\N	\N	\N	0	Una de las Viviendas tiene techo de teja y el otro de zing.
3879608	Sara Peralta	Mónges	FEMENINO	\N	PARAGUAYO/A	0982576118	UNION DE HECHO	\N	\N	\N	SI	NO	402	Algarrobo	\N	\N	\N	0	.
5277048	Yessica Adraina	Montania Cáceres	FEMENINO	1993-03-09	PARAGUAYO/A	0994635421	UNION DE HECHO	\N	\N	\N	SI	NO	1388	Payagua Naranja	\N	\N	\N	0	La Sra. Yessica tiene un lote en l Zona 3 detrás de Recicla y esta cargando para mudarse y temporalmente vive en la casa de su mama.
3657411	Pamela Celeste	Montiel Ureña	FEMENINO	\N	PARAGUAYO/A	0982500408	SOLTERO/A	\N	\N	\N	SI	NO	1800	Bañado koeti	\N	\N	\N	5	Aire Portatil 1.
5494111	Steve Diosnel	Montiel Ureña	MASCULINO	\N	PARAGUAYO/A	0986211642	UNION DE HECHO	\N	\N	\N	SI	NO	1415	Bañado Kueti	\N	\N	\N	0	.
1182756	Beato	Montiel	MASCULINO	\N	PARAGUAYO/A	0984957944	SOLTERO/A	\N	\N	\N	SI	NO	2301	Nuestra Sra. De la Asunción	\N	\N	\N	0	.
5151902	Gisela María	Morai Lezcano	FEMENINO	\N	PARAGUAYO/A	0972688915	UNION DE HECHO	\N	\N	\N	SI	NO	647	23 de Junio	\N	\N	\N	0	La pareja lleva 8 años de concubinato, tienen lote y casa propia e independiente.
1810152	Cecilia	Morai López	FEMENINO	\N	PARAGUAYO/A	0981780684	UNION DE HECHO	\N	\N	\N	SI	NO	645	23 de Junio	\N	\N	\N	0	La pareja  tiene 38 años de concubinato y viviendo en el bañado.
5296148	Guido David	Morales Aguayo	MASCULINO	1990-10-02	PARAGUAYO/A	0983836475	SOLTERO/A	\N	\N	\N	SI	NO	641	Angel Luis	\N	\N	\N	0	Cuenta con otro al costado de su terreno (M40 L8).
5106867	Olga Patricia	Morales Morel	FEMENINO	\N	PARAGUAYO/A	0986878168	UNION DE HECHO	\N	\N	\N	SI	NO	2552	Mandy Jura	\N	\N	\N	0	Vive en el terreno de su suegra en una extencion de la vivienda.
5324830	Marcia Lorena	Morales	FEMENINO	1992-10-08	PARAGUAYO/A	0981354751	CASADO/A	\N	\N	\N	SI	NO	170	Mto. Ursicino Velasco	\N	\N	\N	0	.
384	Dalva Rocío	Moray Aranda	FEMENINO	2017-06-02	PARAGUAYO/A	0971981618	UNION DE HECHO	\N	\N	\N	SI	NO	390	.	\N	\N	\N	0	Tiene una cosa a media altura de la cooperativa.
4516755	Jorge Daniel	Moray Cabañas	MASCULINO	\N	PARAGUAYO/A	0971550927	UNION DE HECHO	\N	\N	\N	SI	NO	2737	Yacare Pito c/ Pilcomayo	\N	\N	\N	3	.
4516805	Yessica Carolina	Moray Cabañas	FEMENINO	\N	PARAGUAYO/A	0982 225 436	UNION DE HECHO	\N	\N	\N	SI	NO	1995	Yacare Yrupe c/ Pilcomayo	\N	\N	\N	3	la pareja lleva 7 años de cocnubinato. No tienen lote propio, pero si un techo precario.
2058602	Armando	Moray López	MASCULINO	1973-06-02	PARAGUAYO/A	0981540459	SOLTERO/A	\N	\N	\N	SI	NO	493	23 de Junio	\N	\N	\N	0	Vive dentro del Astillero en una vivienda de material bien construida. Esta como encargado del Astillero.
1440166	Celsa	Moray López	FEMENINO	2069-11-10	PARAGUAYO/A	0981510134	UNION DE HECHO	\N	\N	\N	SI	NO	504	San Felipe y Santiago	\N	\N	\N	0	.
2222312	Eusebia	Moray López	FEMENINO	\N	PARAGUAYO/A	0992662752	CASADO/A	\N	\N	\N	SI	NO	497	23 de Junio	\N	\N	\N	0	Dentro del lote de Eusebia vive su nuera menor de 25 con 1 hijo menor de edad.
4989540	Ahilin Cristalise	Moray Ocampos	FEMENINO	1989-10-12	PARAGUAYO/A	.	SEPARADO/A	\N	\N	\N	SI	NO	2575	Yacare Pito	\N	\N	\N	0	La titular esta casada legalmente pero hace 6 anos que esta separada,y actualmente esta tambien seaparada del padre del hijo en gestación. Es madre soltera.
4988209	Ángeles Soledad	Moray Ocampos	FEMENINO	\N	PARAGUAYO/A	0984798392	UNION DE HECHO	\N	\N	\N	SI	NO	2441	Mandyjura	\N	\N	\N	0	.
3181629	Francisco Damina	Morel Cabrera	MASCULINO	\N	PARAGUAYO/A	0975379335	UNION DE HECHO	\N	\N	\N	SI	NO	1182	Urcicino Velazco	\N	\N	\N	0	Vive dentro del terreno de la mama en una pieza con su familia
3680627	Alcides	Morel Fernández	MASCULINO	\N	PARAGUAYO/A	0981966881	UNION DE HECHO	\N	\N	\N	SI	NO	2642	Payagua Naranja	\N	\N	\N	0	.
1476073	Carmen	Morel Florentín	FEMENINO	2058-04-11	PARAGUAYO/A	0991677019	CASADO/A	\N	\N	\N	SI	NO	1704	Virgen de Guadalupe	\N	\N	\N	0	.
1428771	Celia	Morel Florentín	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	1752	Ursicino Velasco	\N	\N	\N	0	La Sra. Celia Tiene kiosko donde tiene ventas de comidas rapidas.
3307242	Sergio	Morel Florentín	MASCULINO	1971-01-02	PARAGUAYO/A	0986310725	UNION DE HECHO	\N	\N	\N	SI	NO	1828	Bañado Poty	\N	\N	\N	0	.
5691979	Verónica Victoria	Morel Garcia	FEMENINO	1993-08-10	PARAGUAYO/A	0994952538	UNION DE HECHO	\N	\N	\N	SI	NO	1225	.	\N	\N	\N	0	El bano comparte con los padres vive en una extencion en la casa de sus padres ,casita de madre.
13035	Antonia	Morel	FEMENINO	\N	No quiso censar	.	.	\N	\N	\N	SI	NO	1550	Pajagua Naranja	\N	\N	\N	0	No quiso censarse.
903852	Daniela	Morel	MASCULINO	2061-06-07	PARAGUAYO/A	0982555601	.	\N	\N	\N	SI	NO	1551	Pajagua Naranja	\N	\N	\N	0	El terreno cuenta con un Quincho/Galpón que mide 6,90x8,70 mts.El terreno cuenta con tres construcciones una de ellas es de 2 pisos mide 97 mts2.en la que vive la Sra.La casa cuenta en la prte superior un extructura hormigonada de tres plantas techo de l
4435300	Liz Margarita	Moreno Ocampo	FEMENINO	1990-03-09	PARAGUAYO/A	0994639956	CASADO/A	\N	\N	\N	SI	NO	1121	Virgen de Lourdes C/ Virgen de Lujan	\N	\N	\N	0	.
1123580	Dominga	Morinigo de López	FEMENINO	2055-04-08	PARAGUAYO/A	0994459927	CASADO/A	\N	\N	\N	SI	NO	533	.	\N	\N	\N	0	Dentro del lote de Dominga vive una hija separada con un hijo en una vivienda aparte bien constituida de 6x4 , la casa cuenta con un palafito.
4522477	Epifanía	Orué Vda. De Aliente	FEMENINO	2047-06-01	PARAGUAYO/A	0982487508	.	\N	\N	\N	SI	NO	2074	Ursisino Velazco	\N	\N	\N	0	.
3464445	Elvio	Morinigo Vera	MASCULINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	1253	Pajagua Naranja	\N	\N	\N	0	no es pariente de la dueña del lote, manifiesta que es un lote totalmente independiente, pero no tiene division visible, no le pusieron numero de lote
432830	Marcelina	Morinigo	FEMENINO	2050-06-04	PARAGUAYO/A	0981901221	VIUDO/A	\N	\N	\N	SI	NO	1963	.	\N	\N	\N	0	La señora era del bañado y todos sus hijos nacieron en el bañado, su casa era de Marta Mili, luego se mudo cerca del puente, y ahora se mudo cerca de la escuela Sotero Colmán, tiene un galpon de 4x6
968327	Ramón Bruna	Morinigo	MASCULINO	\N	PARAGUAYO/A	0984251650	SOLTERO/A	\N	\N	\N	SI	NO	1498	Rancho 8 y Mainumby	\N	\N	\N	0	La titular recibe un sueldo del estado(Tercera Edad), sus nieteos viven con ella hace 10 anos.
5713473	Rubén Darío	Morinigo	MASCULINO	1988-10-04	PARAGUAYO/A	.	CASADO/A	\N	\N	\N	SI	NO	1499	Rancho 8 y Mainumby	\N	\N	\N	0	La pareja lleva un ano de casados. Viven en el lote de la titular.Se sostienen independientemente, no tienen hijos.
2041915	Sonia Patricia	Morinigo	FEMENINO	1986-12-05	PARAGUAYO/A	0984540266	SOLTERO/A	\N	\N	\N	SI	NO	2556	Rancho 8	\N	\N	\N	0	.
706	Na Emi	.	.	\N	No quiso censar	.	.	\N	\N	\N	SI	NO	1726	Ursicino Velasco	\N	\N	\N	0	No quiso censarse.
3006779	Julio Cesar	Noceda Cáceres	MASCULINO	\N	PARAGUAYO/A	0984535745	SOLTERO/A	\N	\N	\N	SI	NO	1327	Virgen de Lourdes	\N	\N	\N	3	.
4648923	Liz Andrea	Noceda Cáceres	FEMENINO	1989-03-11	PARAGUAYO/A	0994317580	SOLTERO/A	\N	\N	\N	SI	NO	1432	Ursicino Velasco	\N	\N	\N	0	La Sra. Liz es tutora legal de sus sobrinas desde hace 11 anos.Se maneja de manera independiente con la manuntension de su nucleo fliar., es soltera no tiene lote propio,vive bajho el techo de los padres.
1114909	Vicente	Noceda Espinosa	MASCULINO	\N	PARAGUAYO/A	0982337426	UNION DE HECHO	\N	\N	\N	SI	NO	1620	Ursicino Velasco	\N	\N	\N	0	.
1906294	Lucina Pabla	Noceda Espinoza	FEMENINO	\N	PARAGUAYO/A	0986 821 962	SOLTERO/A	\N	\N	\N	SI	NO	2308	Urcisino Velazco	\N	\N	\N	0	Cuenta con una vivienda de material de 9 x 4. Actualmente viven en una casa precaria de 5,5 x 9 m, dentro del mismo lote.
1161903	Graciela	Noceda Varela	FEMENINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	2137	Agodto Poty	\N	\N	\N	0	.
3234889	Lilian	Noguera Ocampos	FEMENINO	\N	PARAGUAYO/A	0994268968	SOLTERO/A	\N	\N	\N	SI	NO	2576	Rancho 8	\N	\N	\N	0	Placa Electrica 1.
3323945	Silvia Ramona	Noguera	FEMENINO	2061-03-11	PARAGUAYO/A	0983181817	SOLTERO/A	\N	\N	\N	SI	NO	1536	.	\N	\N	\N	0	El terreno pertenecia a su esposo quien nacio en el barrio.La casa de material cayo en la inundacion, desde entonces su casa es de terciada.(Fotos de casa de material 2da. Pagina).
4987576	Blas Ramón	Notario	MASCULINO	1996-01-02	PARAGUAYO/A	0991760789	UNION DE HECHO	\N	\N	\N	SI	NO	2597	Rancho 8	\N	\N	\N	0	Vive en la casa de la mama con su hijo, llevan 4 anos de concubinato.
5055017	Evelin Estefania	Núñez Barrios	FEMENINO	\N	PARAGUAYO/A	0984880849	UNION DE HECHO	\N	\N	\N	SI	NO	1380	Yvoty Rory	\N	\N	\N	0	Vive dentro de la vivienda en una pieza.
3446226	Ilda	Núñez Bernal	FEMENINO	1976-01-09	PARAGUAYO/A	0983488105	UNION DE HECHO	\N	\N	\N	SI	NO	651	Angel Luis	\N	\N	\N	0	.
1117527	Paulina	Nuñez de Chavez	FEMENINO	2060-07-06	PARAGUAYO/A	0981 140 598	CASADO/A	\N	\N	\N	SI	NO	2700	8 de diciembre	\N	\N	\N	0	La titular posee documentación del terreno. En la vivienda vive una pareja, ambos mayores de edad.
2444012	María Purificacion	Núñez de González	FEMENINO	\N	PARAGUAYO/A	0984232558	CASADO/A	\N	\N	\N	SI	NO	851	Pasillo Sin Nombre	\N	\N	\N	0	.
2002896	Inocencia	Núñez de Torrado	FEMENINO	\N	PARAGUAYO/A	0981449205	VIUDO/A	\N	\N	\N	SI	NO	2587	Rancho 8	\N	\N	\N	0	Placa Electrica 1.
3526400	Gloria Elizabeth	Núñez Estigarribia	FEMENINO	\N	PARAGUAYO/A	0982226685	SOLTERO/A	\N	\N	\N	SI	NO	1884	Julio Benitez	\N	\N	\N	0	Las ninas quedaron al cuidado de la tia cuando fallecio la madre ,sufriendo la nina mayor electrocuciones a la edad de 3 anos quedando con Paralisis Cerebral.
3761335	Ana María	Núñez Ferreira	FEMENINO	\N	PARAGUAYO/A	0972238345	UNION DE HECHO	\N	\N	\N	SI	NO	1707	Virgen de Guadalupe	\N	\N	\N	0	.
3761342	Emigdio Teodosio	Núñez Ferreira	MASCULINO	1985-05-08	PARAGUAYO/A	0984625214	CASADO/A	\N	\N	\N	SI	NO	1663	Virgen de Guadalupe	\N	\N	\N	0	.
1447834	Enrique	Núñez Ferreira	MASCULINO	\N	PARAGUAYO/A	021425912	UNION DE HECHO	\N	\N	\N	SI	NO	937	Nuevo Amanecer	\N	\N	\N	4	Tiene dos comercios (depósitos)
3761338	Gloria Juliána	Núñez Ferreira	FEMENINO	1980-02-03	PARAGUAYO/A	0984491089	UNION DE HECHO	\N	\N	\N	SI	NO	1706	Virgen de Guadalupe	\N	\N	\N	0	.
1811575	Ruben	Núñez Ferreira	MASCULINO	2069-09-09	.	0981786080	SOLTERO/A	\N	\N	\N	SI	NO	1151	.	\N	\N	\N	0	.
5095548	Jorge Adalberto	Núñez González	MASCULINO	\N	PARAGUAYO/A	PAR	SOLTERO/A	\N	\N	\N	SI	NO	1639	Pajagua Naranja	\N	\N	\N	0	.
5531260	Jorge Amado	Núñez González	MASCULINO	1991-03-07	PARAGUAYO/A	0982790202	.	\N	\N	\N	SI	NO	437	San Cayetano	\N	\N	\N	0	Vive dentro del lote de la Sra. Graciela Riveros Benitez es su nuera y cuenta con una vivienda precaria de 4x3 .
2091868	Isabel	Núñez Martínez	FEMENINO	\N	PARAGUAYO/A	0986155577	SOLTERO/A	\N	\N	\N	SI	NO	877	Nuevo Amanecer	\N	\N	\N	0	.
6851867	Jessica	Núñez Ojeda	FEMENINO	\N	PARAGUAYO/A	0984891500	UNION DE HECHO	\N	\N	\N	SI	NO	1039	Yvoty Rory e/ Mita Saraki	\N	\N	\N	0	Vive dentro del mismo lote que la madre pero en otra vivienda.
6522716	Ramona	Núñez Pereira	MASCULINO	\N	PARAGUAYO/A	0982437490	UNION DE HECHO	\N	\N	\N	SI	NO	287	Virgen de Lourdes	\N	\N	\N	0	.
4689250	Sandra Elizabeth	Núñez Ramirez	FEMENINO	\N	PARAGUAYO/A	0982790730	UNION DE HECHO	\N	\N	\N	SI	NO	921	Angel Luis	\N	\N	\N	0	No es dueña del lote .Esta alquilando del Sr. Eladio Armoa.
397295	Virgilio	Núñez Rejala	MASCULINO	2046-05-01	PARAGUAYO/A	0991182106	CASADO/A	\N	\N	\N	SI	NO	1883	Julio Benitez	\N	\N	\N	0	La Sra. Cobra Tercera Edad.
4478591	Rosali Faustina	Núñez Santacruz	FEMENINO	\N	PARAGUAYO/A	094631451	SOLTERO/A	\N	\N	\N	SI	NO	1232	.	\N	\N	\N	0	.
7009903	Claire Yaquelin	Núñez Vera	FEMENINO	1995-02-06	PARAGUAYO/A	0983917145	SOLTERO/A	\N	\N	\N	SI	NO	2330	Nuestra Señora de la Asuncion	\N	\N	\N	6	.
5492563	Leila Magaly	Núñez Vera	FEMENINO	1990-05-09	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1594	Ramon Talavera	\N	\N	\N	0	Vive dentro del lote del suegro temporalmente porque su lote esta inundado.
5573438	Neida Estefania	Núñez Vera	FEMENINO	1994-11-03	PARAGUAYO/A	0971946314	SOLTERO/A	\N	\N	\N	SI	NO	2309	Dr. Juan leon Mallorquin	\N	\N	\N	6	La pareja lleva de 8 anos concubinato,tienen lote y techo propio.
2444017	Daniela	Núñez	MASCULINO	2067-03-01	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	2639	Martin Fierro	\N	\N	\N	0	.
3931380	Elida Ramona	Núñez	FEMENINO	2060-02-08	PARAGUAYO/A	0986375455	.	\N	\N	\N	SI	NO	2550	Mandy Jura	\N	\N	\N	0	Es propietaria de la vivienda,pegada a su pared vive su hijo con su flia.
3812334	Maria Cristina	Nuñez	FEMENINO	\N	PARAGUAYO/A	0982713846	UNION DE HECHO	\N	\N	\N	SI	NO	2728	8 de Diciembre	\N	\N	\N	0	.
4120251	María Teresa	Núñez	FEMENINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	1333	Ursicino Velasco	\N	\N	\N	0	.
1909724	Máxima Florentín	Núñez	FEMENINO	2066-08-06	PARAGUAYO/A	0991477521	UNION DE HECHO	\N	\N	\N	SI	NO	356	Virgen de Lujan C/Aromita	\N	\N	\N	0	.
679051	Teolosio	Núñez	MASCULINO	\N	PARAGUAYO/A	0991444990	CASADO/A	\N	\N	\N	SI	NO	1662	Virgen de Guadalupe	\N	\N	\N	0	.
1901078	Inocencia	Ocampo Castro	FEMENINO	2057-04-07	PARAGUAYO/A	0983285977	.	\N	\N	\N	SI	NO	1089	23 de Junio	\N	\N	\N	0	Vive al fondo del lote de Mabel no tienen nada dentro de su vivienda , no es familiar de la titular.Los nietos estan en un albergue es una familia en extrema pobreza.
1342298	Ramona	Ocampo de Bogado	MASCULINO	\N	PARAGUAYO/A	0982129443	UNION DE HECHO	\N	\N	\N	SI	NO	1440	Ursicino Velasco	\N	\N	\N	0	La titular hace tres meses regreso con su cocubinato.El frente de la casa ocupatodo el lote.
806015	Leopoldina	Ocampo de Domínguez	FEMENINO	\N	PARAGUAYO/A	425043	UNION DE HECHO	\N	\N	\N	SI	NO	1429	Ursicino Velasco	\N	\N	\N	0	Vitrina:1. La titular y su esposo cobran Tercera Edad y tiene un almacen ,tiene un primer piso de 9x26 No tiene Cosatdo la construccion ocupa todo el lote de frente.
2487487	Edilberto	Ocampo Ortiz	MASCULINO	1976-03-08	PARAGUAYO/A	0982485851	UNION DE HECHO	\N	\N	\N	SI	NO	2231	Mto.Ursicino Velasco	\N	\N	\N	0	En el lote tiene un salon que mide 3,50x4,50 ,el salon va ser usado como peluqueria.
1933934	Pastora	Ocampo	FEMENINO	\N	PARAGUAYO/A	0982238942	UNION DE HECHO	\N	\N	\N	SI	NO	1120	Virgen de Lourdes C/ Virgen de Lujan	\N	\N	\N	0	.
1244340	Digna	Ocampos Benítez	FEMENINO	\N	PARAGUAYO/A	0984111004	SOLTERO/A	\N	\N	\N	SI	NO	2224	Mto.Ursicino Velasco	\N	\N	\N	0	Cuenta con un corredor.
2927284	Cristina	Ocampos de Caballero	FEMENINO	1975-03-12	PARAGUAYO/A	0972463854	CASADO/A	\N	\N	\N	SI	NO	578	San Cayetano	\N	\N	\N	0	Dentro del techo de la titular vive su hija mayor de edad con su pareja y un menor de edad.
326912	Agustín	Ocampos Maqueda	MASCULINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	1652	Jakare Pito	\N	\N	\N	3	La casa es de Bloque de cemento, pero con una extension del frente de madera, no tiene electrodomesticos
2553120	Rosalina	Ocampos Ortiz	FEMENINO	\N	PARAGUAYO/A	0983323392	SEPARADO/A	\N	\N	\N	SI	NO	2237	Mto.Ursicino Velasco	\N	\N	\N	0	Placa Inducción 1.
1073841	Eusebia	Ocampos	FEMENINO	\N	PARAGUAYO/A	0972727693	SOLTERO/A	\N	\N	\N	SI	NO	2578	Rancho 8	\N	\N	\N	0	La Despensa del atitular mide 6x3,desde hace 10 anos tiene su negociosus dos hijos mayores tienen techo independiente al de la madre de materialcon dos dormitorios y un bano.
3338014	Liliana Jacqueline	Ocampos	FEMENINO	1992-05-03	PARAGUAYO/A	0983615519	UNION DE HECHO	\N	\N	\N	SI	NO	589	8 de Diciembre	\N	\N	\N	0	La Sra Lliliana embarazada de 9 meses compraron la casa y viven hay hace tres anos.
1566790	Elsa	Ojeda  de Duarte	FEMENINO	2062-04-10	PARAGUAYO/A	00984261638	CASADO/A	\N	\N	\N	SI	NO	177	Nuevo Amanecer e/ Julio Jara	\N	\N	\N	4	.
420067	Miguel Ángel	Ojeda Bazzano	MASCULINO	2049-08-05	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	1877	Ursicino Velasco c/Maria Auxiliadora	\N	\N	\N	0	.
1214426	Feliciana	Ojeda de Doldan	FEMENINO	\N	PARAGUAYO/A	0984780435	SOLTERO/A	\N	\N	\N	SI	NO	1504	Rancho 8	\N	\N	\N	0	Tiene Ventas de Minutas. Placa Electrica 1.
1233134	Matilde	Ojeda de Florentín	FEMENINO	\N	PARAGUAYO/A	.	CASADO/A	\N	\N	\N	SI	NO	183	Julio Jara  y Ursicino Velasco	\N	\N	\N	0	.
1830949	María Pabla	Ojeda de Sosa	FEMENINO	\N	PARAGUAYO/A	0982294028	UNION DE HECHO	\N	\N	\N	SI	NO	672	San Cayetano	\N	\N	\N	0	.
4735133	Ramón	Ojeda Echeverría	MASCULINO	1990-05-02	PARAGUAYO/A	0984108799	UNION DE HECHO	\N	\N	\N	SI	NO	837	Ursicino Velazco c/ Martín Fierro	\N	\N	\N	0	La única manera de quitar la foto fue de frente, de costado no se puede
2178074	Teodocia	Ojeda Gauto	FEMENINO	\N	PARAGUAYO/A	0984996796	SOLTERO/A	\N	\N	\N	SI	NO	1038	Yvoty Rory e/ Mita Saraki	\N	\N	\N	0	.
859349	Lidia	Ojeda Peralta	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	2058	Sotero Colman	\N	\N	\N	0	LA Sra es separada hace 9 años y manifiesta ser dueña de la casa y que ya dio su parte a cada uno de sus hijos
2670332	María Adelaida	Ojeda Saldivar	FEMENINO	\N	PARAGUAYO/A	0983858007	UNION DE HECHO	\N	\N	\N	SI	NO	1850	Villa Benitez	\N	\N	\N	0	.
1335928	Agustín	Ojeda Sosa	MASCULINO	1972-05-05	PARAGUAYO/A	0981212901	SOLTERO/A	\N	\N	\N	SI	NO	2254	Mallorquin y Tte. Pratts	\N	\N	\N	0	.
3197440	Ever Emilio	Ojeda	MASCULINO	\N	PARAGUAYO/A	0994828734	UNION DE HECHO	\N	\N	\N	SI	NO	178	Nuevo Amanecer E/ Julio Jara	\N	\N	\N	0	.
5798761	Liz Romina	Ojeda	FEMENINO	1987-07-10	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1288	.	\N	\N	\N	0	.
4878918	Francisco Antonio	Olazar Aquino	MASCULINO	1991-04-03	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	2721	Mady Jura	\N	\N	\N	0	.
5852763	María del Carmen	Olazar Aquino	FEMENINO	\N	PARAGUAYO/A	0985910978	UNION DE HECHO	\N	\N	\N	SI	NO	400	Algarrobo	\N	\N	\N	0	.
1853904	Miguel David	Olazar Cáceres	MASCULINO	\N	PARAGUAYO/A	0992 476 642	SOLTERO/A	\N	\N	\N	SI	NO	2591	27 de Mallorquin	\N	\N	\N	0	En la parte de atrás de la casa tiene una construcción (extención) de material sin terminar.
1774738	Antolin	Olazar Núñez	MASCULINO	\N	PARAGUAYO/A	0982521532	CASADO/A	\N	\N	\N	SI	NO	1407	Bañado Koeti	\N	\N	\N	5	.
2225745	Antonio	Olazar Núñez	MASCULINO	\N	PARAGUAYO/A	0991483071	SEPARADO/A	\N	\N	\N	SI	NO	13	RECICLA	\N	\N	\N	3	.
3674441	Ismael Ramón	Olazar	MASCULINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	1379	Yvoty Rory	\N	\N	\N	0	Ismael y Angelina son propietarios del terrenopero la construccion le pertenece a Ismael .Dentro de la propiedad viven sus sobrinos.
4748	Oleria - 20 de Julio	.	.	\N	.	0992255776	.	\N	\N	\N	SI	NO	949	Los Sauces	\N	\N	\N	0	El Bernal Migdonio es propietario de la Oleria Trabajan en ellas las Flia. , 2 personasy un carrero. Actividad que realizan : la elaboracion de ladrillo y venta. Año de fundacion:1967-50 años .No tiene R.U.C.
2212186	Marcelino	Olería - Acosta Gutierrez	MASCULINO	\N	PARAGUAYO/A	0981503003	.	\N	\N	\N	SI	NO	2740	.	\N	\N	\N	0	Dentro del predio hay una Oleria que no esta en funcionamiento en estos momentos, el titular refiere que durante la crecida del rio el horno y el tinglado de la Oleria se derrumbaron, se observa parte del horno caido.Funciona hace 47 años pero fue compra
5413	Antolin	Oleria - Adorno Pereira	MASCULINO	\N	.	.	.	\N	\N	\N	SI	NO	1053	Mar del Plata	\N	\N	\N	0	Es su fuente de trabajo hace mas de 30 anos que se dedica a la Oleria , esta en refaccion para habilitar la Oleria.
2109319	Oleria - Don Bareiro e Hijos	.	.	\N	.	0981 730 424	.	\N	\N	\N	SI	NO	2702	La olería don Bareiro Hijos, funciona hace 20 años	\N	\N	\N	0	.
43035	Oleria - Don Faustino	.	.	\N	.	0961102135	.	\N	\N	\N	SI	NO	1081	Es un Área productiva cuenta con 6 personas en for	\N	\N	\N	0	.
4728	Oleria - Inocente Adolfo López Chávez	.	.	\N	.	.	.	\N	\N	\N	SI	NO	2414	Mar del Plata	\N	\N	\N	0	En la Oleria hay dos personas trabajando y hace mas de 70 anos funciona la Oleria.
43036	Oleria - La esperanza	.	.	\N	.	0961102135	.	\N	\N	\N	SI	NO	1083	Mar de plata	\N	\N	\N	0	Es un área productiva, cuenta con ocho(8) personales en forma drecta y cuatro(4) personales que no son permanentes. Cuenta con permiso de asendamiento.
43034	Oleria - La Piolita	.	.	\N	.	0983482346	.	\N	\N	\N	SI	NO	1078	El señor Antonio Aranda es propietario del lote y	\N	\N	\N	0	.
478978	Pablo	Oleria - Olmedo Chiuzano	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	939	Mar de Plata	\N	\N	\N	0	Es el lugar de trabajo del señor Pablo Olmedo Chiugano, una Oleria. Poblador antiguo.
524	Oleria Y Felix Reinaldo Jara Chavez	.	.	1971-05-04	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	2719	.	\N	\N	\N	0	Es la fuente de trabajo hace mas de 30 años.
4984399	María Asunción	Olivera Maciel	FEMENINO	\N	PARAGUAYO/A	0983 401 756	UNION DE HECHO	\N	\N	\N	SI	NO	1715	Yacare Pito	\N	\N	\N	3	La señora María Asunción vino de San Roque Gonzalez de Santa Cruz, departamento de Paraguari.
2095824	Isabelino	Olmedo Aguirre	FEMENINO	\N	PARAGUAYO/A	0972767927	UNION DE HECHO	\N	\N	\N	SI	NO	320	P/ 4 de Octubre	\N	\N	\N	0	.
1663625	Gabriela	Olmedo Díaz	FEMENINO	\N	PARAGUAYO/A	0982968910	VIUDO/A	\N	\N	\N	SI	NO	1780	Bañado Koeti	\N	\N	\N	5	Es propietario del lote en el vive con el su ahijado en una vivienda de madera totalmente independiente.
2193496	María Cristina	Olmedo Espinola	FEMENINO	1975-06-05	PARAGUAYO/A	0994387626	UNION DE HECHO	\N	\N	\N	SI	NO	1709	Virgen de Guadalupe c/ Tape Pyahu	\N	\N	\N	0	.
4457673	Ángela Estela	Olmedo	FEMENINO	1983-02-08	PARAGUAYO/A	0981978285	CASADO/A	\N	\N	\N	SI	NO	2568	Mandy Jura y Yacare Pito	\N	\N	\N	0	La pareja lleva 11 anos de casados,anteriormente tenian una casa precaria,hace 7 anos tienen una construcción de material de 90 mts2.
1947600	Celia Avelina	Olmedo	FEMENINO	2063-10-12	PARAGUAYO/A	.	CASADO/A	\N	\N	\N	SI	NO	194	Ursicino Velasco	\N	\N	\N	0	.
1066196	Eustaquia	Olmedo	FEMENINO	2032-02-11	PARAGUAYO/A	.	VIUDO/A	\N	\N	\N	SI	NO	565	8de Diciembre	\N	\N	\N	0	LA vivienda consta de otra construcción Pro-Vida bloque , piso lecherada .
2041925	Graciela	Olmedo	FEMENINO	1974-02-10	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	567	8 de Diciembre	\N	\N	\N	0	.
4030361	Gloria Mabel	Orrego Ferreira	FEMENINO	1983-11-04	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1271	Ursicino Velasco	\N	\N	\N	0	.
4117841	Graciela Raquel	Orrego Ferreira	FEMENINO	\N	PARAGUAYO/A	0982307774	UNION DE HECHO	\N	\N	\N	SI	NO	1342	Primavera	\N	\N	\N	0	.
1192216	Sunilda	Orrego Ferreira	FEMENINO	1970-03-05	PARAGUAYO/A	0986458806	UNION DE HECHO	\N	\N	\N	SI	NO	1007	Ursicino Velasco	\N	\N	\N	0	.
3287140	Asunción	Ortega Cristaldo	FEMENINO	\N	PARAGUAYO/A	0981552012	VIUDO/A	\N	\N	\N	SI	NO	1944	Ursicino Velasco	\N	\N	\N	0	La sra cobra 3ra Edad.
4550888	Derlis Ariel	Ortega Esquivel	MASCULINO	1989-09-03	PARAGUAYO/A	0971911924	UNION DE HECHO	\N	\N	\N	SI	NO	563	8 de Diciembre	\N	\N	\N	0	El dueno compro la vivienda hace 12 anos con la construccion incluida.
2444185	María Basilia	Ortega Esquivel	FEMENINO	\N	PARAGUAYO/A	0984447255	SOLTERO/A	\N	\N	\N	SI	NO	2511	Yacare Yrupe	\N	\N	\N	0	Placa Electrica 1.
895621	Mary Selva Graciela	Ortega	FEMENINO	2062-09-12	PARAGUAYO/A	0984846058	SOLTERO/A	\N	\N	\N	SI	NO	503	San Felipe	\N	\N	\N	0	Cuenta con una Casa con techo de terni , con 8x3 que utiliza como deposito.
2959477	Nicanor	Ortega	MASCULINO	2061-11-01	PARAGUAYO/A	0972514892	SOLTERO/A	\N	\N	\N	SI	NO	2697	San Cayetano	\N	\N	\N	0	.
2953383	Marta Beatríz	Ortíz Chamorro	FEMENINO	\N	PARAGUAYO/A	0983280271	UNION DE HECHO	\N	\N	\N	SI	NO	653	23 de Junio	\N	\N	\N	0	Esta familia vive dentro del lote de la hija Mariel Bolaños quien es la titular del terreno. Tienen techo propio y precario.
4844212	Gustavo Ramón	Ortiz Cuevas	MASCULINO	\N	PARAGUAYO/A	4844212	SOLTERO/A	\N	\N	\N	SI	NO	2660	Nuestra Sra. De la Asunción	\N	\N	\N	6	El titular es padre soltero hace 6 meses ,mantiene a su hija con sus ingresos.
4239053	María Venancia	Ortiz Cuevas	FEMENINO	1981-01-04	PARAGUAYO/A	0986111416	UNION DE HECHO	\N	\N	\N	SI	NO	2661	Nuestra Sra. De la Asunción	\N	\N	\N	6	.
4931191	Natalia Rosalba	Ortíz Cuevas	FEMENINO	\N	PARAGUAYO/A	0992218623	UNION DE HECHO	\N	\N	\N	SI	NO	2325	Nuestra Sra. De la Asunción	\N	\N	\N	6	La pareja lleva 12 anos de concubinato,tienen lote propio techo precario.
776872	María Concepción	Ortiz de Sarabia	FEMENINO	\N	PARAGUAYO/A	0983174956	SEPARADO/A	\N	\N	\N	SI	NO	2181	Mto. Ursicino Velasco	\N	\N	\N	0	.
1056211	Bilma Rosa	Ortiz de Vázquez	FEMENINO	\N	PARAGUAYO/A	0983894176	CASADO/A	\N	\N	\N	SI	NO	2180	Mto. Ursicino Velasco	\N	\N	\N	0	.
722509	Nicolas	Ortiz Díaz	MASCULINO	2053-06-12	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	2185	Mto. Ursicino Velasco	\N	\N	\N	0	El Sr. Nicolas vive en el fondo del lote y tiene una construcción de hormigon armado se mantiene de manera totalmente independiente de la duena.
3620598	Avelina	Ortiz Duarte	FEMENINO	1978-10-09	PARAGUAYO/A	0986431424	UNION DE HECHO	\N	\N	\N	SI	NO	2365	Nuestra Señora de la Asuncion	\N	\N	\N	6	la pareja lleva 15 años de oncubinato, hace 11 años que viven en el barrio, anteriormente vivian en un asentamiento llamado la lomita en ñemby, el negocio que tienen lo iniciaron hace 5 años
3680643	Edith María	Ortiz Gamarra	FEMENINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	596	Malvinas	\N	\N	\N	0	La vivienda es bloque Pro-Vida tiene un extension de madera al frente. La hija Maria vive con la mama tiene un terreno al lado de la vivienda.
4813958	María Adela	Ortiz Garcete	FEMENINO	\N	PARAGUAYO/A	.	CASADO/A	\N	\N	\N	SI	NO	965	Los Sauces y Alegre	\N	\N	\N	0	Este matrimonio tiene 18 años de convivencia y 3 años de de casados por civil.Tienen5 hijos y hace 10 años que viven en el barrio.
5265664	Reinaldo	Ortiz Garcete	MASCULINO	\N	PARAGUAYO/A	09853498529	SOLTERO/A	\N	\N	\N	SI	NO	486	.	\N	\N	\N	0	Vive como encargado de la Arenera que esta al lado de su terreno porque despues de la crecida su vivienda se desplomo.
4393221	Claudia Celestina	Ortiz González	FEMENINO	\N	PARAGUAYO/A	0986820579	UNION DE HECHO	\N	\N	\N	SI	NO	290	Ursicino Velasco	\N	\N	\N	0	.
4837298	Daisy Daiana	Ortiz Jara	FEMENINO	1989-11-10	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	438	Ursicino Velasco	\N	\N	\N	0	.
4837337	Mirta Elizabeth	Ortiz Jara	FEMENINO	\N	PARAGUAYO/A	0984940881	SOLTERO/A	\N	\N	\N	SI	NO	292	Ursicino Velasco	\N	\N	\N	0	Desde los 16 anos vive en techo aparte.
5046810	Luz	Ortiz Jordan	FEMENINO	1998-07-09	PARAGUAYO/A	0981118620	.	\N	\N	\N	SI	NO	2072	Ursisino Velzco	\N	\N	\N	0	No tienen lote ni vivienda, viven en alquiler
2625097	Alfredo Nicolas	Ortiz Maidana	MASCULINO	1978-06-08	PARAGUAYO/A	.	CASADO/A	\N	\N	\N	SI	NO	2184	Mto. Ursicino Velasco	\N	\N	\N	0	.
4409159	Ramona	Ortiz Mercado	MASCULINO	\N	PARAGUAYO/A	0982630662	UNION DE HECHO	\N	\N	\N	SI	NO	1539	Pajagua Naranja	\N	\N	\N	0	.
2156399	Alicia Aurora	Ortiz	FEMENINO	\N	PARAGUAYO/A	0982452598	UNION DE HECHO	\N	\N	\N	SI	NO	2527	Ursicino Velasco	\N	\N	\N	0	Hace 10 anos que viven en inquilinato no tienen terreno de 9x7 ,el inquilinato pertenece a la Sra. Petrona Farfan y alquila el terreno para construir su casita precaria.La sra. Petrona Farfan no estaba de acuerdo en que el inquilinato sea censado con su
6576973	Graciela	Ortiz	FEMENINO	\N	PARAGUAYO/A	0971934804	UNION DE HECHO	\N	\N	\N	SI	NO	1044	Yvoty Rory	\N	\N	\N	0	Vive dentro del mismo lote de la madre en una construccion aparte.
6253331	Patricia Soledad	Ortiz	FEMENINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	2525	Ursicino Velasco c/ Mita Saraki	\N	\N	\N	0	hace un ano que vive en inquilinato no tiene terreno y tiene una vivienda precaria 4x5 ,la alquila el terreno para construir su casita ,la sra. Petrona Farfan no estaba de acuerdo en que el inquilinato sea censado con su nro de lote.
308326	Vicente Chávez	Ortiz	MASCULINO	\N	PARAGUAYO/A	0991283231	.	\N	\N	\N	SI	NO	2083	sin nombre	\N	\N	\N	0	El señor no es el titular, la titular es la sra, le presto su predio por un periodo, ya que está separado de la sra
3228118	Gustavo Fidel	Orue Contrera	MASCULINO	1982-12-05	PARAGUAYO/A	0982299811	UNION DE HECHO	\N	\N	\N	SI	NO	1340	Priamvera c/ Urcicino Velazco	\N	\N	\N	0	.
2411341	Teresa	Osorio Almeida	FEMENINO	2057-03-10	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	1035	Mar de plata	\N	\N	\N	0	En el lote también hay una cosa aparte que es Pro vida y también tienen un palófito.
4638103	Cipriana Concepción	Osorio de Ayolas	FEMENINO	\N	PARAGUAYO/A	0991714359	CASADO/A	\N	\N	\N	SI	NO	2139	Agosto Poty	\N	\N	\N	0	Vive en el mismo terreno de la propietaria.
2510335	Alicia Liz	Osorio de Bogado	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	2685	Ursicino Velazco	\N	\N	\N	0	.
1105857	Elena Clara	Osorio de Cazal	FEMENINO	\N	PARAGUAYO/A	0985560585	CASADO/A	\N	\N	\N	SI	NO	518	Ursicino Velasco C/ Malvinas	\N	\N	\N	4	Al fondo de la vivienda tiene una construccion de material debajo de un palafito donde vive el hijo soltero.
2375086	Gregoria	Osorio de Méndez	FEMENINO	\N	PARAGUAYO/A	0982505227	CASADO/A	\N	\N	\N	SI	NO	1990	Nasaindy	\N	\N	\N	5	.
4313694	Julio Cesar	Osorio Gómez	MASCULINO	\N	PARAGUAYO/A	0984398463	CASADO/A	\N	\N	\N	SI	NO	1564	Ursicino Velasco	\N	\N	\N	0	.
6552418	Elias Joel	Osorio Maciel	MASCULINO	1994-03-08	PARAGUAYO/A	0983227551	.	\N	\N	\N	SI	NO	2202	Mbo-ehara	\N	\N	\N	0	tiene su vivienda dentro del lote de la mamá, vive all
6552419	Juan David	Osorio	MASCULINO	\N	PARAGUAYO/A	PAR	SOLTERO/A	\N	\N	\N	SI	NO	1980	Mbo'ehara	\N	\N	\N	0	es su lote y su casa
2844001	Miryan Bartola	Osorio	FEMENINO	\N	PARAGUAYO/A	0982933323	SOLTERO/A	\N	\N	\N	SI	NO	2025	.	\N	\N	\N	0	.
513254	José Eladio	Otazu Alvarenga	MASCULINO	\N	PARAGUAYO/A	0982883832	UNION DE HECHO	\N	\N	\N	SI	NO	1728	Ursicino Velasco	\N	\N	\N	5	.
1283235	Basilia	Otazu de Brítez	FEMENINO	\N	PARAGUAYO/A	.	VIUDO/A	\N	\N	\N	SI	NO	157	Algarrobo	\N	\N	\N	0	.
947567	Rumilda	Ovelar de Melgarejo	FEMENINO	2058-09-08	PARAGUAYO/A	0981385465	CASADO/A	\N	\N	\N	SI	NO	2729	8 de Diciembre	\N	\N	\N	0	.
5491552	José Luis	Ovelar Rodas	MASCULINO	\N	PARAGUAYO/A	0992826312	UNION DE HECHO	\N	\N	\N	SI	NO	908	Martin Fierro	\N	\N	\N	5	Vive en el terreno del padre.
5491553	Karina Belen	Ovelar Rodas	FEMENINO	1995-08-09	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1273	.	\N	\N	\N	0	Karina tiene al fondo su casa de madera que su mama le dio un pequeño lugar.
1735582	Catalina	Ovelar	FEMENINO	\N	PARAGUAYO/A	0985920097	UNION DE HECHO	\N	\N	\N	SI	NO	1529	Mtro Ursicino Velasco	\N	\N	\N	0	caja de dos plantas
2363688	Leoncia	Oviedo de Yrigoyen	FEMENINO	2047-12-09	PARAGUAYO/A	0992576553	CASADO/A	\N	\N	\N	SI	NO	2358	Nuestra Sra de la Asunción	\N	\N	\N	6	La pareja se mantiene de sueldo que percibe del estado (Tercera Edad).
3512908	Oscar José	Oviedo González	MASCULINO	1985-05-10	PARAGUAYO/A	0983343523	UNION DE HECHO	\N	\N	\N	SI	NO	37	Virgen Lourdes	\N	\N	\N	0	.
5391818	Cinthia Carolina	Oviedo Maldonado	FEMENINO	1991-10-07	PARAGUAYO/A	0982591088	SOLTERO/A	\N	\N	\N	SI	NO	1419	Bañado Koeti	\N	\N	\N	5	.
5255606	Sara Raquel	Oviedo Maldonado	FEMENINO	1993-07-07	PARAGUAYO/A	0971965560	UNION DE HECHO	\N	\N	\N	SI	NO	1403	Bañado Koeti	\N	\N	\N	5	La señora Sara Oviedo, tiene una construcción bastante constituida en el lote de su madre.
5039571	Virginia	Ozorio Almeida	FEMENINO	2056-11-01	PARAGUAYO/A	0984371655	.	\N	\N	\N	SI	NO	2022	.	\N	\N	\N	0	La Sra.vivía en ottras zonas anteriormente.
2844002	Nancy Rosalba	Ozorio de Florentín	FEMENINO	\N	PARAGUAYO/A	.	SEPARADO/A	\N	\N	\N	SI	NO	2681	Ursicino Velazco	\N	\N	\N	0	Es propietria deel lote. Recientemente viajó a la Argentina para trabajar, por un corto periodo.
4267934	Luis Alberto	Ozorio Gómez	MASCULINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	2140	Agosto Poty	\N	\N	\N	0	Actualmente vive en la casa de la mama porque alquila su vivienda.
2129779	Agustín	Ozuna Larrosa	MASCULINO	\N	.	0991 935 914	.	\N	\N	\N	SI	NO	1905	Pilcomayo	\N	\N	\N	0	Esta familia tenia un lote en la zona 1, por más de 20 años.
4708844	Verónica Yolanda	Ozuna Zorrilla	FEMENINO	\N	PARAGUAYO/A	0971739229	UNION DE HECHO	\N	\N	\N	SI	NO	2620	Nuestro Señora de la Asuncion	\N	\N	\N	6	1 Centrifugadora, 1 Placa Electrica.
4708878	Marcos Daniel	Ozuna	MASCULINO	1985-12-05	PARAGUAYO/A	0971148994	CASADO/A	\N	\N	\N	SI	NO	1267	.	\N	\N	\N	0	Casa precaria cuentan con una construccion de material sin terminar.
3750648	María Sunilda	Páez de Ozorio	FEMENINO	\N	PARAGUAYO/A	0984616545	CASADO/A	\N	\N	\N	SI	NO	2141	Agosto Poty	\N	\N	\N	0	.
6153708	Lucy Damiana	Paez Franco	FEMENINO	\N	PARAGUAYO/A	0984215389	UNION DE HECHO	\N	\N	\N	SI	NO	270	Nuevo Amanecer	\N	\N	\N	0	.
4572850	Diana Nicolasa	Palacios Espinoza	FEMENINO	1981-05-04	PARAGUAYO/A	0971267345	SEPARADO/A	\N	\N	\N	SI	NO	1149	Virgen de Lujan c/ Los Sauces	\N	\N	\N	0	Liliana Fleitas esta embarazada de 7 meses.
5852829	María Asunción	Palacios Espinoza	FEMENINO	\N	PARAGUAYO/A	0994951532	UNION DE HECHO	\N	\N	\N	SI	NO	1146	Virgen de Lujan c/ Los Sauces	\N	\N	\N	0	.
1397281	Leonarda	Palma de Martínez	FEMENINO	2060-06-11	PARAGUAYO/A	.	CASADO/A	\N	\N	\N	SI	NO	257	Virgen de Lourdes	\N	\N	\N	0	Tiene Corredor 11x5.
6570257	Agustína	Palma Rojas	MASCULINO	\N	PARAGUAYO/A	0984918628	SEPARADO/A	\N	\N	\N	SI	NO	1684	.	\N	\N	\N	0	Francisco vive con ella desde que nacio, estaba en cunbinato de 7 anos hasta hace 2 meses donde la concubina le deja el hijo ,el tiene dos dormitorios pegados por la casa de la madre , en este momento estas=n en la misma pieza porque la crecida hechpo su
1975218	Dora Leonora	Palma Rojas	FEMENINO	1972-06-02	PARAGUAYO/A	0982794604	SOLTERO/A	\N	\N	\N	SI	NO	726	Pasillo Angel Luis	\N	\N	\N	0	.
429759	Juan Bautista	Panadería - Irala Prieto	MASCULINO	\N	PARAGUAYO/A	0981164998	.	\N	\N	\N	SI	NO	552	Ursicino Velasco	\N	\N	\N	0	La propiedad tiene una construcción 9,50cm de ancho, 12,50cm de largo.
4867515	Carmen Rossana	Paniagua Cordobez	FEMENINO	\N	PARAGUAYO/A	0994354269	UNION DE HECHO	\N	\N	\N	SI	NO	558	San Cayetano	\N	\N	\N	0	.
1210779	Anastacia	Paniagua	FEMENINO	2054-09-07	PARAGUAYO/A	0971165397	SOLTERO/A	\N	\N	\N	SI	NO	671	23 de Junio	\N	\N	\N	0	La hija mayor de la titular cuenta con una discapacidad mental, se medica todos los días, no habla
4275504	Fermina	Paredes Andino	FEMENINO	1978-11-10	PARAGUAYO/A	0992684130	SOLTERO/A	\N	\N	\N	SI	NO	2473	12 de Octubre e/ Virgen de Lujan	\N	\N	\N	5	La titular refiere que fue beneficiada con un casa en la Chacarita.
6014578	Claudia	Paredes Franco	FEMENINO	\N	PARAGUAYO/A	0971975439	UNION DE HECHO	\N	\N	\N	SI	NO	160	Ursicino Velasco	\N	\N	\N	0	.
1334067	Nilsa Ramona	Paredes Giménez	FEMENINO	\N	PARAGUAYO/A	0982861522	UNION DE HECHO	\N	\N	\N	SI	NO	1881	Ursicino Velasco c/ Julio Benitez	\N	\N	\N	0	Tiene una casa de Pro-Vida ,la hija esta aconcubinada hace un ano.
4573509	María Magdalena	Paredes Martínez	FEMENINO	\N	PARAGUAYO/A	0983854094	SOLTERO/A	\N	\N	\N	SI	NO	841	San Cayetano	\N	\N	\N	0	La Sra Maria vive bajo el mismo techo de su hna que es la titular . La misma padece de tuberculosis , pero tratada.La Sra Maria vive hace3 años en el barrio , antes estaba reducida en el Buen Pastor por 5 años.
4573508	Rosalina	Paredes Martínez	FEMENINO	\N	PARAGUAYO/A	0985802526	UNION DE HECHO	\N	\N	\N	SI	NO	840	San Cayetano	\N	\N	\N	0	La familia hace 8 años que en el bañado antes vivian en sajonia en alquiler, bajo el mismo techo vive la herman de la titular con un menor de edad
3559476	Cinthia Carolina	Paredes Mereles	FEMENINO	\N	PARAGUAYO/A	0984438267	SEPARADO/A	\N	\N	\N	SI	NO	1660	Virgen de Guadalupe	\N	\N	\N	0	Es de losa no concluida en la parte de arriba esta preparada para dos piezas mas.
4908648	Elvio Javier	Paredes Pereira	MASCULINO	1993-04-04	PARAGUAYO/A	0972744640	UNION DE HECHO	\N	\N	\N	SI	NO	808	San Cayetano	\N	\N	\N	0	La pareja lleva cinco años de concubinato, tienen techo precario aparte, viven en el lote de la madre
563	Marcelo Miguel	Paredes	MASCULINO	\N	PARAGUAYO/A	0972148693	UNION DE HECHO	\N	\N	\N	SI	NO	2356	Nuestra Sra de la Asunción	\N	\N	\N	6	La pareja lleva 4 anos de concubinato.
253697	Acela Prota	Parra de González	FEMENINO	2040-11-09	PARAGUAYO/A	0986364214	CASADO/A	\N	\N	\N	SI	NO	2227	Mto.Ursicino Velasco	\N	\N	\N	0	.
341	Parroquia San Felipe y Santiago	.	.	\N	.	0961166494	.	\N	\N	\N	SI	NO	2462	Ursicino Velasco	\N	\N	\N	0	Funciona desde el año 1947,dentro del predio funciona 1 salon multiuso y 3 oficinas de 11,50x15,50 mts.
3249	Pasillo sin nombre	.	.	\N	.	.	.	\N	\N	\N	SI	NO	2522	Pasillo sin nombre	\N	\N	\N	0	Casa en construcción. Según los vecinos nunca fue abitada. Vecinos desconocen el nombre del propietario.
2374456	Alcides Fernando	Pedroso Bareiro	MASCULINO	1974-01-11	PARAGUAYO/A	0991396450	SOLTERO/A	\N	\N	\N	SI	NO	1079	Martin Fierro	\N	\N	\N	0	.
518170	Victorina	Pedroso de Ramírez	MASCULINO	2040-06-03	PARAGUAYO/A	422071	CASADO/A	\N	\N	\N	SI	NO	1926	Ursicino Velasco	\N	\N	\N	0	La Sra. Cobra Tercera Edad, y el sr. Cobra Jubilacion.
3395548	Arcenio Santiago	Pedrozo Mencia	MASCULINO	1978-08-05	PARAGUAYO/A	0985158832	UNION DE HECHO	\N	\N	\N	SI	NO	2633	Santa Librada	\N	\N	\N	0	Pagan Arrendamiento del terreno.
1559192	María Yeni	Pedrozo Mencia	FEMENINO	\N	PARAGUAYO/A	0984908129	CASADO/A	\N	\N	\N	SI	NO	2634	Santa Librada	\N	\N	\N	0	.
4186416	Juana Cristina	Pena Ramírez	MASCULINO	\N	PARAGUAYO/A	0985233269	UNION DE HECHO	\N	\N	\N	SI	NO	1278	26 de Julio	\N	\N	\N	0	Tiene una Notebook.
3522383	Victoria Elizabeth	Pena Ramírez	MASCULINO	1981-12-04	PARAGUAYO/A	0983496009	UNION DE HECHO	\N	\N	\N	SI	NO	1688	San Martin de Porres	\N	\N	\N	0	Cocina a Gas 1.La pareja viven juntos hace 7 anos anteriormente vivian en la casa los padres (Blas Pena).
2836180	Emilce Ramona	Penayo Cardozo	FEMENINO	\N	PARAGUAYO/A	0982865390	UNION DE HECHO	\N	\N	\N	SI	NO	1784	Bañado Koeti	\N	\N	\N	5	Es propietaria del lote sus hijos viven en el mismo lote una de ellas cuenta con vivienda propia.
3527461	Yessica Viviana	Penayo Cardozo	FEMENINO	1993-02-12	PARAGUAYO/A	0982394170	UNION DE HECHO	\N	\N	\N	SI	NO	1786	Bañado koeti	\N	\N	\N	5	La vivienda fue cedida por la mama lleva acompanados 2 anos.
2383367	Romilda	Penayo de Báez	FEMENINO	\N	PARAGUAYO/A	0984835204	.	\N	\N	\N	SI	NO	1643	San Luis Rancho Ocho	\N	\N	\N	0	.
4186433	Cynthia Carolina	Peña Ramírez	FEMENINO	\N	PARAGUAYO/A	0982116444	UNION DE HECHO	\N	\N	\N	SI	NO	2101	Nasaindy	\N	\N	\N	0	El propeitario cuneta con una Panaderia en su terreno en una construcción de 5x9 ,hace 6 anos que viene trabajando en ella.
4600558	Claudia Elizabeth	Peña	FEMENINO	1984-03-04	PARAGUAYO/A	0994534098	UNION DE HECHO	\N	\N	\N	SI	NO	1596	Yacare Pito	\N	\N	\N	5	.
450	María Inés	Peralta Caballero	FEMENINO	\N	PARAGUAYO/A	0983366245	UNION DE HECHO	\N	\N	\N	SI	NO	2285	Nuestra Sra. De la Asunción	\N	\N	\N	0	Refiere ser la duena del lote pero esta a nombre de su hijo ya quela Sra. Maria Ines no cxuenta con Cedula de Identidad.
1570461	Patricia	Peralta de Samudio	FEMENINO	\N	PARAGUAYO/A	0985528752	SEPARADO/A	\N	\N	\N	SI	NO	2026	Mbo'ehara	\N	\N	\N	0	Vive con la propietaria en el mismo terreno.
5214604	Deysi	Peralta Gavilan	FEMENINO	1995-01-10	PARAGUAYO/A	0984 867994	UNION DE HECHO	\N	\N	\N	SI	NO	1755	Pyagua Naranja	\N	\N	\N	0	.
3176507	Edgar Osavldo	Peralta Gómez	MASCULINO	\N	PARAGUAYO/A	0992531185	UNION DE HECHO	\N	\N	\N	SI	NO	1958	.	\N	\N	\N	0	Telefono: Celulares 7, Linea Baja 1.
1834618	Amada	Peralta López	FEMENINO	2064-09-10	PARAGUAYO/A	0981168926	CASADO/A	\N	\N	\N	SI	NO	2278	Nuestra Sra. De la Asunción	\N	\N	\N	0	.
1834617	Ángel	Peralta López	MASCULINO	1970-01-03	PARAGUAYO/A	0985528752	SEPARADO/A	\N	\N	\N	SI	NO	2027	Mbo'ehara	\N	\N	\N	5	Tiene una cosbtrucción de 10x9 bien constituida en el terreno de la propietaria.
1834619	Mercedes	Peralta López	FEMENINO	\N	PARAGUAYO/A	0982522830	SOLTERO/A	\N	\N	\N	SI	NO	1915	Ursicino Velasco c/ Sotero	\N	\N	\N	0	.
4780275	Cinthia Carolina	Peralta Rotela	FEMENINO	1989-08-05	PARAGUAYO/A	0981792018	UNION DE HECHO	\N	\N	\N	SI	NO	144	San Felipe Y Santiago	\N	\N	\N	0	.
5512886	Gladys Carolina	Peralta	FEMENINO	\N	PARAGUAYO/A	0986390617	UNION DE HECHO	\N	\N	\N	SI	NO	508	Malvinas e/ María Elena	\N	\N	\N	0	.
2499892	Ilsa	Peralta	FEMENINO	\N	PARAGUAYO/A	0992264200	CASADO/A	\N	\N	\N	SI	NO	519	Malvinas c/ Ursicino Velasco	\N	\N	\N	4	En el lote hay otra vivienda de material donde vive su nuera , su pareja e hijo , su casa mide 4x6.
2701416	José Manuel	Peralta	MASCULINO	\N	PARAGUAYO/A	0971608281	CASADO/A	\N	\N	\N	SI	NO	424	Julio Jara c/ Nuevo amanecer	\N	\N	\N	4	.
6244677	Lia Gabriela	Pereira Acosta	FEMENINO	\N	PARAGUAYO/A	0984662646	UNION DE HECHO	\N	\N	\N	SI	NO	528	Malvinas	\N	\N	\N	0	.
4484970	Mirtha Rossana	Pereira Acosta	FEMENINO	1988-07-03	PARAGUAYO/A	0983 970913	UNION DE HECHO	\N	\N	\N	SI	NO	1748	Virgen de Guadalupe	\N	\N	\N	0	Viv ene le mismo terren de la propietaria, en una construcción de 5x5, precaria.
5869356	Laura Viviana	Pereira Amaro	FEMENINO	1992-02-01	PARAGUAYO/A	0972492109	SOLTERO/A	\N	\N	\N	SI	NO	1757	Koety	\N	\N	\N	0	.
2605512	Vicente Ramón	Pereira Benítez	MASCULINO	\N	PARAGUAYO/A	0971769255	SOLTERO/A	\N	\N	\N	SI	NO	623	San Cayetano	\N	\N	\N	0	Es dueno donde viven su hijo con su esposa e hijos cedio la vivienda a su hijo , el dueno del lote vive en el fondo en una vivienda precaria.
5189665	Anibal Javier	Pereira Britez	MASCULINO	\N	PARAGUAYO/A	0992938552	.	\N	\N	\N	SI	NO	2744	.	\N	\N	\N	0	El sr. Anibal tiene pareja estable y un hijo, viven en la casa de la madre en la Zona 3 (M46-L20B), no pudieron terminar su construcción en el terreno porque el agua sigue subiendo y por esa razón no se mudan.
5004146	Estela Mary	Pereira Brítez	FEMENINO	\N	PARAGUAYO/A	0991244961	UNION DE HECHO	\N	\N	\N	SI	NO	106	.	\N	\N	\N	3	.
4012703	Geronima Felicita	Pereira Brítez	FEMENINO	\N	PARAGUAYO/A	0992423655	UNION DE HECHO	\N	\N	\N	SI	NO	379	YVOTY y Las Orquideas	\N	\N	\N	0	La pareja llevan acompanados 16 anos. La pieza que utilizan como cocina es precaria (terciada ,Eternit).
5004143	Sonia Elizabeth	Pereira Brítez	FEMENINO	\N	PARAGUAYO/A	0992976532	UNION DE HECHO	\N	\N	\N	SI	NO	228	Las Orquideas	\N	\N	\N	0	Este techo se encuentra en el mismo lote de la madre.
4135440	Andrea Lorena	Pereira de Aguilar	FEMENINO	\N	PARAGUAYO/A	0981120245	SEPARADO/A	\N	\N	\N	SI	NO	622	San Felipe y Santiago	\N	\N	\N	0	La hija de la titular Andrea Aguilar cuenta con una discapacidad,(no habla, no escucha). La titular esta separada hace dos (2) años.
5161565	Nelly Noemi	Pereira de Núñez	FEMENINO	\N	PARAGUAYO/A	097187014	CASADO/A	\N	\N	\N	SI	NO	852	Sin nombre	\N	\N	\N	0	.
4690930	Vivian Rossana	Pereira Duarte	FEMENINO	\N	PARAGUAYO/A	0982237579	UNION DE HECHO	\N	\N	\N	SI	NO	2625	Ramon Talavera	\N	\N	\N	6	.
1468113	Enriqueta	Pereira Escobar	FEMENINO	\N	PARAGUAYO/A	0985916689	VIUDO/A	\N	\N	\N	SI	NO	1391	Bañado koeti	\N	\N	\N	0	.
3987751	Gladys Carolina	Pereira Flores	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	566	8 de Diciembre	\N	\N	\N	0	La Sra Gladys es responsable de la casa hace 1 ano
1685266	Beatriz	Pereira Martínez	FEMENINO	\N	PARAGUAYO/A	0986134534	CASADO/A	\N	\N	\N	SI	NO	2055	Ursicino Velasco	\N	\N	\N	0	Tiene un comedor y un Garage.
4160158	Blanca Ramona	Pereira Martínez	FEMENINO	1987-12-01	PARAGUAYO/A	0972753846	SOLTERO/A	\N	\N	\N	SI	NO	2061	Ursicino Velasco c/ Sotero Colman	\N	\N	\N	0	.
4942841	Francisca Leonor	Pereira Martínez	FEMENINO	1987-08-08	PARAGUAYO/A	0986442473	UNION DE HECHO	\N	\N	\N	SI	NO	2095	Nasaindy	\N	\N	\N	0	.
5287066	Mabel Ramona	Pereira Martínez	FEMENINO	\N	PARAGUAYO/A	0984154491	UNION DE HECHO	\N	\N	\N	SI	NO	2057	Ursicino Velasco	\N	\N	\N	0	.
3641883	Roberto Carlos	Pereira Martínez	MASCULINO	\N	PARAGUAYO/A	0991740195	.	\N	\N	\N	SI	NO	1943	Ursicino Velasco	\N	\N	\N	0	El sr posee un taller en parte trasera del terreno con un tinglado de 180 m2. funcionando actualmente con empleados aparte de la casa es de dos pisos.
6326208	Vilma Elizabeth	Pereira Núñez	FEMENINO	\N	PARAGUAYO/A	0972651752	UNION DE HECHO	\N	\N	\N	SI	NO	678	Yacaré Yrupe	\N	\N	\N	0	comparte datos con cassa 02.A1
4004531	Marcos Vicente	Pereira Romero	MASCULINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	2212	Mto. Ursicino Velasco	\N	\N	\N	0	Actualmente la flia. Esta viviendo en alquiler porque el agua hecho toda la vivienda en etos dias ya va a empezar a trabajar por la casa. Tiene un contrato de 8 meses en el alquiler.
3198123	Lorena Beatriz	Pereira Valiente	FEMENINO	\N	PARAGUAYO/A	.	SEPARADO/A	\N	\N	\N	SI	NO	344	Aromita	\N	\N	\N	0	.
4235231	Adriana Leticia	Pereira	FEMENINO	\N	PARAGUAYO/A	0962248244	UNION DE HECHO	\N	\N	\N	SI	NO	625	San Felipe y Santiago	\N	\N	\N	0	La pareja tiene 8 años de concubinato.
1327	Catalina	Pereira	FEMENINO	\N	No quiso censar	.	.	\N	\N	\N	SI	NO	2645	Pajagua Naraja	\N	\N	\N	0	No quiso censarse.
6590923	Cintia Soledad	Pereira	FEMENINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	1694	San Martin de Porres	\N	\N	\N	0	Según vecinos la mama es adicta y vendedora de drogas ,la hija se encuentra en un albergue.
7184305	Dahiana Elizabeth	Pereira	FEMENINO	\N	PARAGUAYO/A	0984864445	UNION DE HECHO	\N	\N	\N	SI	NO	1325	San Felipe y Santiago	\N	\N	\N	0	La pareja lleva 4 anos de concubinato.
2948815	Eugenia	Pereira	FEMENINO	2058-03-09	PARAGUAYO/A	0985734380	UNION DE HECHO	\N	\N	\N	SI	NO	1050	Yvoty Rory	\N	\N	\N	0	.
5591388	Evelina Beatriz	Pereira	FEMENINO	\N	PARAGUAYO/A	0991253955	SOLTERO/A	\N	\N	\N	SI	NO	806	San Cayetano	\N	\N	\N	0	La titular es madre soltera, trabaja en una empresa privada como limpiadora tres veces por semana, y también es empleada doméstica tres vecs por semana. Está separada hace ocho meses del padre de su último hijo, no recibe apoyo económico de ninguno de lo
2446569	Fátima Elodia	Pereira	FEMENINO	\N	PARAGUAYO/A	0991818140	UNION DE HECHO	\N	\N	\N	SI	NO	1042	Mar de Plata E/ Nuevo Amanecer	\N	\N	\N	4	La dueña le alquila unua habitación y un baño independiente al sñor Carlos Bello. Dentro del mismo lote y techo viven dos familias más de los hijos de la dueña.
2374910	Gilda	Pereira	FEMENINO	2057-01-09	PARAGUAYO/A	0984512474	SOLTERO/A	\N	\N	\N	SI	NO	807	San Cayetano	\N	\N	\N	0	La titular del lote, tiene una casa precaria, es viuda y tiene trabajo ocacionales, dentro del 
1223139	Gilda	Pereira	FEMENINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	410	Mar del Plata	\N	\N	\N	0	.
2949962	Ladislaa	Pereira	FEMENINO	\N	PARAGUAYO/A	0971720471	.	\N	\N	\N	SI	NO	77	Angel Luis	\N	\N	\N	0	1 Capilla y una pieza mas.
4212740	Lidia Mabel	Pereira	FEMENINO	1981-04-09	PARAGUAYO/A	0982525901	UNION DE HECHO	\N	\N	\N	SI	NO	14	San Felipe	\N	\N	\N	0	Su hijo y su nuera , con su bebé viven en la misma casa.
4197447	María Gloria	Pereira	FEMENINO	1977-05-09	PARAGUAYO/A	0984512474	.	\N	\N	\N	SI	NO	70	Angel Luis  San Felipe	\N	\N	\N	0	.
3107	Oscar	Pereira	MASCULINO	\N	No quiso censar	.	.	\N	\N	\N	SI	NO	2063	Sotero Colman	\N	\N	\N	0	No quiso censarse.
2374911	Venancia	Pereira	FEMENINO	2053-11-04	PARAGUAYO/A	0982314309	SOLTERO/A	\N	\N	\N	SI	NO	631	San Felipe y Santiago	\N	\N	\N	0	.
3635149	Ángel Daniel	Pérez Agüero	MASCULINO	1987-04-06	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	2518	.	\N	\N	\N	0	La casa y el Lote es una herencia familiar de la abuela.
4676345	Barbara	Pérez Agüero	FEMENINO	\N	PARAGUAYO/A	0983924753	UNION DE HECHO	\N	\N	\N	SI	NO	2189	Sotero Colman	\N	\N	\N	0	Nacio en el barrio, hace 7 años que se acompañó
3635148	Juan Manuel	Pérez Agüero	MASCULINO	1983-11-10	PARAGUAYO/A	0982250026	SOLTERO/A	\N	\N	\N	SI	NO	1595	Ramon Talavera	\N	\N	\N	0	Actualmente vive en la casa de su mama detrás del Escula Sotero Colman según el censado , no se muda porque tiene miedo de la subida del Rio.Dentro del lote hay un cimiento.
4010726	Liliana Patricia	Pérez Chávez	FEMENINO	1984-02-03	PARAGUAYO/A	0986624705	UNION DE HECHO	\N	\N	\N	SI	NO	1475	Mainumby c/Fidelina	\N	\N	\N	0	La pareja lleva 14 anos de concubinato.Hace 10 anos que viven en su lote (construccion de material).Hace 4 anos viven en la otra construccion que fue beneficiada por la Cooperativa Banado Poty,el lote cuenta con dos casas de materiales.
4088077	Abrahan	Pérez	MASCULINO	1974-08-10	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	49	San Felipe y Santiago	\N	\N	\N	3	.
2142333	Ireneo	Pérez	MASCULINO	1973-05-05	PARAGUAYO/A	0982762102	SOLTERO/A	\N	\N	\N	SI	NO	24	San Felipe y Santiago	\N	\N	\N	3	.
1476014	Juan Ruben	Pérez	MASCULINO	\N	PARAGUAYO/A	0981579430	SOLTERO/A	\N	\N	\N	SI	NO	1593	Ramon Talavera	\N	\N	\N	0	Es propietario del lote y la vivienda, actualmente vive detrás de la Escuela Sotero Colman con su pareja e hijos.Presto a su nuera para que viva temporalmente.
3328	María Gloria	Pérez	FEMENINO	1981-05-01	PARAGUAYO/A	0986132369	UNION DE HECHO	\N	\N	\N	SI	NO	23	San Felipe	\N	\N	\N	3	10 anos vivieron en la zona 3 y hace 2 se mudaron en zona 4.
5763077	Marli Melisa	Pérez	FEMENINO	\N	PARAGUAYO/A	0991310761	SOLTERO/A	\N	\N	\N	SI	NO	25	RECICLA	\N	\N	\N	0	.
2916006	Roberto Carlos	Pérez	MASCULINO	\N	PARAGUAYO/A	0982256863	CASADO/A	\N	\N	\N	SI	NO	430	San Cayetano	\N	\N	\N	0	Este predio pertenece a ala Iglesia Nande Jara Roga . Es un casa pastoral donde viven Roberto Perez Y su flia. Comm encargados.
2578392	Vicenta	Pérez	FEMENINO	\N	PARAGUAYO/A	0982222570	SOLTERO/A	\N	\N	\N	SI	NO	22	San Felipe	\N	\N	\N	3	.
2960263	Zunilda	Pérez	FEMENINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	230	San Felipe y Santiago	\N	\N	\N	0	Los Datos fueron proporcionados por la madre.
4752416	Edith Dolores	Pettengil Fernández	FEMENINO	1990-06-04	PARAGUAYO/A	0986941161	UNION DE HECHO	\N	\N	\N	SI	NO	1186	Fidelina	\N	\N	\N	0	.
2231989	Carlos Alberto	Piatti	MASCULINO	1973-07-08	PARAGUAYO/A	0982921269	SOLTERO/A	\N	\N	\N	SI	NO	2491	Sotero Colman	\N	\N	\N	0	Carlos Alberto Piatti refiere que el terreno era de la madre, al fallecer le deja al hijo la casa
4848266	Susana Beatriz	Pinto	FEMENINO	1993-12-02	PARAGUAYO/A	0982248589	SOLTERO/A	\N	\N	\N	SI	NO	1494	Mainumby	\N	\N	\N	0	Susana alquila de Candida una piecita aparte construida de terciada.Tiene un hijo con Cedula Argentina.Es pobladora del banado desde que nacio.
653598	Marina Margarita	Pintos Ferreira	FEMENINO	2053-10-06	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	1266	.	\N	\N	\N	0	.
3583380	Marcos Marcial	Pintos Villalba	MASCULINO	1988-07-09	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	2337	Villa Asunción	\N	\N	\N	6	.
1278524	Roberto Ramón	Piñerua	MASCULINO	2056-01-03	PARAGUAYO/A	0984856157	UNION DE HECHO	\N	\N	\N	SI	NO	2153	San Martin de Porres	\N	\N	\N	0	.
2158080	Daniel	Pirelli DeLeón	MASCULINO	2067-08-07	PARAGUAYO/A	0986859411	CASADO/A	\N	\N	\N	SI	NO	1776	Bañado Koeti	\N	\N	\N	5	.
1214403	Feliciana	Piriz de Cano	FEMENINO	\N	PARAGUAYO/A	0981135927	SOLTERO/A	\N	\N	\N	SI	NO	2253	Mto. Ursicino Velasco	\N	\N	\N	0	.
1220603	Teresa Asuncion	Pizzani	FEMENINO	2067-07-02	PARAGUAYO/A	0983346163	CASADO/A	\N	\N	\N	SI	NO	1345	Primavera	\N	\N	\N	0	.
4526	Plaza Sin Nombre	.	.	\N	.	.	.	\N	\N	\N	SI	NO	2732	Aromita	\N	\N	\N	0	Espacio Publico destinado a recreación y deportes de niños y adultos.Lo conocen como Plaza Aromita.
3177914	Verónica Valentina	Portillo Benítez	FEMENINO	\N	PARAGUAYO/A	0984551788	SEPARADO/A	\N	\N	\N	SI	NO	332	Mar del Plata	\N	\N	\N	0	La Sra. Es del barrio pero esta en alquiler.
4517013	Pedro Ariel	Portillo Caballero	MASCULINO	\N	PARAGUAYO/A	0995955338	SOLTERO/A	\N	\N	\N	SI	NO	809	Aromita	\N	\N	\N	0	Se separo hace una semana de su pareja
1499860	Gilberto Antonio	Portillo Núñez	MASCULINO	2067-02-02	PARAGUAYO/A	0981235086	UNION DE HECHO	\N	\N	\N	SI	NO	1168	Fidelina	\N	\N	\N	0	.
2395949	Verónica Griselda	Portillo Ozuna	FEMENINO	\N	PARAGUAYO/A	0994313339	UNION DE HECHO	\N	\N	\N	SI	NO	133	Virgen de Lujan	\N	\N	\N	0	.
5136661	Dario	Portillo Ramirez	MASCULINO	\N	PARAGUAYO/A	0986824112	.	\N	\N	\N	SI	NO	2370	Nuestra señora de la Asuncion	\N	\N	\N	6	.
4446463	Darío Ramón	Portillo Ramírez	MASCULINO	\N	PARAGUAYO/A	099449540	UNION DE HECHO	\N	\N	\N	SI	NO	803	Virgen de Lujan y Aromita	\N	\N	\N	0	.
4985368	Erika Gabriela	Portillo Recalde	FEMENINO	\N	PARAGUAYO/A	0961606710	UNION DE HECHO	\N	\N	\N	SI	NO	1171	Fidelina	\N	\N	\N	0	Comparte datos con el 8-A1
4985345	Jessica Fermina	Portillo Recalde	FEMENINO	1990-02-06	PARAGUAYO/A	0981369888	SOLTERO/A	\N	\N	\N	SI	NO	1173	Fidelina	\N	\N	\N	0	.
4954783	Marían Monserrat	Portillo Recalde	FEMENINO	1992-09-04	PARAGUAYO/A	0994496367	SOLTERO/A	\N	\N	\N	SI	NO	1170	Fidelina	\N	\N	\N	0	Viven en la misma casa con Gilberto Portillo.
1515659	Olga	Portillo Santacruz	FEMENINO	1971-05-06	PARAGUAYO/A	0992238639	UNION DE HECHO	\N	\N	\N	SI	NO	529	16 de Agosto c/ Malvinas	\N	\N	\N	0	.
5024928	Leda Raquel	Portillo Silvano	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1101	Fidelina	\N	\N	\N	0	.
4041	Predio Vacio	.	.	\N	.	.	.	\N	\N	\N	SI	NO	1147	.	\N	\N	\N	0	Los vecinos desconocen el nombre del propietario.
4011	Guido David	Predio Vacio - Morales Aguayo	MASCULINO	\N	.	.	.	\N	\N	\N	SI	NO	2749	.	\N	\N	\N	0	.
3448129	Inocencia	Prieto de Marecos	FEMENINO	\N	PARAGUAYO/A	0983747618	.	\N	\N	\N	SI	NO	1169	Urcicino Velazco	\N	\N	\N	0	.
2173003	Beatriz	Prieto de Rotela	FEMENINO	\N	PARAGUAYO/A	0981981794	CASADO/A	\N	\N	\N	SI	NO	1119	Virgen de Lourdes C/ Virgen de Lujan	\N	\N	\N	0	.
5444740	Elena Noemi	Prieto Medina	FEMENINO	1992-07-04	PARAGUAYO/A	0991457134	SOLTERO/A	\N	\N	\N	SI	NO	1203	Fidelina	\N	\N	\N	0	.
5491956	Jessica	Prieto Medina	FEMENINO	\N	PARAGUAYO/A	0971398801	UNION DE HECHO	\N	\N	\N	SI	NO	1202	Fidelina	\N	\N	\N	0	.
4013679	Mirian Karina	Prieto Medina	FEMENINO	\N	PARAGUAYO/A	0982888579	UNION DE HECHO	\N	\N	\N	SI	NO	1260	Fidelina	\N	\N	\N	0	La vivienda es de la Cooperativa Banado Poty de dimension 6x9 mts2.
5491957	Mirta Beatriz	Prieto Medina	FEMENINO	1993-07-09	PARAGUAYO/A	0991695881	UNION DE HECHO	\N	\N	\N	SI	NO	1016	Mar de Plata	\N	\N	\N	0	.
5351144	Reina Soledad	Prieto Medina	FEMENINO	\N	PARAGUAYO/A	0992660333	UNION DE HECHO	\N	\N	\N	SI	NO	1201	Fidelina	\N	\N	\N	0	.
282	Puesto Policial	.	.	\N	.	.	.	\N	\N	\N	SI	NO	1060	Ursiscino Velasco	\N	\N	\N	0	.
2478058	Francisco Domingo	Quintana Adorno	MASCULINO	1984-04-08	PARAGUAYO/A	0984853845	UNION DE HECHO	\N	\N	\N	SI	NO	838	Ursicino Velasco	\N	\N	\N	0	.
4652329	Osmar	Quintana Arce	MASCULINO	\N	PARAGUAYO/A	0984167033	UNION DE HECHO	\N	\N	\N	SI	NO	1982	Nasaindy	\N	\N	\N	0	.
5494726	Rosalia	Quintana Arce	FEMENINO	\N	PARAGUAYO/A	0994827602	UNION DE HECHO	\N	\N	\N	SI	NO	2594	.	\N	\N	\N	0	Vive dentro del lote de la suegra cuenta con vivienda propia de material.
6566325	Melissa Jeanette	Quintana Arrúa	FEMENINO	1997-08-10	PARAGUAYO/A	0984530624	UNION DE HECHO	\N	\N	\N	SI	NO	2002	Ñasaindy	\N	\N	\N	5	Es propietario del lote y la casa con ella vive su mama con 4 hijos, lleva 1 ano de concubinato.
5710201	Antoliana	Quintana Báez	FEMENINO	1994-04-04	PARAGUAYO/A	0992826312	UNION DE HECHO	\N	\N	\N	SI	NO	903	Martin Fierro	\N	\N	\N	3	.
6843738	Jonathan	Quintana Baez	MASCULINO	\N	PARAGUAYO/A	0985341587	UNION DE HECHO	\N	\N	\N	SI	NO	906	Martin Fierro	\N	\N	\N	5	Jonathan Tiene esa Construccion hace 2 años.
6845040	Yenifer	Quintana Báez	FEMENINO	1996-07-02	PARAGUAYO/A	0962276608	UNION DE HECHO	\N	\N	\N	SI	NO	910	Martin Fierro	\N	\N	\N	5	Vive en el terreno del padre.
582892	Natividad	Quintana de Dávalos	FEMENINO	\N	PARAGUAYO/A	0972195266	CASADO/A	\N	\N	\N	SI	NO	1731	Ursicino Velasco	\N	\N	\N	0	Dentro de la casa del titular vive una pareja que tiene 7 anos de concubinato, se manejan de manera independiente.
3296798	Edith Gloria	Quintana Duarte	FEMENINO	1974-10-02	PARAGUAYO/A	0994696173	UNION DE HECHO	\N	\N	\N	SI	NO	962	Angel Luis c/ Virgen de Lourdes	\N	\N	\N	0	.
5189095	Griselda	Quintana Duarte	FEMENINO	1985-08-02	PARAGUAYO/A	0984983994	SOLTERO/A	\N	\N	\N	SI	NO	971	Angel Luis c/ Virgen Lourdes	\N	\N	\N	0	.
2146680	Nicolása	Quintana Duarte	MASCULINO	\N	PARAGUAYO/A	0986445116	SOLTERO/A	\N	\N	\N	SI	NO	935	Angel Luis C/ Virgen de Lourdes	\N	\N	\N	0	La Sra. Nicolasa  es la propietaria del terreno M40-L15.
1190774	Candida Rosa	Quintana Echeverría	FEMENINO	\N	PARAGUAYO/A	0981369834	.	\N	\N	\N	SI	NO	2070	Sotero Colman	\N	\N	\N	0	desde hace 12 años que vive con la hna, desde que se entero que tiene cancer e mama, al cual derrotó después de un largo tratamiento
840799	Eugenia	Quintana Echeverría	FEMENINO	2053-05-09	PARAGUAYO/A	0984741445	.	\N	\N	\N	SI	NO	2477	Sotero Colman	\N	\N	\N	0	.
3606148	Robert	Quintana Fariña	MASCULINO	\N	PARAGUAYO/A	0982498688	UNION DE HECHO	\N	\N	\N	SI	NO	2091	Sotero Colman	\N	\N	\N	0	Tiene una construcción al costado de la casa de la mamá se maneja independientemente su entrada es aparte a la de la madre.
4982113	Antonio Javier	Quintana Fleitas	MASCULINO	1990-12-06	PARAGUAYO/A	0985360796	.	\N	\N	\N	SI	NO	1978	Sotero Colman	\N	\N	\N	0	Siempre vivio en el lugar, y hace dos años que se acompaño
4982114	David Daniel	Quintana Fleitas	MASCULINO	\N	PARAGUAYO/A	0983984061	UNION DE HECHO	\N	\N	\N	SI	NO	1971	Sotero Colman	\N	\N	\N	0	.
4732427	Marlene Carolina	Quintana Fleitas	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	478	23 de Junio	\N	\N	\N	0	.
3537429	Milciades Ramón	Quintana Genes	MASCULINO	1982-10-07	PARAGUAYO/A	0982292996	UNION DE HECHO	\N	\N	\N	SI	NO	1974	Sotero olman	\N	\N	\N	0	el señor dice que tine su terreno n el chiquero y no le enumeraron la casa por ahora esta con la suegra
5861779	María Eugenia	Quintana Jara	FEMENINO	1996-12-09	PARAGUAYO/A	0986752375	.	\N	\N	\N	SI	NO	2540	Mandy Jura	\N	\N	\N	0	Vive dentro del lote y la casa de su cunada .Tiene lote pero no vivienda no construyo despues de la inundación.
4780262	Jazmín Andrea	Ramírez Rotela	FEMENINO	1991-10-06	PARAGUAYO/A	0985262261	UNION DE HECHO	\N	\N	\N	SI	NO	45	San Felipe y Santiago	\N	\N	\N	0	.
4529490	Mauro Gabriel	Quintana Olmedo	MASCULINO	1989-01-01	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	958	Ursicino Velasco	\N	\N	\N	0	Comparte Datos con el Lote 3-A1.
1744121	Alberto Rene	Quintana Sánchez	MASCULINO	\N	PARAGUAYO/A	0982677910	CASADO/A	\N	\N	\N	SI	NO	1876	Villa Maria Auxiliadora	\N	\N	\N	0	Hace 4 anos que esta legamente casado.
4469128	Edgar Manuel	Quintana Sánchez	MASCULINO	1986-05-03	PARAGUAYO/A	0994738640	CASADO/A	\N	\N	\N	SI	NO	1875	Villa Maria Auxiliadora	\N	\N	\N	0	.
1002595	Tomasa	Quintana Vda de Acuña	FEMENINO	\N	PARAGUAYO/A	0972766963	VIUDO/A	\N	\N	\N	SI	NO	304	Virgen de Lourdes esq. Yvoty	\N	\N	\N	0	.
1637275	Alberto Rene	Quintana	MASCULINO	2057-05-06	PARAGUAYO/A	0982565638	CASADO/A	\N	\N	\N	SI	NO	1874	Villa Maria Auxiliadora	\N	\N	\N	0	.
4653708	Carlos Euclides	Quintana	MASCULINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	956	Angel Luis C/ Virgen de Lourdes	\N	\N	\N	0	La Señora Nicolasa Quintana es la propietaria del terreno  M40 lote 015
5308340	Cintia Carolina	Quintana	FEMENINO	1986-01-07	PARAGUAYO/A	0985341587	UNION DE HECHO	\N	\N	\N	SI	NO	902	Martin Fierro	\N	\N	\N	3	Tine corredor de madera C/ Eetrenit de frente y en el costado.
664904	Eulogio	Quintana	MASCULINO	2058-11-03	PARAGUAYO/A	0992289570	CASADO/A	\N	\N	\N	SI	NO	954	Ursicino Velasco	\N	\N	\N	0	.
4984682	Katerine Selena	Quintana	FEMENINO	\N	PARAGUAYO/A	0994 630 568	CASADO/A	\N	\N	\N	SI	NO	2678	Pasillo sin nombre	\N	\N	\N	0	Es propietario del lote. Actualmente vive en la casa de la tia, su vivienda esta en reconstrucción.
4891280	Liria Yemima	Quintana	FEMENINO	1991-05-02	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	2679	Ursicino Velazco	\N	\N	\N	0	Vive dentro dl lote de la hermana.
2335712	Lucio Juan	Quintana	MASCULINO	1974-06-07	PARAGUAYO/A	0983475399	SOLTERO/A	\N	\N	\N	SI	NO	1524	Rancho 8	\N	\N	\N	0	Placa Electrica y Centrifugadora 1 c/u.
3979645	Nery Rodolfo	Quintana	MASCULINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	2733	8 de Dicimebre	\N	\N	\N	0	El Sr. No vive en su lote.
4273558	Paola Rocío	Quintana	FEMENINO	\N	PARAGUAYO/A	0992765707	SOLTERO/A	\N	\N	\N	SI	NO	769	Pilcomayoc/ Jakare Yrupe	\N	\N	\N	0	La vivienda cuenta con dos galpones uno en frente y el otro en la parte trasera.
3694245	Mirtha Teresita	Quintero López	FEMENINO	1985-02-02	PARAGUAYO/A	021 422829	UNION DE HECHO	\N	\N	\N	SI	NO	1906	Ursicino velasco y Julio Benitez	\N	\N	\N	0	.
2111952	Marciana	Quinto Pérez	FEMENINO	2046-06-03	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1434	Ursicino Velasco	\N	\N	\N	0	La titular y su esposo cobran Tercera Edad.
6300990	Claudia Rafaela	Quiñonez Arzamendia	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	312	Virgen de Lourdes Y Mar del Plata	\N	\N	\N	0	.
6306482	Samuel	Quiñonez Arzamendia	MASCULINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	313	Virgen de Lourdes Y Mar del Plata	\N	\N	\N	0	.
105316	Teresa	Ramirez Acosta	FEMENINO	\N	PARAGUAYO/A	0982 530 266	SOLTERO/A	\N	\N	\N	SI	NO	1918	Ursicino Velazco	\N	\N	\N	0	Es propietaria del lote y de la casa. Con ella vive un hijo mayor de 25 años con su pareja e hijos.
5866007	Maira	Ramírez Agüero	FEMENINO	\N	PARAGUAYO/A	0984604370	SOLTERO/A	\N	\N	\N	SI	NO	2299	Nuestra Sra. De la Asunción	\N	\N	\N	0	Cocina a Gas 1.La flia. No cuenta con lote ni vivienda,actualmente alquila una habitación y un bano.
4156145	Juan Manuel	Ramírez Arriola	MASCULINO	\N	PARAGUAYO/A	0981 738 947	UNION DE HECHO	\N	\N	\N	SI	NO	1869	.	\N	\N	\N	0	.
4661286	Liliana María	Ramírez Báez	FEMENINO	1988-03-12	PARAGUAYO/A	0972441118	CASADO/A	\N	\N	\N	SI	NO	1846	Maria Auxiliadora	\N	\N	\N	0	.
1637068	Máxima	Ramírez Benítez	FEMENINO	\N	PARAGUAYO/A	09713434	VIUDO/A	\N	\N	\N	SI	NO	1930	Ursicino Velasco c/Puente Pesoa	\N	\N	\N	0	La hija se encuentra trabajando en Espana ,la sra. Cobra 3ra. Edad.
3410496	Myrian	Ramírez Cáceres	FEMENINO	\N	PARAGUAYO/A	0983544343	UNION DE HECHO	\N	\N	\N	SI	NO	2333	Nuestra Sra. De la Asunción	\N	\N	\N	6	Dentro de techo vive en una pareja hace dos anos de concubinato sin hijos.
2095119	Rufina	Ramírez Cáceres	FEMENINO	\N	PARAGUAYO/A	0984274079	UNION DE HECHO	\N	\N	\N	SI	NO	804	San Cayetano	\N	\N	\N	0	La pareja tiene una construccion de material cocido(ladrillo),tiene patio independiente sin division fisica.No es Fliar. De Maria Teresa Roteaqien se le puso el nro. 31,ya que es una flia totalmente independiente y ajena.
1144113	María Dorotea	Ramírez Centurión	FEMENINO	2067-06-02	PARAGUAYO/A	422071	.	\N	\N	\N	SI	NO	1927	Ursicino Velasco	\N	\N	\N	0	La sra. Cobra 3ra Edad.Medidas del Taller de carpinteri 9x 11 del hijo Roberto Cordobes y el otro hijo Pablo Cordobes.Tiene una construccion en la parte de arriba 9x12.El taller funciona hace 25 anos y es fuente de trabajo principal del hijo.
801983	María Fidencia	Ramírez de Alvarez	FEMENINO	\N	PARAGUAYO/A	0982570299	SEPARADO/A	\N	\N	\N	SI	NO	729	Angel Luis	\N	\N	\N	0	No es dueña del lote ni de la vivienda. Esta alquilado hace 5 años aproximadamente del señor Elodio Armoa, Ci: 1577.875
2852502	Ramona	Ramírez de Aquino	MASCULINO	\N	PARAGUAYO/A	0984242616	SEPARADO/A	\N	\N	\N	SI	NO	1797	Bañado Koeti	\N	\N	\N	5	.
4653598	Floria Marlene	Ramírez de Galeano	FEMENINO	\N	PARAGUAYO/A	0984419950	.	\N	\N	\N	SI	NO	455	.	\N	\N	\N	0	Vive dentro del lote y la vivienda de madera con su esposo e hijos.
1388446	Estanislaa Felicita	Ramírez de González	FEMENINO	2052-07-05	PARAGUAYO/A	.	CASADO/A	\N	\N	\N	SI	NO	2049	Sotero Colmán y Mbo'ehára	\N	\N	\N	0	La familia vive en extrema pobreza
2964490	Celina	Ramírez de Portillo	FEMENINO	1976-06-04	PARAGUAYO/A	.	CASADO/A	\N	\N	\N	SI	NO	338	Virgen de Lujan E/ Aromita	\N	\N	\N	0	La casa tiene techo de teja de tja adelante y la parte de atrás etrenit.
3370918	Susana	Ramírez de Rotela	FEMENINO	\N	PARAGUAYO/A	0991342571	CASADO/A	\N	\N	\N	SI	NO	675	San Cayetano	\N	\N	\N	0	.
710179	Eliodora	Ramírez de Varela	FEMENINO	2055-03-07	PARAGUAYO/A	0984105326	CASADO/A	\N	\N	\N	SI	NO	1559	Ursicino Velasco	\N	\N	\N	0	.
4452788	Christián Dejesus	Ramírez Franco	MASCULINO	\N	PARAGUAYO/A	0992258314	UNION DE HECHO	\N	\N	\N	SI	NO	42	San Felipe y Santiago	\N	\N	\N	0	Con CORREDOR.
4452811	Yessica	Ramírez Franco	FEMENINO	1991-09-05	PARAGUAYO/A	0992588504	UNION DE HECHO	\N	\N	\N	SI	NO	972	San Felipe y Santiago	\N	\N	\N	0	.
1467964	Isidora	Ramírez Ibarra	FEMENINO	\N	PARAGUAYO/A	098335739	CASADO/A	\N	\N	\N	SI	NO	1218	Angel de la Guardia	\N	\N	\N	0	Casa antigua remoldeada por la propietaria, era de la madre Oliva Ramona Ibarra.
6764522	Dahiana Francisca	Ramírez López	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	316	Virgen de Lourdes y Mar del Plata	\N	\N	\N	0	La pareja convive hace 3 anos.
1404681	Isabelino	Ramírez Maciel	FEMENINO	2060-12-11	PARAGUAYO/A	0991845815	UNION DE HECHO	\N	\N	\N	SI	NO	2298	Nuestra Sra. De la Asunción	\N	\N	\N	0	La flia. Tiene una casilla de ventas de comestibles.Cuenta con una inquilina.
3473684	Lilian Elizabeth	Ramírez Martínez	FEMENINO	\N	PARAGUAYO/A	0986132985	CASADO/A	\N	\N	\N	SI	NO	279	Ursicino Velasco	\N	\N	\N	0	.
1433327	Benita Agustína	Ramírez Pedroso	FEMENINO	2069-06-05	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	2627	Maria Auxiliadora	\N	\N	\N	0	La duena de la casa esta en Espana por motivos laborales sus hijos viven en la casa.
585124	Petrona Celestina	Ramírez Pedrozo	FEMENINO	\N	PARAGUAYO/A	0982494215	SEPARADO/A	\N	\N	\N	SI	NO	1729	.	\N	\N	\N	0	.
1526507	Julia	Ramírez Vda de Adorno	FEMENINO	2040-12-04	PARAGUAYO/A	0983378569	VIUDO/A	\N	\N	\N	SI	NO	451	Mtro Ursicino Velazco c/ Martín Fierro	\N	\N	\N	0	El señor Marcelino Adorno es soltero y cuenta con una construcción de 4 mts de ancho x 3,50 mts de largo de ladrillo con techo de chapa zing, y hace tres años que construyó en el mismo terreno de la sra Julia Ramirez vda de Adorno.
3922981	Nery Isabel	Ramírez Vera	FEMENINO	\N	PARAGUAYO/A	0971193025	UNION DE HECHO	\N	\N	\N	SI	NO	2641	Remancito	\N	\N	\N	0	Seca Ropas 1.
3890156	Natalia María	Ramírez Zanabria	FEMENINO	\N	PARAGUAYO/A	0992753562	UNION DE HECHO	\N	\N	\N	SI	NO	2480	Recicla	\N	\N	\N	0	.
6678963	Aida Carolina	Ramírez	FEMENINO	1984-02-02	PARAGUAYO/A	0981336993	UNION DE HECHO	\N	\N	\N	SI	NO	1607	Nuestra Senora de la Asunción	\N	\N	\N	6	Actualmente la propietaria vive en la S.E.N.,hace 6 meses.
2844292	Ana María	Ramírez	FEMENINO	1977-03-09	PARAGUAYO/A	0981764933	UNION DE HECHO	\N	\N	\N	SI	NO	517	Ursicino Velasco C/ Malvinas	\N	\N	\N	0	.
4955177	Angelina	Ramírez	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1796	Bañado Koeti	\N	\N	\N	5	Es hermana del propietario del lote tiene una vivienda de material totalmente independiente y bien constituida.
4689183	Cynthia Elizabeth	Ramírez	FEMENINO	\N	PARAGUAYO/A	0986158067	UNION DE HECHO	\N	\N	\N	SI	NO	731	Angel Luis	\N	\N	\N	0	No es dueña del lote y de la vivienda. Está alquilada, el dueño es el señor Eladio Armoa. CI: 1.577.875
7375637	Dahiana Jacqueline	Ramírez	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1793	Bañado koeti	\N	\N	\N	5	Vivio 2 anos en Argentina, según la censada su pareja es inestable, desconoce algunos de los datos de su pareja.
552150	Ernesto	Ramírez	MASCULINO	\N	PARAGUAYO/A	.	SEPARADO/A	\N	\N	\N	SI	NO	331	Mar del Plata	\N	\N	\N	0	Tiene un Corredorcito.
1116587	Eulalio	Ramírez	MASCULINO	\N	PARAGUAYO/A	0992660410	UNION DE HECHO	\N	\N	\N	SI	NO	41	San Felipey Santiago	\N	\N	\N	0	tiene sala y corredor
653597	Fabiána	Ramírez	FEMENINO	\N	PARAGUAYO/A	0982143185	UNION DE HECHO	\N	\N	\N	SI	NO	1374	Bañado Koeti	\N	\N	\N	0	.
1417043	Graciela del Pilar	Ramírez	FEMENINO	\N	PARAGUAYO/A	0981381058	SOLTERO/A	\N	\N	\N	SI	NO	721	Pasillo Ange Luis	\N	\N	\N	0	.
4127631	Julia	Ramírez	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	1825	Bañado Koeti	\N	\N	\N	5	En el lote viven dos flias una de ellas compuesta por una menor de edad con un mayor de edad ,tienen un hijo de 8 meses, y una vivienda precaria aparte.
3636398	Julio Cesar	Ramírez	MASCULINO	1979-07-10	PARAGUAYO/A	0981211964	SOLTERO/A	\N	\N	\N	SI	NO	1794	Bañado Koeti	\N	\N	\N	0	Julio es propietario del lote y una de las viviendas ,uno de los primos vive en el lote en una vivienda precaria de 4x4 de dimencion llevan en pareja 1 ano.
7377227	Leticia	Ramírez	FEMENINO	\N	PARAGUAYO/A	0992976269	UNION DE HECHO	\N	\N	\N	SI	NO	1798	Bañado Koeti	\N	\N	\N	5	Cocina Electrica 1.
2015875	María Alfonsa	Ramírez	FEMENINO	2039-02-08	PARAGUAYO/A	0984830216	SOLTERO/A	\N	\N	\N	SI	NO	117	Virgen de Lourdes	\N	\N	\N	0	.
5592159	María Belén	Ramírez	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1315	Jakare Yrupe	\N	\N	\N	0	Vive dentro del  lote de Cecilia Amarilla de Gamarra con su pareja.
721355	Marta Concepción	Ramírez	FEMENINO	\N	PARAGUAYO/A	0981691604	CASADO/A	\N	\N	\N	SI	NO	2142	Virgen de Guadalupe	\N	\N	\N	0	.
4673413	Miguela	Ramírez	FEMENINO	\N	PARAGUAYO/A	0992975886	UNION DE HECHO	\N	\N	\N	SI	NO	1792	Bañado Koeti	\N	\N	\N	5	Es propietaria del lote,su hija Dahiana vive dentro del mismo en una extención.
7376477	Nilsa Johana	Ramírez	FEMENINO	\N	PARAGUAYO/A	0991984002	UNION DE HECHO	\N	\N	\N	SI	NO	1827	Bañado Koeti	\N	\N	\N	5	.
4569348	Pablo Daniel	Ramírez	MASCULINO	1983-02-03	PARAGUAYO/A	0984935879	UNION DE HECHO	\N	\N	\N	SI	NO	1795	Bañado Koeti	\N	\N	\N	0	Anteriormente vivia como encargado en una vivienda,vive dentro del lote del hno. ,tiene una vivienda independiente de madera.
1783960	Ricardo Lima	Ramírez	MASCULINO	\N	PARAGUAYO/A	0981676237	SOLTERO/A	\N	\N	\N	SI	NO	683	Yacare Yrupe	\N	\N	\N	0	.
653602	Sebastiana	Ramírez	MASCULINO	1972-10-05	PARAGUAYO/A	0985627978	SOLTERO/A	\N	\N	\N	SI	NO	1812	Mandyjura	\N	\N	\N	0	Dentro del lote y techo de la titular viven sus hijos mayores, con sus respectivas parejas
5116053	Claudia Noemi	Ramos	FEMENINO	\N	PARAGUAYO/A	0986272282	UNION DE HECHO	\N	\N	\N	SI	NO	1831	Bañado Koeti	\N	\N	\N	5	.
2702606	Juana Elizabeth	Ramos	MASCULINO	\N	PARAGUAYO/A	0991660601	.	\N	\N	\N	SI	NO	2581	Rancho 8	\N	\N	\N	0	Dentro del lote viven 3 nucleos de familia.
5005283	Juana Violeta	Real Olmedo	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	196	Ursicino Velasco	\N	\N	\N	0	.
4160149	Nelson Rodrigo	Real Olmedo	MASCULINO	1983-12-04	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	197	Ursicino Velasco	\N	\N	\N	0	El Sr. Nelson Rodrigo tiene una construcción muy bien constituida el dueno de la casa compro el terreno hace 12 anos que esta al lado del terreno de su madre al cual no tiene una division fisica y no tiene numero de lote.
3437844	Osmar Raul	Real Olmedo	MASCULINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	195	Mto. Ursicino Velasco	\N	\N	\N	0	.
1283502	Jorgelina Dionicia	Recalde  de Figeredo	MASCULINO	2051-09-10	PARAGUAYO/A	480892	CASADO/A	\N	\N	\N	SI	NO	1924	Urcicino Velazco y 23 de Julio	\N	\N	\N	0	.
654076	Emigdia Evangelista	Recalde de Espinola	FEMENINO	\N	PARAGUAYO/A	0981514609	CASADO/A	\N	\N	\N	SI	NO	198	Mto. Urcisino Velasco e/ Nuevo Amanecer	\N	\N	\N	0	.
660584	Alcides Simeón	Recalde Galli	MASCULINO	2057-08-10	PARAGUAYO/A	0981564839	SOLTERO/A	\N	\N	\N	SI	NO	2247	Ursicino Velasco	\N	\N	\N	0	Es propietario del lote y la casa.Actualmente vive en Cala'a y Trejo de Sajonia se mudo por la inundación,cuenta con titulo de propiedad.
1108828	Juana Petrona	Recalde Galli	MASCULINO	\N	PARAGUAYO/A	480892	.	\N	\N	\N	SI	NO	1928	.	\N	\N	\N	0	.
5069449	Adelaida	Recalde Giménez	FEMENINO	\N	PARAGUAYO/A	0982647474	CASADO/A	\N	\N	\N	SI	NO	384	YVOTY C/ Orquideas	\N	\N	\N	0	La titular lleva siete anos casada.el esposo nacio en el barrio la sra. Esta hace 20 anos en el barrio.
5312524	Asuncion	Recalde Giménez	FEMENINO	\N	PARAGUAYO/A	0991760228	UNION DE HECHO	\N	\N	\N	SI	NO	1382	Yvoty Rory	\N	\N	\N	0	.
5456468	Claudelina	Recalde Giménez	FEMENINO	\N	PARAGUAYO/A	0994267279	UNION DE HECHO	\N	\N	\N	SI	NO	1553	Pajagua Naranja	\N	\N	\N	0	.
5108740	Eugenia	Recalde Giménez	FEMENINO	\N	PARAGUAYO/A	0991987648	UNION DE HECHO	\N	\N	\N	SI	NO	1629	Pajagua Naranja	\N	\N	\N	0	Hace 8 anos que estan juntos pero que formaron un hogar hace5 anos.
4431127	Ever Manuel	Recalde	MASCULINO	\N	PARAGUAYO/A	0984 137 577	UNION DE HECHO	\N	\N	\N	SI	NO	2510	Urcisino Velazco	\N	\N	\N	0	No es pariente de Fidelina Martinez, refiere que su muralla se derrumbó. Es una familia totalmente independiente.
94	Recicladora Leguizamon	.	.	\N	.	0961408589	.	\N	\N	\N	SI	NO	1619	Ursicino Velasco	\N	\N	\N	0	El lote con casa de material y 2 galpones de techo Zinc es utilizado para la compra y venta de materiales de reciclaje.El terreno con casa compraron hace 13 anos.No tiene socio, No. R.U.C.:412056.
4380558	Richard David	Jimenez	MASCULINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	2334	.	\N	\N	\N	6	Actualmente vive en la casa de la madre,porque el agua hecho toda la construccion.
872246	María Zoila	Ríos	FEMENINO	\N	PARAGUAYO/A	0991893646	UNION DE HECHO	\N	\N	\N	SI	NO	2348	Nuestra Señora de la Asuncion	\N	\N	\N	6	.
4801488	María Agustína	Riquelme Benítez	FEMENINO	\N	PARAGUAYO/A	0981737800	UNION DE HECHO	\N	\N	\N	SI	NO	2339	Villa Asunción	\N	\N	\N	6	.
800465	Susana	Riquelme de Benitez	FEMENINO	\N	PARAGUAYO/A	0992994495	CASADO/A	\N	\N	\N	SI	NO	146	San Felipe y Santiago	\N	\N	\N	0	.
1705566	Irma Beatriz	Riquelme González	FEMENINO	\N	PARAGUAYO/A	0971277501	UNION DE HECHO	\N	\N	\N	SI	NO	592	Malvinas	\N	\N	\N	0	La vivienda tiene una extension de madera de 6x4 m , el galponcito de frente se utiliza como cocina. Tiene un pasillo/corredor.
2122223	Marina Elizabeth	Riquelme González	FEMENINO	1971-01-05	PARAGUAYO/A	0971116035	CASADO/A	\N	\N	\N	SI	NO	1979	Mbo'eHara c/ Nasaindy	\N	\N	\N	5	.
4621126	Nimia Beatriz	Riquelme Lezcano	FEMENINO	\N	PARAGUAYO/A	0991749325	SOLTERO/A	\N	\N	\N	SI	NO	345	AROMITA	\N	\N	\N	0	.
5309017	Divia Denisse Monserrat	Riquelme	FEMENINO	1990-02-08	PARAGUAYO/A	0982585288	UNION DE HECHO	\N	\N	\N	SI	NO	2529	Primavera	\N	\N	\N	0	Cocina a Gas y Secaropas 1.
21	Placida	Riquelme	FEMENINO	2047-03-02	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	129	ÁROMITA	\N	\N	\N	0	.
1396508	Teodora	Riquelme	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	555	Ursicino Velasco	\N	\N	\N	0	Cocina dentro del Corredor. Utilizan un galponcito frontal para cocinar.
5467610	Rosana	Rivarola Alcaraz	FEMENINO	1989-04-07	PARAGUAYO/A	0986233975	.	\N	\N	\N	SI	NO	1320	26 de Julio	\N	\N	\N	0	.
2444016	Anibal	Rivarola	MASCULINO	\N	PARAGUAYO/A	0986323461	CASADO/A	\N	\N	\N	SI	NO	800	Virgen de Luján	\N	\N	\N	0	.
1198879	Oscar Ramón	Rivas Bordón	MASCULINO	\N	PARAGUAYO/A	0985246417	SOLTERO/A	\N	\N	\N	SI	NO	1774	Bañado koeti	\N	\N	\N	0	.
3965393	Agustín	Rivas López	MASCULINO	1984-05-02	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	627	Angel Luis	\N	\N	\N	0	Vive temporalmente hace 1 ano como cuidador del lote y la casa , el cunado es el propietario del lote.
612840	Margarita	Rivas Vda. De Talavera	FEMENINO	2030-09-06	PARAGUAYO/A	0961311013	.	\N	\N	\N	SI	NO	1575	Ramon Talavera	\N	\N	\N	0	La propietaria tiene una construccion del Poryecto Pro-Vida de 6x4, donde viven sus nietos Pedro y Hugo Talavera.
2421859	Francisca	Rivas Villareal	FEMENINO	1972-02-04	PARAGUAYO/A	0985451238	UNION DE HECHO	\N	\N	\N	SI	NO	1822	Mainumby	\N	\N	\N	0	.
2884235	Mirta Beatriz	Riveiro de Cáceres	FEMENINO	\N	PARAGUAYO/A	0982694296	CASADO/A	\N	\N	\N	SI	NO	1624	Pajagua Naranja	\N	\N	\N	0	.
4993777	Liliana Rocío	Riveiro Morel	FEMENINO	1988-12-04	PARAGUAYO/A	0972800629	UNION DE HECHO	\N	\N	\N	SI	NO	245	Virgen de Lourdes	\N	\N	\N	0	.
4950960	Pedro Manuel	Rivero Bogado	MASCULINO	1989-05-08	PARAGUAYO/A	0986374286	UNION DE HECHO	\N	\N	\N	SI	NO	2033	Sagrado Corazón de Jesús	\N	\N	\N	0	Vive dentro del lote y la casa del abuelo con su flia. ,comparte electrodomesticos con la duena del lote.
2373228	Teodocia Dolores	Riveros Acuña	FEMENINO	2063-05-04	PARAGUAYO/A	0982969732	SOLTERO/A	\N	\N	\N	SI	NO	2670	Nuestra Sra. De la Asunción	\N	\N	\N	6	Seca Ropas 1.
3787272	Graciela	Riveros Benítez	FEMENINO	1978-05-10	PARAGUAYO/A	0982857597	UNION DE HECHO	\N	\N	\N	SI	NO	436	San Cayetano	\N	\N	\N	0	.
5644178	Julio César	Riveros Britez	MASCULINO	1994-07-04	PARAGUAYO/A	0971298813	SOLTERO/A	\N	\N	\N	SI	NO	31	San Felipe y Santiago	\N	\N	\N	0	El lote cuenta con dos casas una de ellas es de pisos, arriba de madera con dos dormitorios de madera y un baño de material. La otra es  una consttrucción noormal de material.
3740370	Liliana Ramona	Riveros Brítez	FEMENINO	\N	PARAGUAYO/A	0983759471	UNION DE HECHO	\N	\N	\N	SI	NO	4	San Felipe y Santiago	\N	\N	\N	3	.
3740369	Ricardo Andrés	Riveros Brítez	MASCULINO	\N	PARAGUAYO/A	0982381576	SEPARADO/A	\N	\N	\N	SI	NO	80	Virgen de Lourdes	\N	\N	\N	0	La casa cuenta con otra edificación en ele fondo q en este momento esta alquilado
3313061	Rosa Elizabeth	Riveros Brítez	FEMENINO	1981-06-03	PARAGUAYO/A	0984106961	UNION DE HECHO	\N	\N	\N	SI	NO	2226	Mto.Ursicino Velasco	\N	\N	\N	0	Tiene un salon que mide 8,20x520 donde tiene venta de Merceria,Boutique,Mini Cargay Librería.
1887582	Lidia	Riveros de Rojas	FEMENINO	2063-06-09	PARAGUAYO/A	0991998976	.	\N	\N	\N	SI	NO	2519	Fidelina (Santa Ana'i)	\N	\N	\N	0	Placa Electrica 1.
6082434	Giselle Anahi	Riveros Zavala	FEMENINO	1994-09-12	PARAGUAYO/A	0991894104	UNION DE HECHO	\N	\N	\N	SI	NO	1336	Ursicino Velasco	\N	\N	\N	0	Vive dentro del lote de la suegra tiene una vivienda de material bien constituida.
3210428	Enrique Javier	Riveros	MASCULINO	\N	PARAGUAYO/A	0995 621 502	SOLTERO/A	\N	\N	\N	SI	NO	2514	Urcisino Velazco	\N	\N	\N	0	.
534606	Ricardo	Riveros	MASCULINO	2055-07-02	PARAGUAYO/A	0981207063	CASADO/A	\N	\N	\N	SI	NO	3	San Felipe y Santiago	\N	\N	\N	4	Casa de 2 pisos
4267951	Rosalina del Carmen	Riveros	FEMENINO	\N	PARAGUAYO/A	0971853126	CASADO/A	\N	\N	\N	SI	NO	1999	Nasaindy	\N	\N	\N	5	.
4234812	Nancy Liz Paola	Robledo Morel	FEMENINO	\N	PARAGUAYO/A	0981267210	UNION DE HECHO	\N	\N	\N	SI	NO	76	Angel Luis	\N	\N	\N	0	.
4026137	Jorge Tomás	Rodas Adorno	MASCULINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	205	Virgen de Lourdes esq. Virgen de Luján	\N	\N	\N	0	.
4672583	Pabla Elizabeth	Rodas Adorno	FEMENINO	\N	PARAGUAYO/A	0991186302	CASADO/A	\N	\N	\N	SI	NO	204	Virgen de Luján	\N	\N	\N	0	.
5272884	Claudio	Rodas Florentín	MASCULINO	1998-12-08	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	54	San Felipe y Santiago	\N	\N	\N	3	.
1020171	Alberta	Rodas Martínez	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1272	Ursicino Velasco	\N	\N	\N	0	La duena del lote tiene una extencion 10x9 tambien tiene una despensa que es su fuente de ingreso.
2540539	Arminda Concepcion	Rodas	FEMENINO	\N	PARAGUAYO/A	0985625069	SOLTERO/A	\N	\N	\N	SI	NO	1375	Yvoty Rory	\N	\N	\N	0	.
1978876	Steven Gabriel	Rodas	MASCULINO	\N	PARAGUAYO/A	0991346819	UNION DE HECHO	\N	\N	\N	SI	NO	432	San Cayetano	\N	\N	\N	0	.
1161999	María Gladys	Rodríguez Cabral	FEMENINO	2064-08-11	PARAGUAYO/A	0983474382	UNION DE HECHO	\N	\N	\N	SI	NO	1161	Fidelina	\N	\N	\N	0	.
4673431	Maura Evelin	Rodríguez Cabral	FEMENINO	\N	PARAGUAYO/A	096137465	SOLTERO/A	\N	\N	\N	SI	NO	1162	Fidelina	\N	\N	\N	0	Comparte Datos con Maria Gladys Cabral (5-A1).tiene su teereno aparte ,no pudo construir luego de la inundacion.
2203543	Carmen Carolina	Rodríguez de Canteros	FEMENINO	\N	PARAGUAYO/A	0982798868	CASADO/A	\N	\N	\N	SI	NO	1158	Fidelina	\N	\N	\N	0	.
4758991	Carmen	Rodríguez de González	FEMENINO	\N	PARAGUAYO/A	0984674581	CASADO/A	\N	\N	\N	SI	NO	1117	Virgen de Lourdes c/ Virgen de Lujan	\N	\N	\N	0	Estan Casados have 11 anos.
1060908	Francisco	Rodríguez Demofonte	MASCULINO	\N	PARAGUAYO/A	0982374502	UNION DE HECHO	\N	\N	\N	SI	NO	2622	Ursicino Velasco	\N	\N	\N	0	Lleva 16 anos en la casa del hermano sobre Luis Camino y Lirio .Refiere que su casa se habia derrumbado y ya nopudo levantar ,actualmente su vivienda se encuentra en reconstrucción.
5010570	Alejandro Ramón	Rodríguez Echeverría	MASCULINO	\N	PARAGUAYO/A	0984294103	UNION DE HECHO	\N	\N	\N	SI	NO	2012	Nasaindy	\N	\N	\N	5	Hace 4 anos qu estan de pareja y vive en el terreno de la propietaria.
3519168	Agustína Liliana	Rodríguez Fernández	MASCULINO	\N	PARAGUAYO/A	0986563505	.	\N	\N	\N	SI	NO	2207	Nasaindy	\N	\N	\N	0	.
4435860	Elizabeth	Rojas	FEMENINO	\N	PARAGUAYO/A	0992 821 829	UNION DE HECHO	\N	\N	\N	SI	NO	1764	Virgen de Guadalupe	\N	\N	\N	0	No se pudo quitar la foto de otra manera.
4637754	Delia Mabel	Rodríguez Fernández	FEMENINO	1987-03-04	PARAGUAYO/A	0971532557	CASADO/A	\N	\N	\N	SI	NO	1783	Bañado Koeti	\N	\N	\N	5	Vive dentro del lote de la mama cuenta con vivienda propia totalmente independiente.
722194	Vicenta	Rodríguez Fernández	FEMENINO	2056-02-03	PARAGUAYO/A	0981835173	SOLTERO/A	\N	\N	\N	SI	NO	1275	Ursicino Velasco	\N	\N	\N	0	tiene 2 maquinas de coser industrialesy 2 familiares , una maquina para hacer helados y una vitrina, se dedica a ahacer helados para su venta y confecciones de ropa.
3494354	Willian Javier	Rodríguez Fernández	MASCULINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1396	.	\N	\N	\N	0	.
4731871	Lorena Elizabeth	Rodríguez Garcia	FEMENINO	1982-09-05	PARAGUAYO/A	0982752414	UNION DE HECHO	\N	\N	\N	SI	NO	1227	Yvoty Rory	\N	\N	\N	0	.
3840395	Mirta Dolores	Rodríguez González	FEMENINO	1976-09-04	PARAGUAYO/A	0995683335	UNION DE HECHO	\N	\N	\N	SI	NO	870	Martin Fierro	\N	\N	\N	0	.
1546408	Victor Manuel	Rodríguez Morinigo	MASCULINO	\N	PARAGUAYO/A	.	SEPARADO/A	\N	\N	\N	SI	NO	1966	.	\N	\N	\N	0	La niña Yohana fue entregada Judicialmente al hno  Juan Manuel, porque la madre esta incapacitada para su cuidado
360202	Mauricio	Rodríguez Ortigoza	MASCULINO	\N	PARAGUAYO/A	0992509793	SOLTERO/A	\N	\N	\N	SI	NO	1157	Fidelina	\N	\N	\N	0	Mauricio es dueno de la casa y del lote. Tiene una extencion junto a a la casa donde vive su hija y familia.
4854586	Ana Liz Lorena	Rodríguez Zalazar	FEMENINO	1987-04-11	PARAGUAYO/A	0972841534	UNION DE HECHO	\N	\N	\N	SI	NO	662	23 de Junio	\N	\N	\N	0	La pareja lleva 10 años de concubinato.Casa de la cooperativa..
731816	Eleuterio	Rodríguez Zaracho	MASCULINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	1394	.	\N	\N	\N	0	.
836387	Leonor	Rodríguez	MASCULINO	2030-01-07	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	2194	Mbo-ehara c/ Sotero Colman	\N	\N	\N	0	Están sustentadas por los hijos y nietos
1482036	Pedro Velasco	Rodríguez	MASCULINO	\N	ESP	0961166494	SOLTERO/A	\N	\N	\N	SI	NO	145	San Felipe y Santiago	\N	\N	\N	0	.
1748507	Carlos	Rojas Aguirre	MASCULINO	\N	PARAGUAYO/A	09941436445	CASADO/A	\N	\N	\N	SI	NO	613	Mtro Ursicino Velazco	\N	\N	\N	0	.
1734767	Amado Maximo	Rojas Aranda	MASCULINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	2220	.	\N	\N	\N	0	El sr. Casimiro no quiso que se le saque la foto a la vivienda.
6196410	Valentina Librada	Rojas Barrios	FEMENINO	\N	PARAGUAYO/A	0986521760	UNION DE HECHO	\N	\N	\N	SI	NO	1687	San Martin de Porres	\N	\N	\N	0	El padre se encuentra privado de su libertad y es el titular del terreno.
4721928	Nidia Carolina	Rojas Benítez	FEMENINO	1984-05-08	PARAGUAYO/A	0984730358	UNION DE HECHO	\N	\N	\N	SI	NO	2183	Chiquero	\N	\N	\N	0	Nacio en el lugar, la casa era de sus padres el cual repartio, para cada hijo sus partes
4511694	Liliana Mabel	Rojas Cardenas	FEMENINO	\N	PARAGUAYO/A	0994676886	UNION DE HECHO	\N	\N	\N	SI	NO	1637	Pajagua Naranja	\N	\N	\N	0	Tiene Cocina y Comedor.
4002488	Norma Beatriz	Rojas Cubilla	FEMENINO	\N	PARAGUAYO/A	.	SEPARADO/A	\N	\N	\N	SI	NO	2751	Maestro Urcisino Velasco	\N	\N	\N	0	.
1589090	Marciana	Rojas de Amaro	FEMENINO	\N	PARAGUAYO/A	0984725823	CASADO/A	\N	\N	\N	SI	NO	1695	Bañado Koeti	\N	\N	\N	5	.
949667	María Luzandra	Rojas de Giménez	FEMENINO	1976-09-07	PARAGUAYO/A	0982477920	CASADO/A	\N	\N	\N	SI	NO	1561	Ursicino Velasco	\N	\N	\N	0	Cuneta con dos construcciones una utiliza para dormir y la otra para sala, comedor una sala sola para juegos y tambien la cocina.
2452383	Victoria	Rojas de Palma	MASCULINO	2035-03-04	PARAGUAYO/A	.	VIUDO/A	\N	\N	\N	SI	NO	724	Pasillo Angel Luis	\N	\N	\N	0	.
3200411	Lidia Raquel	Rojas Delvalle	FEMENINO	1978-11-03	PARAGUAYO/A	0984 464 270	UNION DE HECHO	\N	\N	\N	SI	NO	1934	Martin Fierro	\N	\N	\N	3	La pareja lleva cuatro años de concubinato. Se maneja de forma independiente.
4303241	Luz Marina	Rojas Escobar	FEMENINO	1984-03-07	PARAGUAYO/A	0984812418	UNION DE HECHO	\N	\N	\N	SI	NO	2306	Nuestra Sra. De la Asunción	\N	\N	\N	0	.
949027	María Celedonia	Rojas Fabio	FEMENINO	2057-03-03	PARAGUAYO/A	0993578393	UNION DE HECHO	\N	\N	\N	SI	NO	2209	Mto Ursicino Velasco	\N	\N	\N	0	.
6817079	Miguel Estanislao	Rojas Florentín	MASCULINO	\N	PARAGUAYO/A	0992994797	UNION DE HECHO	\N	\N	\N	SI	NO	369	Virgen de Lourdes casi Oleros	\N	\N	\N	0	.
3664644	Carolina Feliciana	Rojas Fretes	FEMENINO	\N	PARAGUAYO/A	0991722364	UNION DE HECHO	\N	\N	\N	SI	NO	399	Algarrobo	\N	\N	\N	0	Tiene dos construcciones dentro del predio una de ellas es de la Cooperativa.
6131882	Betania	Rojas Garcia	FEMENINO	\N	PARAGUAYO/A	0985739433	UNION DE HECHO	\N	\N	\N	SI	NO	1739	Ursicino Velasco	\N	\N	\N	0	.
3502400	Crispin Gilberto	Rojas Garcia	MASCULINO	\N	PARAGUAYO/A	0982822940	UNION DE HECHO	\N	\N	\N	SI	NO	1740	Ursicino Velasco	\N	\N	\N	0	.
1848499	Damacia Liliana	Rojas Martínez	FEMENINO	1981-11-11	PARAGUAYO/A	0983282169	UNION DE HECHO	\N	\N	\N	SI	NO	457	Ursicino Velasco	\N	\N	\N	0	.
1848500	Sofia Ramona	Rojas Martínez	FEMENINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	1274	Ursicino Velasco	\N	\N	\N	0	En el terreno de Sofia Rojas , su hna. Felicita tiene una lomiteria.
4160133	Gustavo Gabriel	Rojas Ramírez	MASCULINO	\N	PARAGUAYO/A	0994344102	UNION DE HECHO	\N	\N	\N	SI	NO	1814	Mandyjura	\N	\N	\N	0	la pareja tiene 7 años de concubinato, viven bajo el mismo techo de la madre, no tienen lote
4160135	Natalia Gisel	Rojas Ramírez	FEMENINO	1989-09-12	PARAGUAYO/A	0994344102	UNION DE HECHO	\N	\N	\N	SI	NO	1813	Mandyjura	\N	\N	\N	0	la pareja lleva dos años de concubinato, no tienen lote propio, viven dentro del lote de la titular, no tienen hijos, utilizan un dormitorio de 4x4
4160156	Claudia Regina	Rojas Riveros	FEMENINO	1983-07-09	PARAGUAYO/A	0992957417	UNION DE HECHO	\N	\N	\N	SI	NO	2328	Nuestra Sra. De la Asunción	\N	\N	\N	6	.
4229082	Lidia Catalina	Rojas Riveros	FEMENINO	\N	PARAGUAYO/A	0991670317	UNION DE HECHO	\N	\N	\N	SI	NO	1781	Bañado Koeti	\N	\N	\N	5	Vive dentro del lote de Gabriel cuenta con vivienda propia.
4671017	Rocio Raquel	Rojas Riveros	FEMENINO	\N	PARAGUAYO/A	0972799227	UNION DE HECHO	\N	\N	\N	SI	NO	2349	Nuestra Sra de la Asunción	\N	\N	\N	6	.
4627599	Marta Andrea	Rojas Zalazar	FEMENINO	\N	PARAGUAYO/A	0994563723	UNION DE HECHO	\N	\N	\N	SI	NO	679	Yacare Yrupé	\N	\N	\N	0	.
4112577	Patricia Catalina	Rojas Zalazar	FEMENINO	\N	PARAGUAYO/A	0984927266	SOLTERO/A	\N	\N	\N	SI	NO	2064	Ursisino Velzco	\N	\N	\N	0	La  familia no tiene lote ni vivienda, viven en alquiler.
4187852	María Liz	Rojas Zarate	FEMENINO	\N	PARAGUAYO/A	0992381169	UNION DE HECHO	\N	\N	\N	SI	NO	708	Albaroto C/ Ursicino Velasco	\N	\N	\N	0	.
4435843	Angela	Rojas	FEMENINO	\N	PARAGUAYO/A	0984 436 146	UNION DE HECHO	\N	\N	\N	SI	NO	1751	Virgen de Guadalupe	\N	\N	\N	0	Sus hijos todavía no poseen cédula.
1066297	Bernardino	Rojas	MASCULINO	\N	PARAGUAYO/A	0984623147	CASADO/A	\N	\N	\N	SI	NO	1292	Ursicina Velasco C/ Remancito	\N	\N	\N	0	La señora jorgelina Rojas vive en el terreno de su padre y cuenta con una construcción y están en pareja hace 1 año, la construcción cuenta con 15 años de antigüedad.
1182925	Buenaventura	Rojas	MASCULINO	\N	PARAGUAYO/A	0992970777	UNION DE HECHO	\N	\N	\N	SI	NO	732	Mto. Ursicino Velasco	\N	\N	\N	0	La vivienda cuenta con dos galpones uno en el frente y otroen la parte posterior de la vivienda.Tiene una Cabina Telefonica.
335964	Casimiro Rigoberto	Rojas	MASCULINO	2038-04-03	PARAGUAYO/A	.	VIUDO/A	\N	\N	\N	SI	NO	2218	.	\N	\N	\N	0	.
2326280	Ernesta	Rojas	FEMENINO	2064-07-11	PARAGUAYO/A	0986400403	VIUDO/A	\N	\N	\N	SI	NO	905	Maestro Ursicino Velasco	\N	\N	\N	0	Cuenta con la casa del palafito.
4563775	Jessica Dahiana	Rojas	FEMENINO	\N	PARAGUAYO/A	0995670823	UNION DE HECHO	\N	\N	\N	SI	NO	2219	.	\N	\N	\N	0	El sr. Casimiro no quiso que se le saque la foto a la vivienda.
2636634	Lino	Rojas	MASCULINO	\N	PARAGUAYO/A	0972563368	SOLTERO/A	\N	\N	\N	SI	NO	612	Ursicino Velazco y San Felipe y Santiago	\N	\N	\N	0	El titular es Soltero sin hijos, hace más de 40 años que vive en  el bañado, tiene un hermano mayor soltero que vive con él
4066792	María Concepcion	Rojas	FEMENINO	1981-03-12	PARAGUAYO/A	0972125770	UNION DE HECHO	\N	\N	\N	SI	NO	328	Virgen de Lourdes y Mar del Plata	\N	\N	\N	0	.
3048	Miguel	Rojas	MASCULINO	\N	No quiso censar	.	.	\N	\N	\N	SI	NO	2112	Sotero Colman	\N	\N	\N	0	No quiso censarse.
4168442	Modesta Dominga	Rojas	FEMENINO	2039-12-06	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	2668	Nuestra Sra. De la Asunción	\N	\N	\N	6	.
919005	Teófilo Alfredo	Rojas	MASCULINO	2063-08-01	PARAGUAYO/A	0985338013	UNION DE HECHO	\N	\N	\N	SI	NO	2186	Sotero Colman	\N	\N	\N	0	hace 17 años se hacen cargo de sus nietos
336027	Modesta	Roldan	FEMENINO	\N	PARAGUAYO/A	0985634250	UNION DE HECHO	\N	\N	\N	SI	NO	1303	Ursicino Velasco c/ Remancito	\N	\N	\N	0	.
1825019	Tomasa	Roldán	FEMENINO	\N	PARAGUAYO/A	0982 160 106	VIUDO/A	\N	\N	\N	SI	NO	2245	.	\N	\N	\N	0	Ella vive en una pieza aparte, al cuidado de su hija.
4360148	Lilian Raquel	Rolón Cubilla	FEMENINO	1985-07-07	PARAGUAYO/A	0981770053	SOLTERO/A	\N	\N	\N	SI	NO	2590	Rancho 8	\N	\N	\N	0	la pareja lleva 14 anos de concubinato no tiene lote propio.La pareja se maneja individualmente.
818402	Andronico	Rolón González	MASCULINO	2064-09-10	PARAGUAYO/A	0981185044	CASADO/A	\N	\N	\N	SI	NO	1565	Ursicino Velasco	\N	\N	\N	0	El terreno esta utilizado como gomeria y el encargado se llama Nicanor Medina y hace 8 anos esta trabjando en la misma.Tiene un tinglado.
746756	Isabel	Rolón González	FEMENINO	\N	PARAGUAYO/A	0982335972	UNION DE HECHO	\N	\N	\N	SI	NO	1538	Pajagua Naranja	\N	\N	\N	0	Vivienda con plata baja y planta alta (10x8).
1215084	Mirian	Rolón González	FEMENINO	\N	PARAGUAYO/A	0981567885	CASADO/A	\N	\N	\N	SI	NO	1568	Ursicino Velasco	\N	\N	\N	0	.
4312425	Yenilda Elizabeth	Rolón Morai	FEMENINO	1983-10-10	PARAGUAYO/A	0984484045	UNION DE HECHO	\N	\N	\N	SI	NO	498	23 de Junio	\N	\N	\N	0	Vive dentro del lote de la mama en una vivienda precaria con sus hijos y concubino.
967895	Graciela	Rolón Ortigoza	FEMENINO	\N	PARAGUAYO/A	0982277438	UNION DE HECHO	\N	\N	\N	SI	NO	2289	Urcisino Velazco c/ Talavera	\N	\N	\N	0	.
1048100	Julio César	Rolón Ortigoza	MASCULINO	2066-05-04	PARAGUAYO/A	0982534421	CASADO/A	\N	\N	\N	SI	NO	2589	Rancho 8	\N	\N	\N	0	Placa Electrica 1.
1016866	Felipa Alicia	Rolón	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1061	Ursicino Velasco	\N	\N	\N	0	Cuenta con dos viviendas según propietaria en una de ellas vive su hijo soltero.
5481689	Yanet Elizabeth	Román Alegre	FEMENINO	\N	PARAGUAYO/A	0972800613	UNION DE HECHO	\N	\N	\N	SI	NO	1437	Ursicino Velasco	\N	\N	\N	0	Placa Electrica y Centrifugadora una unidad c/u.La pareja lleva dos anos de concubinato , la titular es duena del lote y techo.Nacio en el Barrio.
1614504	Adolfina	Román Calonga	FEMENINO	\N	PARAGUAYO/A	0971711666	UNION DE HECHO	\N	\N	\N	SI	NO	1334	Ursicino Velasco	\N	\N	\N	0	Tiene una vivienda amplia de dos pisos.
1360459	Aurora Belén	Román Chávez	FEMENINO	1973-12-09	PARAGUAYO/A	0992661293	SOLTERO/A	\N	\N	\N	SI	NO	1000	Mar de la plata	\N	\N	\N	0	La dueña del terreno refiere que hace 3 años se separó de su pareja, pero que el mismo vive dentro del lote, en una ampliación.
1109616	Luis Alberto	Román Chávez	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	1003	Mar de Plata	\N	\N	\N	0	Uno de los hijos se encuentra privado de su ibertad.
1494831	María Delpilar	Román de Bobadilla	FEMENINO	2061-12-10	PARAGUAYO/A	0971228409	CASADO/A	\N	\N	\N	SI	NO	272	Ursicino Velasco	\N	\N	\N	0	Dispone de otro terreno dentro del barrio en la zona 1.
2151460	Elisa	Román de Brítez	FEMENINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	1433	Ursicino Velasco	\N	\N	\N	0	Maria Ignacia tiene su casa de material en el mismo lote (4x4)
4488501	Ortencia	Román Diana	FEMENINO	\N	PARAGUAYO/A	0983162932	UNION DE HECHO	\N	\N	\N	SI	NO	599	Malvinas c/ 16 de Agosto	\N	\N	\N	0	.
4529660	Andrea Celeste	Román Franco	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	395	.	\N	\N	\N	0	.
5378901	Esmilce Rocío	Román Franco	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	269	Nuevo Amanecer	\N	\N	\N	0	.
4781738	Gabriela	Román Medina	FEMENINO	\N	PARAGUAYO/A	0962231217	UNION DE HECHO	\N	\N	\N	SI	NO	1029	Mto. Ursicino Velasco	\N	\N	\N	0	.
4487258	Juan Manuel	Román Quinto	MASCULINO	\N	PARAGUAYO/A	0971994082	UNION DE HECHO	\N	\N	\N	SI	NO	1435	Ursicino Velasco	\N	\N	\N	0	Su hija solo poseen partida de nacimiento.
3467096	Victor Javier	Román Quinto	MASCULINO	1979-01-01	PARAGUAYO/A	0994882131	SOLTERO/A	\N	\N	\N	SI	NO	2396	Maynumby	\N	\N	\N	0	Es propietario del lote y la casa, cuenta con permiso de ocupacion
6252993	Yessica Pamela	Román Rodríguez	FEMENINO	\N	PARAGUAYO/A	0986690504	UNION DE HECHO	\N	\N	\N	SI	NO	1531	Rancho 8	\N	\N	\N	0	Placa Electrica 1.La pareja lleva 8 anos de concubinato.
3700821	Justina Soledad	Romero Barreto	FEMENINO	1980-01-06	PARAGUAYO/A	0983150509	UNION DE HECHO	\N	\N	\N	SI	NO	1801	Bañado koeti	\N	\N	\N	5	.
5689987	Luz Marina	Romero Barreto	FEMENINO	\N	PARAGUAYO/A	0972484225	UNION DE HECHO	\N	\N	\N	SI	NO	1832	Bañado Koeti	\N	\N	\N	3	.
4705665	Oscar Dario	Romero Barreto	MASCULINO	1986-08-01	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	1422	Bañado koeti	\N	\N	\N	5	.
7317069	Victor Daniel	Romero Barreto	MASCULINO	1998-02-09	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	2619	Banado Koeti	\N	\N	\N	3	.
4815733	Ana Macarena	Romero Benítez	FEMENINO	1989-12-12	PARAGUAYO/A	0992434077	CASADO/A	\N	\N	\N	SI	NO	1024	Mto. Ursicino Velasco	\N	\N	\N	0	Vive en el mismo lote de la mdre en una construccion aparte.Tienen 4 aos de convivencia.
6133201	Karen Lliliana	Romero Cáceres	FEMENINO	1996-10-07	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1269	.	\N	\N	\N	0	Tiene una casa muy precaria en el lote de la madre.
2817473	Adriana Elvira	Romero de Benitez	FEMENINO	1978-02-10	PARAGUAYO/A	0981148868	SEPARADO/A	\N	\N	\N	SI	NO	2647	Ramon Talavera	\N	\N	\N	0	La misma manifiesta que es Dueña del lote y no la señora Isaura Cristaldo Garcete.
1354586	Santa	Romero Galeano	FEMENINO	2057-02-11	PARAGUAYO/A	0984734674	SEPARADO/A	\N	\N	\N	SI	NO	1047	Yvoty Rory	\N	\N	\N	0	.
1639059	Miguel Angel	Romero González	MASCULINO	\N	PARAGUAYO/A	0984450524	UNION DE HECHO	\N	\N	\N	SI	NO	1096	26 de Julio	\N	\N	\N	0	El señor Miguel Romero, Nació en san Cosme y Damian.
3528545	Juliána	Romero Maciel	MASCULINO	1978-07-04	PARAGUAYO/A	0981165263	SOLTERO/A	\N	\N	\N	SI	NO	610	Ursicino Velazco y San Felipe y Santiago	\N	\N	\N	0	La señora es madre soltera adulta y vive con la titular del lote bajo su techo
3221636	Myrian del Carmen	Romero Maciel	FEMENINO	\N	PARAGUAYO/A	0995696210	UNION DE HECHO	\N	\N	\N	SI	NO	1114	Virgen de Lourdes c/ Angel Luis	\N	\N	\N	0	Estan en pareja hace 10 anos.
5643586	Epifanio Javier	Silva Bernal	MASCULINO	1989-08-04	PARAGUAYO/A	0972187112	UNION DE HECHO	\N	\N	\N	SI	NO	661	Yvoty	\N	\N	\N	0	.
760223	Dora	Romero Méndez	FEMENINO	2053-11-12	PARAGUAYO/A	0982918222	SOLTERO/A	\N	\N	\N	SI	NO	859	Ursicino Velasco	\N	\N	\N	0	Este nucleo vive bajo el mismo techo del titular del lote 17
3713133	Deisy Alicia	Romero Mesa	FEMENINO	\N	PARAGUAYO/A	0993 531 484	SOLTERO/A	\N	\N	\N	SI	NO	1854	Ursicino Velazco	\N	\N	\N	0	.
907128	Leonarada	Romero Vda. De Leguizamon	FEMENINO	\N	PARAGUAYO/A	0984886803	VIUDO/A	\N	\N	\N	SI	NO	1965	Ursicino Velasco	\N	\N	\N	0	.
4363735	Dora Alicia	Romero	FEMENINO	1986-02-05	PARAGUAYO/A	0982918222	CASADO/A	\N	\N	\N	SI	NO	858	Ursicino Velasco	\N	\N	\N	0	Dentro del techo de la titutlar vive otro nucleo : su madre , su hna. , su sobrina.
3296795	Elvira	Romero	FEMENINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	676	Yacare Yrupé	\N	\N	\N	0	.
1485213	Feliciana Leiva	Romero	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	872	.	\N	\N	\N	0	.
4378091	Gloria Concepción	Romero	FEMENINO	1981-09-07	PARAGUAYO/A	0995673104	UNION DE HECHO	\N	\N	\N	SI	NO	1318	Jakare Yrupe	\N	\N	\N	0	Es propietaria del lote, vive en una vivienda de material bien construido posee dos construcciones de madera en donde viven con sus hijos.
4443735	José Domingo	Romero	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	2501	Oleros	\N	\N	\N	3	Radio 1.
5145960	Juana Laura	Romero	MASCULINO	\N	PARAGUAYO/A	0984734674	SOLTERO/A	\N	\N	\N	SI	NO	1048	Yvoty Rory	\N	\N	\N	0	Vive dentro del mismo lote y vivienda de la madre.
4301316	Mario Reinaldo	Romero	MASCULINO	1982-04-09	PARAGUAYO/A	0982408925	UNION DE HECHO	\N	\N	\N	SI	NO	212	Aromita	\N	\N	\N	0	.
2453008	Román	Romero	MASCULINO	2050-09-08	PARAGUAYO/A	.	VIUDO/A	\N	\N	\N	SI	NO	1449	.	\N	\N	\N	0	.
5877097	Juan Angel	Rosas Colmán	MASCULINO	1995-06-09	PARAGUAYO/A	0992676284	UNION DE HECHO	\N	\N	\N	SI	NO	911	Martin Fierro	\N	\N	\N	0	.
5527500	Julio Ramón	Rotela Adorno	MASCULINO	\N	PARAGUAYO/A	0992484405	UNION DE HECHO	\N	\N	\N	SI	NO	571	San Cayetano	\N	\N	\N	0	La pareja tiene 7 años de conubinato, tienen dos hijos menores y viven el lote de la titular con techo propio.
4887084	Margarita Celeste	Rotela Adorno	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	83	Virgen de Lujan y V. de Lourdes	\N	\N	\N	0	.
4983955	Gloria Patricia	Rotela Alegre	FEMENINO	\N	PARAGUAYO/A	0985538513	UNION DE HECHO	\N	\N	\N	SI	NO	2705	.	\N	\N	\N	3	.
7934737	Laura Paola	Rotela Alegre	FEMENINO	\N	PARAGUAYO/A	0982694547	SOLTERO/A	\N	\N	\N	SI	NO	16	.	\N	\N	\N	3	.
4666592	Mirian Beatriz	Rotela Alegre	FEMENINO	1988-11-05	PARAGUAYO/A	0984783722	SEPARADO/A	\N	\N	\N	SI	NO	782	Jakare Yrupe	\N	\N	\N	0	.
4364786	Rossana Elizabeth	Rotela Alegre	FEMENINO	1984-06-12	PARAGUAYO/A	0984535745	SOLTERO/A	\N	\N	\N	SI	NO	1329	Yacare Yrupe	\N	\N	\N	0	Su padre ayuda mensualmente a la titular con aporte economico.
2041523	Teresa del Pilar	Rotela Martínez	FEMENINO	1970-12-10	PARAGUAYO/A	0981139948	UNION DE HECHO	\N	\N	\N	SI	NO	8	San Felipe y Santiago	\N	\N	\N	0	.
5388217	Estela Mary	Rotela Rolón	FEMENINO	1987-05-01	PARAGUAYO/A	0994279121	UNION DE HECHO	\N	\N	\N	SI	NO	765	Martin Fierro E/ Pilcomayo	\N	\N	\N	0	.
5218048	María Teresa	Rotela Rolón	FEMENINO	\N	PARAGUAYO/A	0991987473	UNION DE HECHO	\N	\N	\N	SI	NO	574	San Cayetano	\N	\N	\N	0	La pareja lleva 11 años de concubinato. Tienen dos hijos menores,lote y techo propio.
805	Domingo	Rotela	MASCULINO	\N	.	.	.	\N	\N	\N	SI	NO	1856	Ursicino Velazco	\N	\N	\N	0	No quiso censarse.
5435334	Nidia Leticia	Rotela	FEMENINO	\N	PARAGUAYO/A	0983 556 877	UNION DE HECHO	\N	\N	\N	SI	NO	2463	Mandyjura	\N	\N	\N	0	.
3796250	Juan Benigno	Ruíz Barria	MASCULINO	\N	PARAGUAYO/A	0982294615	UNION DE HECHO	\N	\N	\N	SI	NO	2343	Nuestra Sra de la Asunción	\N	\N	\N	6	.
4778066	Liliana Elizabeth	Ruíz de Adorno	FEMENINO	1978-12-06	PARAGUAYO/A	0992475597	CASADO/A	\N	\N	\N	SI	NO	1127	Virgen de Lujan	\N	\N	\N	0	La Sra. Brigida vive dentro del lote de su hija cuenta con vivienda propia padece de transtornos mentales.
5165607	Georgina	Ruiz de Ezquivel	FEMENINO	\N	PARAGUAYO/A	0992217252	CASADO/A	\N	\N	\N	SI	NO	1344	Primavera	\N	\N	\N	0	.
3986493	Liz Mabel	Ruiz de Vega	FEMENINO	1983-07-06	PARAGUAYO/A	0911 142 978	CASADO/A	\N	\N	\N	SI	NO	1618	Rancho 8 c/ San Luis	\N	\N	\N	0	Dormitorio y cocina dentro de la pieza.
518106	Juana Bautista	Ruiz Díaz Leiva	MASCULINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1562	Ursicino Velasco	\N	\N	\N	0	Es propietario dl lote actualmente vive y trabaja en la Argentina según manifestacion de la encargada la propietaria viene cada 6 meses hace 20 anos que vive en la Argentina.
2538451	María	Ruiz Díaz	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1534	Rancho 8 y San Luis	\N	\N	\N	0	.
1763734	Martin Ramón	Ruíz Díaz	MASCULINO	2053-11-11	.	0985588446	SEPARADO/A	\N	\N	\N	SI	NO	2452	San Felipe y Santiago	\N	\N	\N	3	Hace 4 meses se acentarón en el lugar.
3482546	Reynaldo	Ruiz Díaz	MASCULINO	1980-08-10	PARAGUAYO/A	0995678262	SOLTERO/A	\N	\N	\N	SI	NO	1967	Ursicino Velasco	\N	\N	\N	0	Secarropas 1. Desde  que nacio vive en el barrio, hace dos anos fallecio la madre alquilo la casa de la Sra. Leonarda , luego quedandose el hijo con la flia. Inquilina hace mas de 15 anos el es el que cuida de la sra. Se maneja totalmente aparte con entr
4206811	Fermina	Ruiz Villaba	FEMENINO	1978-08-07	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	1223	Yvoty Rory	\N	\N	\N	0	.
5277050	María Dolores	Salazar Cáceres	FEMENINO	1993-09-04	PARAGUAYO/A	0971698501	SOLTERO/A	\N	\N	\N	SI	NO	1790	Bañado koeti	\N	\N	\N	5	Radio 1.
4804269	María Magdalena	Salazar Penayo	FEMENINO	\N	PARAGUAYO/A	0985949822	SOLTERO/A	\N	\N	\N	SI	NO	1785	Bañado Salazar	\N	\N	\N	5	Vive dentro del lote de la mama en una extencion.
5124746	Cristian Gabriel	Salcedo Espinola	MASCULINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	946	Nuevo Amanecer	\N	\N	\N	0	Actualmente vive Aliana Ferreira Bogado como encargada .Cristian Salcedo el dueño del terreno viaja por periodos largos 5 a 6 meses por lo mismo sus pertenencias estan en la casa de su madre en Bo. Obrero vivo en el abñado por 2 años , hace 1 año que no
5315222	María Fatima	Salinas Benítez	FEMENINO	\N	PARAGUAYO/A	0982460707	UNION DE HECHO	\N	\N	\N	SI	NO	1143	Aromita	\N	\N	\N	0	Vive en el terreno de su suegra Maria Mino
1956834	Eugenia	Salinas de Paniagua	FEMENINO	\N	PARAGUAYO/A	0985455445	SEPARADO/A	\N	\N	\N	SI	NO	1699	Bañado Poty	\N	\N	\N	5	.
4486675	Juana Evangelista	Salinas	MASCULINO	\N	PARAGUAYO/A	0981149678	UNION DE HECHO	\N	\N	\N	SI	NO	820	Vecinos Unidos	\N	\N	\N	3	Anteriormente vivia en Zona 6-Villa Galli.
4246974	Limpia Concepción	Salinas	FEMENINO	1982-09-10	PARAGUAYO/A	0985838937	CASADO/A	\N	\N	\N	SI	NO	468	Martin Fierro	\N	\N	\N	0	Vivne dentro del lote de Teodora en una vivienda de material bien construida.
4064424	María Miguela	Salinas	FEMENINO	\N	PARAGUAYO/A	0982196085	SOLTERO/A	\N	\N	\N	SI	NO	828	Vecinos Unidas	\N	\N	\N	0	Anteriormente vivia en la Zona 6.
4655941	Vanessa Soledad	Salinas	FEMENINO	\N	PARAGUAYO/A	0992423556	UNION DE HECHO	\N	\N	\N	SI	NO	942	Calle Sin nombre (Recicla)	\N	\N	\N	0	.
952198	Andrés Agustín	Samaniego Aranda	MASCULINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1277	.	\N	\N	\N	0	Andres Agustin tiene un salon de 4,35x6,50, y en la misma vende comida rapida.Tambien tiene otro salon de 4,35x 6,50 que esta usando como dormitorio.
5174155	María Leona	Silva Bernal	FEMENINO	2068-02-01	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	943	Virgen de Lourdes	\N	\N	\N	0	.
3774398	Cresencia	Samaniego Benítez	FEMENINO	\N	PARAGUAYO/A	0981782319	UNION DE HECHO	\N	\N	\N	SI	NO	2543	Mandy Jura	\N	\N	\N	0	Al fonod del lote vive la mamá en una vivienda precaria de 6x6 totalmente independiente.
3190978	Justo	Samaniego Benítez	MASCULINO	2057-11-06	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	1106	Fidelina	\N	\N	\N	0	El señor Justo Samaniego tiene una pequeña actividad, se dedica en su casa al juego de billar.
3711278	Patricia Raquel	Samaniego Vega	FEMENINO	\N	PARAGUAYO/A	.	SEPARADO/A	\N	\N	\N	SI	NO	819	26 de Julio	\N	\N	\N	3	.
1036805	Tomas	Samaniego	MASCULINO	2052-07-03	PARAGUAYO/A	0985454859	.	\N	\N	\N	SI	NO	1049	yvoty Rory	\N	\N	\N	0	.
4672429	Javier	Samudio Gamarra	MASCULINO	1990-07-05	PARAGUAYO/A	0994744283	UNION DE HECHO	\N	\N	\N	SI	NO	849	Sin Nombre	\N	\N	\N	3	Tiene una extencion por la casa de su mama.
3622557	Norma Elizabeth	Samudio Gamarra	FEMENINO	1983-02-07	PARAGUAYO/A	0972685966	UNION DE HECHO	\N	\N	\N	SI	NO	901	Martin Fierro	\N	\N	\N	0	.
6933134	María Guadalupe	Samudio Valdéz	FEMENINO	\N	PARAGUAYO/A	0983262325	.	\N	\N	\N	SI	NO	60	.	\N	\N	\N	3	.
995922	Emigdio	Samudio	MASCULINO	2052-05-08	PARAGUAYO/A	0982507155	UNION DE HECHO	\N	\N	\N	SI	NO	1882	Julio Benitez	\N	\N	\N	0	El Sr. Posee un despensa bien surtida.
2302500	María Teresa	Samudio	FEMENINO	\N	PARAGUAYO/A	0982705393	UNION DE HECHO	\N	\N	\N	SI	NO	2081	Pasillo Sin Nombre	\N	\N	\N	0	Son propietarios de la vivienda actualmente vive en la propiedad de la Guarderia como encargado hace 6 meses aprox. ,dice no mudarce por las refacciones que le hace falta a su vivienda.
1859404	Jorge Daniel	Sanabria Candia	MASCULINO	1972-12-03	PARAGUAYO/A	0981340804	UNION DE HECHO	\N	\N	\N	SI	NO	1209	San Felipe	\N	\N	\N	3	.
6013478	Adriana Lizzete	Sanabria López	FEMENINO	1992-04-03	.	0991244594	UNION DE HECHO	\N	\N	\N	SI	NO	2390	Mainumby esq. Fidelina	\N	\N	\N	0	.
6628020	Monica Araceli	Sanabria Núñez	FEMENINO	\N	PARAGUAYO/A	0982237954	UNION DE HECHO	\N	\N	\N	SI	NO	847	Sin Nombre	\N	\N	\N	0	.
5644768	Patricia Marlene	Sanabria Núñez	FEMENINO	1982-03-04	PARAGUAYO/A	0981780103	UNION DE HECHO	\N	\N	\N	SI	NO	846	SIN NOMBRE	\N	\N	\N	0	.
6932241	Enmanuel Benigno D.	Sanabria Velázquez	MASCULINO	\N	PARAGUAYO/A	0981194106	CASADO/A	\N	\N	\N	SI	NO	865	Ursicino Velasco	\N	\N	\N	0	La pareja lleva dos años de casados, vive bajo el mismo techo de la madre quien es la titular del mismo lote y techo.
5007559	María Victoria	Sanabria Velázquez	FEMENINO	1990-07-10	PARAGUAYO/A	0986941646	UNION DE HECHO	\N	\N	\N	SI	NO	866	Ursicino Velasco	\N	\N	\N	0	La pareja lleva cuatro años de concubinato . No tienen terreno ni lote propio , viven bajo el mismo techo de la titular.
2883394	Aristides	Sanabria	MASCULINO	\N	PARAGUAYO/A	0983716688	CASADO/A	\N	\N	\N	SI	NO	1354	Primavera	\N	\N	\N	0	.
2652605	Francisca Fleitas	Sanabria	FEMENINO	\N	PARAGUAYO/A	0982260448	VIUDO/A	\N	\N	\N	SI	NO	27	Costado de Recicla	\N	\N	\N	3	.
2047051	José Antonio	Sanabria	MASCULINO	1970-04-05	PARAGUAYO/A	0986 851 589	SOLTERO/A	\N	\N	\N	SI	NO	1851	Ursicino Velazco	\N	\N	\N	0	Vive dentro de la casa de María porque su casa se derrumbó.
2194843	Juan Ramón	Sanabria	MASCULINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	1183	Urcicino Velazco	\N	\N	\N	0	posee una vivienda de bloque de cemento que no utiliza
4203444	Nidia Elizabeth	Sanabria	FEMENINO	1983-07-06	PARAGUAYO/A	0992375464	CASADO/A	\N	\N	\N	SI	NO	1156	Vecinos Unidos	\N	\N	\N	0	.
1971375	Carmen Beatriz	Sanbria López	FEMENINO	1978-03-09	PARAGUAYO/A	0992993272	UNION DE HECHO	\N	\N	\N	SI	NO	1184	Urcicino Velazco	\N	\N	\N	0	vive dentro de terreno de su papa con su pareja y sus hijos y dos de los hijos ya tienen pareja
1818934	Hugo Javier	Sánchez Adorno	MASCULINO	2068-09-04	PARAGUAYO/A	0971305637	CASADO/A	\N	\N	\N	SI	NO	835	Ursicino Velazco y Martín Fierro	\N	\N	\N	0	Dentro del techo del titular vive su hija con su pareja y un menor, pareja jóven con un año de concubinato
4561343	Fátima Liliana	Sánchez Arce	FEMENINO	1987-12-05	PARAGUAYO/A	0995628940	UNION DE HECHO	\N	\N	\N	SI	NO	450	San Cayetano	\N	\N	\N	0	La titular tiene un hijo con parálisis Infantil (El niño no habla, no camina)
5333051	Ana Carolina	Sánchez Areco	FEMENINO	\N	PARAGUAYO/A	0991247551	UNION DE HECHO	\N	\N	\N	SI	NO	914	Ursicino Velasco y Martin Fierro	\N	\N	\N	0	.
5025173	Diego Ariel	Sánchez Areco	MASCULINO	\N	PARAGUAYO/A	0972178494	UNION DE HECHO	\N	\N	\N	SI	NO	459	Ursicino Velasco c/ Martin Fierro	\N	\N	\N	0	Sr. Diego Sanchez cuenta con otro terreno en la manzana 30 lote 2 con una dimension de 5,60 y 17 con una construccion bien constituida de 4 MT X4MT con antigüedad de 3 anos y la misma utiliza par las ventas de helados.
5333052	María Evangelista	Sánchez Areco	FEMENINO	\N	PARAGUAYO/A	0985175378	UNION DE HECHO	\N	\N	\N	SI	NO	912	Martin Fierro	\N	\N	\N	0	La pareja lleva 12 años de concubinato y 10 viviendo en su lote y techo propio. Centrifugadora :1 , Cocina Electrica:1.
4633948	Francisco Diego	Sánchez Caparro	MASCULINO	1988-09-06	PARAGUAYO/A	0985114835	SOLTERO/A	\N	\N	\N	SI	NO	1381	Yvoty Rory	\N	\N	\N	0	Es propietario del lote actualmente vive en la casa de la mama porque la casa esta en reconstruccion.
4984533	Walter Daniel	Sánchez Chaparro	MASCULINO	\N	PARAGUAYO/A	0971105507	UNION DE HECHO	\N	\N	\N	SI	NO	762	26 de Julio	\N	\N	\N	5	La casa tiene más años porque era de sus padres.
1149097	Evarista	Sánchez de Amarilla	FEMENINO	\N	PARAGUAYO/A	0981191033	.	\N	\N	\N	SI	NO	152	Ursicino Velasco c/ 8 de Diciembre	\N	\N	\N	0	Esta vivienda cuenta con un taller de fundicion particular .En este lote viven 5 nucleos fliares.
4227852	Zulma	Sánchez de Barrios	FEMENINO	1986-07-04	PARAGUAYO/A	0981119559	.	\N	\N	\N	SI	NO	203	Nuevo Amanecer	\N	\N	\N	0	.
5420237	Yessica Paola	Sánchez González	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	475	23 de Junio	\N	\N	\N	0	Vive dentro del lote de Santa en una extensión de la vivienda.
3970177	sofia Diana	Sánchez López	FEMENINO	\N	PARAGUAYO/A	0994665068	UNION DE HECHO	\N	\N	\N	SI	NO	833	Vecinos Unidos	\N	\N	\N	0	El terreno y la construccion pertenecen a la Guarderia Divino Niño y la Comunidad. La Sra. Sofia es la encargada.
3597591	Mirian Celeste	Sánchez Portillo	FEMENINO	1989-12-06	PARAGUAYO/A	0985929939	UNION DE HECHO	\N	\N	\N	SI	NO	2256	Juan Leon Mallorquin 1935	\N	\N	\N	0	La casa fue construida hace 40 anos y la flia actual lo heredo.
2130682	Sonia María	Sánchez	FEMENINO	\N	PARAGUAYO/A	0985 898450	UNION DE HECHO	\N	\N	\N	SI	NO	1614	San Luis	\N	\N	\N	0	- Cocinan en el galponcito con brasero a carbón
1764186	Miguel	Sandoval	MASCULINO	\N	PARAGUAYO/A	0986426313	UNION DE HECHO	\N	\N	\N	SI	NO	88	Virgen de Lourdes	\N	\N	\N	0	.
1372129	Paulina Quintana	Sandoval	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	217	Virgen de Lujan y Virgen de Lourdes	\N	\N	\N	0	.
1662716	Roque	Santa Cruz	MASCULINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	680	Yacare Yrupe	\N	\N	\N	0	.
781520	Rosalia	Santacruz Benítez	FEMENINO	\N	PARAGUAYO/A	0982 712 461	SOLTERO/A	\N	\N	\N	SI	NO	2671	Ursicino Velazco	\N	\N	\N	0	Es propietaria del lote. Cada uno de sus hijos posee una vivienda precaria de Sim.
4005414	Liz Rossana	Santacruz de Figueredo	FEMENINO	1985-07-05	PARAGUAYO/A	0982718989	CASADO/A	\N	\N	\N	SI	NO	69	RECICLA	\N	\N	\N	3	.
346510	Protacio	Silva Escobar	MASCULINO	2040-05-12	PARAGUAYO/A	0982715839	CASADO/A	\N	\N	\N	SI	NO	2272	Nuestra Sra. De la Asunción	\N	\N	\N	0	.
1761252	Clara Nimia	SantaCruz de Vera	FEMENINO	1970-01-12	PARAGUAYO/A	0213385128	CASADO/A	\N	\N	\N	SI	NO	745	Mto. Ursicino Velasco e/ Martin Fierro	\N	\N	\N	0	La vivienda cuenta con un corredor y un galpon, tambien cuenta con una construccion de hormigon que falta terminar.
4001221	José	Santacruz Franco	MASCULINO	2061-02-07	PARAGUAYO/A	0991202523	CASADO/A	\N	\N	\N	SI	NO	886	Martin Fierro c/Ursicino Velasco	\N	\N	\N	0	.
736989	Teodoro Francisco	Santacruz Mendoza	MASCULINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	66	San Felipe y Santiago	\N	\N	\N	3	.
5586264	Marcos	Santacruz Núñez	MASCULINO	1997-03-05	PARAGUAYO/A	0992417530	UNION DE HECHO	\N	\N	\N	SI	NO	1370	Yvoty Rory	\N	\N	\N	0	.
2268592	Ramón Valentín	Santacruz Ojeda	MASCULINO	1973-09-03	PARAGUAYO/A	0985402146	SEPARADO/A	\N	\N	\N	SI	NO	2709	Ursicino Velazco Esquina	\N	\N	\N	0	.
6685800	Aurelia Mabel	Santacruz Villar	FEMENINO	1993-08-06	PARAGUAYO/A	09853548887	UNION DE HECHO	\N	\N	\N	SI	NO	1443	Bañado Koeti	\N	\N	\N	5	vive en el mismo lote y vivienda que la madre, en una ampliación en la parte posterior de la vivienda
3006814	Alicia	Santacruz	FEMENINO	\N	PARAGUAYO/A	0991657522	SOLTERO/A	\N	\N	\N	SI	NO	684	Yacare Yrupe	\N	\N	\N	5	.
4632492	Claudia Andrea	Santacruz	FEMENINO	\N	PARAGUAYO/A	0992651521	UNION DE HECHO	\N	\N	\N	SI	NO	688	Ycare Yrupe	\N	\N	\N	5	.
5434950	Edgar Ramón	Santacruz	MASCULINO	\N	PARAGUAYO/A	0991657522	UNION DE HECHO	\N	\N	\N	SI	NO	685	Yacare Yrupe	\N	\N	\N	5	.
2678360	Marta	SantaCruz	FEMENINO	2062-11-07	PARAGUAYO/A	0982646269	UNION DE HECHO	\N	\N	\N	SI	NO	1085	Martin Fierro	\N	\N	\N	0	Es propietaria del terreno con ella vive su hija y sus nietos en una de las parejas.
1546108	Angela	Santander Díaz	FEMENINO	2062-01-03	PARAGUAYO/A	0991582565	SOLTERO/A	\N	\N	\N	SI	NO	50	San Felipe	\N	\N	\N	3	13 ano vivio en zona 2. Hce 7 anos vivie en zona 7.
4419873	Alfredo Javier	Santo Brítez	MASCULINO	\N	PARAGUAYO/A	0984758078	UNION DE HECHO	\N	\N	\N	SI	NO	237	Julio Jara	\N	\N	\N	4	.
4978470	Blanca Azucena	Santo Britez	FEMENINO	\N	PARAGUAYO/A	0981914172	CASADO/A	\N	\N	\N	SI	NO	182	Julio Jara	\N	\N	\N	0	.
4120451	Gloria Elizabeth	Santo Brítez	FEMENINO	1985-08-06	PARAGUAYO/A	0971179041	UNION DE HECHO	\N	\N	\N	SI	NO	236	Julio Jara	\N	\N	\N	0	.
5186202	María Magdalena	Santo Brítez	FEMENINO	\N	PARAGUAYO/A	0972294746	DIVORCIADO/A	\N	\N	\N	SI	NO	235	Julio Jara C/ Ursicino Velasco	\N	\N	\N	0	Laa vivienda se construyo hace 37 años y la Familia heredo la misma.
5491962	Deysi Patricia	Santos Aranda	FEMENINO	\N	PARAGUAYO/A	0991860695	UNION DE HECHO	\N	\N	\N	SI	NO	2353	Nuestra Sra de la Asunción	\N	\N	\N	6	Cocina a Gas y Secarropas 1.
5491966	Nancy Elizabeth	Santos Aranda	FEMENINO	\N	PARAGUAYO/A	0983552145	SOLTERO/A	\N	\N	\N	SI	NO	2320	Nuestra Sra. De la Asunción	\N	\N	\N	6	.
1576762	Librada	Santos Aranda	FEMENINO	\N	PARAGUAYO/A	0991860695	SOLTERO/A	\N	\N	\N	SI	NO	2318	Nuestra Sra. De la Asunción	\N	\N	\N	6	Cocina a Gas y Secarropas 1.
2808171	Carlos	Santos Brítez	MASCULINO	\N	PARAGUAYO/A	0971179041	SOLTERO/A	\N	\N	\N	SI	NO	238	Julio Jara	\N	\N	\N	0	Construcción totalmente independiente de buena calidad.
2215085	Ramón	Santos Brítez	MASCULINO	\N	PARAGUAYO/A	0991401225	SOLTERO/A	\N	\N	\N	SI	NO	234	Julio Jara	\N	\N	\N	0	Su casa esta en construccion pero igual vive dentro del lote mientrs se termina su contruccion vive con la hna.
1677695	María Stela	Santos de Ferreira	FEMENINO	2068-08-04	PARAGUAYO/A	0971563083	CASADO/A	\N	\N	\N	SI	NO	193	Mto. Urcisino Velasco	\N	\N	\N	4	Artculos: Sierra Carnicera 1
4912957	Johana Poala	Santos Orrego	FEMENINO	1993-04-08	PARAGUAYO/A	0961511724	CASADO/A	\N	\N	\N	SI	NO	1011	.	\N	\N	\N	0	El fuente de ingreso de Johana es el de la Peluqueria , tiene una construccion de hormigon que mide 3,50x10 mts.en el mismo lugar que tiene un salon que usa para trabajar en la Pluqueria.
3207489	Fátima Beatriz	Santos Ramírez	FEMENINO	\N	PARAGUAYO/A	0981621632	CASADO/A	\N	\N	\N	SI	NO	974	San Felipe y Santiago	\N	\N	\N	0	La casa cuenta con un galpón grande en el fondo. 11x4
3207412	Lilian Andrea	Santos Ramírez	FEMENINO	1978-04-02	PARAGUAYO/A	0961846036	CASADO/A	\N	\N	\N	SI	NO	2222	.	\N	\N	\N	0	.
2315522	Lilia Eugenia	Santos Riquelme	FEMENINO	\N	PARAGUAYO/A	0982669558	UNION DE HECHO	\N	\N	\N	SI	NO	252	Virgen de Lourdes 1167	\N	\N	\N	0	.
4971070	Petrona Elizabeth	Santos Zorrila	FEMENINO	\N	PARAGUAYO/A	0982864876	.	\N	\N	\N	SI	NO	1675	Pasillo IVU	\N	\N	\N	0	.
6219390	Alma Rosario	Santos Zorrilla	FEMENINO	1995-07-10	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1677	Pasillo IVU	\N	\N	\N	0	Tiene una extencion por la casa de la madre de 2 piezas.
2149377	Saturnino Ramón	Santos	MASCULINO	\N	PARAGUAYO/A	0994139614	UNION DE HECHO	\N	\N	\N	SI	NO	163	Nuevo Amanecer y Los Sauces	\N	\N	\N	0	Casa de 2 pisos.
4276047	Helen Melina	Scheunemann Adorno	FEMENINO	\N	PARAGUAYO/A	423496	UNION DE HECHO	\N	\N	\N	SI	NO	2611	Ursicino Velazco	\N	\N	\N	0	Este núcleo familiar vive en la vivienda del titular Federico Scheunemann.
979684	Federico Guillermo	Scheunemann Widmann	MASCULINO	2048-03-08	PARAGUAYO/A	423496	SOLTERO/A	\N	\N	\N	SI	NO	2608	Ursicino Velazco	\N	\N	\N	0	El titular paga por cuotas el terreno donde tiene su casa.
4258795	Leonardo	Segovia Adorno	MASCULINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	790	Virgen de Lourdes	\N	\N	\N	0	Anteriormente la casab pertenecia a sus padres. El titular vive en la casa desde los 18 anos. Seseparo de su pareja hace 1 ano tiene una hija la cual vive con la madre.
4670606	Victoria Esmilce	Segovia Adorno	MASCULINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	82	Virgen de Lujan	\N	\N	\N	0	El lote Consta de 2 construcciones , 1 vivienda 1 negocio
2530810	Claudelina	Segovia de Bareiro	FEMENINO	1974-06-12	PARAGUAYO/A	0984726716	CASADO/A	\N	\N	\N	SI	NO	302	Las Orquideas c/ Yvoty	\N	\N	\N	0	.
3529049	Sonia Elizabeth	Segovia Escobar	FEMENINO	\N	PARAGUAYO/A	097248569	UNION DE HECHO	\N	\N	\N	SI	NO	362	Yvotu y Virgen de Lourdes	\N	\N	\N	0	Dentro del lote hay 3 construcciones.Hay una pareja de 18 y 17 años con dos años de concubinatoen una casa independiente.
5918620	Nicacio	Segovia	MASCULINO	\N	PARAGUAYO/A	0983410024	SOLTERO/A	\N	\N	\N	SI	NO	1128	Virgen de Lujan	\N	\N	\N	0	El terreno esta dividido en dos parte.
479493	Oscar Alejandro	Segovia	MASCULINO	2041-09-07	PARAGUAYO/A	0994112873	VIUDO/A	\N	\N	\N	SI	NO	727	Mto. Ursicno Velasco	\N	\N	\N	0	.
1334105	Jorgelina	Servian	MASCULINO	\N	PARAGUAYO/A	0971577172	SOLTERO/A	\N	\N	\N	SI	NO	1516	Rancho 8	\N	\N	\N	0	.
901195	Asunción	Servín vda. De Duarte	FEMENINO	\N	PARAGUAYO/A	0994285640	VIUDO/A	\N	\N	\N	SI	NO	94	Virgen de Lourdes	\N	\N	\N	3	La Casa Cuenta con cielo raso, Tiene una construcción frente a su vivienda 4x3 utilizando para su mercería
35556931	Arnaldo Ariel	Servín	MASCULINO	\N	PARAGUAYO/A	0985946133	UNION DE HECHO	\N	\N	\N	SI	NO	90	Virgen de Lourdes	\N	\N	\N	0	No Habitan en el barrio desde la crecida , estan en alquiler.
4517886	Hector Daniel	Servín	MASCULINO	\N	PARAGUAYO/A	0961494098	CASADO/A	\N	\N	\N	SI	NO	91	Virgen de Lourdes	\N	\N	\N	0	.
1558375	Rosa	Servín	FEMENINO	\N	PARAGUAYO/A	0982890973	SOLTERO/A	\N	\N	\N	SI	NO	89	Virgen de Loures	\N	\N	\N	0	Actualment la flia no habita la casa estan en alquiler desde las crecidas.
1849332	Sabino Santiago	Servín	MASCULINO	\N	PARAGUAYO/A	0994285640	SOLTERO/A	\N	\N	\N	SI	NO	141	Orquidea esq/ Yvoty	\N	\N	\N	0	.
2915467	Otilia Concepción	Silva Fernández	FEMENINO	\N	PARAGUAYO/A	0984713550	SOLTERO/A	\N	\N	\N	SI	NO	2274	Nuestra Sra. De la Asunción	\N	\N	\N	0	Casa de dos plantas.
1876521	Sixto Carlos	Silva Fernández	MASCULINO	1973-07-04	PARAGUAYO/A	0985864934	CASADO/A	\N	\N	\N	SI	NO	2273	Nuestra Sra. De la Asunción	\N	\N	\N	0	.
4226486	Aldo Waldimiro	Silva López	MASCULINO	\N	PARAGUAYO/A	3282970	UNION DE HECHO	\N	\N	\N	SI	NO	2266	Nuestra Sra. De la Asuncion	\N	\N	\N	0	.
2838383	Blas Antonio	Silva López	MASCULINO	1977-03-02	PARAGUAYO/A	0983491238	SOLTERO/A	\N	\N	\N	SI	NO	2267	Nuestra Sra. De la Asuncion	\N	\N	\N	0	Secarropas 1.
4516430	Elizabeth Noemi	Silva López	FEMENINO	1987-11-03	PARAGUAYO/A	0992282421	CASADO/A	\N	\N	\N	SI	NO	2257	Juan Leon Mallorquin	\N	\N	\N	0	.
2104427	Oscar Fidencio	Silva López	MASCULINO	\N	PARAGUAYO/A	3282970	CASADO/A	\N	\N	\N	SI	NO	2265	.	\N	\N	\N	0	Cocina a Gas 1.
4791704	Gustavo Alexander	Silva Marecos	MASCULINO	1996-07-12	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	2615	San Masrtin de Porres	\N	\N	\N	0	Era la casa de la madre por eso la antigüedad de la vivienda.
6994315	María Magdalena	Silva	FEMENINO	\N	PARAGUAYO/A	0992421033	UNION DE HECHO	\N	\N	\N	SI	NO	1309	Mita Saraki	\N	\N	\N	0	Vive dentro del mismo lote de la madre una construccion aparte.
6781403	Máxima	Silva	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	18	San Felipe y Santiago	\N	\N	\N	3	.
553202	Silvia Estelvina	Silva	FEMENINO	2053-03-11	PARAGUAYO/A	0992913322	UNION DE HECHO	\N	\N	\N	SI	NO	1185	Fidelina	\N	\N	\N	0	.
2235809	Gervacia	Silvano López	FEMENINO	\N	PARAGUAYO/A	0994700425	CASADO/A	\N	\N	\N	SI	NO	1098	Fidelina	\N	\N	\N	0	La vivienda tiene una pequeña despensa la señora Gervacia, también trabaja dentro de su casa como costurera.
4231717	Laureana	Silvano López	FEMENINO	\N	PARAGUAYO/A	0991307786	UNION DE HECHO	\N	\N	\N	SI	NO	778	26 de Julio	\N	\N	\N	3	.
5244802	Lilian Elizabeth	Silvano López	FEMENINO	\N	PARAGUAYO/A	0992303226	UNION DE HECHO	\N	\N	\N	SI	NO	1236	Fidelina	\N	\N	\N	0	La Sra. Lilian vive dentro de la vivienda de su padre con su hijo.
3423531	María Cristina	Silvano López	FEMENINO	1976-05-12	PARAGUAYO/A	0992342329	SEPARADO/A	\N	\N	\N	SI	NO	781	26 de Julio Z-5B	\N	\N	\N	3	La señora María Silvana cuenta con una despensa.
57181842	Clea Mara	Silvera	FEMENINO	\N	Brasileña	0991 843 246	SOLTERO/A	\N	\N	\N	SI	NO	2683	Ursicino Velazco	\N	\N	\N	0	.
817098	Catalina	Simón de Sánchez	FEMENINO	\N	PARAGUAYO/A	.	VIUDO/A	\N	\N	\N	SI	NO	2242	.	\N	\N	\N	0	.
2142481	Emilio Alberto	Simón Vargas	MASCULINO	1979-08-01	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	1723	Ursicino Velasco	\N	\N	\N	0	No tiene ningun tipo de electrodomesticos.
1956050	Francisco Daniel	Simón Vargas	MASCULINO	\N	PARAGUAYO/A	0981745316	UNION DE HECHO	\N	\N	\N	SI	NO	1722	Ursicino Velasco	\N	\N	\N	0	La propietaria actualamente vive en alquiler en Villa Elisa porque no cumplio el contrato.
3224593	Lorena Raquel	Simón Vargas	FEMENINO	\N	PARAGUAYO/A	0983354982	SOLTERO/A	\N	\N	\N	SI	NO	1867	Villa Maria Auxiliadora	\N	\N	\N	0	.
5497437	Nancy Leticia	Solis	FEMENINO	1993-07-08	PARAGUAYO/A	0994688978	UNION DE HECHO	\N	\N	\N	SI	NO	147	San Felipe y Santiago	\N	\N	\N	0	Vive dentro del lote de la titular M41 Lote 2A.
2151478	Sonia Emilce	Soliz Giménez	FEMENINO	1974-07-10	PARAGUAYO/A	0991818390	UNION DE HECHO	\N	\N	\N	SI	NO	492	23 de Junio c/ Remancito	\N	\N	\N	0	Estan de concubinato hace 10 anos.
1824114	María Cristina	Soria Congo	FEMENINO	\N	PARAGUAYO/A	0982105265	UNION DE HECHO	\N	\N	\N	SI	NO	2478	Vecinos Unidos	\N	\N	\N	0	Sus dos nietos viven con la pareja,la mamá ,hija de la jefa de hogar fallecio hace dos anos.
3238057	Andrea Verónica	Sosa de Bobadilla	FEMENINO	\N	PARAGUAYO/A	0971896306	CASADO/A	\N	\N	\N	SI	NO	416	Mar del Plata	\N	\N	\N	0	.
4908299	Sonia Raquel	Sosa Donsert	FEMENINO	\N	PARAGUAYO/A	021-425-427	.	\N	\N	\N	SI	NO	1871	.	\N	\N	\N	0	.
2647584	Natividad	Sosa Florentín	FEMENINO	2048-08-09	PARAGUAYO/A	0982407598	.	\N	\N	\N	SI	NO	244	Virgen de Lourdes	\N	\N	\N	0	.
6235208	Andrea Macarena	Sosa Galeano	FEMENINO	1996-03-05	PARAGUAYO/A	0991482441	SOLTERO/A	\N	\N	\N	SI	NO	1712	Yacare Pito	\N	\N	\N	3	El lote pertenecia a la abuela Emerita Martinez, quien vivia anteriormente en la zona 7
1436533	Evaristo	Sosa Garcia	FEMENINO	2057-09-05	.	0981952029	.	\N	\N	\N	SI	NO	2664	Nuestra Sra. De la Asunción	\N	\N	\N	6	El titular es soltero,dentro de su lote vive un amigo con techo aparte hace 6 anos.Todos los electrodomesticos son del sr. Eusebio Bruno.
6681327	María Carolina	Sosa Martínez	FEMENINO	1996-02-09	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	2520	.	\N	\N	\N	0	Tienen la cocina y el comedor juntos. El baño utiliza con la mamá Faustina Martinez. Cocina con Brasero.
1956842	Martiniana	Sosa Núñez	FEMENINO	\N	PARAGUAYO/A	0983779878	SEPARADO/A	\N	\N	\N	SI	NO	98	Virgen de Lourdes	\N	\N	\N	0	.
959065	Verónica	Sosa Núñez	FEMENINO	2056-09-02	PARAGUAYO/A	0971229918	SOLTERO/A	\N	\N	\N	SI	NO	261	Virgen de Lourdes	\N	\N	\N	0	.
4695988	María Elena	Sosa Rotela	FEMENINO	1981-05-03	PARAGUAYO/A	0992590104	SOLTERO/A	\N	\N	\N	SI	NO	136	Las Orquideas Esq/ Aromita	\N	\N	\N	0	.
2064838	Victor Marcos	Sosa Samaniego	MASCULINO	\N	PARAGUAYO/A	0981296460	CASADO/A	\N	\N	\N	SI	NO	1837	Santa Librada	\N	\N	\N	0	La casa es de 2 pisos y la parte de arriba es de Teja el techo.
1468122	Mercedes	Sosa Silguero	FEMENINO	2041-08-09	PARAGUAYO/A	.	VIUDO/A	\N	\N	\N	SI	NO	1690	San Martin de Porres	\N	\N	\N	0	Cobra Tercera Edad y vive con los dos nietos huerfanos.
4360322	Gabriela	Sosa Suarez	FEMENINO	\N	PARAGUAYO/A	0972579527	UNION DE HECHO	\N	\N	\N	SI	NO	1022	Mto Ursicino Velascoe/ Yvoty R.	\N	\N	\N	0	.
1160282	Cipriana	Sosa	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1371	Bañado koeti	\N	\N	\N	0	.
4840265	María Cristina	Sosa	FEMENINO	\N	PARAGUAYO/A	0986235982	UNION DE HECHO	\N	\N	\N	SI	NO	448	San Cayetano	\N	\N	\N	0	.
1214438	Mirta Elizabeth	Sosa	FEMENINO	\N	PARAGUAYO/A	0983942225	UNION DE HECHO	\N	\N	\N	SI	NO	1727	Ursicino Velasco	\N	\N	\N	0	.
1202343	Ramón	Sosa	MASCULINO	\N	PARAGUAYO/A	0992590104	SOLTERO/A	\N	\N	\N	SI	NO	137	Las Orquideas Esq/ Aromita	\N	\N	\N	0	.
889622	Wilfrido Ramón	Sosa	MASCULINO	2064-05-06	PARAGUAYO/A	.	SEPARADO/A	\N	\N	\N	SI	NO	2255	.	\N	\N	\N	0	.
36	Sra Gladys	.	FEMENINO	\N	No quiso censar	.	.	\N	\N	\N	SI	NO	2161	Ursicino Velasco y Maria Auxiliadora	\N	\N	\N	0	No quiso censarse.
4297427	Claudia Rebeca	Suares Torres	FEMENINO	\N	PARAGUAYO/A	0984704917	UNION DE HECHO	\N	\N	\N	SI	NO	2548	Mandy Jura	\N	\N	\N	0	Es la propietaria.
7277282	Romina	Suares Torres	FEMENINO	1992-01-07	PARAGUAYO/A	0986685033	CASADO/A	\N	\N	\N	SI	NO	2549	Mandy Jura	\N	\N	\N	0	Vive dentro del lote de la hna. En una extencion de madera.
2338837	Daniela	Suarez Carisimo	FEMENINO	\N	PARAGUAYO/A	0971335980	UNION DE HECHO	\N	\N	\N	SI	NO	1034	Yvoty Rory e/ Mita Saraki	\N	\N	\N	0	La vivienda cuenta con un galpon.
811476	Hilda	Suarez Carisismo	FEMENINO	\N	PARAGUAYO/A	0992246379	UNION DE HECHO	\N	\N	\N	SI	NO	1021	Mto Ursicino Velascoe/ Yvoty R.	\N	\N	\N	0	.
4446793	Esmilce Adriana	Suarez de Méndez	FEMENINO	1982-07-07	PARAGUAYO/A	0986766804	CASADO/A	\N	\N	\N	SI	NO	1400	Mita Saraki	\N	\N	\N	0	.
5934611	Castorina Angelica	Verón Giménez	FEMENINO	\N	PARAGUAYO/A	0981958305	SOLTERO/A	\N	\N	\N	SI	NO	1135	Virgen de Lujan	\N	\N	\N	0	.
1503171	Enrique	Talavera Colher	MASCULINO	\N	PARAGUAYO/A	0981940201	UNION DE HECHO	\N	\N	\N	SI	NO	1574	Ursicino Velascoc/ Ramon Talavera	\N	\N	\N	0	.
589090	Justa Rufina	Tavalera de Echague	FEMENINO	\N	PARAGUAYO/A	0981470486	CASADO/A	\N	\N	\N	SI	NO	499	San Felipe c/ 23 de Junio	\N	\N	\N	0	.
5222150	César	Tilleria Rodríguez	MASCULINO	\N	PARAGUAYO/A	0983243459	SOLTERO/A	\N	\N	\N	SI	NO	2351	Nuestra Sra de la Asunción	\N	\N	\N	6	Cocina Electrica 1.
5031949	Laura María José	Tillner Vera	FEMENINO	\N	PARAGUAYO/A	0982100115	UNION DE HECHO	\N	\N	\N	SI	NO	634	Angel Luis	\N	\N	\N	0	.
1042106	Ignacia	Toledo Ayala	FEMENINO	\N	PARAGUAYO/A	0992307715	UNION DE HECHO	\N	\N	\N	SI	NO	1456	Ursicino Velasco	\N	\N	\N	0	La preja lleva 25 anos de concubinato ,antes tenian un techo precario.
1117135	María Magdalena	Toledo Ayala	FEMENINO	\N	PARAGUAYO/A	0986922389	SOLTERO/A	\N	\N	\N	SI	NO	1459	Ursicino Velasco	\N	\N	\N	0	La titular cuenta con lote propio y techo de material, vive con su hijo de 19 anos.Dentro del mismo lote vive su hija menor con su flia bajo el mismo techo.
4645908	Mariela Joanna	Toledo de Guzmán	FEMENINO	\N	PARAGUAYO/A	0981284948	CASADO/A	\N	\N	\N	SI	NO	1484	Vecinos Unidos	\N	\N	\N	0	.
3418460	Claudia Elizabeth	Toledo de Maldonado	FEMENINO	1980-05-04	PARAGUAYO/A	0991662203	SEPARADO/A	\N	\N	\N	SI	NO	1457	Ursicino Velasco	\N	\N	\N	0	Centrifugadora y Cocina Induccion: 1. La titular tenia una relacion legal con el padre de sus hijos durante 13 anos.Hace 3 anos que esta separada ,vive bajo su techo propio de material con sus hijos menores menores.El padre de susu hijos no pasa ninguna
1167405	Reina Isabel	Toledo Liuzzi	FEMENINO	2068-08-07	PARAGUAYO/A	0986354818	CASADO/A	\N	\N	\N	SI	NO	2573	Pajagua Naranja	\N	\N	\N	0	Aparte de la casa de dos pisos cuenta con una construccion de 9m por 3, 50m.
1962037	Lucía	Toledo Silva	FEMENINO	\N	PARAGUAYO/A	0982937101	SOLTERO/A	\N	\N	\N	SI	NO	795	Virgen de Lourdes	\N	\N	\N	0	.
5477589	Nancy Paola	Torales González	FEMENINO	1996-08-10	PARAGUAYO/A	0985635347	UNION DE HECHO	\N	\N	\N	SI	NO	1622	Ursicino Velasco y Pajagua Naranja	\N	\N	\N	0	Era la casa de la mama el cual dividio a dos de sus hijas siendo ellas las que realizaran las mejoras de los destrozos ocasionados por la crecida.
1495712	Pedro Silveiro	Torales González	MASCULINO	\N	PARAGUAYO/A	0994214674	UNION DE HECHO	\N	\N	\N	SI	NO	1621	Ursicino Velasco y Pajagua Naranja	\N	\N	\N	0	El Hno del Sordo Mudo vive en una casita precaria en el fondo de su terreno.
2873643	Atanasia Fretes	Torales	FEMENINO	1973-09-10	PARAGUAYO/A	0983917687	CASADO/A	\N	\N	\N	SI	NO	573	Malvinas	\N	\N	\N	0	la casa cuenta con otra construccion dentro del predio donde vive la hija con su bebe 6x3 es la dimension es de material con techo de chapa.
1250099	Ernestina Anicia	Torres Candia	FEMENINO	\N	PARAGUAYO/A	0982 717 167	UNION DE HECHO	\N	\N	\N	SI	NO	1766	Payagua Naranja	\N	\N	\N	0	.
5174653	Ruben Dario	Torres Díaz	MASCULINO	\N	PARAGUAYO/A	0961787225	SOLTERO/A	\N	\N	\N	SI	NO	1960	Urcicino Velazco casi Julio Benitez	\N	\N	\N	0	.
7348910	Adriana Carolina	Torres López	FEMENINO	1996-06-03	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	776	Jakare Yrupe	\N	\N	\N	0	.
347040	Félix	Torres Ortigoza	MASCULINO	\N	PARAGUAYO/A	0981215590	UNION DE HECHO	\N	\N	\N	SI	NO	1326	Ursicino Velasco c/ 26 de julio	\N	\N	\N	0	.
5852781	Leonarda	Torres Ruiz	FEMENINO	2063-06-11	PARAGUAYO/A	0984557895	.	\N	\N	\N	SI	NO	1226	Yvoty Rory	\N	\N	\N	0	.
4089418	Nilsa Nicolása	Torres Ruiz	FEMENINO	2069-10-09	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1244	Pajagua Naranja	\N	\N	\N	0	en el lugar hay una despensa el cual es su fuente de ingreso
5459126	Tamara Eduarda	Torres Ruiz	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	883	Martin Fierro	\N	\N	\N	0	.
4012758	Buenaventura	Torres	MASCULINO	1980-05-07	PARAGUAYO/A	0976869416	SOLTERO/A	\N	\N	\N	SI	NO	1383	Primavera	\N	\N	\N	0	.
733373	Silvia	Torres	FEMENINO	\N	PARAGUAYO/A	0981686141	.	\N	\N	\N	SI	NO	187	Mto. Ursicino Velasco	\N	\N	\N	0	Luis no es hijo de la Sra. No tiene parentesco con la misma , pero vive con ella hace 20 anos.
3222993	Damaris Natalia	Trepowiski Gamarra	FEMENINO	1991-09-08	PARAGUAYO/A	0972214299	UNION DE HECHO	\N	\N	\N	SI	NO	7	San Felipe y Santiago	\N	\N	\N	0	.
2191363	Pedro Dario	Trinidad Encina	MASCULINO	\N	PARAGUAYO/A	0982301019	CASADO/A	\N	\N	\N	SI	NO	754	26 de Junio Z-5B	\N	\N	\N	3	.
5670127	Gladys	Trinidad Vega	FEMENINO	\N	PARAGUAYO/A	0991678418	UNION DE HECHO	\N	\N	\N	SI	NO	746	Vecinos Unidos Z 5B	\N	\N	\N	4	Cuenta con un Corredor
3833539	Juan Manuel	Troche Mereles	MASCULINO	\N	PARAGUAYO/A	0981335210	UNION DE HECHO	\N	\N	\N	SI	NO	2565	Mtro Urcisino Velazco	\N	\N	\N	0	.
583319	Generosa	Uliambre de Patino	FEMENINO	\N	PARAGUAYO/A	0991697555	CASADO/A	\N	\N	\N	SI	NO	1573	Ursicino Velasco	\N	\N	\N	0	Es propietaria del lote y la casa con ellos vive ssuhijos soltero y uno de ellos tiene su pareja y lleva 1 ano de concubinato.
411	Victor	Uliambre Maidana	MASCULINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	2170	Mto. Ursicino Velasco	\N	\N	\N	0	.
3400676	Romina Monserrat	Uliambre Vázquez	MASCULINO	1983-01-10	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	2171	.	\N	\N	\N	0	El salon que mide 6x4 se utiliza para la venta de comidas rapida y productos varios.
914988	Floria	Uliambre Vda. De Idoyaga	FEMENINO	2055-05-07	PARAGUAYO/A	0982487656	VIUDO/A	\N	\N	\N	SI	NO	1923	Ursicino Velasco c/ Sotero Colman	\N	\N	\N	0	Los hijos se encuentran trabajando en Espana por la crisis laboral en nuestro pais y posee una Despensa bien surtido.
1601991	Unión Evangelica Misionera del Paraguay	.	.	\N	.	0982256863	.	\N	\N	\N	SI	NO	553	Ursicino Velasco c/ San Cayetano	\N	\N	\N	0	Dentro del lote hay un tinglado que se utiliza para actividades Religiosas, cultos, actividades para niños, merienda y enseñanzas biblicas, cuenta con 30 miembros su personeria Jurídica es. Dispone de estos elementos:
736423	María Graciela	Urbieta	FEMENINO	\N	PARAGUAYO/A	0986554970	SOLTERO/A	\N	\N	\N	SI	NO	1737	Ursicino Velasco	\N	\N	\N	0	.
2316927	Sebastian	Ureña Aquino	MASCULINO	2053-06-05	PARAGUAYO/A	0992533236	SOLTERO/A	\N	\N	\N	SI	NO	1411	Bañado Koeti	\N	\N	\N	5	.
783004	Felipa	Ureña de Montiel	FEMENINO	2058-01-05	PARAGUAYO/A	0972427816	SEPARADO/A	\N	\N	\N	SI	NO	1768	Koety	\N	\N	\N	0	.
5593548	Gabriela Elizabeth	Ureña Martínez	FEMENINO	\N	PARAGUAYO/A	0995633232	UNION DE HECHO	\N	\N	\N	SI	NO	1413	Bañado Koeti	\N	\N	\N	5	.
31	USF	.	.	\N	.	.	.	\N	\N	\N	SI	NO	2160	Ursicino Velasco y Santa Librada	\N	\N	\N	0	.
2902585	Loreto	Valazquez Chávez	MASCULINO	2050-10-12	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	1922	Urcicino Velazco c/ Julio	\N	\N	\N	0	la Sra Julissa  es esposa del nieto del jefe de hogar  y el niño Thiago es hijo de la pareja, osea bisnieto del titular. Poseen una casa de material, el dormitorio y los otros dos son de madera
1193287	Rosa Ramona	Valdés González	FEMENINO	\N	PARAGUAYO/A	0986241290	UNION DE HECHO	\N	\N	\N	SI	NO	2078	Ursisino Velazco	\N	\N	\N	0	Tiene un quincho en la parte superior de 110 m2 de perfil u chapa de zinc , en frente tiene un corredor de 3x10 enrejado
1120013	Jorge Ramón	Valdéz Escobar	MASCULINO	\N	PARAGUAYO/A	0992 468255	CASADO/A	\N	\N	\N	SI	NO	1670	Yacare Pito	\N	\N	\N	3	la casa era de la tia de Jorge Ramón Valdez.
4298894	Elizabeth Concepción	Verón Giménez	FEMENINO	\N	PARAGUAYO/A	0991754129	UNION DE HECHO	\N	\N	\N	SI	NO	343	AROMITA	\N	\N	\N	0	.
2459221	Lorenza Marlene	Valdéz Escobar	FEMENINO	2059-10-08	PARAGUAYO/A	0981952005	CASADO/A	\N	\N	\N	SI	NO	1647	Jakare Pito	\N	\N	\N	3	El señor Alcides Restituto es hermano de la sra Lorenza, vive con la familia, tiene su propia pieza
1129235	Gilda Estela	Valdéz Escovar	FEMENINO	\N	PARAGUAYO/A	.	CASADO/A	\N	\N	\N	SI	NO	1685	Yacare Pito	\N	\N	\N	3	El señor Mariano Escobar, de Lambaré, es tio de la señora Gilda Estela. Vive en el fondo del lote, hace 5 años.
3741459	Claudia Valvina	Valdéz Mendoza	FEMENINO	1983-02-03	PARAGUAYO/A	0982155369	UNION DE HECHO	\N	\N	\N	SI	NO	1697	Bañado Koeti	\N	\N	\N	0	.
4631587	María Alejandra	Valdéz Núñez	FEMENINO	1990-06-11	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1661	Yacare Pito	\N	\N	\N	3	La pareja vive con los abuelos de María Alejandra Valdez.
2323313	Juan Hugo Enrique	Valdéz	MASCULINO	\N	PARAGUAYO/A	0981165925	CASADO/A	\N	\N	\N	SI	NO	2402	Maynumby	\N	\N	\N	0	.
1545373	Viviana Noemi	Valenzuela Mencia	FEMENINO	1981-12-07	PARAGUAYO/A	0981805116	.	\N	\N	\N	SI	NO	1945	Ursicino Velasco c/ 28 y Juan Leon Mallorquin	\N	\N	\N	0	Era la casa de la abuela luego se les quedo a la madre donde viven la misma y una hna. Mas.
2641151	Néstor Damián	Valiente González	MASCULINO	\N	PARAGUAYO/A	282156	.	\N	\N	\N	SI	NO	2040	Ursicino Velasco	\N	\N	\N	0	La mama del propietario lleva 50 anos en el Banado cuenta con permiso de ocupación.
2365967	María Angélica	Vallejos Ortigoza	FEMENINO	2054-09-08	PARAGUAYO/A	0991946838	SOLTERO/A	\N	\N	\N	SI	NO	673	San Cayetano y 23 de Junio	\N	\N	\N	0	.
1984651	Bernarda	Vallejos	FEMENINO	2050-01-04	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	179	Julio Jara C/ Nuevo Amanecer	\N	\N	\N	0	.
6007550	Viviana Concepción	Vallejos	FEMENINO	1991-09-06	PARAGUAYO/A	0992740704	UNION DE HECHO	\N	\N	\N	SI	NO	674	San Cayetano	\N	\N	\N	0	La pareja vive en el lote del titular quien es su madre, tienen un techo precario
5555035	José Ramón	Varela Brizuela	MASCULINO	1995-07-01	PARAGUAYO/A	0986388247	UNION DE HECHO	\N	\N	\N	SI	NO	2500	.	\N	\N	\N	0	.
6215389	Lourdes Mabel	Varela Estigarribia	FEMENINO	1997-06-12	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1680	Pasillo IVU	\N	\N	\N	0	La Sra. Se aconcubino a los 14 anos.
5355076	Francisco Javier	Varela Gamarra	MASCULINO	\N	PARAGUAYO/A	0991242056	UNION DE HECHO	\N	\N	\N	SI	NO	821	Vecinos Unidos	\N	\N	\N	3	.
3644249	Juan Antonio	Varela Gamarra	MASCULINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1323	.	\N	\N	\N	0	La casa se derrumbo todo cuando subio el agua y actualmente el Sr. Juan vive en alquiler y tiene un contrato de 8 meses ma.
5873876	Luis Cesar	Varela Gamarra	MASCULINO	\N	PARAGUAYO/A	0972835990	UNION DE HECHO	\N	\N	\N	SI	NO	1470	Ursicino Velasco	\N	\N	\N	0	Centrifugadora y cocina electrica:1 c/u.La pareja lleva dos anos de concubinato,viven en el lote de la titular con un techo aparte de material ,el joven Luis Varela mantiene su casa a traves de su trabajo en uno de los Astilleros del Banado.No tienen lot
5010105	Wilson Daniel	Varela Gamarra	MASCULINO	\N	PARAGUAYO/A	0986358505	UNION DE HECHO	\N	\N	\N	SI	NO	1204	Fidelina	\N	\N	\N	0	.
824	José Joaquín	Varela Martínez	MASCULINO	\N	PARAGUAYO/A	0984768866	SOLTERO/A	\N	\N	\N	SI	NO	2145	Agosto Poty	\N	\N	\N	0	.
674000	Eleuterio	Varela Olmedo	MASCULINO	\N	PARAGUAYO/A	0994697744	SOLTERO/A	\N	\N	\N	SI	NO	770	26 de Julio Z-5B	\N	\N	\N	0	Es un señor que vive solo, trabaja como herrero.
3322075	Lourdes Beatriz	Varela Ramírez	FEMENINO	\N	PARAGUAYO/A	0983155829	SOLTERO/A	\N	\N	\N	SI	NO	1560	Ursicino Velasco	\N	\N	\N	0	Vive con la propietaria en una construccion precaria 10x6, y es madre soltera.
1226831	José Joaquín	Varela	MASCULINO	\N	PARAGUAYO/A	0992210477	VIUDO/A	\N	\N	\N	SI	NO	2484	.	\N	\N	\N	0	.
1800672	Graciela	Vargas de Bobadilla	FEMENINO	\N	PARAGUAYO/A	0982426010	CASADO/A	\N	\N	\N	SI	NO	426	Julio Jara	\N	\N	\N	4	Vive dentro del lote de Ceferino . Tiene una vivienda bien construida de material independiente.
1220455	Librada	Vargas González	FEMENINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	1651	Jakare Pito	\N	\N	\N	3	Dormitorio y cocina dentro de la vivienda, es beneficiaria del cobro de la tercera edad, se maneja de manera independiente
2970477	Osvalo Daniel	Vargas	MASCULINO	1979-04-07	PARAGUAYO/A	0985 885708	UNION DE HECHO	\N	\N	\N	SI	NO	1681	Yacare Pito	\N	\N	\N	3	El baño comparte con su madre.
1235706	Rogelio	Vargas	MASCULINO	\N	PARAGUAYO/A	.	VIUDO/A	\N	\N	\N	SI	NO	1009	Ursicino Velasco	\N	\N	\N	0	El Sr Rodrigo vive en el lote de la nieta y tiene una casa muy bien constituida vieve solo y se dedica al reciclafdo.
3806316	Arturo Osmar	Vázquez Ortiz	MASCULINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	2182	.	\N	\N	\N	0	Notebook 1.Tiene sala, cocina ,comedor.
4618886	Elias Hernan	Vega Acosta	MASCULINO	1990-12-03	PARAGUAYO/A	0972283922	UNION DE HECHO	\N	\N	\N	SI	NO	629	Angel Luis c/ Ursicino Velasco	\N	\N	\N	0	.
1142208	Blanca Alicia	Vega Barrios	FEMENINO	2066-07-11	PARAGUAYO/A	0982142639	CASADO/A	\N	\N	\N	SI	NO	513	Urcicino Velazco	\N	\N	\N	0	.
1564976	Juan Carlos	Vega Barrios	MASCULINO	\N	PARAGUAYO/A	0972724071	UNION DE HECHO	\N	\N	\N	SI	NO	630	Angel Luis	\N	\N	\N	0	.
930588	Lucíana	Vega Barrios	FEMENINO	2052-07-01	PARAGUAYO/A	0981396829	SOLTERO/A	\N	\N	\N	SI	NO	632	Angel Luis	\N	\N	\N	0	.
2378212	Anania	Vega Benítez	FEMENINO	2046-01-12	PARAGUAYO/A	482572	SOLTERO/A	\N	\N	\N	SI	NO	1916	Ursicino Velasco c/ Sotero Colman	\N	\N	\N	0	Tiene un salon totalmente aparte para su despensa ,tiene un sueldo ,la sra. Es soltera sin hijos el terreno es de als dos hermanas.
634711	Dominga	Vega Benítez	FEMENINO	\N	PARAGUAYO/A	424966	CASADO/A	\N	\N	\N	SI	NO	1921	Ursicino Velasco c/ Sotero Colman	\N	\N	\N	0	El primer piso de la vivienda es de hormigon y la segunda es de teja.
5497443	María Ursulina	Vega Garcia	FEMENINO	\N	PARAGUAYO/A	0991 600 902	UNION DE HECHO	\N	\N	\N	SI	NO	2326	Nuestra Señora de la Asunción	\N	\N	\N	6	La pareja lleva ocho años de casados.
5968598	Santiago	Vega Garcia	MASCULINO	\N	PARAGUAYO/A	0984816954	SOLTERO/A	\N	\N	\N	SI	NO	1949	Ursicino Valasco C/ Puente Pesoa	\N	\N	\N	0	La casa era de la madre la cual fallecio hace 1 ano dejandolo como titular y a la nieta la cual crio desde pquena.
4212741	Balbina Beatriz	Vega SantaCruz	FEMENINO	\N	PARAGUAYO/A	0992955103	UNION DE HECHO	\N	\N	\N	SI	NO	740	Ursicino Velasco C/ Martin Fierro	\N	\N	\N	0	.
1515664	Mirian Celeste	Vega SantaCruz	FEMENINO	\N	PARAGUAYO/A	0983952206	SOLTERO/A	\N	\N	\N	SI	NO	681	Yacare Yrupe	\N	\N	\N	5	.
1341033	Honoria Paola	Vega Vera	FEMENINO	\N	PARAGUAYO/A	0991544338	CASADO/A	\N	\N	\N	SI	NO	1942	Ursicino Velasco c/ Julio Benitez	\N	\N	\N	0	Hace 12 anos estan casados y siguen tartamientos para tener hijos.
3500294	Olivia de los Angeles	Vega Vera	FEMENINO	\N	PARAGUAYO/A	0981935359	CASADO/A	\N	\N	\N	SI	NO	1941	Ursicino Velasco c/ Julio Benitez	\N	\N	\N	0	Es la casa de la madre los ninos son hijastros pero viven con ellos se caso hace 2 anos con 4 anos de relacion con el marido.
4467318	Cinthia Beatriz	Vega	FEMENINO	1983-09-11	PARAGUAYO/A	0982700668	SEPARADO/A	\N	\N	\N	SI	NO	1951	Ursicino Valasco C/ Puente Pesoa	\N	\N	\N	0	Cocina 1.
3010	Pedro - Casa Parroquial	Velasco	MASCULINO	\N	.	0961166494	SOLTERO/A	\N	\N	\N	SI	NO	1219	Ursicino Velasco	\N	\N	\N	0	Es una propiedad de CAMSAT.
4951854	Virginia	Verón Giménez	FEMENINO	1981-11-01	PARAGUAYO/A	0991458145	UNION DE HECHO	\N	\N	\N	SI	NO	1138	Virgen del Lujan	\N	\N	\N	0	.
2358010	Arcenio Javier	Velázquez Benega	MASCULINO	1977-11-09	PARAGUAYO/A	0982988903	UNION DE HECHO	\N	\N	\N	SI	NO	2166	Julio Benitez c/ Ursicino Velasco	\N	\N	\N	0	Son pareja hace 12 anos pero no tienen hijos juntos.
4503219	Carolina	Velázquez Benega	FEMENINO	\N	PARAGUAYO/A	PAR	UNION DE HECHO	\N	\N	\N	SI	NO	1950	.	\N	\N	\N	0	.
4210978	Cintia	Velázquez Benegas	FEMENINO	1980-09-05	PARAGUAYO/A	0984189619	UNION DE HECHO	\N	\N	\N	SI	NO	899	Martin Fierro	\N	\N	\N	0	.
1221422	Ignacia	Velázquez Chávez	FEMENINO	\N	PARAGUAYO/A	0991231020	.	\N	\N	\N	SI	NO	1892	Julio Benitez	\N	\N	\N	0	.
4140863	Laura Patricia	Velázquez Cubilla	FEMENINO	\N	PARAGUAYO/A	0982660509	UNION DE HECHO	\N	\N	\N	SI	NO	1471	El terreno tiene una construcción de material hace	\N	\N	\N	0	.
1267902	Norma	Velázquez de Barrios	FEMENINO	2068-11-05	PARAGUAYO/A	0982848450	SEPARADO/A	\N	\N	\N	SI	NO	977	Malvinas c/ 16 de Agosto	\N	\N	\N	0	.
1421942	María Belen	Velázquez de Bauza	FEMENINO	\N	PARAGUAYO/A	0982891840	SOLTERO/A	\N	\N	\N	SI	NO	1294	Mto Ursicino VelascoE/ Remancito	\N	\N	\N	0	La vivienda cuenta con un galpon y un corredor.
4211018	Lorena	Velázquez de Cristaldo	FEMENINO	\N	PARAGUAYO/A	0985200899	SEPARADO/A	\N	\N	\N	SI	NO	2447	Ursicino Velasco	\N	\N	\N	0	.
772607	Victoria de los Angeles	Velázquez de Sanabria	MASCULINO	2057-03-09	PARAGUAYO/A	098277129	CASADO/A	\N	\N	\N	SI	NO	864	Ursicino Velasco	\N	\N	\N	0	Dentro de este lote y bajo el mismo techo vievn 3 nucleos fliares.
4986644	Fatima	Velázquez Encina	FEMENINO	1991-12-05	PARAGUAYO/A	0982 252 278	CASADO/A	\N	\N	\N	SI	NO	2229	Chiquero	\N	\N	\N	0	La familia vive en extrema pobreza. Su sobrina le regaló la parte del fondo del terreno para que cargue y construya su casa.
5289656	Juan Agustín	Velázquez Fernández	MASCULINO	\N	PARAGUAYO/A	0983128928	UNION DE HECHO	\N	\N	\N	SI	NO	1527	Mtro Ursicino Velasco	\N	\N	\N	0	La pareja refiere que convive hace 3 años, viven dentro del mismo lote de la madre pero en una ampliacion de la vivienda.
3883852	Flora	Velázquez	FEMENINO	\N	PARAGUAYO/A	0986308486	SOLTERO/A	\N	\N	\N	SI	NO	372	Cristhian Ferreira	\N	\N	\N	0	Los hijos de la titular son padres solteros.
1030156	Dolly Cristina	Vera Aranda	FEMENINO	\N	PARAGUAYO/A	0982606276	SOLTERO/A	\N	\N	\N	SI	NO	1805	Mainumby y Ranco 8	\N	\N	\N	0	Detrás del techo de la titular vive su ex marido, con una casa precaria independiente a la de ella, Tienen 3 piezas que tilizzan como dormitorio y un baño precario, ambos son recicladores (la titular y su ex marido). Llevan separado siete años, dentro de
1127588	Iisidro Eduardo	Vera Aranda	MASCULINO	2064-06-02	PARAGUAYO/A	0971536529	CASADO/A	\N	\N	\N	SI	NO	2157	Mto. Ursicino Velasco	\N	\N	\N	0	La flia cuenta con un inquilinato , con 5 habitaciones independientes para alquilar,actualmente 3 estan alquilando.
772809	Marga Esther	Vera Aranda	FEMENINO	\N	PARAGUAYO/A	0981513331	SOLTERO/A	\N	\N	\N	SI	NO	2263	Nuestra Sra. De la Asuncion	\N	\N	\N	0	Placas Electricas 2.
1059551	María Cristina Delpilar	Vera Brítez	FEMENINO	2061-11-10	PARAGUAYO/A	0994741789	UNION DE HECHO	\N	\N	\N	SI	NO	2493	Rancho 8	\N	\N	\N	0	el horno es industrial, 1 ventilador de pie, refinadora 1, maquina de moler maiz 1, cierra cárnica 1
2182506	Victor Leonor	Vera Brítez	MASCULINO	1971-01-07	PARAGUAYO/A	0994765731	CASADO/A	\N	\N	\N	SI	NO	1817	Rancho 8	\N	\N	\N	0	Dentro de la Vivienda del titular vive su hijo Adan Vera, con su pareja, llevan un año de concubinato. Los mismos cuentan con una pieza de 3 metros de frente y 2 metros de fondo, no tienen hijos, ni lote propio, pero si cuentan con un techo precario, la
1240606	Margarita	Vera de Avalos	FEMENINO	2057-10-06	PARAGUAYO/A	0972546871	CASADO/A	\N	\N	\N	SI	NO	735	Mto. Ursicino Velasco	\N	\N	\N	0	La pareja Liz Carolina Y Jose Zalazar vive en el mismo lote pero con un techo precario aparte a la del titular.
882284	Gladys Marina	Vera de Diana	FEMENINO	\N	PARAGUAYO/A	0983376603	SOLTERO/A	\N	\N	\N	SI	NO	2275	.	\N	\N	\N	0	Casa de dos plantas.
2702746	Elisa	Vera de González	FEMENINO	2052-01-09	PARAGUAYO/A	0993565991	SEPARADO/A	\N	\N	\N	SI	NO	165	Nuevo Amanecer	\N	\N	\N	0	.
3511793	Silvia	Vera de Ramírez	FEMENINO	2061-03-11	PARAGUAYO/A	0982468924	VIUDO/A	\N	\N	\N	SI	NO	1305	Remancito	\N	\N	\N	0	Uno de los miembros de la flia. Se necuentra privado de su libertad.
937103	Margarita	Vera de Vega	FEMENINO	\N	PARAGUAYO/A	426376	CASADO/A	\N	\N	\N	SI	NO	1940	Ursicino Velasco c/ Julio Benitez	\N	\N	\N	0	Telefono:Linea Baja 1,Celulares 3.
1680370	Gloria	Vera de zacarias	FEMENINO	\N	PARAGUAYO/A	0971652647	CASADO/A	\N	\N	\N	SI	NO	470	Martin Fierro	\N	\N	\N	0	Es duena del lote donde viven sus hijos solteros uno de elllos tiene un hijo menor y esta embarazada.
4486914	Luis María	Vera Delvalle	MASCULINO	\N	PARAGUAYO/A	0981166033	UNION DE HECHO	\N	\N	\N	SI	NO	601	Malvinas c/16 de Agosto	\N	\N	\N	0	el palafito era del padre según los vecinos el hijo no vive hay esta en un alquiler con su pareja en otro lugar.
5007608	Karen Rocío	Vera Esquivel	FEMENINO	\N	PARAGUAYO/A	0972654513	UNION DE HECHO	\N	\N	\N	SI	NO	1150	Aromita	\N	\N	\N	0	.
5223616	Andrea Celeste	Vera Figueredo	FEMENINO	1990-06-11	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1145	Aromita	\N	\N	\N	0	Vive en el terreno de Maria Mino.
3702488	Debora Gabriela	Vera Franco	FEMENINO	\N	PARAGUAYO/A	0972501296	UNION DE HECHO	\N	\N	\N	SI	NO	1818	Rancho 8	\N	\N	\N	0	La pareja lleva 3 años de concubinato, no tienen lote, viven bajo el techo del titular del padre, utilizan un dormitorio de manejan de manera independiente a la familia titular
3184676	Wilfrido	Vera López	MASCULINO	\N	PARAGUAYO/A	0981982891	UNION DE HECHO	\N	\N	\N	SI	NO	482	Remancito	\N	\N	\N	0	.
4087355	Osmar Rene	Vera Morel	MASCULINO	\N	PARAGUAYO/A	0982786922	UNION DE HECHO	\N	\N	\N	SI	NO	1357	Ursicino Velasco	\N	\N	\N	0	Hace 15 años vive en el barrio, vive en la propiedad de la señora Juana Viuda de Ayala, en calidad de inquilinato en una construcción de 6x4 de material.
1933901	Pedro	Vera Parra	MASCULINO	2065-09-09	PARAGUAYO/A	0984 968 986	CASADO/A	\N	\N	\N	SI	NO	2680	Pasillo sin nombre	\N	\N	\N	0	El señor Pedro Vera manifiesta que sus hijos y su pareja vivia con él en el lote. 06-11. Se mudaron a altos durante la inundación. El mismo manifiesta que su familia va a volver, pero desconoce mayores datos.
21021	Estanislao	Vera Sandoval	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	1322	26 de Julio	\N	\N	\N	0	.
4274217	Mario	Vera Sanguina	MASCULINO	1977-10-02	PARAGUAYO/A	0984545627	UNION DE HECHO	\N	\N	\N	SI	NO	1447	Bañado Koeti	\N	\N	\N	5	.
6649983	María Noemi	Vera Torales	FEMENINO	\N	PARAGUAYO/A	0991884996	UNION DE HECHO	\N	\N	\N	SI	NO	1365	Yvoty Rory	\N	\N	\N	0	.
1112398	María Aurora	Vera Vda. De Barreto	FEMENINO	\N	PARAGUAYO/A	0983861051	VIUDO/A	\N	\N	\N	SI	NO	278	Ursicino Velasco	\N	\N	\N	0	.
1214460	De los Santos	Vera Vda. De Benítez	MASCULINO	2052-01-11	PARAGUAYO/A	0994762070	VIUDO/A	\N	\N	\N	SI	NO	1834	Santa Librada	\N	\N	\N	0	.
4783940	Gustavo Adolfo	Vera	MASCULINO	1988-05-03	PARAGUAYO/A	0992137113	UNION DE HECHO	\N	\N	\N	SI	NO	2109	Chiquero	\N	\N	\N	0	.
1096204	Petronilo	Vera	MASCULINO	\N	PARAGUAYO/A	0994741789	SOLTERO/A	\N	\N	\N	SI	NO	1819	Rancho 8	\N	\N	\N	0	el titular posee pension
4523617	Cinthia Carolina	Verón de Arriola	FEMENINO	\N	PARAGUAYO/A	0981435786	CASADO/A	\N	\N	\N	SI	NO	816	26 de Julio	\N	\N	\N	0	.
4535728	Fátima Bogginer	Verón Villalba	FEMENINO	1985-10-11	PARAGUAYO/A	0982274822	UNION DE HECHO	\N	\N	\N	SI	NO	1102	26 de juio	\N	\N	\N	0	El dueño no permitio que se le saque fotos a su propiedad, la foto frontal fue tomada desde afuera.
672018	Basilio	Verza González	MASCULINO	\N	Praguaya	0985 204 639	UNION DE HECHO	\N	\N	\N	SI	NO	1909	Martin Fierro y Pilcomayo	\N	\N	\N	0	La pareja lleva cincuenta años de concubinato.
3357019	Mirian Ángela	Vian	FEMENINO	\N	PARAGUAYO/A	0992682773	SOLTERO/A	\N	\N	\N	SI	NO	2395	Yvoty Rory	\N	\N	\N	0	.
5334573	Gabriela Verónica	Vichini Franco	FEMENINO	\N	PARAGUAYO/A	0982223723	UNION DE HECHO	\N	\N	\N	SI	NO	2032	Sagrado Corazón de Jesús	\N	\N	\N	0	Vive dentro del lote y la casa del abuelo hace 7 anos, comparte electrodomesticos con la duena del lote.
55	Susana	Vidal Benítez	FEMENINO	1984-02-02	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	251	.	\N	\N	\N	0	Se nego a que se le quite la foto.
4202006	Eulogio	Villalba Agüero	MASCULINO	1985-04-02	PARAGUAYO/A	0986197254	UNION DE HECHO	\N	\N	\N	SI	NO	125	Nuevo Amanecer	\N	\N	\N	0	.
4465867	Elida Clariza	Villalba Alfonso	FEMENINO	1993-12-04	PARAGUAYO/A	0982738972	SOLTERO/A	\N	\N	\N	SI	NO	1721	Ursicino Velasco	\N	\N	\N	0	Vive dentro del lote y la casa de la mama con sus hijos.Comparte electrodomesticos con la mama.
2949961	Celso Bonifacio	Villalba Bareiro	MASCULINO	\N	PARAGUAYO/A	0992377845	UNION DE HECHO	\N	\N	\N	SI	NO	340	Nuevo Amanecer	\N	\N	\N	0	Cuenta con un PALAFITO ,nose puede quitar de costado.
794409	Balbina	Villalba Barreto	FEMENINO	2059-01-04	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	1708	Virgen de Guadalupe	\N	\N	\N	0	El Sr. Narciso tiene una casita muy precaria en el lote de su hermana.
725723	Casimira	Villalba Barreto	FEMENINO	2049-04-03	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	2674	Nuestra Sra. De la Asunción	\N	\N	\N	6	.
693749	Lucía	Villalba Barreto	FEMENINO	2055-02-02	PARAGUAYO/A	0972488781	SOLTERO/A	\N	\N	\N	SI	NO	1815	Rancho 8	\N	\N	\N	0	Durante 17 años la titular tuvo una construcción de madera precaria, hace 5 años fue adjudicada, con una vivienda de la cooperativa bañado
5271103	Marilda	Villalba Benítez	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1454	Virgen de Guadalupez	\N	\N	\N	0	.
3554907	Alejandra	Villalba Brítez	FEMENINO	1979-05-07	PARAGUAYO/A	0981712829	UNION DE HECHO	\N	\N	\N	SI	NO	997	Maria Elena c/ Mar de Plata	\N	\N	\N	0	.
4117081	Alicia Genara	Villalba Brítez	FEMENINO	1988-05-09	PARAGUAYO/A	0911404838	CASADO/A	\N	\N	\N	SI	NO	720	Virgen de Lujan	\N	\N	\N	5	.
5007215	Eli Johana	Villalba Cuevas	FEMENINO	\N	PARAGUAYO/A	0994564875	UNION DE HECHO	\N	\N	\N	SI	NO	1071	Mar de Plata	\N	\N	\N	0	.
5989794	Nidia Concepción	Villalba de Benítez	FEMENINO	1986-08-12	PARAGUAYO/A	0984168907	CASADO/A	\N	\N	\N	SI	NO	2542	Mandy Jura	\N	\N	\N	0	.
2484424	Dora Elizabeth	Villalba E.	FEMENINO	1977-08-04	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	2035	.	\N	\N	\N	0	.
1340001	Julián Ricardo	Villalba Figueredo	MASCULINO	2055-07-02	PARAGUAYO/A	0981712829	SOLTERO/A	\N	\N	\N	SI	NO	999	María Elena C/ Mar de Plata	\N	\N	\N	0	Casa precaria.
6590922	Estela Concepcion	Villalba Franco	FEMENINO	\N	PARAGUAYO/A	0983710308	UNION DE HECHO	\N	\N	\N	SI	NO	1668	.	\N	\N	\N	0	.
787030	Ramón Ovidio	Villalba Méndez	MASCULINO	2059-07-09	PARAGUAYO/A	0981186259	DIVORCIADO/A	\N	\N	\N	SI	NO	1217	Mto. Ursicino Velasco	\N	\N	\N	4	Tiene 1 sierra carnicera, elementos de trabajo ( redes de pesca , 3 motores fuera de borda, 3 canoas).
5992745	Juan Rafael	Villalba Silva	MASCULINO	\N	PARAGUAYO/A	0972844973	SOLTERO/A	\N	\N	\N	SI	NO	1187	Fidelina	\N	\N	\N	0	Tres menores conviven con su tio mayor.
3306071	Amanda	Villalba	FEMENINO	\N	PARAGUAYO/A	0991463482	UNION DE HECHO	\N	\N	\N	SI	NO	2456	Mandyjura	\N	\N	\N	0	vive dentro del lote de su hija con su familia
6254015	Belén Isabel	Villalba	FEMENINO	\N	ARGENTINO/A	0971710278	SOLTERO/A	\N	\N	\N	SI	NO	1452	Virgen de Guadalupe	\N	\N	\N	0	Tiene una despensa que es su fuente de ingreso
6250662	Isidora	Villalba	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	2449	Mandyjura	\N	\N	\N	0	Es propietaria del lote. En el fondo vive la mamá con su familia.
5491951	Mabel Rocio	Villalba	FEMENINO	\N	PARAGUAYO/A	0992672471	CASADO/A	\N	\N	\N	SI	NO	2610	Pasillo IVU	\N	\N	\N	0	La sra. Posee un negocio enNuflo y tte. Robles un copetin hace 10 anos,según vecinos la sra no vive en el Banado vive en la casa de la suegra hace 12 anos y que solo viene para el censo.
362404	Vidal Luis	Villalba	MASCULINO	\N	PARAGUAYO/A	0983710308	UNION DE HECHO	\N	\N	\N	SI	NO	1667	.	\N	\N	\N	0	El Sr. Es empleado Militar esta en la parte de mantenimiento con un sueldo minimo y tiene un hijo en cama.
2279997	Fermina	Villamallor Aquino	FEMENINO	\N	PARAGUAYO/A	0981713538	UNION DE HECHO	\N	\N	\N	SI	NO	960	Angel Luis c/ Virgen de Lourdes	\N	\N	\N	0	.
3312425	Blanca Elizabeth	Villamayor Quintana	FEMENINO	\N	PARAGUAYO/A	0982931020	CASADO/A	\N	\N	\N	SI	NO	717	Pasillo Angel mis	\N	\N	\N	0	.
5940725	Daniel Alejandro	Villanueva Ayala	MASCULINO	1993-12-11	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	2311	Payagua Naranja	\N	\N	\N	0	.
2224891	Mirian Elizabeth	Villanueva Bolaño	FEMENINO	\N	PARAGUAYO/A	0991222351	UNION DE HECHO	\N	\N	\N	SI	NO	2656	Nuestra Sra. De la Asunción	\N	\N	\N	6	Seca Ropas 1.
2687990	Casimira	Villar Giménez	FEMENINO	2062-04-03	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	1669	Pasillo Sin Nombre	\N	\N	\N	0	Tiene una casa de la cas Cooperativa y una casa de la Pro-Vida.
1932542	Celestino	Villar Giménez	MASCULINO	\N	PARAGUAYO/A	0994 230 920	SOLTERO/A	\N	\N	\N	SI	NO	1779	Pasillo sin nombre	\N	\N	\N	0	tiene dos dormitorios de pro vida y uno de madera
1670433	Delfina	Villar Giménez	FEMENINO	\N	PARAGUAYO/A	0986337757	UNION DE HECHO	\N	\N	\N	SI	NO	1441	Bañado Koeti	\N	\N	\N	5	La familia compró la vivienda hace 5 años, y no sabe hace cuanto años fue construida la vivienda
4695485	Elena Elizabet	Villar Maldonado	FEMENINO	\N	PARAGUAYO/A	0971285783	UNION DE HECHO	\N	\N	\N	SI	NO	120	Virgen de Lourdes	\N	\N	\N	0	.
6111037	Ovidio Adolfo	Villasanti Ledesma	MASCULINO	1996-09-09	PARAGUAYO/A	0991915682	CASADO/A	\N	\N	\N	SI	NO	1134	Virgen de Lujan c/ Los Sauces	\N	\N	\N	0	.
3662502	Gladys Zunilda	Villasboa de Barreto	FEMENINO	\N	PARAGUAYO/A	0992722179	CASADO/A	\N	\N	\N	SI	NO	1026	Mar de plata c/ Nuevo Amanecer	\N	\N	\N	0	.
5852774	Andrea Elizabeth	Villlamayor Martínez	FEMENINO	1994-01-07	PARAGUAYO/A	0991872802	UNION DE HECHO	\N	\N	\N	SI	NO	112	Virgen de Lourdes e/ Yvoty	\N	\N	\N	3	.
3553063	Hugo Gilberto	Villordo	MASCULINO	\N	PARAGUAYO/A	0971 162 163	UNION DE HECHO	\N	\N	\N	SI	NO	2172	Nuevo Amanecer	\N	\N	\N	0	.
268886	Silveira	Viveros Melgarejo	FEMENINO	\N	PARAGUAYO/A	0981534075	UNION DE HECHO	\N	\N	\N	SI	NO	709	Algarrobo C/ Ursicino Velasco	\N	\N	\N	0	.
1214457	Mirta Elizabeth	Viveros	FEMENINO	\N	PARAGUAYO/A	0981947321	CASADO/A	\N	\N	\N	SI	NO	980	Malvinas	\N	\N	\N	0	.
2151457	Rosa Estela	Viveros	FEMENINO	\N	PARAGUAYO/A	09722178233	UNION DE HECHO	\N	\N	\N	SI	NO	711	Albarroba C/ Ursicino Velasco	\N	\N	\N	0	Tiene bajo su tutela a dos menores de edad que viven con ella desde pequeños .
4757593	María Esther	Zacarias Vera	FEMENINO	1989-04-11	PARAGUAYO/A	0972486769	UNION DE HECHO	\N	\N	\N	SI	NO	433	San Cayetano	\N	\N	\N	0	La ocupante manifiesta que su pareja esta asentada en este lugar hace 24 anos y estan de pareja hace 9 anos.
6312964	Deisy Mabel	Zalazar Cáceres	FEMENINO	\N	PARAGUAYO/A	.	UNION DE HECHO	\N	\N	\N	SI	NO	110	OLEROS	\N	\N	\N	0	.
3743460	Dollys Ramona	Zalazar Cáceres	FEMENINO	\N	PARAGUAYO/A	0985258653	.	\N	\N	\N	SI	NO	1399	Bañado Koeti	\N	\N	\N	5	.
4995978	Eugenia Yolanda	Zalazar Cáceres	FEMENINO	2068-01-01	PARAGUAYO/A	0982454472	UNION DE HECHO	\N	\N	\N	SI	NO	660	23 de Junio	\N	\N	\N	0	.
4134613	Antonio Gervacio	Zalazar	MASCULINO	\N	PARAGUAYO/A	0992306110	SOLTERO/A	\N	\N	\N	SI	NO	585	San Cayetano	\N	\N	\N	0	El señor Antonio tiene su techo precario dentro del lote de su hija. Hace 27 años que vive separado de su señora.
2591478	José Luis	Zaracho González	MASCULINO	\N	PARAGUAYO/A	0982775995	UNION DE HECHO	\N	\N	\N	SI	NO	256	San Felipe y Santiago	\N	\N	\N	0	.
2329604	Hector David	Zaracho López	MASCULINO	1976-01-03	PARAGUAYO/A	0981779386	CASADO/A	\N	\N	\N	SI	NO	2546	Santa Librada	\N	\N	\N	0	.
5555823	Ana María	Zaracho Rios	FEMENINO	\N	PARAGUAYO/A	0972791966	SOLTERO/A	\N	\N	\N	SI	NO	2350	Nuestra Señora de la Asuncion	\N	\N	\N	6	.
1889184	Fatima Lucía	Zaracho Vega	FEMENINO	\N	PARAGUAYO/A	00985704772	UNION DE HECHO	\N	\N	\N	SI	NO	47	Fondo Recicla	\N	\N	\N	0	.
1751519	Alba Rosa	Zarate de Gomez	FEMENINO	2068-05-05	PARAGUAYO/A	0961765764	CASADO/A	\N	\N	\N	SI	NO	2745	Angel Luis	\N	\N	\N	0	En la casa se realiza termo forrado con tejido a mano.La pareja vive dentro de la casa de la madre.
2460799	María de la Cruz	Zarate de Ramírez	FEMENINO	1977-03-05	PARAGUAYO/A	0994985262	SEPARADO/A	\N	\N	\N	SI	NO	2206	Ñasaindy	\N	\N	\N	0	es dueña del lote y de la casa, dentro de la misma tbm viven sus dos hijos con sus respectivas parejas
1511106	Bernardina	Zarate Godoy	FEMENINO	\N	PARAGUAYO/A	0986567979	UNION DE HECHO	\N	\N	\N	SI	NO	131	AROMITA	\N	\N	\N	0	.
5081609	Milva María Verónica	Zárate Valdéz	FEMENINO	\N	PARAGUAYO/A	0994255616	.	\N	\N	\N	SI	NO	2082	.	\N	\N	\N	0	Tiene un salón de 6x10.
3172979	María Dolores	Zarate	FEMENINO	\N	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	SI	NO	1324	26 de Julio	\N	\N	\N	0	Vive con su hijo, discapacitado mental, ella es ciega y no conoce la dimensión de su terreno.
46625513	Justo Ariel	Zarza Morel	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	1235	.	\N	\N	\N	0	Justo A. Zarza tiene un lote aparte y no le pusieron numero de lote ni de manzana, está ubicado al lado del lote 022 manzana 018, el señor se dedica a reciclar
4508140	Victoria de Jesus	Zarza Morel	MASCULINO	\N	PARAGUAYO/A	0986463705	UNION DE HECHO	\N	\N	\N	SI	NO	1319	Remancito	\N	\N	\N	0	.
5772861	Natalia Carolina	Zarza Soria	FEMENINO	\N	PARAGUAYO/A	0986815811	UNION DE HECHO	\N	\N	\N	SI	NO	728	Mto. Ursicino Velasco	\N	\N	\N	0	Vive dentro del lote del abuelo, pero tienen una casa precaria aparte.
1747707	Elodia	Zavala Ojeda	FEMENINO	1971-11-03	PARAGUAYO/A	0984716558	SOLTERO/A	\N	\N	\N	SI	NO	1503	Rancho 8	\N	\N	\N	0	Dentro del lote de la titular viven tres nucleos de familia bajo el mismo techo en piezas independientes.Tambien existe otro techo independiente donde vive su hijo mayor con su familia.
4218471	Ruth Verónica	Zayas Cabrera	FEMENINO	\N	PARAGUAYO/A	0971761876	UNION DE HECHO	\N	\N	\N	SI	NO	483	Remancito	\N	\N	\N	0	Posee un depositode 4,15x2,15 de dimensión , el bano tiene una dimensión de 3x14.
1706334	Ramona	Zayas Geraldo	MASCULINO	2058-01-07	PARAGUAYO/A	0984745295	CASADO/A	\N	\N	\N	SI	NO	1092	Fidelina	\N	\N	\N	0	La vivienda tiene una despensa, es su fuente de trabajo.
5993424	Vivian Mariel	Zoler	FEMENINO	\N	PARAGUAYO/A	0986532865	UNION DE HECHO	\N	\N	\N	SI	NO	1148	Virgen de Lujan c/ Los Sauces	\N	\N	\N	0	.
3721984	Cluadio José	Zorrila	MASCULINO	\N	PARAGUAYO/A	0983425094	.	\N	\N	\N	SI	NO	229	Detrás de RECICLA	\N	\N	\N	3	.
802069	Ramona Concepcion	Zorrilla de Yegros	MASCULINO	\N	PARAGUAYO/A	0994635434	SOLTERO/A	\N	\N	\N	SI	NO	1510	Rancho 8	\N	\N	\N	0	Centrifugadora y placa electrica 1 c/u.Dentro del techo B vive una pareja de jovenes con un ano de concubinato y embarazada de 1 mes
1334083	Gladys Joséfina	Zorrilla Servian	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	1674	Pasillo Sin Nombre	\N	\N	\N	0	.
3649626	Claide	Zorrilla Valdez	FEMENINO	\N	PARAGUAYO/A	0983375668	CASADO/A	\N	\N	\N	SI	NO	2580	Rancho 8 y San Luis	\N	\N	\N	0	Placa Electrica 1.
3976961	Leidys Antonia	Zorrilla Valdéz	FEMENINO	\N	PARAGUAYO/A	.	CASADO/A	\N	\N	\N	SI	NO	1657	Yacare Pito	\N	\N	\N	3	Comparte baño, cocina. Utilizan 2 dormitorios.
2374217	Zulma Emilia	Zuchini	FEMENINO	2062-09-12	PARAGUAYO/A	.	.	\N	\N	\N	SI	NO	2663	Nuestra Sra. De la Asunción	\N	\N	\N	6	La titular esta separada de su ex marido hace 3 anos.
4851392	Carmen Belén	Rojas Delvalle	FEMENINO	1993-11-29	PARAGUAYO/A	0981987305	SOLTERO/A	\N	\N	\N	NO	SI	5061	.	\N	\N	\N	1	.
6739326	Yohana María	Gonzalez	FEMENINO	1999-02-15	PARAGUAYO/A	0982428385	SOLTERO/A	\N	\N	\N	NO	SI	5063	.	\N	\N	\N	1	.
5244803	Dionicia Beatriz	Silvano López	FEMENINO	1987-10-08	PARAGUAYO/A	.	SOLTERO/A	\N	\N	\N	NO	SI	5066	.	\N	\N	\N	1	.
6948537	Monica Belen	Vega Santacruz	FEMENINO	1997-12-30	PARAGUAYO/A	0982874543	UNION DE HECHO	\N	\N	\N	NO	SI	5002	.	\N	\N	\N	1	.
5373371	Katerin Celeste	Martinez Sosa	FEMENINO	1993-09-03	PARAGUAYO/A	0982190412	UNION DE HECHO	\N	\N	\N	NO	SI	5011	.	\N	\N	\N	1	.
9999903	Wilfrida	Ramirez Sanchez	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	SI	0	.	\N	\N	\N	1	Este codigo de censo 2467 figura con otro nombre en el Censo 2020 por eso se le agregó este numero de cedula provisorio luego debe ser corregida
9999904	Carlos	Colmán Gonzélez	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	SI	0	.	\N	\N	\N	1	Este codigo de censo 108 figura con otro nombre Liboria Lopez Rodas en el Censo 2017/2020 por eso se le agregó este numero de cedula provisorio luego debe ser corregida
9999905	Monica Belen	Vega Santacruz	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	1	Se generó esta persona como nueva, se le agregó un numero de cedula provisorio que debe ser cambiada y luego completar los demas datos
9999906	Wence Fabiana	Noceda Rotela	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	1	Se generó esta persona como nueva, se le agregó un numero de cedula provisorio que debe ser cambiada y luego completar los demas datos
9999907	Carmen Belén	Rojas Delvalle	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	1	Se generó esta persona como nueva, se le agregó un numero de cedula provisorio que debe ser cambiada y luego completar los demas datos
9999908	Mónica María	Esteche Pereira	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	1	Se generó esta persona como nueva, se le agregó un numero de cedula provisorio que debe ser cambiada y luego completar los demas datos, no figura en ninguno de los 2 censos
9999909	Karen Soledad	Bobadilla Maciel	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	3	Se generó esta persona como nueva, se le agregó un numero de cedula provisorio que debe ser cambiada y luego completar los demas datos, no figura en ninguno de los 2 censos
9999910	Ana Paola	López Achucarro	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	3	En el Censo del 2017 el codigo de censo 1719 corresponde a otra Persona Gregorio Lopez por eso se le agregó esta cedula que posteriormente debe ser actualizada
9999952	Sara	Colmán	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	2	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
9999911	Enmanuel	Pereira Garcia	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	3	En el Censo del 2017 el codigo de censo 1719 corresponde a otra persona TEOFILO GONZALEZ por eso se le agregó esta cedula que posteriormente debe ser actualizada
9999912	Dionicio Ramon	Marecos Martinez	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	3	Se creo esta cuenta que debe ser modificada Posteriormente
9999913	Sonia Leonarda	Gonzalez Velazquez	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	3	El codigo del censo 2489 corresponde a otra persona DIEGO ANTONIO GARCETE FERNANDEZ por eso se agrego esta cedula que debe ser cambiada posteriormente
9999914	Teresita	Perez	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	3	El codigo del censo 230 corresponde a otra persona  por eso se agrego esta cuenta que posteriormente debe ser cambiada
9999915	Pedro Alberto	Cristaldo Esteche	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	3	Se creo esta cuenta que debe ser modificada Posteriormente EL CODIGO DE CENSO 5001 FIGURA EN EL CENSO 2020 CON EL NOMBRE DE JUAN RAMON ARZAMENDIA
9999916	Vicenta	Riquelme	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	3	Se creo esta cuenta que debe ser modificada Posteriormente EL CODIGO DE CENSO 5010 FIGURA EN EL CENSO 2020 CON EL NOMBRE DE JORGE RAMON VALDEZ ESCOBAR
9999917	Richar Rodrigo	Ramirez Cardozo	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	3	Se creo esta cuenta que debe ser modificada Posteriormente EL CODIGO DE CENSO 116 FIGURA EN EL CENSO 2017 CON EL NOMBRE DE MARIA CELESTE DIANA GALEANO
9999918	Sonia Elizabeth	Pereira Britez	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	3	El codigo de censo 114 en el Censo 2017 figura como CASA VACIA por eso se generó esta cuenta QUE POSTERIORMENTE DEBE SER CAMBIADA
9999919	Isabelino	Candado Aguirre	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	3	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA
9999920	Walter Ariel	Martinez Sosa	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	3	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA
9999921	Jose Gabriel	Silva Bernal	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	3	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA
9999922	Arnaldo Gabriel	Candia Avalos	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	3	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA
9999923	Rosa Sonia	Lopez Sanabria	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	3	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA
9999924	Carmen Mirta	Zalazar Caceres	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	3	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA
9999925	Antoria	Zelada de Collante	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	2	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA
9999926	Fredy Ramon	Franco Mendoza	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	2	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA
9999927	Rafael	Ruiz Diaz	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	2	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
9999928	Daniel	Ruiz Diaz Aguilera	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	2	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
9999929	Francisco	Aguilera Esteche	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	2	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
9999930	Isidro	Miralles	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	2	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
9999931	Pabrla de la Cruz	Miralles	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	2	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
9999932	Juan Regalado	Mendoza Ortega	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	2	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
9999933	Saturnino	Mendoza Ortega	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	2	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
9999934	Petrona Eulalia	Cañete Portillo	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	2	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
9999935	Daniel	Sanchez Quintana	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	2	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
9999936	Nemecio	Caballero Martinez	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	2	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
9999937	Benigno	Arguello Castillo	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	2	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
9999938	Juan Humberto	Sanchez Amarilla	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	2	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
9999939	Tiburcio	Villalba	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	2	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
9999940	Omar	Ozuna Acosta	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	2	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
9999941	Antonio	Alderete Garay	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	2	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
9999942	Victor	Alderete Zarate	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	2	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
9999943	Eligio	Gonzalez	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	2	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
9999944	Basilio	Lescano Jimenez	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	2	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
9999945	Juan Antonio	Villalba Britez	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	2	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
9999946	María Cristina	Silvero	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	2	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
9999947	Juan Ramon	Mendoza	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	2	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
9999948	José	Gonzalez Cáceres	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	2	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
9999949	Alfonso	Martinez Pizurno	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	2	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
9999950	Venancio Esteban	Cáceres Gamarra	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	2	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
9999951	Mirian Nancy	Gómez	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	2	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
9999953	Pablino	Torres	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	2	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
9999954	María Cristina	Nuñez Gaona	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	2	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
9999955	Genaro	Godoy Cáceres	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	2	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
9999956	Ramón	Castillo	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	2	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
9999957	Francisca	Barrios Ojeda	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	2	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
9999958	Andrés Ramón	Castillo Leguizamón	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	2	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
9999959	Fidel	Arguello Duarte	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	2	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
9999960	Santiago	Arguello Barrios	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	2	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
1140465	Atanacio Felix	Arrua Gonzalez	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	4	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
1197163	Celestina	Martinez de Franco	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	4	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
4010615	Luis de los Santos	Lopez	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	4	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
1194356	Eulogio	Benitez	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	4	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
2263096	Bernardo	Martínez Ohingginz	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	4	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
1091340	Pablo	Noguera	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	4	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
1440855	Bernarda	Sosa	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	4	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
6629990	Hugo Javier	Martinez Peralta	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	4	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
828409	Fidel	Orué Gonzalez	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	4	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
3977243	Roberto	Jiménez Zoilan	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	4	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
2689748	Hilarion	Arguello	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	4	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
3612339	Sebastian	Pinto	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	4	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
5529575	Valeria Araseli	Villalba Alfonzo	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	4	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
1467214	Teódulo	Román Calonga	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	4	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
2142479	Julia	Martinez	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	4	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
1105331	Julián	Cassera	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	4	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
3325211	Felipe Santiago	Dapollo Ayala	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	4	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
979413	Julián	Esteche	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	4	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
4350136	Osvaldo Daniel	Pereira Britez	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	9	.	\N	\N	\N	4	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
1116940	Luciano	Ferreira	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	4	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
839579	Isabelino	Benítez Ayala	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	4	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
5852758	María Anuncia	Ferreira Troche	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	4	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
5111595	David Antonio	Ocampo Amaro	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	4	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
1663943	María Teresa	Mendoza	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	4	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
1436636	Pedro Ramón	Caceres Duarte	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	4	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
815882	Raúl Gill	Real Miño	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	4	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
2577425	Roberto Carlos	Benítez	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	4	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
4987625	Hugo Enrique	Núñez Mendoza	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	4	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
1351630	Secundino	Cazal Aponte	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	4	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
2684628	Cesar Victorio	Pereira	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	4	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
5107359	Jorge Ramon	Pavon González	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	4	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
4904790	Tobías	Cazal Osorio	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	4	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
2107309	German	López Pereira	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	4	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
4837418	Edgar Darío	Deleon Benítez	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	4	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
2229749	Marcelino Teodocio	Deleon Servín	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	4	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
3847278	Cirilo Manuel	González Samaniego	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	4	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
2529726	Juan Emilio	Amarilla Pereira	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	4	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
1448788	Gregorio	Gonzalez	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	4	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
9999961	Pablo Vicente	Gonzalez	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	4	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA  - ESTA PERSONA CON CI 1421931 YA FIGURA COMO BENEFICIARIO DEL BARRIO TEMPORAL CODIGO DE CENSO 107
1490834	Miguel Ángel	Galeano Cazal	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	4	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
1568458	Edgar Eladio	Caballero Brítez	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	4	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
5691932	Walter Wladimir	Castillo Galeano	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	4	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
3178650	Marcos Manuel	Peralta Acosta	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	4	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
3300293	Santiago	Deleon Servín	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	4	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
4899564	Edgar Eladio	Caballero Centurión	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	4	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
3727707	Adriana Teresita	Ayala	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	4	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
855332	Nélida	Centurion	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	4	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
9999962	Arminda	Mareco	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	1808	.	\N	\N	\N	6	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA  - ESTA PERSONA CON CI 4067291 YA FIGURA COMO BENEFICIARIO DEL BARRIO TEMPORAL CODIGO DE CENSO 1808 Y NOMBRE CINTIA CAROLINA DUARTE
9999963	Gerardo Ernesto	Rodríguez	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	5	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
9999964	Emigdia	Romero	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	5	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
9999965	Francisca	de León	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	5	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
9999966	Reinaldo	Ruíz Díaz	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	5	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
9999967	Agueda Milagros	Ramírez	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	5	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
9999968	Heidy Analia	Ramírez	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	5	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
9999969	Ramón Eder	Britez Ramírez	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	5	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
9999970	Juan Gabriel	Quintana	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	5	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
9999971	Eva Elizabeth	Báez Cuevas	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	5	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
9999972	Nélida Soledad	Giménez	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	5	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
9999973	Arturo Ezequiel	Rotela	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	5	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
9999974	Fermín	Esquivel	MASCULINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	5	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
9999975	Verónica	Cordovez	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	5	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
9999976	Liiz Romina	Vega Santacruz	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	5	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
9999977	Patricia Guadalupe	Santacruz	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	5	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
9999978	Vidalina	Lopez	FEMENINO	\N	PARAGUAYO/A	.	.	\N	\N	\N	NO	NO	0	.	\N	\N	\N	5	SE GENERO ESTA CUENTA QUE POSTERIORMENTE DEBE SER CAMBIADA Y ACTUALIZADA
\.


--
-- TOC entry 2154 (class 2606 OID 43442)
-- Name: medidas_compensaciones_pkey; Type: CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.medidas_compensaciones
    ADD CONSTRAINT medidas_compensaciones_pkey PRIMARY KEY (codigo);


--
-- TOC entry 2152 (class 2606 OID 43429)
-- Name: padron_pkey; Type: CONSTRAINT; Schema: aplicacion; Owner: hugo
--

ALTER TABLE ONLY aplicacion.padron_familias
    ADD CONSTRAINT padron_pkey PRIMARY KEY (cedula);


-- Completed on 2023-01-13 14:44:57 -03

--
-- PostgreSQL database dump complete
--

