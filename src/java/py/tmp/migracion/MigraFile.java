/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.tmp.migracion;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.ORM.sql.ReaderT;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;




/**
 *
 * @author hugo
 */
public class MigraFile {
    
    private Persistencia persistencia = new Persistencia();   
    

    public Integer leer (  String pathfile ) {
        
        
        try {
            
            File file = new File(pathfile); 
            
            FileInputStream inputStream = new FileInputStream(file);
            Workbook workbook = new XSSFWorkbook(inputStream);
            Sheet firstSheet = workbook.getSheetAt(0);
            
            int npag = workbook.getNumberOfSheets();            
            Iterator iterator = firstSheet.iterator();
            
            
            MigraFile basef = new MigraFile();
            basef.recorrerXlsx( iterator  );
         
            
        } catch (Exception e) {
            e.printStackTrace();
        }      
        
        return 0;
    }
       
    
            
    
    
    
    
    
    
    
    
    public void recorrerXlsx ( Iterator iterator )   {    
            
        MigraFile basef = new MigraFile();

        Integer i = 1;

        iterator.next();
        //MigracionBase01DAO dao = new MigracionBase01DAO();

        String lineareg = "";
        while (iterator.hasNext()) {

            Row nextRow = (Row) iterator.next();
            Iterator cellIterator = nextRow.cellIterator();                     
            lineareg = "";
            
                  
            
            
            while(cellIterator.hasNext()) {
                Cell cell = (Cell) cellIterator.next();
                Cell contenidoCelda = cell;
                lineareg = lineareg +(contenidoCelda.toString().trim() + ";");
            }

//System.out.println(lineareg);

            String[] arraylinea = lineareg.split(";");
            
//            System.out.println(arraylinea);

            basef.insert( arraylinea );                

        }

    
    }
    
    
    
    
    
    
    
    
    
 
    public Integer insert ( String[] ar ) {
                
        try {            
  
            
//  String sql = new MigracionBase01SQL().insert( ar, migracion_cod,fecha_id );                        
            
            //System.out.println("  -  " + ar.length);
  
            MigraFile basef = new MigraFile();
            
            basef.sql_insert( ar  );   
            
            
        } 
        
        catch (Exception ex) {
            throw new Exception(ex);        
        }
        
        finally{
            return 0;
        }

    }
        
    
        
    
    
    
    
    
    public void sql_insert ( String[] ar ) throws IOException
     {
         String sql = "";
         
        try {
            
            
            
            
            
            ReaderT readerSQL = new ReaderT("Migracion");
            readerSQL.fileExt = "insert1.sql";
            sql = readerSQL.get();
            
            
            sql = sql.replaceAll("v0", ar[0] );
            sql = sql.replaceAll("v1", ar[1] );
            sql = sql.replaceAll("v2", ar[2] );
            sql = sql.replaceAll("v3", ar[3] );
                        
            String strfecha = ar[4];
            if (strfecha.trim().equals("-   -")){
                strfecha = "null";
            }
            else{
                strfecha = "'"+ar[4]+"'";
            }
            sql = sql.replaceAll("v4", strfecha );
            
            
            sql = sql.replaceAll("v5", ar[5] );
            sql = sql.replaceAll("v6", ar[6] );
            sql = sql.replaceAll("v7", ar[7] );
            
            sql = sql.replaceAll("a11", ar[11] );
            sql = sql.replaceAll("a12", ar[12] );
            sql = sql.replaceAll("a13", ar[13] );
            sql = sql.replaceAll("a14", ar[14] );
            sql = sql.replaceAll("a18", ar[18] );
            sql = sql.replaceAll("a19", ar[19] );
            
            
            
            //System.out.println(sql);
            
            /*
            System.out.println("");
            System.out.println(sql);
            
            
            
            System.out.println(ar[0]);
            System.out.println(ar[1]);
            System.out.println(ar[2]);
            System.out.println(ar[3]);
            System.out.println(ar[4]);
            
            */
            
            
            Integer cod  =  0;
            persistencia.ejecutarSQL ( sql) ;
            
            
        
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
            System.err.println( sql );
            
        }
        
        
    }        
        
    
        
    
    
    
    
    
}
