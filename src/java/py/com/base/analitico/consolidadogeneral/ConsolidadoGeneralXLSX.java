/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package py.com.base.analitico.consolidadogeneral;


import java.io.FileOutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import nebuleuse.ORM.db.ResultadoSet;
import nebuleuse.file.FileXlsx;
import nebuleuse.util.Datetime;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.xssf.usermodel.DefaultIndexedColorMap;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import py.com.base.aplicacion.barriotemporal.BarrioTemporalSQL;
import py.com.base.aplicacion.beneficiario.BeneficiarioSQL;
import py.com.base.aplicacion.gestionsocial.GestionSocialSQL;
import py.com.base.aplicacion.ordenservicio.OrdenServicioSQL;
import py.com.base.aplicacion.utilizacionvehiculo.UtilizacionVehiculoSQL;

/**
 *
 * @author hugo
 */

public class ConsolidadoGeneralXLSX {
    
    
    public  void gen ( FileXlsx filexlsx, 
            String fecha1, String fecha2 )  {    
           
        try {            
            
            Integer row = 0;
            ArrayList<String> cabecera = new ArrayList<String>();
            ArrayList<String> campos = new ArrayList<String>();
            String sql = "";
            ResultSet resultset;   
                    
            java.awt.Color color = new java.awt.Color(85, 139, 47);            
            color = filexlsx.getColor();
            
            //this.hoja01_consulta1(filexlsx, fecha1, fecha2);
                        
            filexlsx.newhoja("hoja1");
            //filexlsx.newfila(row);
            
            
            cabecera.add("");
            
            filexlsx.setCabecera(cabecera);                        
            filexlsx.writeText(row);     
            row++;
            
            
            
            cabecera = new ArrayList<String>();            
            cabecera.add("LOGISTICA ORDENES DE SERVICIOS");            
            filexlsx.setCabecera(cabecera);                        
            filexlsx.writeText(row);     
            row++;
            
            
            cabecera = new ArrayList<String>();   
            cabecera.add("cantidad");
            cabecera.add("sillas");
            cabecera.add("mesas");
            cabecera.add("refrigerios");
            cabecera.add("microfono");
            cabecera.add("vehiculo");
            cabecera.add("proyector");
            cabecera.add("computadora");
            cabecera.add("impresos");
            cabecera.add("boligrafos");
            cabecera.add("hojas");
            cabecera.add("otros");
            filexlsx.setCabecera(cabecera);                        
            filexlsx.writeText(row);     
            filexlsx.formato(color, row);
            
            row++;      
            
            
            campos = new ArrayList<String>();
            campos.add("cantidad");
            campos.add("sillas");
            campos.add("mesas");
            campos.add("refrigerios");
            campos.add("microfono_parlante");
            campos.add("vehiculos");
            campos.add("proyector");
            campos.add("computadora");
            campos.add("materiales_impresos");
            campos.add("boligrafos");
            campos.add("hojas_blancas");
            campos.add("otros");
            filexlsx.setCampos(campos);                                               
            sql = new OrdenServicioSQL().consulta2_xlsx(fecha1, fecha2);                        
            resultset = new ResultadoSet().resultset(sql);               
            row = filexlsx.writeContenido(resultset, row); 
            
            
            
            // LOGISTICA VEHICULOS
            row++;                            
            row++; 
            cabecera = new ArrayList<String>();            
            cabecera.add("LOGISTICA VEHICULOS");            
            filexlsx.setCabecera(cabecera);                        
            filexlsx.writeText(row);     
            row++;
            
            
            cabecera = new ArrayList<String>();   
            cabecera.add("cantidad");
            cabecera.add("vehiculo");
            cabecera.add("zafira");
            cabecera.add("ba temporal");
            cabecera.add("ba tacumbu");
            cabecera.add("apuf camsap");
            cabecera.add("otro");
            cabecera.add("kms recorridos");            
            filexlsx.setCabecera(cabecera);                        
            filexlsx.writeText(row);    
            filexlsx.formato(color, row);
            row++; 
            
            
            campos = new ArrayList<String>();
            campos.add("cant");            
            campos.add("vehiculo_nombre");
            campos.add("zafira");
            campos.add("temporal");
            campos.add("tacumbu");
            campos.add("camsap");
            campos.add("otro");
            campos.add("kms");
            filexlsx.setCampos(campos);    
            sql = new UtilizacionVehiculoSQL().consulta2_xlsx(
                    fecha1, fecha2, -1);
            resultset = new ResultadoSet().resultset(sql);   
            row = filexlsx.writeContenido(resultset, row); 
                                   
            
            
            // LOGISTICA MANTENIMIENTO BARRIO TEMPORAL
            row++;                            
            row++; 
            cabecera = new ArrayList<String>();            
            cabecera.add("LOGISTICA MANTENIMIENTO BARRIO TEMPORAL");            
            filexlsx.setCabecera(cabecera);                        
            filexlsx.writeText(row);     
            row++;

            
            cabecera = new ArrayList<String>(); 
            cabecera.add("cantidad");
            cabecera.add("jardineria");
            cabecera.add("transformador");
            cabecera.add("bomba_agua");
            cabecera.add("luminica");
            cabecera.add("limpieza");
            cabecera.add("incendio");
            cabecera.add("otros");
            filexlsx.setCabecera(cabecera);                        
            filexlsx.writeText(row);    
            filexlsx.formato(color, row);
            row++; 
            
            
            campos = new ArrayList<String>();
            campos.add("cantidad");
            campos.add("jardineria");
            campos.add("transformador");
            campos.add("bomba_agua");
            campos.add("luminica");
            campos.add("limpieza");
            campos.add("incendio");
            campos.add("otros");
            filexlsx.setCampos(campos);    
            sql = new BarrioTemporalSQL().consulta2_xlsx(fecha1, fecha2);   
            resultset = new ResultadoSet().resultset(sql);   
            row = filexlsx.writeContenido(resultset, row); 
            
            
            
            
            
            // OFICINA DE CAMPO - ATENCION A BENEFICIARIOS
            row++;                            
            row++; 
            cabecera = new ArrayList<String>();            
            cabecera.add("OFICINA DE CAMPO - ATENCION A BENEFICIARIOS");            
            filexlsx.setCabecera(cabecera);                        
            filexlsx.writeText(row);     
            row++;
            
            
            
            cabecera = new ArrayList<String>(); 
            cabecera.add("masculino");
            cabecera.add("femenino");
            cabecera.add("sexo_otro");
            cabecera.add("sexo_nocontesta");            
            cabecera.add("edad_5_12");
            cabecera.add("edad_13_17");
            cabecera.add("edad_18_29");
            cabecera.add("edad_30_49");
            cabecera.add("edad_50_64");
            cabecera.add("edad_65_mas");
            cabecera.add("edad_nocon");            
            cabecera.add("zona_1");
            cabecera.add("zona_2");
            cabecera.add("zona_3");
            cabecera.add("zona_4");
            cabecera.add("zona_5");
            cabecera.add("zona_6");
            cabecera.add("zona_7");
            cabecera.add("zona_8");
            cabecera.add("zona_9");
            cabecera.add("zona_10");            
            cabecera.add("zona_hovy");
            cabecera.add("zona_sen");
            cabecera.add("zona_otro");            
            cabecera.add("acti_quejas");
            cabecera.add("acti_ac_social");
            cabecera.add("acti_informacion");
            cabecera.add("acti_int_laboral");
            cabecera.add("acti_otro");
            filexlsx.setCabecera(cabecera);                        
            filexlsx.writeText(row);    
            filexlsx.formato(color, row);
            row++; 
            
            
            campos = new ArrayList<String>();
            campos.add("sexo_masc");
            campos.add("sexo_feme");
            campos.add("sexo_otro");
            campos.add("sexo_nocon");            
            campos.add("edad_5_12");
            campos.add("edad_13_17");
            campos.add("edad_18_29");
            campos.add("edad_30_49");
            campos.add("edad_50_64");
            campos.add("edad_65_mas");
            campos.add("edad_nocon");            
            campos.add("zona_1");
            campos.add("zona_2");
            campos.add("zona_3");
            campos.add("zona_4");
            campos.add("zona_5");
            campos.add("zona_6");
            campos.add("zona_7");
            campos.add("zona_8");
            campos.add("zona_9");
            campos.add("zona_10");            
            campos.add("zona_hovy");
            campos.add("zona_sen");
            campos.add("zona_otro");            
            campos.add("acti_quejas");
            campos.add("acti_acompa");
            campos.add("acti_info");
            campos.add("acti_interme");
            campos.add("acti_otro");
            filexlsx.setCampos(campos);       
            sql = new BeneficiarioSQL().consulta2_xlsx(fecha1, fecha2);   
            resultset = new ResultadoSet().resultset(sql);   
            row = filexlsx.writeContenido(resultset, row); 
            
            
            // PLAN DE GESTION SOCIAL            
            row++;                            
            row++; 
            cabecera = new ArrayList<String>();            
            cabecera.add("PLAN DE GESTION SOCIAL");            
            filexlsx.setCabecera(cabecera);                        
            filexlsx.writeText(row);     
            row++;
                        
            
            
            cabecera = new ArrayList<String>(); 
            cabecera.add("programa");         
            cabecera.add("participantes");
            cabecera.add("masculino");
            cabecera.add("femenino");
            cabecera.add("otro");            
            cabecera.add("zona1");
            cabecera.add("zona2");
            cabecera.add("zona3");
            cabecera.add("zona4");
            cabecera.add("zona5");
            cabecera.add("zona6");
            cabecera.add("zona7");
            cabecera.add("zona8");
            cabecera.add("zona9");               
            cabecera.add("zona_banco_hovy");            
            cabecera.add("zona_refugio_sen");            
            cabecera.add("zona_otro");                
            cabecera.add("edad_5_12");    
            cabecera.add("edad_13_17");    
            cabecera.add("edad_18_29");    
            cabecera.add("edad_30_49");    
            cabecera.add("edad_50_64");    
            cabecera.add("edad_65_mas");    
            filexlsx.setCabecera(cabecera);                        
            filexlsx.writeText(row);    
            filexlsx.formato(color, row);
            row++; 


            campos = new ArrayList<String>();
            campos.add("descripcion");            
            campos.add("participantes_cantidad");
            campos.add("sexo_masculino");
            campos.add("sexo_femenino");
            campos.add("sexo_otros");            
            campos.add("zona1");
            campos.add("zona2");
            campos.add("zona3");
            campos.add("zona4");
            campos.add("zona5");
            campos.add("zona6");
            campos.add("zona7");
            campos.add("zona8");
            campos.add("zona9");            
            campos.add("zona_banco_hovy");            
            campos.add("zona_refugio_sen");            
            campos.add("zona_otro");                 
            campos.add("edad_5_12");    
            campos.add("edad_13_17");    
            campos.add("edad_18_29");    
            campos.add("edad_30_49");    
            campos.add("edad_50_64");    
            campos.add("edad_65_mas");  
            filexlsx.setCampos(campos);                
            sql = new GestionSocialSQL().consulta2_xlsx(
                    fecha1, fecha2, -1);            
            resultset = new ResultadoSet().resultset(sql);   
            row = filexlsx.writeContenido(resultset, row); 
            
            
            
            
            
            
            
            String ruta = "";
            ruta = filexlsx.getFilePath();

            FileOutputStream file = new FileOutputStream( ruta );       
            filexlsx.getLibro().write(file);
            file.close();            
            
            
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();            
        }
    }    
      
    
    
    
    /*
    public  FileXlsx hoja01_consulta1 ( FileXlsx filexlsx, 
                String fecha1, String fecha2) 
            throws SQLException, Exception {
        

            ArrayList<String> cabecera = new ArrayList<String>();

            cabecera.add("fecha");
            cabecera.add("jardineria");
            cabecera.add("transformador");
            cabecera.add("bomba_agua");
            cabecera.add("luminica");
            cabecera.add("limpieza");
            cabecera.add("incendio");
            cabecera.add("otros");
            cabecera.add("observaciones");
            
            
            filexlsx.setCabecera(cabecera);
            ArrayList<String> campos = new ArrayList<String>();
                        
            campos.add("fecha");
            campos.add("jardineria");
            campos.add("transformador");
            campos.add("bomba_agua");
            campos.add("luminica");
            campos.add("limpieza");
            campos.add("incendio");
            campos.add("otros");
            campos.add("observaciones");
            
            
            filexlsx.setCampos(campos);                                
               
            
            String sql = new ConsolidadoGeneralSQL().consulta1_xlsx(fecha1, fecha2);            
            
            ResultSet resultset = new ResultadoSet().resultset(sql);             
            
           
            filexlsx.newhoja("hoja1");
            filexlsx.writeCabecera(0);     
                        
            filexlsx.writeContenido(resultset);           
            
            // this.formato(filexlsx);
        
            return filexlsx;                                                              
            
    }
     
    
*/
    
    public  void formato ( FileXlsx filexlsx  ) {        
        
        int i = 0;
        for (String titulo : filexlsx.getCabecera()) {  
            
            Cell cell = filexlsx.getHoja().getRow(0).getCell(i);
            
            // formato             
            //System.out.println(this.hoja.getRow(0).getCell(i+1).getCellType());
            
            XSSFCellStyle cellStyle = filexlsx.getHoja().getWorkbook().createCellStyle();            
            
            java.awt.Color color = new java.awt.Color(220, 220, 220);            
            cellStyle.setFillForegroundColor(new XSSFColor(color, new DefaultIndexedColorMap()));
            cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);                         
            cell.setCellStyle(cellStyle);
            i++;
        }
        
    }   
    
    

    
}
