/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.documento;


import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.ORM.xml.Global;



/**
 *
 * @author hugom_000
 */

public class DocumentoDAO  {

    
    
    public DocumentoDAO () throws IOException  {
    }
    
    


    public Integer insert ( String file_name, String file_id, 
            Integer codid, String class_name ) 
            throws SQLException {

        
        Integer intID = 0;

        
        String strSQL =
            " INSERT INTO aplicacion.documentos\n " +
            " (file_name, file_id, codid, class_name)\n " +
            " VALUES('"+file_name+"', '"+file_id+"', " +
            " "+codid+", '"+class_name+"')\n" +
            " RETURNING id ; ";
        

        Persistencia persinstencia = new Persistencia();
        
        intID = persinstencia.ejecutarSQL(strSQL, "id" );

        
        return intID;

    }
    
    
    
    

    public void delete ( Integer codid ) 
            throws SQLException {
        
        Integer intID = 0;
        
        String strSQL =
                " DELETE\n" +
                " FROM aplicacion.documentos\n" +
                " where codid = " + codid ;

        
        Persistencia persinstencia = new Persistencia();
        
        persinstencia.ejecutarSQL(strSQL);


    }
    



    public Documento filterDoc ( Integer codid ) 
            throws SQLException, Exception {

        
        Documento obj  = new Documento();

        
        String strSQL =
                " SELECT id, file_name, file_id, codid, class_name \n" +
                " FROM aplicacion.documentos\n" +
                " WHERE codid =  " + codid ;
        
        
        Persistencia persinstencia = new Persistencia();        
        obj = (Documento) persinstencia.sqlToObject(strSQL, obj);
        
        return obj;

    }
    
    


    public void deleteFile ( Integer codid ) 
            throws SQLException {
        
        try {
            
            
            Documento obj = this.filterDoc(codid);            
            
            if (obj != null)
            {   
                String dir_files = new Global().getValue("dir_files");            
                
                File file = new File( dir_files + obj.getFile_id());                        
                
                if (file.exists()) {                                                         
                    file.delete();                    
                }
            }
        }         
        catch (Exception ex) 
        {
            System.err.println(ex.getMessage());
        }

    }
    
    
    
    
    
          
        
}
