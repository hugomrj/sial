/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.vehiculo;


import java.io.IOException;



/**
 *
 * @author hugo
 */

public class VehiculoExt extends Vehiculo{
    
    private String descripcion  ;
    
    
    public void extender() throws IOException {
                     
        this.descripcion = this.getMarca() 
                + " " + this.getModelo()
                + " " + this.getChapa();
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    
     
            
}
