/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.vehiculo;

import nebuleuse.ORM.sql.ReaderT;
import nebuleuse.ORM.sql.SentenciaSQL;

/**
 *
 * @author hugo
 */
public class VehiculoSQL {
    
    
    public String search ( String busqueda )
            throws Exception {
    
        String sql = "";                                 
        sql = SentenciaSQL.select( new Vehiculo(), busqueda );        
        
        return sql ;             
    }        
           
    
    
    
    public String all ( )
            throws Exception {
    
        
        String sql = "";                                 
        
        ReaderT reader = new ReaderT("Vehiculo");
        reader.fileExt = "all.sql";
        
        sql = reader.get( );    
        
      
        
        return sql ;      
    }
        
    
    
    
}
