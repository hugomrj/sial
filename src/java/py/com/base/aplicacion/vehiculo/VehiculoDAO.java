/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.vehiculo;


import java.io.IOException;
import java.util.List;
import nebuleuse.ORM.db.Coleccion;
import nebuleuse.ORM.db.Persistencia;

/**
 *
 * @author hugom_000
 */
public class VehiculoDAO {

    private Persistencia persistencia = new Persistencia();      
    
    
    public VehiculoDAO ( ) throws IOException  {
   
    }
      
        
    

    public List<Vehiculo>  all () {
                
        List<Vehiculo>  lista = null;        
        try {                        
                        
            VehiculoRS rs = new VehiculoRS();       
            
            lista = new Coleccion<Vehiculo>().resultsetToList(
                    new Vehiculo(),
                    rs.all()
            );                        
                    
        }         
        catch (Exception ex) {                                    
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
          
    
    
    
        
    
    
}
