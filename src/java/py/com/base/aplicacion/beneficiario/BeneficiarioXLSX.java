/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.base.aplicacion.beneficiario;

import java.io.FileOutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import nebuleuse.ORM.db.ResultadoSet;
import nebuleuse.file.FileXlsx;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.xssf.usermodel.DefaultIndexedColorMap;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;

/**
 *
 * @author hugo
 */
public class BeneficiarioXLSX {
    
    
    public  void gen_consulta1 ( FileXlsx filexlsx, 
            String fecha1, String fecha2 )  {    
           
        try {
            
            
            this.hoja01_consulta1(filexlsx, fecha1, fecha2);
            
            
            String ruta = "";
            ruta = filexlsx.getFilePath();

            FileOutputStream file = new FileOutputStream( ruta );       
            filexlsx.getLibro().write(file);
            file.close();                

            
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();            
        }
    }    
      
    
    
    
    
    public  FileXlsx hoja01_consulta1 ( FileXlsx filexlsx, 
                String fecha1, String fecha2) 
            throws SQLException, Exception {
        

            ArrayList<String> cabecera = new ArrayList<String>();

            cabecera.add("fecha");
            
            cabecera.add("masculino");
            cabecera.add("femenino");
            cabecera.add("sexo_otro");
            cabecera.add("sexo_nocontesta");
            
            cabecera.add("edad_5_12");
            cabecera.add("edad_13_17");
            cabecera.add("edad_18_29");
            cabecera.add("edad_30_49");
            cabecera.add("edad_50_64");
            cabecera.add("edad_65_mas");
            cabecera.add("edad_nocon");
            
            cabecera.add("zona_1");
            cabecera.add("zona_2");
            cabecera.add("zona_3");
            cabecera.add("zona_4");
            cabecera.add("zona_5");
            cabecera.add("zona_6");
            cabecera.add("zona_7");
            cabecera.add("zona_8");
            cabecera.add("zona_9");
            cabecera.add("zona_10");
            cabecera.add("zona_hovy");
            cabecera.add("zona_sen");
            cabecera.add("zona_otro");            
            
            cabecera.add("acti_quejas");
            cabecera.add("acti_ac_social");
            cabecera.add("acti_informacion");
            cabecera.add("acti_int_laboral");
            cabecera.add("acti_otro");
            
            
            filexlsx.setCabecera(cabecera);
            ArrayList<String> campos = new ArrayList<String>();
            
            campos.add("fecha");
            
            campos.add("sexo_masc");
            campos.add("sexo_feme");
            campos.add("sexo_otro");
            campos.add("sexo_nocon");
            
            campos.add("edad_5_12");
            campos.add("edad_13_17");
            campos.add("edad_18_29");
            campos.add("edad_30_49");
            campos.add("edad_50_64");
            campos.add("edad_65_mas");
            campos.add("edad_nocon");
            
            campos.add("zona_1");
            campos.add("zona_2");
            campos.add("zona_3");
            campos.add("zona_4");
            campos.add("zona_5");
            campos.add("zona_6");
            campos.add("zona_7");
            campos.add("zona_8");
            campos.add("zona_9");
            campos.add("zona_10");
            campos.add("zona_hovy");
            campos.add("zona_sen");
            campos.add("zona_otro");            
            
            campos.add("acti_quejas");
            campos.add("acti_acompa");
            campos.add("acti_info");
            campos.add("acti_interme");
            campos.add("acti_otro");
            
            
            filexlsx.setCampos(campos);                                
               
            String sql = new BeneficiarioSQL().consulta1_xlsx(fecha1, fecha2);            
            
            ResultSet resultset = new ResultadoSet().resultset(sql);             
            
           
            filexlsx.newhoja("hoja1");
            filexlsx.writeCabecera(0);     
                        
            filexlsx.writeContenido(resultset);           
            
            filexlsx.formato();
        
            return filexlsx;                                                              
            
    }
     
    


    
    public  void gen_consulta2 ( FileXlsx filexlsx, 
            String fecha1, String fecha2 )  {    
           
        try {
            
            
            this.hoja01_consulta2(filexlsx, fecha1, fecha2);
            
            
            String ruta = "";
            ruta = filexlsx.getFilePath();

            FileOutputStream file = new FileOutputStream( ruta );       
            filexlsx.getLibro().write(file);
            file.close();                

            
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();            
        }
    }    
      
        

    
    public  FileXlsx hoja01_consulta2 ( FileXlsx filexlsx, 
                String fecha1, String fecha2) 
            throws SQLException, Exception {
        

            ArrayList<String> cabecera = new ArrayList<String>();

            
            
            cabecera.add("masculino");
            cabecera.add("femenino");
            cabecera.add("sexo_otro");
            cabecera.add("sexo_nocontesta");
            
            cabecera.add("edad_5_12");
            cabecera.add("edad_13_17");
            cabecera.add("edad_18_29");
            cabecera.add("edad_30_49");
            cabecera.add("edad_50_64");
            cabecera.add("edad_65_mas");
            cabecera.add("edad_nocon");
            
            cabecera.add("zona_1");
            cabecera.add("zona_2");
            cabecera.add("zona_3");
            cabecera.add("zona_4");
            cabecera.add("zona_5");
            cabecera.add("zona_6");
            cabecera.add("zona_7");
            cabecera.add("zona_8");
            cabecera.add("zona_9");
            cabecera.add("zona_10");
            cabecera.add("zona_hovy");
            cabecera.add("zona_sen");
            cabecera.add("zona_otro");            
            
            cabecera.add("acti_quejas");
            cabecera.add("acti_ac_social");
            cabecera.add("acti_informacion");
            cabecera.add("acti_int_laboral");
            cabecera.add("acti_otro");
            
            
            filexlsx.setCabecera(cabecera);
            ArrayList<String> campos = new ArrayList<String>();
            
            
            
            campos.add("sexo_masc");
            campos.add("sexo_feme");
            campos.add("sexo_otro");
            campos.add("sexo_nocon");
            
            campos.add("edad_5_12");
            campos.add("edad_13_17");
            campos.add("edad_18_29");
            campos.add("edad_30_49");
            campos.add("edad_50_64");
            campos.add("edad_65_mas");
            campos.add("edad_nocon");
            
            campos.add("zona_1");
            campos.add("zona_2");
            campos.add("zona_3");
            campos.add("zona_4");
            campos.add("zona_5");
            campos.add("zona_6");
            campos.add("zona_7");
            campos.add("zona_8");
            campos.add("zona_9");
            campos.add("zona_10");
            campos.add("zona_hovy");
            campos.add("zona_sen");
            campos.add("zona_otro");            
            
            campos.add("acti_quejas");
            campos.add("acti_acompa");
            campos.add("acti_info");
            campos.add("acti_interme");
            campos.add("acti_otro");
            
            
            filexlsx.setCampos(campos);                                
               
            String sql = new BeneficiarioSQL().consulta2_xlsx(fecha1, fecha2);            
            
            ResultSet resultset = new ResultadoSet().resultset(sql);             
            
           
            filexlsx.newhoja("hoja1");
            filexlsx.writeCabecera(0);     
                        
            filexlsx.writeContenido(resultset);           
            
            filexlsx.formato();
        
            return filexlsx;                                                              
            
    }
     
    
        
        
    
}
