/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.beneficiario;


import java.util.Date;

/**
 *
 * @author hugo
 */
public class Beneficiario {
    
    private Integer id ;
    private Date fecha;
    private Boolean sexo_masc;
    private Boolean sexo_feme;
    private Boolean sexo_otro;
    private Boolean sexo_nocon;
    private String sexo_seleccion;
    private Boolean edad_5_12;
    private Boolean edad_13_17;
    private Boolean edad_18_29;
    private Boolean edad_30_49;
    private Boolean edad_50_64;
    private Boolean edad_65_mas;
    private Boolean edad_nocon;
    private String edad_seleccion;
    private Boolean zona_1;
    private Boolean zona_2;
    private Boolean zona_3;
    private Boolean zona_4;
    private Boolean zona_5;
    private Boolean zona_6;
    private Boolean zona_7;
    private Boolean zona_8;
    private Boolean zona_9;
    private Boolean zona_10;
    private Boolean zona_hovy;
    private Boolean zona_sen;
    private Boolean zona_otro;    
    private String zona_otro_texto;
    private String zona_seleccion;
    private Boolean acti_quejas;
    private Boolean acti_acompa;
    private Boolean acti_info;
    private Boolean acti_interme;
    private Boolean acti_otro;
    private String acti_otro_texto;
    private String acti_seleccion;
    private Integer usuario;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Boolean getSexo_masc() {
        return sexo_masc;
    }

    public void setSexo_masc(Boolean sexo_masc) {
        this.sexo_masc = sexo_masc;
    }

    public Boolean getSexo_feme() {
        return sexo_feme;
    }

    public void setSexo_feme(Boolean sexo_feme) {
        this.sexo_feme = sexo_feme;
    }

    public Boolean getSexo_otro() {
        return sexo_otro;
    }

    public void setSexo_otro(Boolean sexo_otro) {
        this.sexo_otro = sexo_otro;
    }

    public Boolean getSexo_nocon() {
        return sexo_nocon;
    }

    public void setSexo_nocon(Boolean sexo_nocon) {
        this.sexo_nocon = sexo_nocon;
    }

    public String getSexo_seleccion() {
        return sexo_seleccion;
    }

    public void setSexo_seleccion(String sexo_seleccion) {
        this.sexo_seleccion = sexo_seleccion;
    }

    public Boolean getEdad_5_12() {
        return edad_5_12;
    }

    public void setEdad_5_12(Boolean edad_5_12) {
        this.edad_5_12 = edad_5_12;
    }

    public Boolean getEdad_13_17() {
        return edad_13_17;
    }

    public void setEdad_13_17(Boolean edad_13_17) {
        this.edad_13_17 = edad_13_17;
    }

    public Boolean getEdad_18_29() {
        return edad_18_29;
    }

    public void setEdad_18_29(Boolean edad_18_29) {
        this.edad_18_29 = edad_18_29;
    }

    public Boolean getEdad_30_49() {
        return edad_30_49;
    }

    public void setEdad_30_49(Boolean edad_30_49) {
        this.edad_30_49 = edad_30_49;
    }

    public Boolean getEdad_50_64() {
        return edad_50_64;
    }

    public void setEdad_50_64(Boolean edad_50_64) {
        this.edad_50_64 = edad_50_64;
    }

    public Boolean getEdad_65_mas() {
        return edad_65_mas;
    }

    public void setEdad_65_mas(Boolean edad_65_mas) {
        this.edad_65_mas = edad_65_mas;
    }

    public Boolean getEdad_nocon() {
        return edad_nocon;
    }

    public void setEdad_nocon(Boolean edad_nocon) {
        this.edad_nocon = edad_nocon;
    }

    public String getEdad_seleccion() {
        return edad_seleccion;
    }

    public void setEdad_seleccion(String edad_seleccion) {
        this.edad_seleccion = edad_seleccion;
    }

    public Boolean getZona_1() {
        return zona_1;
    }

    public void setZona_1(Boolean zona_1) {
        this.zona_1 = zona_1;
    }

    public Boolean getZona_2() {
        return zona_2;
    }

    public void setZona_2(Boolean zona_2) {
        this.zona_2 = zona_2;
    }

    public Boolean getZona_3() {
        return zona_3;
    }

    public void setZona_3(Boolean zona_3) {
        this.zona_3 = zona_3;
    }

    public Boolean getZona_4() {
        return zona_4;
    }

    public void setZona_4(Boolean zona_4) {
        this.zona_4 = zona_4;
    }

    public Boolean getZona_5() {
        return zona_5;
    }

    public void setZona_5(Boolean zona_5) {
        this.zona_5 = zona_5;
    }

    public Boolean getZona_6() {
        return zona_6;
    }

    public void setZona_6(Boolean zona_6) {
        this.zona_6 = zona_6;
    }

    public Boolean getZona_7() {
        return zona_7;
    }

    public void setZona_7(Boolean zona_7) {
        this.zona_7 = zona_7;
    }

    public Boolean getZona_8() {
        return zona_8;
    }

    public void setZona_8(Boolean zona_8) {
        this.zona_8 = zona_8;
    }

    public Boolean getZona_9() {
        return zona_9;
    }

    public void setZona_9(Boolean zona_9) {
        this.zona_9 = zona_9;
    }

    public Boolean getZona_hovy() {
        return zona_hovy;
    }

    public void setZona_hovy(Boolean zona_hovy) {
        this.zona_hovy = zona_hovy;
    }

    public Boolean getZona_sen() {
        return zona_sen;
    }

    public void setZona_sen(Boolean zona_sen) {
        this.zona_sen = zona_sen;
    }

    public Boolean getZona_otro() {
        return zona_otro;
    }

    public void setZona_otro(Boolean zona_otro) {
        this.zona_otro = zona_otro;
    }



    public String getZona_otro_texto() {
        return zona_otro_texto;
    }

    public void setZona_otro_texto(String zona_otro_texto) {
        this.zona_otro_texto = zona_otro_texto;
    }

    public String getZona_seleccion() {
        return zona_seleccion;
    }

    public void setZona_seleccion(String zona_seleccion) {
        this.zona_seleccion = zona_seleccion;
    }

    public Boolean getActi_quejas() {
        return acti_quejas;
    }

    public void setActi_quejas(Boolean acti_quejas) {
        this.acti_quejas = acti_quejas;
    }

    public Boolean getActi_acompa() {
        return acti_acompa;
    }

    public void setActi_acompa(Boolean acti_acompa) {
        this.acti_acompa = acti_acompa;
    }

    public Boolean getActi_info() {
        return acti_info;
    }

    public void setActi_info(Boolean acti_info) {
        this.acti_info = acti_info;
    }

    public Boolean getActi_interme() {
        return acti_interme;
    }

    public void setActi_interme(Boolean acti_interme) {
        this.acti_interme = acti_interme;
    }

    public Boolean getActi_otro() {
        return acti_otro;
    }

    public void setActi_otro(Boolean acti_otro) {
        this.acti_otro = acti_otro;
    }

    public String getActi_otro_texto() {
        return acti_otro_texto;
    }

    public void setActi_otro_texto(String acti_otro_texto) {
        this.acti_otro_texto = acti_otro_texto;
    }

    public String getActi_seleccion() {
        return acti_seleccion;
    }

    public void setActi_seleccion(String acti_seleccion) {
        this.acti_seleccion = acti_seleccion;
    }

    public Integer getUsuario() {
        return usuario;
    }

    public void setUsuario(Integer usuario) {
        this.usuario = usuario;
    }

    public Boolean getZona_10() {
        return zona_10;
    }

    public void setZona_10(Boolean zona_10) {
        this.zona_10 = zona_10;
    }
    
    
    
    
    
}

