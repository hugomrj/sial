/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.beneficiario;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.List;
import nebuleuse.ORM.db.JsonObjeto;
import nebuleuse.ORM.db.ResultadoSet;
import nebuleuse.ORM.sql.SentenciaSQL;


public class BeneficiarioJSON  {


    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();   
    
    
    
    public BeneficiarioJSON ( ) throws IOException  {
    
    }
      
    
    
    

    public JsonObject  lista ( Integer page, String buscar ) {
                
        JsonObject jsonObject = new JsonObject();

        try 
        {   
  
            ResultadoSet resSet = new ResultadoSet();         
            
            
            String sql = "";
            if (buscar == null) {                
                sql = SentenciaSQL.select(new Beneficiario());    
            }
            else{
                
                sql = sql + SentenciaSQL.select(new Beneficiario());   
                sql = sql + new BeneficiarioSQL().filtro(buscar);   ;   
    
            }            
            sql = sql + " order by id ";
            

            
            BeneficiarioDAO dao = new BeneficiarioDAO();
            
            List<Beneficiario> lista = dao.lista(page, sql);

            //datos
            JsonArray jsonArrayDatos = new JsonArray();
            JsonParser jsonParser = new JsonParser();
            jsonArrayDatos = (JsonArray) jsonParser.parse(gson.toJson( lista ));                 
            
            
            // paginacipon
            JsonObject jsonPaginacion = new JsonObject();            
            jsonPaginacion = new JsonObjeto().json_paginacion(sql, page);
            
            
            // union de partes
            jsonObject.add("paginacion", jsonPaginacion);            
            jsonObject.add("datos", jsonArrayDatos);    
            
            //jsonObject.add("summary", jsonarraySuma);            
            
     
        }         
        catch (Exception ex) {                   
            
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonObject ;         
        }
    }      
    
    
    
    
    
    

    public JsonObject  consulta1 ( String fecha1, String fecha2) {
                
        JsonObject jsonObject = new JsonObject();

        try 
        {   
  

            ResultadoSet resSet = new ResultadoSet();                   
                        
            String sql = new BeneficiarioSQL().consulta1(fecha1, fecha2);     
            
            // datos
            ResultSet rsData = new ResultadoSet().resultset(sql);              
            JsonArray jsonarrayDatos = new JsonArray();
            jsonarrayDatos = new JsonObjeto().array_datos(rsData);            
                
            
            // paginacipon
            /*
            JsonObject jsonPaginacion = new JsonObject();            
            jsonPaginacion = new JsonObjeto().json_paginacion(sql, page);
            */
            

            
            // suma
            sql = new BeneficiarioSQL().consulta1suma(fecha1, fecha2);  
            
            ResultSet rsDatas = new ResultadoSet().resultset(sql);  
            JsonArray jsonArraySum = new JsonArray();
            jsonArraySum = new JsonObjeto().array_datos(rsDatas);    
            
            
            
            // union de partes
            //jsonObject.add("paginacion", jsonPaginacion);            
            jsonObject.add("datos", jsonarrayDatos);    
            jsonObject.add("summary", jsonArraySum);                    
            
            
        }         
        catch (Exception ex) {                   
            
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonObject ;         
        }
    }      
    
    
    
    
        
    
    
    
        
}
