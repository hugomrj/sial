/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.barriotemporal;


import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.util.Enumeration;
import nebuleuse.ORM.xml.Global;
import py.com.base.aplicacion.documento.DocumentoDAO;


@WebServlet(
        name = "BarrioTemporalUpload", 
        urlPatterns = { "/barriotemporal/upload" }
)


@MultipartConfig(
  fileSizeThreshold = 1024 * 1024 * 1, // 1 MB
  maxFileSize = 1024 * 1024 * 10,      // 10 MB
  maxRequestSize = 1024 * 1024 * 100   // 100 MB
)


public class BarrioTemporalUpload extends HttpServlet {

  public void doPost(HttpServletRequest request, HttpServletResponse response) 
          throws ServletException, IOException {
      
     
      
      
      
      try {
          
          
          
          Integer codid = 0;
          Enumeration<String> names = request.getHeaderNames();
          while (names.hasMoreElements()) {
              String headerName = names.nextElement();
              if (headerName.equals("codid")){
                  codid =  Integer.parseInt(request.getHeader(headerName));
              }
          }
          
          
          String dir_files = new Global().getValue("dir_files");
   

          
          long now = System.currentTimeMillis();
          String file_id = String.valueOf(now);
                    
          /* Receive file uploaded to the Servlet from the HTML5 form */
          Part filePart = request.getPart("file");
          String fileName = filePart.getSubmittedFileName();
          
          
          
          for (Part part : request.getParts()) {
              //part.write("/home/host/archivos/" + fileName);
              //part.write( dir_files + fileName);
              part.write( dir_files + file_id);              
          }
                    
                    
          DocumentoDAO docDAO = new DocumentoDAO();          
          
          docDAO.deleteFile(codid);
          docDAO.delete(codid);          
          docDAO.insert(fileName, file_id, 
                  codid, BarrioTemporal.class.getSimpleName() );
                              
          
      } 
      

      catch (Exception ex) {
          System.out.println(ex.getMessage());
      }
  }

}    
    
    

