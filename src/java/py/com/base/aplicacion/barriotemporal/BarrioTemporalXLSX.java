/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.base.aplicacion.barriotemporal;


import java.io.FileOutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import nebuleuse.ORM.db.ResultadoSet;
import nebuleuse.file.FileXlsx;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.xssf.usermodel.DefaultIndexedColorMap;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;

/**
 *
 * @author hugo
 */
public class BarrioTemporalXLSX {
    
    
    public  void gen_consulta1 ( FileXlsx filexlsx, 
            String fecha1, String fecha2 )  {    
           
        try {
            
            
            this.hoja01_consulta1(filexlsx, fecha1, fecha2);
            
            
            String ruta = "";
            ruta = filexlsx.getFilePath();

            FileOutputStream file = new FileOutputStream( ruta );       
            filexlsx.getLibro().write(file);
            file.close();                

            
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();            
        }
    }    
      
    
    
    
    
    public  FileXlsx hoja01_consulta1 ( FileXlsx filexlsx, 
                String fecha1, String fecha2) 
            throws SQLException, Exception {
        

            ArrayList<String> cabecera = new ArrayList<String>();

            cabecera.add("fecha");
            cabecera.add("jardineria");
            cabecera.add("transformador");
            cabecera.add("bomba_agua");
            cabecera.add("luminica");
            cabecera.add("limpieza");
            cabecera.add("incendio");
            cabecera.add("otros");
            cabecera.add("observaciones");
            
            
            filexlsx.setCabecera(cabecera);
            ArrayList<String> campos = new ArrayList<String>();
                        
            campos.add("fecha");
            campos.add("jardineria");
            campos.add("transformador");
            campos.add("bomba_agua");
            campos.add("luminica");
            campos.add("limpieza");
            campos.add("incendio");
            campos.add("otros");
            campos.add("observaciones");
            
            
            filexlsx.setCampos(campos);                                
               
            String sql = new BarrioTemporalSQL().consulta1_xlsx(fecha1, fecha2);            
            
            ResultSet resultset = new ResultadoSet().resultset(sql);             
            
           
            filexlsx.newhoja("hoja1");
            filexlsx.writeCabecera(0);     
                        
            filexlsx.writeContenido(resultset);           
            
            filexlsx.formato();
        
            return filexlsx;                                                              
            
    }
     
    

    
    public  void gen_consulta2 ( FileXlsx filexlsx, 
            String fecha1, String fecha2 )  {    
           
        try {
            
            
            this.hoja01_consulta2(filexlsx, fecha1, fecha2);
            
            
            String ruta = "";
            ruta = filexlsx.getFilePath();

            FileOutputStream file = new FileOutputStream( ruta );       
            filexlsx.getLibro().write(file);
            file.close();                

            
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();            
        }
    }    
      
        

    
    public  FileXlsx hoja01_consulta2 ( FileXlsx filexlsx, 
                String fecha1, String fecha2) 
            throws SQLException, Exception {
        

            ArrayList<String> cabecera = new ArrayList<String>();


            cabecera.add("cantidad");
            cabecera.add("jardineria");
            cabecera.add("transformador");
            cabecera.add("bomba_agua");
            cabecera.add("luminica");
            cabecera.add("limpieza");
            cabecera.add("incendio");
            cabecera.add("otros");

            
            
            filexlsx.setCabecera(cabecera);
            ArrayList<String> campos = new ArrayList<String>();
            

            campos.add("cantidad");
            campos.add("jardineria");
            campos.add("transformador");
            campos.add("bomba_agua");
            campos.add("luminica");
            campos.add("limpieza");
            campos.add("incendio");
            campos.add("otros");
            
            filexlsx.setCampos(campos);                                
               
            String sql = new BarrioTemporalSQL().consulta2_xlsx(fecha1, fecha2);       



            
            ResultSet resultset = new ResultadoSet().resultset(sql);             
            
           
            filexlsx.newhoja("hoja1");
            filexlsx.writeCabecera(0);     
                        
            filexlsx.writeContenido(resultset);           
            
            filexlsx.formato();
        
            return filexlsx;                                                              
            
    }
     
    
        
        
    
}
