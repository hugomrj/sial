/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.barriotemporal;


import nebuleuse.ORM.db.Persistencia;
import py.com.base.aplicacion.documento.Documento;


/**
 *
 * @author hugo
 */

public class BarrioTemporalExt extends BarrioTemporal {
    
    private Documento documento;
    
    
    public void extender() throws Exception {
        
        Persistencia persistencia = new Persistencia();     
                
        this.documento = new Documento();        
        
        String strSQL = " SELECT id, file_name, file_id, codid, class_name\n" +
                        " FROM aplicacion.documentos\n" +
                        " where codid = " +this.getId();
        
        this.documento  
                = (Documento) persistencia.sqlToObject(
                        strSQL, this.documento);
        
    }    

    
    
    
    public Documento getDocumento() {
        return documento;
    }

    public void setDocumento(Documento documento) {
        this.documento = documento;
    }



}
