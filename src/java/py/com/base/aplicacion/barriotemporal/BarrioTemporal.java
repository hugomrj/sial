/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.barriotemporal;

import java.util.Date;

/**
 *
 * @author hugo
 */
public class BarrioTemporal {
    
    private Integer id;
    private Date fecha;
    private Boolean jardineria;
    private Boolean transformador;
    private Boolean bomba_agua;
    private Boolean luminica;
    private Boolean limpieza;
    private Boolean incendio;
    private Boolean otros;
    private String otros_texto;
    private String observaciones ;
    private Integer usuario;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Boolean getJardineria() {
        return jardineria;
    }

    public void setJardineria(Boolean jardineria) {
        this.jardineria = jardineria;
    }

    public Boolean getTransformador() {
        return transformador;
    }

    public void setTransformador(Boolean transformador) {
        this.transformador = transformador;
    }

    public Boolean getBomba_agua() {
        return bomba_agua;
    }

    public void setBomba_agua(Boolean bomba_agua) {
        this.bomba_agua = bomba_agua;
    }

    public Boolean getLuminica() {
        return luminica;
    }

    public void setLuminica(Boolean luminica) {
        this.luminica = luminica;
    }


    public Boolean getIncendio() {
        return incendio;
    }

    public void setIncendio(Boolean incendio) {
        this.incendio = incendio;
    }

    public Boolean getOtros() {
        return otros;
    }

    public void setOtros(Boolean otros) {
        this.otros = otros;
    }

    public String getOtros_texto() {
        return otros_texto;
    }

    public void setOtros_texto(String otros_texto) {
        this.otros_texto = otros_texto;
    }

    public Integer getUsuario() {
        return usuario;
    }

    public void setUsuario(Integer usuario) {
        this.usuario = usuario;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Boolean getLimpieza() {
        return limpieza;
    }

    public void setLimpieza(Boolean limpieza) {
        this.limpieza = limpieza;
    }
    
    
    
}

