/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.barriotemporal;

import nebuleuse.ORM.sql.ReaderT;

/**
 *
 * @author hugo
 */
public class BarrioTemporalSQL {
    
    
    
    
    public String consulta1 (String fecha1, String fecha2 )
            throws Exception {
    
        String sql = "";                                 
        
        ReaderT reader = new ReaderT("BarrioTemporal");
        reader.fileExt = "consulta1.sql";
        
        sql = reader.get( fecha1, fecha2 );    
        
        return sql ;      
    }

    
    
    
    
    public String consulta1suma (String fecha1, String fecha2 )
            throws Exception {
    
        String sql = "";                                 
        
        ReaderT reader = new ReaderT("BarrioTemporal");
        reader.fileExt = "consulta1_suma.sql";
        
        sql = reader.get( fecha1, fecha2 );    
        
        return sql ;      
    }
        
        

    public String consulta1_xlsx (String fecha1, String fecha2 )
            throws Exception {
    
        String sql = "";                                 
        
        ReaderT reader = new ReaderT("BarrioTemporal");
        reader.fileExt = "consulta1_xlsx.sql";
        
        sql = reader.get( fecha1, fecha2 );    
        
        return sql ;      
    }
    
    
    

    public String consulta2_xlsx (String fecha1, String fecha2 )
            throws Exception {
    
        String sql = "";                                 
        
        ReaderT reader = new ReaderT("BarrioTemporal");
        reader.fileExt = "consulta2_xlsx.sql";
        
        sql = reader.get( fecha1, fecha2 );    
        
        return sql ;      
    }
    
        
    
    public String filtro (String buscar )
            throws Exception {
    
        String sql = "";                                 
        
        if (buscar != null) {
            buscar = buscar.replace(" ", "%") ;    
        }        
        
        ReaderT reader = new ReaderT("BarrioTemporal");
        reader.fileExt = "filtro.sql";
        
        sql = reader.get( buscar );    
        
        return sql ;             
    
    }   
    

    
    
}
