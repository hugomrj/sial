/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.gestionsocial;


import nebuleuse.ORM.sql.ReaderT;

/**
 *
 * @author hugo
 */
public class GestionSocialSQL {
    
    
    
    
    public String consulta1 (String fecha1, String fecha2, Integer programa )
            throws Exception {
    
        String sql = "";                                 
        
        ReaderT reader = new ReaderT("GestionSocial");
        
        if (programa == -1) {

            reader.fileExt = "consulta1_all.sql";
            sql = reader.get( fecha1, fecha2 );              
            
        }
        else {
            
            reader.fileExt = "consulta1.sql";
            sql = reader.get( fecha1, fecha2, programa );            
        
        }
        
        return sql ;      
    }

    
    
    
    
    
    
    public String consulta2 (String fecha1, String fecha2, Integer programa )
            throws Exception {
    
        String sql = "";                                 
        
        ReaderT reader = new ReaderT("GestionSocial");
        
        if (programa == -1) {

            reader.fileExt = "consulta2_all.sql";
            sql = reader.get( fecha1, fecha2 );              
            
        }
        else {
            
            reader.fileExt = "consulta2.sql";
            sql = reader.get( fecha1, fecha2, programa );            
        
        }
        
        return sql ;      
    }

    
        
    
    
    
    
    
    
    
    public String consulta1suma (String fecha1, String fecha2, Integer programa)
            throws Exception {
    
        String sql = "";                                         
        ReaderT reader = new ReaderT("GestionSocial");        
        
        if (programa == -1) {

            reader.fileExt = "consulta1_suma_all.sql";        
            sql = reader.get( fecha1, fecha2 );    
            
        }
        else {
            
            reader.fileExt = "consulta1_suma.sql";        
            sql = reader.get( fecha1, fecha2, programa );    
        
        }
        
        return sql ;      
    }
        
        

    
    
    public String consulta2suma (String fecha1, String fecha2, Integer programa)
            throws Exception {
    
        String sql = "";                                         
        ReaderT reader = new ReaderT("GestionSocial");        
        
        if (programa == -1) {

            reader.fileExt = "consulta2_suma_all.sql";        
            sql = reader.get( fecha1, fecha2 );    
            
        }
        else {
            
            reader.fileExt = "consulta2_suma.sql";        
            sql = reader.get( fecha1, fecha2, programa );    
        
        }
        
        return sql ;      
    }
        
            
    
    
    

    

    public String consulta1_xlsx (String fecha1, String fecha2, 
            Integer programa )
            throws Exception {
    
        String sql = "";              
        
        
        ReaderT reader = new ReaderT("GestionSocial");

        
        if (programa == -1) {
            
            reader.fileExt = "consulta1_xlsx_all.sql";    
            sql = reader.get( fecha1, fecha2 );    
            
        }
        else {
                        
            reader.fileExt = "consulta1_xlsx.sql";
            sql = reader.get( fecha1, fecha2, programa );    
        
        }        
        
        
        
        return sql ;      
    }
    
            
    
    


    public String consulta2_xlsx (String fecha1, String fecha2, 
            Integer programa )
            throws Exception {
    
        String sql = "";              
        
        
        ReaderT reader = new ReaderT("GestionSocial");

        
        if (programa == -1) {
            
            reader.fileExt = "consulta2_xlsx_all.sql";    
            sql = reader.get( fecha1, fecha2 );    
            
        }
        else {
                        
            reader.fileExt = "consulta2_xlsx.sql";
            sql = reader.get( fecha1, fecha2, programa );    
        
        }        
        
        
        
        return sql ;      
    }
    
    
    
    public String select ()
            throws Exception {
    
        String sql = "";                                 
        
        ReaderT reader = new ReaderT("GestionSocial");
        reader.fileExt = "select.sql";
        
        sql = reader.get( );    
        
        return sql ;             
    
    }   
        
    
        
    
    public String filtro (String buscar )
            throws Exception {
    
        String sql = "";                                 
        
        if (buscar != null) {
            buscar = buscar.replace(" ", "%") ;    
        }        
        
        ReaderT reader = new ReaderT("GestionSocial");
        reader.fileExt = "filtro.sql";
        
        sql = reader.get( buscar );    
        
        return sql ;             
    
    }   
    
    
    
}
