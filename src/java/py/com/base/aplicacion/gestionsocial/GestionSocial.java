/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.gestionsocial;

import java.util.Date;
import py.com.base.aplicacion.programa.Programa;

/**
 *
 * @author hugo
 */
public class GestionSocial {
    
    private Integer id;
    private Date fecha;
    private Programa programa;
    private String actividad_nombre;
    private Integer participantes_cantidad;
    private Integer sexo_masculino;
    private Integer sexo_femenino;
    private Integer sexo_otros;
    private Integer zona1;
    private Integer zona2;
    private Integer zona3;
    private Integer zona4;
    private Integer zona5;
    private Integer zona6;
    private Integer zona7;
    private Integer zona8;
    private Integer zona9;
    private Integer zona10;
    private Integer zona_banco_hovy;
    private Integer zona_refugio_sen;
    private Integer zona_otro;
    private Integer edad_5_12;
    private Integer edad_13_17;
    private Integer edad_18_29;
    private Integer edad_30_49;
    private Integer edad_50_64;
    private Integer edad_65_mas;
    private Integer usuario;
    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Programa getPrograma() {
        return programa;
    }

    public void setPrograma(Programa programa) {
        this.programa = programa;
    }

    public String getActividad_nombre() {
        return actividad_nombre;
    }

    public void setActividad_nombre(String actividad_nombre) {
        this.actividad_nombre = actividad_nombre;
    }

    public Integer getParticipantes_cantidad() {
        return participantes_cantidad;
    }

    public void setParticipantes_cantidad(Integer participantes_cantidad) {
        this.participantes_cantidad = participantes_cantidad;
    }

    public Integer getSexo_masculino() {
        return sexo_masculino;
    }

    public void setSexo_masculino(Integer sexo_masculino) {
        this.sexo_masculino = sexo_masculino;
    }

    public Integer getSexo_femenino() {
        return sexo_femenino;
    }

    public void setSexo_femenino(Integer sexo_femenino) {
        this.sexo_femenino = sexo_femenino;
    }

    public Integer getSexo_otros() {
        return sexo_otros;
    }

    public void setSexo_otros(Integer sexo_otros) {
        this.sexo_otros = sexo_otros;
    }

    public Integer getZona1() {
        return zona1;
    }

    public void setZona1(Integer zona1) {
        this.zona1 = zona1;
    }

    public Integer getZona2() {
        return zona2;
    }

    public void setZona2(Integer zona2) {
        this.zona2 = zona2;
    }

    public Integer getZona3() {
        return zona3;
    }

    public void setZona3(Integer zona3) {
        this.zona3 = zona3;
    }

    public Integer getZona4() {
        return zona4;
    }

    public void setZona4(Integer zona4) {
        this.zona4 = zona4;
    }

    public Integer getZona5() {
        return zona5;
    }

    public void setZona5(Integer zona5) {
        this.zona5 = zona5;
    }

    public Integer getZona6() {
        return zona6;
    }

    public void setZona6(Integer zona6) {
        this.zona6 = zona6;
    }

    public Integer getZona7() {
        return zona7;
    }

    public void setZona7(Integer zona7) {
        this.zona7 = zona7;
    }

    public Integer getZona8() {
        return zona8;
    }

    public void setZona8(Integer zona8) {
        this.zona8 = zona8;
    }

    public Integer getZona9() {
        return zona9;
    }

    public void setZona9(Integer zona9) {
        this.zona9 = zona9;
    }

    public Integer getZona_banco_hovy() {
        return zona_banco_hovy;
    }

    public void setZona_banco_hovy(Integer zona_banco_hovy) {
        this.zona_banco_hovy = zona_banco_hovy;
    }

    public Integer getZona_refugio_sen() {
        return zona_refugio_sen;
    }

    public void setZona_refugio_sen(Integer zona_refugio_sen) {
        this.zona_refugio_sen = zona_refugio_sen;
    }

    public Integer getZona_otro() {
        return zona_otro;
    }

    public void setZona_otro(Integer zona_otro) {
        this.zona_otro = zona_otro;
    }

    public Integer getEdad_5_12() {
        return edad_5_12;
    }

    public void setEdad_5_12(Integer edad_5_12) {
        this.edad_5_12 = edad_5_12;
    }

    public Integer getEdad_13_17() {
        return edad_13_17;
    }

    public void setEdad_13_17(Integer edad_13_17) {
        this.edad_13_17 = edad_13_17;
    }

    public Integer getEdad_18_29() {
        return edad_18_29;
    }

    public void setEdad_18_29(Integer edad_18_29) {
        this.edad_18_29 = edad_18_29;
    }

    public Integer getEdad_30_49() {
        return edad_30_49;
    }

    public void setEdad_30_49(Integer edad_30_49) {
        this.edad_30_49 = edad_30_49;
    }

    public Integer getEdad_50_64() {
        return edad_50_64;
    }

    public void setEdad_50_64(Integer edad_50_64) {
        this.edad_50_64 = edad_50_64;
    }

    public Integer getEdad_65_mas() {
        return edad_65_mas;
    }

    public void setEdad_65_mas(Integer edad_65_mas) {
        this.edad_65_mas = edad_65_mas;
    }

    public Integer getUsuario() {
        return usuario;
    }

    public void setUsuario(Integer usuario) {
        this.usuario = usuario;
    }

    public Integer getZona10() {
        return zona10;
    }

    public void setZona10(Integer zona10) {
        this.zona10 = zona10;
    }

    
    
}







