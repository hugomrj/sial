/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.base.aplicacion.gestionsocial;


import java.io.FileOutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import nebuleuse.ORM.db.ResultadoSet;
import nebuleuse.file.FileXlsx;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.xssf.usermodel.DefaultIndexedColorMap;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;

/**
 *
 * @author hugo
 */
public class GestionSocialXLSX {
    
    
    public  void gen_consulta1 ( FileXlsx filexlsx, 
            String fecha1, String fecha2, Integer programa )  {    
           
        try {
            
            
            this.hoja01_consulta1(filexlsx, fecha1, fecha2, programa);
            
            
            String ruta = "";
            ruta = filexlsx.getFilePath();

            FileOutputStream file = new FileOutputStream( ruta );       
            filexlsx.getLibro().write(file);
            file.close();                

            
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();            
        }
    }    
      
    
    
    
    
    public  FileXlsx hoja01_consulta1 ( FileXlsx filexlsx, 
                String fecha1, String fecha2, Integer programa) 
            throws SQLException, Exception {
        

            ArrayList<String> cabecera = new ArrayList<String>();

            cabecera.add("fecha");
            cabecera.add("programa");
            cabecera.add("actividad_nombre");
            cabecera.add("participantes");
            cabecera.add("masculino");
            cabecera.add("femenino");
            cabecera.add("otro");
            
            cabecera.add("zona1");
            cabecera.add("zona2");
            cabecera.add("zona3");
            cabecera.add("zona4");
            cabecera.add("zona5");
            cabecera.add("zona6");
            cabecera.add("zona7");
            cabecera.add("zona8");
            cabecera.add("zona9");   
            cabecera.add("zona10");   
            
            cabecera.add("zona_banco_hovy");            
            cabecera.add("zona_refugio_sen");            
            cabecera.add("zona_otro");    
            
            cabecera.add("edad_5_12");    
            cabecera.add("edad_13_17");    
            cabecera.add("edad_18_29");    
            cabecera.add("edad_30_49");    
            cabecera.add("edad_50_64");    
            cabecera.add("edad_65_mas");    
            
            
            filexlsx.setCabecera(cabecera);
            ArrayList<String> campos = new ArrayList<String>();
            
            
            campos.add("fecha");
            campos.add("descripcion");
            campos.add("actividad_nombre");
            campos.add("participantes_cantidad");
            campos.add("sexo_masculino");
            campos.add("sexo_femenino");
            campos.add("sexo_otros");
            
            campos.add("zona1");
            campos.add("zona2");
            campos.add("zona3");
            campos.add("zona4");
            campos.add("zona5");
            campos.add("zona6");
            campos.add("zona7");
            campos.add("zona8");
            campos.add("zona9");
            campos.add("zona10");
            
            campos.add("zona_banco_hovy");            
            campos.add("zona_refugio_sen");            
            campos.add("zona_otro");     
            
            campos.add("edad_5_12");    
            campos.add("edad_13_17");    
            campos.add("edad_18_29");    
            campos.add("edad_30_49");    
            campos.add("edad_50_64");    
            campos.add("edad_65_mas");    
            
            
            
            
            
            filexlsx.setCampos(campos);    
            
            String sql = new GestionSocialSQL().consulta1_xlsx(
                    fecha1, fecha2, programa);            
            
            
            ResultSet resultset = new ResultadoSet().resultset(sql);             
            
           
            filexlsx.newhoja("hoja1");
            filexlsx.writeCabecera(0);     
                        
            filexlsx.writeContenido(resultset);           
            
            filexlsx.formato();
        
            return filexlsx;                                                              
            
    }
     
    


    

    
    public  void gen_consulta2 ( FileXlsx filexlsx, 
            String fecha1, String fecha2, Integer programa )  {    
           
        try {
            
            
            this.hoja01_consulta2(filexlsx, fecha1, fecha2, programa);
            
            
            String ruta = "";
            ruta = filexlsx.getFilePath();

            FileOutputStream file = new FileOutputStream( ruta );       
            filexlsx.getLibro().write(file);
            file.close();                

            
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();            
        }
    }    
      
        
    
    
    
    
    public  FileXlsx hoja01_consulta2 ( FileXlsx filexlsx, 
                String fecha1, String fecha2, Integer programa) 
            throws SQLException, Exception {
        

            ArrayList<String> cabecera = new ArrayList<String>();

      
            cabecera.add("programa");
         
            cabecera.add("participantes");
            cabecera.add("masculino");
            cabecera.add("femenino");
            cabecera.add("otro");
            
            cabecera.add("zona1");
            cabecera.add("zona2");
            cabecera.add("zona3");
            cabecera.add("zona4");
            cabecera.add("zona5");
            cabecera.add("zona6");
            cabecera.add("zona7");
            cabecera.add("zona8");
            cabecera.add("zona9");   
            cabecera.add("zona10");   
            
            cabecera.add("zona_banco_hovy");            
            cabecera.add("zona_refugio_sen");            
            cabecera.add("zona_otro");    
            
            cabecera.add("edad_5_12");    
            cabecera.add("edad_13_17");    
            cabecera.add("edad_18_29");    
            cabecera.add("edad_30_49");    
            cabecera.add("edad_50_64");    
            cabecera.add("edad_65_mas");    
            
            
            filexlsx.setCabecera(cabecera);
            ArrayList<String> campos = new ArrayList<String>();
            
            
            
            campos.add("descripcion");
            
            campos.add("participantes_cantidad");
            campos.add("sexo_masculino");
            campos.add("sexo_femenino");
            campos.add("sexo_otros");
            
            campos.add("zona1");
            campos.add("zona2");
            campos.add("zona3");
            campos.add("zona4");
            campos.add("zona5");
            campos.add("zona6");
            campos.add("zona7");
            campos.add("zona8");
            campos.add("zona9");
            campos.add("zona10");
            
            campos.add("zona_banco_hovy");            
            campos.add("zona_refugio_sen");            
            campos.add("zona_otro");     
            
            campos.add("edad_5_12");    
            campos.add("edad_13_17");    
            campos.add("edad_18_29");    
            campos.add("edad_30_49");    
            campos.add("edad_50_64");    
            campos.add("edad_65_mas");    
            
            
            
            
            
            filexlsx.setCampos(campos);    
            
            String sql = new GestionSocialSQL().consulta2_xlsx(
                    fecha1, fecha2, programa);            
            
            
            ResultSet resultset = new ResultadoSet().resultset(sql);             
            
           
            filexlsx.newhoja("hoja1");
            filexlsx.writeCabecera(0);     
                        
            filexlsx.writeContenido(resultset);           
            
            filexlsx.formato();
        
            return filexlsx;                                                              
            
    }
     
        
    
        
    
}
