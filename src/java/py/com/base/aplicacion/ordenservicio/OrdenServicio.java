
package py.com.base.aplicacion.ordenservicio;

import java.util.Date;

/**
 *
 * @author hugo
 */
public class OrdenServicio {
    
    private Integer orden_servicio ;
    private Date fecha;    
    private Boolean sillas;
    private Boolean mesas;
    private Boolean refrigerios;
    private Boolean microfono_parlante;
    private Boolean vehiculos;
    private Boolean proyector;
    private Boolean computadora;
    private Boolean materiales_impresos;
    private Boolean boligrafos;
    private Boolean hojas_blancas;
    private Boolean otros;
    private String otros_texto;
    private String observaciones ;
    private Integer usuario;

    public Integer getOrden_servicio() {
        return orden_servicio;
    }

    public void setOrden_servicio(Integer orden_servicio) {
        this.orden_servicio = orden_servicio;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Boolean getSillas() {
        return sillas;
    }

    public void setSillas(Boolean sillas) {
        this.sillas = sillas;
    }

    public Boolean getMesas() {
        return mesas;
    }

    public void setMesas(Boolean mesas) {
        this.mesas = mesas;
    }

    public Boolean getRefrigerios() {
        return refrigerios;
    }

    public void setRefrigerios(Boolean refrigerios) {
        this.refrigerios = refrigerios;
    }

    public Boolean getMicrofono_parlante() {
        return microfono_parlante;
    }

    public void setMicrofono_parlante(Boolean microfono_parlante) {
        this.microfono_parlante = microfono_parlante;
    }

    public Boolean getVehiculos() {
        return vehiculos;
    }

    public void setVehiculos(Boolean vehiculos) {
        this.vehiculos = vehiculos;
    }

    public Boolean getProyector() {
        return proyector;
    }

    public void setProyector(Boolean proyector) {
        this.proyector = proyector;
    }

    public Boolean getComputadora() {
        return computadora;
    }

    public void setComputadora(Boolean computadora) {
        this.computadora = computadora;
    }

    public Boolean getMateriales_impresos() {
        return materiales_impresos;
    }

    public void setMateriales_impresos(Boolean materiales_impresos) {
        this.materiales_impresos = materiales_impresos;
    }

    public Boolean getBoligrafos() {
        return boligrafos;
    }

    public void setBoligrafos(Boolean boligrafos) {
        this.boligrafos = boligrafos;
    }

    public Boolean getHojas_blancas() {
        return hojas_blancas;
    }

    public void setHojas_blancas(Boolean hojas_blancas) {
        this.hojas_blancas = hojas_blancas;
    }

    public Boolean getOtros() {
        return otros;
    }

    public void setOtros(Boolean otros) {
        this.otros = otros;
    }

    public String getOtros_texto() {
        return otros_texto;
    }

    public void setOtros_texto(String otros_texto) {
        this.otros_texto = otros_texto;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Integer getUsuario() {
        return usuario;
    }

    public void setUsuario(Integer usuario) {
        this.usuario = usuario;
    }
    
    
    
}

