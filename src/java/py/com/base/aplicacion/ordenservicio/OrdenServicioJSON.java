/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.ordenservicio;


import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.sql.ResultSet;
import nebuleuse.ORM.db.JsonObjeto;
import nebuleuse.ORM.db.ResultadoSet;
import nebuleuse.ORM.sql.SentenciaSQL;



public class OrdenServicioJSON  {


    
    
    public OrdenServicioJSON ( ) throws IOException  {
    
    }
      
    
    
    
    public JsonObject  lista ( Integer page, String buscar ) {        
        
        
        JsonObject jsonObject = new JsonObject();


        
        try 
        {   
  
            ResultadoSet resSet = new ResultadoSet();                   
            
                        
            String sql = "";
            sql = SentenciaSQL.select(new OrdenServicio());  
            
            if (buscar != null) {                
                sql = sql + new OrdenServicioSQL().filtro(buscar);   ;   
            }
            
            sql = sql + " order by orden_servicio ";
            
            
            
            ResultSet rsData = resSet.resultset(sql, page);                
            
            JsonArray jsonarrayDatos = new JsonArray();
            jsonarrayDatos = new JsonObjeto().array_datos(rsData);
            
            
            // paginacipon
            JsonObject jsonPaginacion = new JsonObject();            
            jsonPaginacion = new JsonObjeto().json_paginacion(sql, page);
            
            
            // union de partes
            jsonObject.add("paginacion", jsonPaginacion);
            
            jsonObject.add("datos", jsonarrayDatos);    
            //jsonObject.add("summary", jsonarraySuma);            
            
     
        }         
        catch (Exception ex) {                   
            
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonObject ;         
        }
    }      
    
    
    
    
    

    public JsonObject  consulta1 ( String fecha1, String fecha2) {
                
        JsonObject jsonObject = new JsonObject();

        try 
        {   
  

            ResultadoSet resSet = new ResultadoSet();                   
                        
            String sql = new OrdenServicioSQL().consulta1(fecha1, fecha2);     
            
            
            // datos
            ResultSet rsData = new ResultadoSet().resultset(sql);              
            JsonArray jsonarrayDatos = new JsonArray();
            jsonarrayDatos = new JsonObjeto().array_datos(rsData);            
                


            
            // suma
            sql = new OrdenServicioSQL().consulta1_suma(fecha1, fecha2);  
            
            ResultSet rsDatas = new ResultadoSet().resultset(sql);  
            JsonArray jsonArraySum = new JsonArray();
            jsonArraySum = new JsonObjeto().array_datos(rsDatas);    
            
            
            
            // union de partes
            //jsonObject.add("paginacion", jsonPaginacion);            
            jsonObject.add("datos", jsonarrayDatos);    
            jsonObject.add("summary", jsonArraySum);                    
            
            
        }         
        catch (Exception ex) {                   
            
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonObject ;         
        }
    }      
    
    
    
            

    

    public JsonObject  consulta2 ( String fecha1, String fecha2) {
                
        JsonObject jsonObject = new JsonObject();

        try 
        {   
  

            ResultadoSet resSet = new ResultadoSet();                   
                        
            //String sql = new OrdenServicioSQL().consulta1(fecha1, fecha2);     
            
            /*
            // datos
            ResultSet rsData = new ResultadoSet().resultset(sql);              
            JsonArray jsonarrayDatos = new JsonArray();
            jsonarrayDatos = new JsonObjeto().array_datos(rsData);            
             */  


            
            // suma
            String sql = new OrdenServicioSQL().consulta2_suma(fecha1, fecha2);  
            
            ResultSet rsDatas = new ResultadoSet().resultset(sql);  
            JsonArray jsonArraySum = new JsonArray();
            jsonArraySum = new JsonObjeto().array_datos(rsDatas);    
            
            
            
            // union de partes
            //jsonObject.add("paginacion", jsonPaginacion);            
            //jsonObject.add("datos", jsonarrayDatos);    
            jsonObject.add("summary", jsonArraySum);                    
            
            
        }         
        catch (Exception ex) {                   
            
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonObject ;         
        }
    }      
    
    
        
    
    
        
}
