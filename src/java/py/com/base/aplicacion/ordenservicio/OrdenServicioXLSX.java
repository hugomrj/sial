/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.base.aplicacion.ordenservicio;


import java.io.FileOutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import nebuleuse.ORM.db.ResultadoSet;
import nebuleuse.file.FileXlsx;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.xssf.usermodel.DefaultIndexedColorMap;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;

/**
 *
 * @author hugo
 */
public class OrdenServicioXLSX {
    
    
    public  void gen_consulta1 ( FileXlsx filexlsx, 
            String fecha1, String fecha2 )  {    
           
        try {
            
            
            this.hoja01_consulta1(filexlsx, fecha1, fecha2);
            
            
            String ruta = "";
            ruta = filexlsx.getFilePath();

            FileOutputStream file = new FileOutputStream( ruta );       
            filexlsx.getLibro().write(file);
            file.close();                

            
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();            
        }
    }    
      
    
    
    
    
    public  FileXlsx hoja01_consulta1 ( FileXlsx filexlsx, 
                String fecha1, String fecha2) 
            throws SQLException, Exception {
        

            ArrayList<String> cabecera = new ArrayList<String>();

            cabecera.add("fecha");
            cabecera.add("sillas");
            cabecera.add("mesas");
            cabecera.add("refrigerios");
            cabecera.add("microfonos");
            cabecera.add("vehiculo");
            cabecera.add("proyector");
            cabecera.add("computadora");
            cabecera.add("impresos");
            cabecera.add("boligrafos");
            cabecera.add("hojas");
            cabecera.add("otros");
            cabecera.add("observaciones");
            
            
            filexlsx.setCabecera(cabecera);
            ArrayList<String> campos = new ArrayList<String>();
            
            campos.add("fecha");
            campos.add("sillas");
            campos.add("mesas");
            campos.add("refrigerios");
            campos.add("microfono_parlante");
            campos.add("vehiculos");
            campos.add("proyector");
            campos.add("computadora");
            campos.add("materiales_impresos");
            campos.add("boligrafos");
            campos.add("hojas_blancas");
            campos.add("otros");
            campos.add("observaciones");
            
            filexlsx.setCampos(campos);                                
               
            String sql = new OrdenServicioSQL().consulta1_xlsx(fecha1, fecha2);            
            
            ResultSet resultset = new ResultadoSet().resultset(sql);             
            
           
            filexlsx.newhoja("hoja1");
            filexlsx.writeCabecera(0);     
                        
            filexlsx.writeContenido(resultset);           
            
            filexlsx.formato();
        
            return filexlsx;                                                              
            
    }

        
    
    
    public  void gen_consulta2 ( FileXlsx filexlsx, 
            String fecha1, String fecha2 )  {    
           
        try {
            
            
            this.hoja01_consulta2(filexlsx, fecha1, fecha2);
            
            
            String ruta = "";
            ruta = filexlsx.getFilePath();

            FileOutputStream file = new FileOutputStream( ruta );       
            filexlsx.getLibro().write(file);
            file.close();                

            
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();            
        }
    }    
      
        

    
    public  FileXlsx hoja01_consulta2 ( FileXlsx filexlsx, 
                String fecha1, String fecha2) 
            throws SQLException, Exception {
        

            ArrayList<String> cabecera = new ArrayList<String>();
            
            
            cabecera.add("cantidad");
            cabecera.add("sillas");
            cabecera.add("mesas");
            cabecera.add("refrigerios");
            cabecera.add("microfono");
            cabecera.add("vehiculo");
            cabecera.add("proyector");
            cabecera.add("computadora");
            cabecera.add("impresos");
            cabecera.add("boligrafos");
            cabecera.add("hojas");
            cabecera.add("otros");
            
            
            
            filexlsx.setCabecera(cabecera);
            ArrayList<String> campos = new ArrayList<String>();
            
            
            campos.add("cantidad");
            campos.add("sillas");
            campos.add("mesas");
            campos.add("refrigerios");
            campos.add("microfono_parlante");
            campos.add("vehiculos");
            campos.add("proyector");
            campos.add("computadora");
            campos.add("materiales_impresos");
            campos.add("boligrafos");
            campos.add("hojas_blancas");
            campos.add("otros");
            
            
            
            filexlsx.setCampos(campos);                                
               
            String sql = new OrdenServicioSQL().consulta2_xlsx(fecha1, fecha2);            
            
            ResultSet resultset = new ResultadoSet().resultset(sql);             
            
           
            filexlsx.newhoja("hoja1");
            filexlsx.writeCabecera(0);     
                        
            filexlsx.writeContenido(resultset);           
            
            filexlsx.formato();
        
            return filexlsx;                                                              
            
    }
     
    
           
    
    
        
    
}
