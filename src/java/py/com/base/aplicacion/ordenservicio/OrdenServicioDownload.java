/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.ordenservicio;


import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.logging.Level;
import java.util.logging.Logger;
import nebuleuse.ORM.xml.Global;
import nebuleuse.util.Download;
import py.com.base.aplicacion.documento.Documento;
import py.com.base.aplicacion.documento.DocumentoDAO;





@WebServlet(
    name = "OrdenServicioDownload", 
    urlPatterns = { "/ordenservicio/download" }
)



public class OrdenServicioDownload extends HttpServlet {

    

@Override
protected void doGet(HttpServletRequest req, HttpServletResponse resp)
  throws ServletException, IOException {
    
    try {
        
                
        String dir_files = new Global().getValue("dir_files");
        
        // obtener RequestHeader
        Integer codid = Integer.parseInt(req.getHeader("codid"));
        
        Documento doc = new DocumentoDAO().filterDoc(codid);
        
        Download down = new Download();
        down.downloadFile(resp, dir_files+doc.getFile_id(), doc.getFile_name() );
        
        
    } catch (Exception ex) {
        Logger.getLogger(OrdenServicioDownload.class.getName()).log(Level.SEVERE, null, ex);
    }
    
    
    
}    
 
    
    

}    
    
    

