/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.ordenservicio;


import nebuleuse.ORM.sql.ReaderT;

/**
 *
 * @author hugo
 */
public class OrdenServicioSQL {
    
    
    
    
    public String consulta1 (String fecha1, String fecha2 )
            throws Exception {
    
        String sql = "";                                 
        
        ReaderT reader = new ReaderT("OrdenServicio");
        reader.fileExt = "consulta1.sql";
        
        sql = reader.get( fecha1, fecha2 );    
        
        return sql ;      
    }

    
    
    
    
    public String consulta1_suma (String fecha1, String fecha2 )
            throws Exception {
    
        String sql = "";                                 
        
        ReaderT reader = new ReaderT("OrdenServicio");
        reader.fileExt = "consulta1_suma.sql";
        
        sql = reader.get( fecha1, fecha2 );    
        
        return sql ;      
    }
          
    
    
    
    public String consulta2_suma (String fecha1, String fecha2 )
            throws Exception {
    
        String sql = "";                                 
        
        ReaderT reader = new ReaderT("OrdenServicio");
        reader.fileExt = "consulta2_suma.sql";
        
        sql = reader.get( fecha1, fecha2 );    
        
        return sql ;      
    }
          
    
        
    
    
    

    public String consulta1_xlsx (String fecha1, String fecha2 )
            throws Exception {
    
        String sql = "";                                 
        
        ReaderT reader = new ReaderT("OrdenServicio");
        reader.fileExt = "consulta1_xlsx.sql";
        
        sql = reader.get( fecha1, fecha2 );    
        
        
        return sql ;      
    }
        
    
    

    public String consulta2_xlsx (String fecha1, String fecha2 )
            throws Exception {
    
        String sql = "";                                 
        
        ReaderT reader = new ReaderT("OrdenServicio");
        reader.fileExt = "consulta2_xlsx.sql";
        
        sql = reader.get( fecha1, fecha2 );    
        
        return sql ;      
    }
    
         
    
    
    public String filtro (String buscar )
            throws Exception {
    
        String sql = "";                                 
        
        if (buscar != null) {
            buscar = buscar.replace(" ", "%") ;    
        }        
        
        ReaderT reader = new ReaderT("OrdenServicio");
        reader.fileExt = "filtro.sql";
        
        sql = reader.get( buscar );    
        
        return sql ;             
    
    }   
    
        
    
    
    
}
