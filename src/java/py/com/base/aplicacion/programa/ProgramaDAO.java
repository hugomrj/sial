/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.programa;



import java.io.IOException;
import java.util.List;
import nebuleuse.ORM.db.Coleccion;
import nebuleuse.ORM.db.Persistencia;

/**
 *
 * @author hugom_000
 */
public class ProgramaDAO {

    private Persistencia persistencia = new Persistencia();      
    
    
    public ProgramaDAO ( ) throws IOException  {
   
    }
      
        
    

    public List<Programa>  all () {
                
        List<Programa>  lista = null;        
        try {                        
                        
            ProgramaRS rs = new ProgramaRS();       
            
            lista = new Coleccion<Programa>().resultsetToList(
                    new Programa(),
                    rs.all()
            );                        
                    
        }         
        catch (Exception ex) {                                    
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
          
    
    
    
        
    
    
}
