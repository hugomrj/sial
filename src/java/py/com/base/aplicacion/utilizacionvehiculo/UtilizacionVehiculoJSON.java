/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.utilizacionvehiculo;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.List;
import nebuleuse.ORM.db.JsonObjeto;
import nebuleuse.ORM.db.ResultadoSet;
import nebuleuse.ORM.sql.SentenciaSQL;


public class UtilizacionVehiculoJSON  {


    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();   
    
    
    
    public UtilizacionVehiculoJSON ( ) throws IOException  {
    
    }
      
    
    
    

    public JsonObject  lista ( Integer page, String buscar ) {
        
        
        JsonObject jsonObject = new JsonObject();

        
        try 
        {   
  

            ResultadoSet resSet = new ResultadoSet();                   
                        
            
            String sql = "";
            sql = sql + new UtilizacionVehiculoSQL().select();   ;   
            
            if (buscar != null) {                
                sql = sql + new UtilizacionVehiculoSQL().filtro(buscar);   
            }            
            sql = sql + " order by id ";
                        
            
            UtilizacionVehiculoDAO dao = new UtilizacionVehiculoDAO();
            
            List<UtilizacionVehiculo> lista = dao.lista(page, sql);
            

            //datos
            JsonArray jsonArrayDatos = new JsonArray();
            JsonParser jsonParser = new JsonParser();
            jsonArrayDatos = (JsonArray) jsonParser.parse(gson.toJson( lista ));                 
            
            
            
            // paginacipon
            JsonObject jsonPaginacion = new JsonObject();            
            jsonPaginacion = new JsonObjeto().json_paginacion(sql, page);
            
            
            // union de partes
            jsonObject.add("paginacion", jsonPaginacion);            
            jsonObject.add("datos", jsonArrayDatos);    
            
            //jsonObject.add("summary", jsonarraySuma);            
            
     
        }         
        catch (Exception ex) {                   
            
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonObject ;         
        }
    }      
    
    
    
        
    
    
    
    

    public JsonObject  consulta1 ( String fecha1, String fecha2,
            Integer vehiculo) {
                
        JsonObject jsonObject = new JsonObject();

        try 
        {   

            ResultadoSet resSet = new ResultadoSet();                   
                        
            String sql = new UtilizacionVehiculoSQL().consulta1(
                    fecha1, fecha2, vehiculo);     
            
            // datos
            ResultSet rsData = new ResultadoSet().resultset(sql);              
            JsonArray jsonarrayDatos = new JsonArray();
            jsonarrayDatos = new JsonObjeto().array_datos(rsData);            
              
            
            // suma            
            sql = new UtilizacionVehiculoSQL().consulta1_suma(
                    fecha1, fecha2, vehiculo);   

            
            ResultSet rsDatas = new ResultadoSet().resultset(sql);  
            JsonArray jsonArraySum = new JsonArray();
            jsonArraySum = new JsonObjeto().array_datos(rsDatas);    
            
            
            
            // union de partes
            //jsonObject.add("paginacion", jsonPaginacion);            
            jsonObject.add("datos", jsonarrayDatos);    
            jsonObject.add("summary", jsonArraySum);                    
            
            
        }         
        catch (Exception ex) {                   
            
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonObject ;         
        }
    }      
    
    
        
        
        

    public JsonObject  consulta2 ( String fecha1, String fecha2,
            Integer vehiculo) {
                
        JsonObject jsonObject = new JsonObject();

        try 
        {   

            ResultadoSet resSet = new ResultadoSet();                   
                        
            String sql = new UtilizacionVehiculoSQL().consulta2(
                    fecha1, fecha2, vehiculo);     
            
            
            // datos
            ResultSet rsData = new ResultadoSet().resultset(sql);              
            JsonArray jsonarrayDatos = new JsonArray();
            jsonarrayDatos = new JsonObjeto().array_datos(rsData);            
                
            
            // suma
            
            sql = new UtilizacionVehiculoSQL().consulta2_suma(
                    fecha1, fecha2, vehiculo);   

            
            ResultSet rsDatas = new ResultadoSet().resultset(sql);  
            JsonArray jsonArraySum = new JsonArray();
            jsonArraySum = new JsonObjeto().array_datos(rsDatas);    
            
            
            
            // union de partes
            //jsonObject.add("paginacion", jsonPaginacion);            
            jsonObject.add("datos", jsonarrayDatos);    
            jsonObject.add("summary", jsonArraySum);                    
            
            
        }         
        catch (Exception ex) {                   
            
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonObject ;         
        }
    }      
    
    
        
        
    
    
        
}
