/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package py.com.base.aplicacion.utilizacionvehiculo;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.UriInfo;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.MatrixParam;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import java.io.File;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.file.FileXlsx;
import nebuleuse.seguridad.Autentificacion;



/**
 * REST Web Service
 * @author hugo
 */


@Path("utilizacionvehiculo_qry")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)



public class UtilizacionVehiculoQRY {

    @Context
    private UriInfo context;    
    private Persistencia persistencia = new Persistencia();   
    private Autentificacion autorizacion = new Autentificacion();
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();   
    private Response.Status status  = Response.Status.OK;
    
    String json = "";
    
    UtilizacionVehiculo com = new UtilizacionVehiculo();       
                         
    public UtilizacionVehiculoQRY() {
    }

        
    
    @GET    
    @Path("/consulta1")
    public Response lista ( 
            @HeaderParam("token") String strToken,
            @MatrixParam("fe1") String fe1,
            @MatrixParam("fe2") String fe2,
            @MatrixParam("veh") Integer veh
            ) {
        
            
        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();                                
                
                
                JsonObject jsonObject = new UtilizacionVehiculoJSON()
                        .consulta1(fe1, fe2, veh);
                
                return Response
                        .status(Response.Status.OK)
                        .entity(jsonObject.toString() )
                        .header("token", autorizacion.encriptar())
                        .build();                       
            }
            else
            {
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();                                             
            }        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }      
    }    
    
    
    
    
  
    @GET
    @Path("/consulta1/xlsx/")
    @Produces("aapplication/vnd.ms-excel")    
    public Response consulta1_xlsx( 
            @HeaderParam("token") String strToken,
            @MatrixParam("fe1") String fe1,
            @MatrixParam("fe2") String fe2,
            @MatrixParam("veh") Integer veh
            ) {
        
        String path = "";


        try {                    
           
            if (autorizacion.verificar(strToken))
            {
                autorizacion.actualizar();                  

                
                FileXlsx filexlsx = new FileXlsx();                
                filexlsx.iniciar();
                filexlsx.folder = "/files";                
                filexlsx.name = "/base.xlsx";      
                filexlsx.newlibro();
                
                
                UtilizacionVehiculoXLSX xlsx = new UtilizacionVehiculoXLSX();                
                
                xlsx.gen_consulta1(filexlsx, fe1, fe2, veh);                
                
                filexlsx.newFileStream();
                path = filexlsx.getFilePath();
        
                File file = new File(path);

                Response.ResponseBuilder response = Response.ok((Object) file);
                response.header("Content-Disposition", "attachment; filename=\"test_excel_file.xlsx\"");
                response.header("token", autorizacion.encriptar());

                return response.build();                
                
            }
            else
            {

                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();                             
                
            }        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")   
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }  
        

    }
  
  
    
    
    
    
    
    @GET    
    @Path("/consulta2")
    public Response consulta2 ( 
            @HeaderParam("token") String strToken,
            @MatrixParam("fe1") String fe1,
            @MatrixParam("fe2") String fe2,
            @MatrixParam("veh") Integer veh
            ) {
        
            
        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();                                
                
                
                JsonObject jsonObject = new UtilizacionVehiculoJSON()
                        .consulta2(fe1, fe2, veh);
                
                return Response
                        .status(Response.Status.OK)
                        .entity(jsonObject.toString() )
                        .header("token", autorizacion.encriptar())
                        .build();                       
            }
            else
            {
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();                                             
            }        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }      
    }    
    
        
    

    
    
  
    @GET
    @Path("/consulta2/xlsx/")
    @Produces("aapplication/vnd.ms-excel")    
    public Response consulta2_xlsx( 
            @HeaderParam("token") String strToken,
            @MatrixParam("fe1") String fe1,
            @MatrixParam("fe2") String fe2,
            @MatrixParam("veh") Integer veh
            ) {
        
        String path = "";


        try {                    
           
            if (autorizacion.verificar(strToken))
            {
                autorizacion.actualizar();                  

                
                FileXlsx filexlsx = new FileXlsx();                
                filexlsx.iniciar();
                filexlsx.folder = "/files";                
                filexlsx.name = "/base.xlsx";      
                filexlsx.newlibro();
                
                
                UtilizacionVehiculoXLSX xlsx = new UtilizacionVehiculoXLSX();                
                
                xlsx.gen_consulta2(filexlsx, fe1, fe2, veh);                
                
                filexlsx.newFileStream();
                path = filexlsx.getFilePath();
        
                File file = new File(path);

                Response.ResponseBuilder response = Response.ok((Object) file);
                response.header("Content-Disposition", "attachment; filename=\"test_excel_file.xlsx\"");
                response.header("token", autorizacion.encriptar());

                return response.build();                
                
            }
            else
            {

                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();                             
                
            }        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")   
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }  
        

    }
  
  

    
    
    
    
}