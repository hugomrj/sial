/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.base.aplicacion.utilizacionvehiculo;


import java.io.FileOutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import nebuleuse.ORM.db.ResultadoSet;
import nebuleuse.file.FileXlsx;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.xssf.usermodel.DefaultIndexedColorMap;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;

/**
 *
 * @author hugo
 */
public class UtilizacionVehiculoXLSX {
    
    
    public  void gen_consulta1 ( FileXlsx filexlsx, 
            String fecha1, String fecha2, Integer vehiculo )  {    
           
        try {
            
            
            this.hoja01_consulta1(filexlsx, fecha1, fecha2, vehiculo);
            
            
            String ruta = "";
            ruta = filexlsx.getFilePath();

            FileOutputStream file = new FileOutputStream( ruta );       
            filexlsx.getLibro().write(file);
            file.close();                

            
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();            
        }
    }    
      
    
    
    
    
    public  FileXlsx hoja01_consulta1 ( FileXlsx filexlsx, 
                String fecha1, String fecha2, Integer vehiculo) 
            throws SQLException, Exception {
        

            ArrayList<String> cabecera = new ArrayList<String>();

            cabecera.add("fecha");
            cabecera.add("vehiculo");
            cabecera.add("zafira");
            cabecera.add("ba temporal");
            cabecera.add("ba tacumbu");
            cabecera.add("apuf camsap");
            cabecera.add("otro");
            cabecera.add("kms recorridos");
                      
                        
            filexlsx.setCabecera(cabecera);
            ArrayList<String> campos = new ArrayList<String>();
            
            
            campos.add("fecha");
            campos.add("vehiculo_nombre");
            campos.add("zafira");
            campos.add("temporal");
            campos.add("tacumbu");
            campos.add("camsap");
            campos.add("otro");
            campos.add("kms");
                       
           
            
            filexlsx.setCampos(campos);    
            
            String sql = new UtilizacionVehiculoSQL().consulta1_xlsx(
                    fecha1, fecha2, vehiculo);            
            
            
            ResultSet resultset = new ResultadoSet().resultset(sql);             
            
           
            filexlsx.newhoja("hoja1");
            filexlsx.writeCabecera(0);     
                        
            filexlsx.writeContenido(resultset);           
            
            filexlsx.formato();
        
            return filexlsx;                                                              
            
    }
     
    

    
    
    

    
    public  void gen_consulta2 ( FileXlsx filexlsx, 
            String fecha1, String fecha2, Integer vehiculo )  {    
           
        try {
            
            
            this.hoja01_consulta2(filexlsx, fecha1, fecha2, vehiculo);
            
            
            String ruta = "";
            ruta = filexlsx.getFilePath();

            FileOutputStream file = new FileOutputStream( ruta );       
            filexlsx.getLibro().write(file);
            file.close();                

            
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();            
        }
    }    
      
        
    
    
    
    
    public  FileXlsx hoja01_consulta2 ( FileXlsx filexlsx, 
                String fecha1, String fecha2, Integer programa) 
            throws SQLException, Exception {
        

            ArrayList<String> cabecera = new ArrayList<String>();

      

            
            cabecera.add("cant");
            cabecera.add("vehiculo");
            cabecera.add("zafira");
            cabecera.add("ba temporal");
            cabecera.add("ba tacumbu");
            cabecera.add("apuf camsap");
            cabecera.add("otro");
            cabecera.add("kms recorridos");
                      
                        
            filexlsx.setCabecera(cabecera);
            ArrayList<String> campos = new ArrayList<String>();
            
            
            campos.add("cant");            
            campos.add("vehiculo_nombre");
            campos.add("zafira");
            campos.add("temporal");
            campos.add("tacumbu");
            campos.add("camsap");
            campos.add("otro");
            campos.add("kms");
                       
            
            
            filexlsx.setCampos(campos);    
            
            String sql = new UtilizacionVehiculoSQL().consulta2_xlsx(
                    fecha1, fecha2, programa);            
            
            
            ResultSet resultset = new ResultadoSet().resultset(sql);             
            
           
            filexlsx.newhoja("hoja1");
            filexlsx.writeCabecera(0);     
                        
            filexlsx.writeContenido(resultset);           
            
            filexlsx.formato();
        
            return filexlsx;                                                              
            
    }
     
        
    
        
    
}
