/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.utilizacionvehiculo;



import java.io.IOException;
import java.util.List;
import nebuleuse.ORM.db.Coleccion;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.ORM.db.ResultadoSet;


/**
 *
 * @author hugom_000
 */

public class UtilizacionVehiculoDAO  {

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public UtilizacionVehiculoDAO ( ) throws IOException  {
    }
      
    
    
    public List<UtilizacionVehiculo>  lista (Integer page, String sql) {
                
        List<UtilizacionVehiculo>  lista = null;        
        try {    
            
            ResultadoSet rs = new ResultadoSet();      
            
            lista = new Coleccion<UtilizacionVehiculo>().resultsetToList(
                    new UtilizacionVehiculo(),
                    rs.resultset(sql, page)
            );                        
                  
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
      
    
          
        
}
