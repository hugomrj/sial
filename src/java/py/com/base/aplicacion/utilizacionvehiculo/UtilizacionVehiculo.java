/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.utilizacionvehiculo;

import java.util.Date;
import py.com.base.aplicacion.vehiculo.Vehiculo;
import py.com.base.aplicacion.vehiculo.VehiculoExt;

/**
 *
 * @author hugo
 */
public class UtilizacionVehiculo {
    
    private Integer id;
    private Date fecha;
    private VehiculoExt vehiculo;
    private Boolean zafira;
    private Boolean temporal;
    private Boolean tacumbu;
    private Boolean camsap;
    private Boolean otro;
    private String otro_texto;
    private Integer kms;
    private Integer usuario;
    private String nombre_destino;
    private String otro_vehiculo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Boolean getZafira() {
        return zafira;
    }

    public void setZafira(Boolean zafira) {
        this.zafira = zafira;
    }

    public Boolean getTemporal() {
        return temporal;
    }

    public void setTemporal(Boolean temporal) {
        this.temporal = temporal;
    }

    public Boolean getTacumbu() {
        return tacumbu;
    }

    public void setTacumbu(Boolean tacumbu) {
        this.tacumbu = tacumbu;
    }

    public Boolean getCamsap() {
        return camsap;
    }

    public void setCamsap(Boolean camsap) {
        this.camsap = camsap;
    }

    public Boolean getOtro() {
        return otro;
    }

    public void setOtro(Boolean otro) {
        this.otro = otro;
    }

    public String getOtro_texto() {
        return otro_texto;
    }

    public void setOtro_texto(String otro_texto) {
        this.otro_texto = otro_texto;
    }

    public Integer getKms() {
        return kms;
    }

    public void setKms(Integer kms) {
        this.kms = kms;
    }

    public Integer getUsuario() {
        return usuario;
    }

    public void setUsuario(Integer usuario) {
        this.usuario = usuario;
    }

    public VehiculoExt getVehiculo() {
        return vehiculo;
    }

    public void setVehiculo(VehiculoExt vehiculo) {
        this.vehiculo = vehiculo;
    }

    public String getNombre_destino() {
        return nombre_destino;
    }

    public void setNombre_destino(String nombre_destino) {
        this.nombre_destino = nombre_destino;
    }

    public String getOtro_vehiculo() {
        return otro_vehiculo;
    }

    public void setOtro_vehiculo(String otro_vehiculo) {
        this.otro_vehiculo = otro_vehiculo;
    }

    
    
}


