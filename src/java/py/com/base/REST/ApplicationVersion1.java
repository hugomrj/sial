/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.REST;

import java.util.Set;
import jakarta.ws.rs.core.Application;

/**
 *
 * @author hugo
 */


@jakarta.ws.rs.ApplicationPath("api")
public class ApplicationVersion1 extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }
    
    

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        
        
        resources.add(py.com.base.sistema.rol.RolWS.class);
        resources.add(py.com.base.sistema.rol_selector.RolSelectorWS.class);
        resources.add(py.com.base.sistema.selector.SelectorWS.class);        
        resources.add(py.com.base.sistema.usuario.UsuarioWS.class);
        resources.add(py.com.base.sistema.usuario_rol.UsuarioRolWS.class);
        
        resources.add(py.com.base.aplicacion.vehiculo.VehiculoWS.class);
        resources.add(py.com.base.aplicacion.programa.ProgramaWS.class);
    
        
        resources.add(py.com.base.aplicacion.barriotemporal.BarrioTemporalWS.class);
        resources.add(py.com.base.aplicacion.barriotemporal.BarrioTemporalQRY.class);
        
        
        resources.add(py.com.base.aplicacion.utilizacionvehiculo.UtilizacionVehiculoWS.class);
        resources.add(py.com.base.aplicacion.utilizacionvehiculo.UtilizacionVehiculoQRY.class);
        
        
        resources.add(py.com.base.aplicacion.beneficiario.BeneficiarioWS.class);
        resources.add(py.com.base.aplicacion.beneficiario.BeneficiarioQRY.class);
        
        
        resources.add(py.com.base.aplicacion.gestionsocial.GestionSocialWS.class);
        resources.add(py.com.base.aplicacion.gestionsocial.GestionSocialQRY.class);
                        
        
        resources.add(py.com.base.analitico.consolidadogeneral.ConsolidadoGeneralQRY.class );
                
        
        resources.add(py.com.base.aplicacion.ordenservicio.OrdenServicioWS.class);    
        resources.add(py.com.base.aplicacion.ordenservicio.OrdenServicioQRY.class);
        
        
        
    }
    
}
